# Copyright 2013-2021 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

# ----------------------------------------------------------------------------
# If you submit this package back to Spack as a pull request,
# please first remove this boilerplate and all FIXME comments.
#
# This is a template package file for Spack.  We've put "FIXME"
# next to all the things you'll want to change. Once you've handled
# them, you can save this file and test your package like this:
#
#     spack install pbbam
#
# You can edit this file again by typing:
#
#     spack edit pbbam
#
# See the Spack documentation for more information on packaging.
# ----------------------------------------------------------------------------

from spack import *


class Pbbam(MesonPackage):
    """FIXME: Put a proper description of your package here."""

    # FIXME: Add a proper url for your package's homepage here.
    homepage = "https://www.example.com"
    url      = "https://github.com/PacificBiosciences/pbbam/archive/refs/tags/v2.1.0.tar.gz"

    # FIXME: Add a list of GitHub accounts to
    # notify when the package is updated.
    # maintainers = ['github_user1', 'github_user2']

    version('2.1.0', sha256='605944f09654d964ce12c31d67e6766dfb1513f730ef5d4b74829b2b84dd464f')

    # FIXME: Add dependencies if required.
    depends_on('boost@1.55:')
    depends_on('samtools@1.3.1:')
    depends_on('pbcopper')

    def meson_args(self):
        # FIXME: If not needed delete this function
        args = []
        return args
