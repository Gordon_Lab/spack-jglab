# Copyright 2013-2021 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

# ----------------------------------------------------------------------------
# If you submit this package back to Spack as a pull request,
# please first remove this boilerplate and all FIXME comments.
#
# This is a template package file for Spack.  We've put "FIXME"
# next to all the things you'll want to change. Once you've handled
# them, you can save this file and test your package like this:
#
#     spack install opera-ms
#
# You can edit this file again by typing:
#
#     spack edit opera-ms
#
# See the Spack documentation for more information on packaging.
# ----------------------------------------------------------------------------

from spack import *


class OperaMs(MakefilePackage):
    """FIXME: Put a proper description of your package here."""

    # FIXME: Add a proper url for your package's homepage here.
    homepage = "https://www.example.com"
    url      = "OPERA-MS-0.9.0.zip"

    # FIXME: Add a list of GitHub accounts to
    # notify when the package is updated.
    # maintainers = ['github_user1', 'github_user2']

    version('0.9.0', sha256='f761c8ca3a30dedbe418118eb24678b36eadf4d4be9d9eb4e7253706cce23cf2')

    depends_on('megahit@1.1.4')
    depends_on('spades@3.15.3')
    depends_on('samtools@:0.1.18')
    depends_on('bwa@0.7.17')
    #wasn't able to install blasr due to issues with pbbam
    depends_on('minimap2@2.14')
    depends_on('racon@1.4.3')
    depends_on('mash@2.3')
    depends_on('mummer@3.23')
    depends_on('pilon@1.22')
    depends_on('r@4.1.1')
    depends_on('perl@5.34.0')
    depends_on('perl-switch@2.17')
    depends_on('perl-file-which@1.22')
    depends_on('perl-pathtools@3.75')
    depends_on('perl-statistics-basic@1.6611')
    depends_on('perl-statistics-r@0.34')
    depends_on('perl-getopt-long@2.52')

#    def edit(self, spec, prefix):
        # FIXME: Edit the Makefile if necessary
        # FIXME: If not needed delete this function
        # makefile = FileFilter('Makefile')
        # makefile.filter('CC = .*', 'CC = cc')

    @run_after('build')
    def fix_perl_scripts(self):
        filter_file(r'#use strict;',
                    '#!/usr/bin/env perl\n#use strict;',
                    'OPERA-MS.pl')

    def install(self, spec, prefix):
         install_tree('.', prefix)
