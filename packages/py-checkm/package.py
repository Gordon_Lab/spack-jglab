# Copyright 2013-2021 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

# ----------------------------------------------------------------------------
# If you submit this package back to Spack as a pull request,
# please first remove this boilerplate and all FIXME comments.
#
# This is a template package file for Spack.  We've put "FIXME"
# next to all the things you'll want to change. Once you've handled
# them, you can save this file and test your package like this:
#
#     spack install py-checkm
#
# You can edit this file again by typing:
#
#     spack edit py-checkm
#
# See the Spack documentation for more information on packaging.
# ----------------------------------------------------------------------------

from spack import *


class PyCheckm(PythonPackage):
    """FIXME: Put a proper description of your package here."""

    # FIXME: Add a proper url for your package's homepage here.
    homepage = "https://www.example.com"
    url      = "https://github.com/Ecogenomics/CheckM/archive/refs/tags/v1.1.3.tar.gz"

    # FIXME: Add a list of GitHub accounts to
    # notify when the package is updated.
    # maintainers = ['github_user1', 'github_user2']

    version('1.2.0',  sha256='9d2f51409a72e90f1b59c811fa4d8b430051683ca0f3001aa770003f9201cd71')
    version('1.1.11', sha256='60f2c763b03874a0fc79c317ad6205abf740dbea27d348368a9d5968bbe27538')
    version('1.1.10', sha256='68778c4107f397f9fb678086b0f2d8b9b21e9a17af1038d262f86cb969952535')
    version('1.1.9',  sha256='4ec395e1dfce2aa9cb5059dd899d4b4160c2d46ea0b32a72c86ddd964595c743')
    version('1.1.8',  sha256='c6e9d007622808ae3312de73d54866292a83857837119380a036036e799c1f38')
    version('1.1.7',  sha256='1d74133b92ceb2b8ca36070c4254a3386753c8aeb86309c5fcc2208af63cbde6')
    version('1.1.6',  sha256='fd740d31c8792b816e80b851c34153763898fe0fbd55dccba914eaff85b01e92')
    version('1.1.5',  sha256='2dc304a973ae9ae1b201a3367156f0a9e6dbe8176d292ba6449e6fa1fb1dfbef')
    version('1.1.3',  sha256='64f0f5fe890ffbb3807388bde4aad407d6169bf8c79811a62c5524778f61880c')
    version('1.1.2',  sha256='15d12ae655f021d9a04799f6f2e94b9ea6d76393a5161304a8e1c0d087f2daf5')

    # FIXME: Add dependencies if required. Only add the python dependency
    # if you need specific versions. A generic python dependency is
    # added implicity by the PythonPackage class.
    # depends_on('python@2.X:2.Y,3.Z:', type=('build', 'run'))
    # depends_on('py-setuptools', type='build')
    # depends_on('py-foo',        type=('build', 'run'))

    depends_on('py-setuptools')
    depends_on('hmmer@3.1b2', type=('build', 'run'))
    depends_on('pplacer@1.1.alpha19', type=('build', 'run'))
    depends_on('prodigal@2.6.3', type=('build', 'run'))
    depends_on('py-numpy')
    depends_on('py-scipy')
    depends_on('py-matplotlib')
    depends_on('py-dendropy')
    depends_on('py-pysam')

    def build_args(self, spec, prefix):
        # FIXME: Add arguments other than --prefix
        # FIXME: If not needed delete this function
        args = []
        return args
