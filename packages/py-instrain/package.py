# Copyright 2013-2021 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

# ----------------------------------------------------------------------------
# If you submit this package back to Spack as a pull request,
# please first remove this boilerplate and all FIXME comments.
#
# This is a template package file for Spack.  We've put "FIXME"
# next to all the things you'll want to change. Once you've handled
# them, you can save this file and test your package like this:
#
#     spack install py-instrain
#
# You can edit this file again by typing:
#
#     spack edit py-instrain
#
# See the Spack documentation for more information on packaging.
# ----------------------------------------------------------------------------

from spack import *


class PyInstrain(PythonPackage):
    """FIXME: Put a proper description of your package here."""

    # FIXME: Add a proper url for your package's homepage here.
    homepage = "https://www.example.com"
    url      = "https://github.com/MrOlm/inStrain/archive/refs/tags/v1.3.1.tar.gz"

    # FIXME: Add a list of GitHub accounts to
    # notify when the package is updated.
    # maintainers = ['github_user1', 'github_user2']

    version('1.3.1', sha256='ff5dc94796168e3da0f13184c16b81160f1b3bbcde267ba83091482c813997c0')

    # FIXME: Add dependencies if required. Only add the python dependency
    # if you need specific versions. A generic python dependency is
    # added implicity by the PythonPackage class.
    # depends_on('python@2.X:2.Y,3.Z:', type=('build', 'run'))
    # depends_on('py-setuptools', type='build')
    # depends_on('py-foo',        type=('build', 'run'))

    depends_on('python@3.7')
    depends_on('py-setuptools')
    depends_on('py-numpy', type=('build', 'run'))
    depends_on('py-pandas@0.25:')
    depends_on('py-seaborn')
    depends_on('py-matplotlib')
    depends_on('py-biopython@:1.74')
    depends_on('py-scikit-learn')
    depends_on('py-pytest')
    depends_on('py-tqdm')
    depends_on('py-pysam@0.15:', type=('build', 'run'))
    depends_on('py-networkx')
    depends_on('py-h5py')
    depends_on('py-psutil')
    depends_on('py-lmfit')
    depends_on('py-numba')
    depends_on('py-drep')

    def build_args(self, spec, prefix):
        # FIXME: Add arguments other than --prefix
        # FIXME: If not needed delete this function
        args = []
        return args
