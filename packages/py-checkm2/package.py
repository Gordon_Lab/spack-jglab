# Copyright 2013-2021 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

# ----------------------------------------------------------------------------
# If you submit this package back to Spack as a pull request,
# please first remove this boilerplate and all FIXME comments.
#
# This is a template package file for Spack.  We've put "FIXME"
# next to all the things you'll want to change. Once you've handled
# them, you can save this file and test your package like this:
#
#     spack install py-checkm2
#
# You can edit this file again by typing:
#
#     spack edit py-checkm2
#
# See the Spack documentation for more information on packaging.
# ----------------------------------------------------------------------------

from spack import *


class PyCheckm2(PythonPackage):
    """FIXME: Put a proper description of your package here."""

    # FIXME: Add a proper url for your package's homepage here.
    homepage = "https://www.example.com"
    url      = "/scratch/jglab/hibberdm/spack/CheckM2-1.0.tar.gz"

    # FIXME: Add a list of GitHub accounts to
    # notify when the package is updated.
    # maintainers = ['github_user1', 'github_user2']

    version('1.0', sha256='1b1abb29ea8ad7c00c1a52c0a3e2c688c423a0ae18a96f2b2c7bc48e45f27154')

    # FIXME: Add dependencies if required. Only add the python dependency
    # if you need specific versions. A generic python dependency is
    # added implicity by the PythonPackage class.
    depends_on("python@3.6:")
    depends_on("py-scikit-learn@0.23.2")
    depends_on("py-h5py@2.10.0")
    depends_on("diamond@2.0.4:")
    depends_on("py-numpy@1.19.2")
    depends_on("py-lightgbm@3.2.1:")
    depends_on("py-pandas")
    depends_on("py-scipy")
    depends_on("prodigal@2.6.3")
    depends_on("py-setuptools")
    depends_on("py-requests")
    depends_on("py-packaging")
    depends_on("py-tqdm")
    depends_on("py-tensorflow@2.4.1")

    def build_args(self, spec, prefix):
        # FIXME: Add arguments other than --prefix
        # FIXME: If not needed delete this function
        args = []
        return args
