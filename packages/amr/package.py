# Copyright 2013-2021 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

# ----------------------------------------------------------------------------
# If you submit this package back to Spack as a pull request,
# please first remove this boilerplate and all FIXME comments.
#
# This is a template package file for Spack.  We've put "FIXME"
# next to all the things you'll want to change. Once you've handled
# them, you can save this file and test your package like this:
#
#     spack install amr
#
# You can edit this file again by typing:
#
#     spack edit amr
#
# See the Spack documentation for more information on packaging.
# ----------------------------------------------------------------------------

from spack import *


class Amr(Package):
    """FIXME: Put a proper description of your package here."""

    # FIXME: Add a proper url for your package's homepage here.
    homepage = "https://www.example.com"
    url      = "https://github.com/ncbi/amr/archive/refs/tags/amrfinder_v3.11.2.tar.gz"

    # FIXME: Add a list of GitHub accounts to
    # notify when the package is updated.
    # maintainers = ['github_user1', 'github_user2']

    version('3.11.2', sha256='3eb2bea1dc25ca4d213c247178fecba44c40aa8035d70422a33c0af627082b46')   

    # FIXME: Add dependencies if required.
    depends_on("blast-plus")
    depends_on("curl")
    depends_on("hmmer")

    def install(self, spec, prefix):
        # FIXME: Unknown build system
        make()
        make('clean')
        make('DEFAULT_DB_DIR=/ref/jglab/data/amrfinderplus')
        make('install', 'INSTALL_DIR=' + prefix)

