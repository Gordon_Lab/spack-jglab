# Copyright 2013-2021 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

# ----------------------------------------------------------------------------
# If you submit this package back to Spack as a pull request,
# please first remove this boilerplate and all FIXME comments.
#
# This is a template package file for Spack.  We've put "FIXME"
# next to all the things you'll want to change. Once you've handled
# them, you can save this file and test your package like this:
#
#     spack install r-ragg
#
# You can edit this file again by typing:
#
#     spack edit r-ragg
#
# See the Spack documentation for more information on packaging.
# ----------------------------------------------------------------------------

from spack import *


class RRagg(RPackage):
    """Anti-Grain Geometry (AGG) is a high-quality and high-performance 2D drawing library. The 'ragg' package provides a set of graphic devices based on AGG to use as alternative to the raster devices provided through the 'grDevices' package."""

    homepage = "https://github.com/r-lib/ragg"
    url      = "http://cran.wustl.edu/src/contrib/ragg_1.2.2.tar.gz"

    maintainers = ['thomasp85']

    version('1.2.2', sha256='3b6151c93a79093f9e638448db8abf88a57fd7b1e1093867cc0f6b492bbf83e0')
    
    #depends_on('systemfonts@1.0.3:')
    #depends_on('textshaping@0.3.0:')

    #def configure_args(self):
        # FIXME: Add arguments to pass to install via --configure-args
        # FIXME: If not needed delete this function
        # args = []
        # return args
