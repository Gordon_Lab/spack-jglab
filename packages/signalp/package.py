# Copyright 2013-2021 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

import os

from spack import *


class Signalp(Package):
    """SignalP predicts the presence and location of signal peptide cleavage
       sites in amino acid sequences from different organisms: Gram-positive
       bacteria, Gram-negative bacteria, and eukaryotes.
       Note: A manual download is required for SignalP.
       Spack will search your current directory for the download file.
       Alternatively, add this file to a mirror so that Spack can find it.
       For instructions on how to set up a mirror, see
       https://spack.readthedocs.io/en/latest/mirrors.html"""

    homepage = "https://www.cbs.dtu.dk/services/SignalP/"
    url      = "file://{0}/signalp-4.1g.Linux.tar.gz".format(os.getcwd())
    manual_download = True

    version('4.1g', sha256='d72d9fcf3c74e92267bb570b02f4a32a0286826eaf165eed046ad70796db9dc1') 
    version('5.0b', sha256='3bfbfa2c0a903bbd568877e6ae8560833eab90c8a9aa18473edf03037e9f61cf') 
    version('6.0g.fast', sha256='a16fcea2b30067d2622d446031978bd86927e2e1cecf29a567c7922f6861b5aa')

    depends_on('perl', type=('build', 'run'))
    depends_on('gnuplot')

#    def patch(self):
#        edit = FileFilter('signalp')
#        edit.filter("ENV{SIGNALP} = .*",
#                    "ENV{SIGNALP} = '%s'" % self.prefix)

    def install(self, spec, prefix):
        mkdirp(prefix.share.man)
        install('signalp', prefix)
        install('signalp.1', prefix.share.man)
        install_tree('bin', prefix.bin)
        install_tree('lib', prefix.lib)
        install_tree('syn', prefix.syn)

    def setup_run_environment(self, env):
        env.prepend_path('PATH', self.prefix)
