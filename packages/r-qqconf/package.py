# Copyright 2013-2021 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

# ----------------------------------------------------------------------------
# If you submit this package back to Spack as a pull request,
# please first remove this boilerplate and all FIXME comments.
#
# This is a template package file for Spack.  We've put "FIXME"
# next to all the things you'll want to change. Once you've handled
# them, you can save this file and test your package like this:
#
#     spack install r-qqconf
#
# You can edit this file again by typing:
#
#     spack edit r-qqconf
#
# See the Spack documentation for more information on packaging.
# ----------------------------------------------------------------------------

from spack import *


class RQqconf(RPackage):
    """An R Package to Put Simultaneous Testing Bands on QQ and PP Plots."""

    homepage = "https://github.com/eweine/qqconf"
    url      = "http://cran.wustl.edu/src/contrib/qqconf_1.2.3.tar.gz"

    maintainers = ['eweine', 'nileshpatra']

    version('1.2.3', sha256='9b5b6042ea8e52e6e049807c0b5e3bfd534b624bd257be769de69cf505fece62')

    depends_on('r@3.0.0:', type = ('build', 'run'))
    depends_on('r-dplyr@1.0.0:')
    depends_on('r-magrittr@1.5:')
    depends_on('r-rlang@0.4.9:')
    depends_on('r-mass@7.3:')
    depends_on('r-robustbase@0.93:')
    depends_on('r-rcpp')
    depends_on('fftw@3.1.2:')

    def configure_args(self):
        # FIXME: Add arguments to pass to install via --configure-args
        # FIXME: If not needed delete this function
        args = []
        return args
