# Copyright 2013-2021 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

# ----------------------------------------------------------------------------
# If you submit this package back to Spack as a pull request,
# please first remove this boilerplate and all FIXME comments.
#
# This is a template package file for Spack.  We've put "FIXME"
# next to all the things you'll want to change. Once you've handled
# them, you can save this file and test your package like this:
#
#     spack install py-medaka
#
# You can edit this file again by typing:
#
#     spack edit py-medaka
#
# See the Spack documentation for more information on packaging.
# ----------------------------------------------------------------------------

from spack import *


class PyMedaka(PythonPackage):
    """Medaka long read genome assembly polisher"""

    # FIXME: Add a proper url for your package's homepage here.
    homepage = "https://github.com/nanoporetech/medaka"
    url      = "https://github.com/nanoporetech/medaka/archive/refs/tags/v1.9.1.tar.gz"

    # FIXME: Add a list of GitHub accounts to
    # notify when the package is updated.
    # maintainers = ['github_user1', 'github_user2']

    version('1.9.1', sha256='1d7c254b10eb1e8bc5c9f24e09bed468cbd58a4105c8f07576ac3c243e5cbe98')

    depends_on('python@3.7:3.10')
    depends_on('py-cffi@1.15.0')
    depends_on('edlib')
    depends_on('py-grpcio')
    depends_on('py-h5py')
    depends_on('py-intervaltree')
    depends_on('py-numpy@1.21.6:')
    depends_on('py-mappy')
    depends_on('py-ont-fast5-api')
    depends_on('py-parasail-python')
    depends_on('py-pysam@0.16.0.1:')
    depends_on('py-pyspoa@0.0.3:')
    depends_on('py-requests')
    depends_on('py-tensorflow@2.10.0:')

    def build_args(self, spec, prefix):
        # FIXME: Add arguments other than --prefix
        # FIXME: If not needed delete this function
        args = []
        return args
