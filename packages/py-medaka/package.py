# Copyright 2013-2021 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

# ----------------------------------------------------------------------------
# If you submit this package back to Spack as a pull request,
# please first remove this boilerplate and all FIXME comments.
#
# This is a template package file for Spack.  We've put "FIXME"
# next to all the things you'll want to change. Once you've handled
# them, you can save this file and test your package like this:
#
#     spack install py-medaka
#
# You can edit this file again by typing:
#
#     spack edit py-medaka
#
# See the Spack documentation for more information on packaging.
# ----------------------------------------------------------------------------

from spack import *


class PyMedaka(PythonPackage):
    """FIXME: Put a proper description of your package here."""

    # FIXME: Add a proper url for your package's homepage here.
    homepage = "https://www.example.com"
    url      = "https://github.com/nanoporetech/medaka/archive/refs/tags/v1.8.0.tar.gz"

    # FIXME: Add a list of GitHub accounts to
    # notify when the package is updated.
    # maintainers = ['github_user1', 'github_user2']

    version('1.8.0', sha256='3d42907f79b996c1b53b8e4a26f87c8b45990bdc83e0b6bc298a13b261904ce2')

#    patch("htslib.patch", when="@1.8.0")

#    depends_on("bcftools@1.17:", type=("build", "run"))
#    depends_on("bzip2@1.0.8:2.0a0", type=("build", "run"))
#    depends_on("py-cffi@1.15.0", type=("build", "run"))
#    depends_on("py-grpcio", type=("build", "run"))
#    depends_on("py-h5py", type=("build", "run"))
#    depends_on("htslib@1.17:1.19.0a0", type=("build", "run"))
#    depends_on("py-intervaltree", type=("build", "run"))
#    depends_on("py-mappy", type=("build", "run"))
#    depends_on("minimap2@2.26:", type=("build", "run"))
#    depends_on("py-numpy@1.21.6:", type=("build", "run"))
#    depends_on("py-ont-fast5-api", type=("build", "run"))
#    depends_on("openssl@1.1.1t:1.1.2a", type=("build", "run"))
#    depends_on("py-parasail-python", type=("build", "run"))
#    depends_on("py-pysam@0.16.0.1:", type=("build", "run"))
#    depends_on("py-pyspoa@0.0.3:", type=("build", "run"))
#    depends_on("python@3.10:3.11.0a0", type=("build", "run"))
#    depends_on("py-edlib", type=("build", "run"))
#    depends_on("py-abipy", type=("build", "run"))
#    depends_on("py-requests", type=("build", "run"))
#    depends_on("samtools@1.17:", type=("build", "run"))
#    depends_on("py-tensorflow@2.8.0:2.9.dev0", type=("build", "run"))
#    depends_on("xz@5.2.6:5.3.0a0", type=("build", "run"))
#    depends_on("curl@7.87.0:8.0a0", type=("build", "run"))
#    depends_on("zlib@1.2.13:1.3.0a0", type=("build", "run"))

    def build_args(self, spec, prefix):
        # FIXME: Add arguments other than --prefix
        # FIXME: If not needed delete this function
        args = []
        return args
