# Copyright 2013-2021 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

# ----------------------------------------------------------------------------
# If you submit this package back to Spack as a pull request,
# please first remove this boilerplate and all FIXME comments.
#
# This is a template package file for Spack.  We've put "FIXME"
# next to all the things you'll want to change. Once you've handled
# them, you can save this file and test your package like this:
#
#     spack install maxbin
#
# You can edit this file again by typing:
#
#     spack edit maxbin
#
# See the Spack documentation for more information on packaging.
# ----------------------------------------------------------------------------

from spack import *


class Maxbin(Package):
    """FIXME: Put a proper description of your package here."""

    # FIXME: Add a proper url for your package's homepage here.
    homepage = "https://www.example.com"
    #url      = "https://sourceforge.net/projects/maxbin2/files/MaxBin-2.2.7.tar.gz"
    url      = "https://gitlab.com/Gordon_Lab/maxbin/-/archive/2.2.8/maxbin-2.2.8.tar.gz"

    # FIXME: Add a list of GitHub accounts to
    # notify when the package is updated.
    # maintainers = ['github_user1', 'github_user2']

    version('2.2.7', sha256='cb6429e857280c2b75823c8cd55058ed169c93bc707a46bde0c4383f2bffe09e')
    version('2.2.8', sha256='5e48bc63d8068c09bbc72dc3a1f0662ceef89da4bc54547bd8a22139b36228bb')

    # FIXME: Add dependencies if required.
    # depends_on('foo')

    depends_on("perl-findbin@1.52_01")
    depends_on("perl-libwww-perl@6.66")
    depends_on("perl-http-message@6.13")
    depends_on("fraggenescan@1.31")
    depends_on("hmmer@3.3.2")
    depends_on("bowtie2@2.3.4.1")

    def install_args(self, spec, prefix):
        return ["PREFIX={0}".format(prefix)]

    def install(self, spec, prefix):
        # FIXME: Unknown build system 
        cd("src")
        make()
        cd("../")
        install_tree('.', prefix)
     
    def setup_run_environment(self, env):
        env.append_path('PATH', self.prefix)

    
