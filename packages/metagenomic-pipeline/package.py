# Copyright 2013-2021 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

# ----------------------------------------------------------------------------
# If you submit this package back to Spack as a pull request,
# please first remove this boilerplate and all FIXME comments.
#
# This is a template package file for Spack.  We've put "FIXME"
# next to all the things you'll want to change. Once you've handled
# them, you can save this file and test your package like this:
#
#     spack install metagenomic-pipeline
#
# You can edit this file again by typing:
#
#     spack edit metagenomic-pipeline
#
# See the Spack documentation for more information on packaging.
# ----------------------------------------------------------------------------

from spack import *


class MetagenomicPipeline(Package):
    """FIXME: Put a proper description of your package here."""

    # FIXME: Add a proper url for your package's homepage here.
#    homepage = "https://www.example.com"
    url      = "/scratch/jglab/hibberdm/spack/metagenomic_pipeline-4.4.tar.gz"

    # FIXME: Add a list of GitHub accounts to
    # notify when the package is updated.
    # maintainers = ['github_user1', 'github_user2']

    # FIXME: Add proper versions and checksums here.
    version('4.4', sha256='628bfe6ced8d8323aac7e0e01541714ffaed598d369e1a112aeca079e636843e') 

    def install(self, spec, prefix):
        install_tree('.', prefix)

    def setup_run_environment(self, env):
        print("\necho [STATUS] Code is available from https://gitlab.com/Gordon_Lab/metagenomic_pipeline;echo \n\tPlease run retrieve_sbatch.sh for example config, sbatch, and mapping files.;")
