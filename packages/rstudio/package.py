# Copyright 2013-2021 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

# ----------------------------------------------------------------------------
# If you submit this package back to Spack as a pull request,
# please first remove this boilerplate and all FIXME comments.
#
# This is a template package file for Spack.  We've put "FIXME"
# next to all the things you'll want to change. Once you've handled
# them, you can save this file and test your package like this:
#
#     spack install rstudio
#
# You can edit this file again by typing:
#
#     spack edit rstudio
#
# See the Spack documentation for more information on packaging.
# ----------------------------------------------------------------------------

from spack import *


class Rstudio(CMakePackage):
    """FIXME: Put a proper description of your package here."""

    # FIXME: Add a proper url for your package's homepage here.
    homepage = "https://www.example.com"
    url      = "https://github.com/rstudio/rstudio/archive/refs/tags/v1.4.1743.tar.gz"

    # FIXME: Add a list of GitHub accounts to
    # notify when the package is updated.
    # maintainers = ['github_user1', 'github_user2']

    version('1.4.1743', sha256='f046b2e91d4b27794d989e9bb2d7ad951b913ae86ed485364fc5b7fccba9c927')

    # FIXME: Add dependencies if required.
    depends_on('r')
    depends_on('yaml-cpp@0.6.3')
    #depends_on('libyaml')
    depends_on('boost@1.77.0')
    depends_on('qt@5.15.2')

    def cmake_args(self):
        # FIXME: Add arguments other than
        # FIXME: CMAKE_INSTALL_PREFIX and CMAKE_BUILD_TYPE
        # FIXME: If not needed delete this function
        spec = self.spec
        return [
                '-DRSTUDIO_USE_SYSTEM_YAML_CPP=true',
                '-DQT_QMAKE_EXECUTABLE=/ref/jglab/software/spack-0.17.2/opt/spack/linux-rocky8-x86_64/gcc-8.5.0/qt-5.15.2-rih2aucpplpfuzjqkr5tjtu4srycrj5u/bin/qmake'
                #'DYAML_CPP_LIBRARY={0}'.format(spec['libyaml'].libs.joined(";"))
            ]
