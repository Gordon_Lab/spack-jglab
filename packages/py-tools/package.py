# Copyright 2013-2021 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

# ----------------------------------------------------------------------------
# If you submit this package back to Spack as a pull request,
# please first remove this boilerplate and all FIXME comments.
#
# This is a template package file for Spack.  We've put "FIXME"
# next to all the things you'll want to change. Once you've handled
# them, you can save this file and test your package like this:
#
#     spack install py-tools
#
# You can edit this file again by typing:
#
#     spack edit py-tools
#
# See the Spack documentation for more information on packaging.
# ----------------------------------------------------------------------------

from spack import *


class PyTools(PythonPackage):
    """FIXME: Put a proper description of your package here."""

    # FIXME: Add a proper url for your package's homepage here.
    homepage = "https://www.example.com"
    url      = "https://github.com/nf-core/tools/archive/refs/tags/2.8.tar.gz"

    # FIXME: Add a list of GitHub accounts to
    # notify when the package is updated.
    # maintainers = ['github_user1', 'github_user2']

    version('2.8', sha256='fb3c6e4a9bcd8a6e44bb9fe8f31a33f2232ac51ba61651731a558c5e9abc6939')

    # FIXME: Add dependencies if required. Only add the python dependency
    # if you need specific versions. A generic python dependency is
    # added implicity by the PythonPackage class.
    # depends_on('python@2.X:2.Y,3.Z:', type=('build', 'run'))
    depends_on('py-setuptools', type=('build', 'run'))
    depends_on('py-click')
    depends_on('py-filetype-py')
    depends_on('py-gitpython')
    depends_on('py-jinja2')
    depends_on('py-jsonschema@3.0:')
    depends_on('py-markdown@3.3:')
    depends_on('py-packaging')
    depends_on('py-pre-commit')
    depends_on('py-python-prompt-toolkit@3.0.3:')
    depends_on('py-pytest@7.0.0:')
    depends_on('py-pytest-workflow@1.6.0:')
    depends_on('py-pyyaml')
    depends_on('py-questionary@1.8.0:')
    depends_on('py-refgenie')
    depends_on('py-requests')
    depends_on('py-requests_cache')
    depends_on('py-rich-click@1.6.1:')
    depends_on('py-rich@13.3.1:')
    depends_on('py-tabulate')

#    def build_args(self, spec, prefix):
        # FIXME: Add arguments other than --prefix
        # FIXME: If not needed delete this function
#        args = []
#        return args
