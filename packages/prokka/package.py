# Copyright 2013-2021 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

# ----------------------------------------------------------------------------
# If you submit this package back to Spack as a pull request,
# please first remove this boilerplate and all FIXME comments.
#
# This is a template package file for Spack.  We've put "FIXME"
# next to all the things you'll want to change. Once you've handled
# them, you can save this file and test your package like this:
#
#     spack install prokka
#
# You can edit this file again by typing:
#
#     spack edit prokka
#
# See the Spack documentation for more information on packaging.
# ----------------------------------------------------------------------------

from spack import *


class Prokka(Package):
    """FIXME: Put a proper description of your package here."""

    # FIXME: Add a proper url for your package's homepage here.
    homepage = "https://www.example.com"
    url      = "prokka-1.14.6-custom.tar.gz"

    # FIXME: Add a list of GitHub accounts to
    # notify when the package is updated.
    # maintainers = ['github_user1', 'github_user2']

    version('1.14.6-custom', sha256='bfe4609592458344331b3c0915d2816b57ae609a42d433f4635994109df21218')

    depends_on('perl', type='run')
    depends_on('perl-bioperl', type='run')
    depends_on('perl-xml-simple', type='run')
    depends_on('perl-bio-searchio-hmmer', type='run')
    depends_on('hmmer', type='run')
    depends_on('blast-plus', type='run')
    depends_on('prodigal', type='run')
    depends_on('tbl2asn', type='run')
#    depends_on('signalp', type='run')
    depends_on('barrnap', type='run')

    def install(self, spec, prefix):
        install_tree('bin', prefix.bin)
        install_tree('binaries', prefix.binaries)
        install_tree('db', prefix.db)
        install_tree('doc', prefix.doc)


    
   
  
 
