# Copyright 2013-2021 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

# ----------------------------------------------------------------------------
# If you submit this package back to Spack as a pull request,
# please first remove this boilerplate and all FIXME comments.
#
# This is a template package file for Spack.  We've put "FIXME"
# next to all the things you'll want to change. Once you've handled
# them, you can save this file and test your package like this:
#
#     spack install coverm
#
# You can edit this file again by typing:
#
#     spack edit coverm
#
# See the Spack documentation for more information on packaging.
# ----------------------------------------------------------------------------

from spack import *


class Coverm(Package):
    """FIXME: Put a proper description of your package here."""

    # FIXME: Add a proper url for your package's homepage here.
    homepage = "https://www.example.com"
    url      = "https://github.com/wwood/CoverM/releases/download/v0.6.1/coverm-x86_64-unknown-linux-musl-0.6.1.tar.gz"

    # FIXME: Add a list of GitHub accounts to
    # notify when the package is updated.
    # maintainers = ['github_user1', 'github_user2']

    version('0.6.1', sha256='42281ee1a941ef7bd8c460a6b9af5a60fd34f65506bb2d30a934a26ed9aff016')

    # FIXME: Add dependencies if required.
    # depends_on('foo')

    depends_on('samtools@1.9')
    depends_on('minimap2@2.21')
    depends_on('bwa-mem2@2.0:')

    def install(self, spec, prefix):
         install_tree('.', prefix)
 
