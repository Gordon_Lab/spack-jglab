# Copyright 2013-2021 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack import *


class Genometools(MakefilePackage):
    """genometools is a free collection of bioinformatics tools (in the realm
       of genome informatics) combined into a single binary named gt."""

    homepage = "http://genometools.org/"
    url      = "https://github.com/genometools/genometools/archive/refs/tags/v1.5.5.tar.gz"

    version('1.5.5', sha256='a17c0ba9b7458dddfb6577472e32b2343a4be30d8cd76bcbf01f57bff81c76a8')

    depends_on('perl', type=('build', 'run'))
#    depends_on('cairo+pdf')
#    depends_on('pango')

    # build fails with gcc 7"
    conflicts('%gcc@7.1.0:', when='@:1.5.9')

    patch('signed.patch', when='%fj')

    def install(self, spec, prefix):
        make('install', 'prefix=%s' % prefix)

    def setup_dependent_build_environment(self, env, dependent_spec):
        env.set('CPATH', self.prefix.include.genometools)
