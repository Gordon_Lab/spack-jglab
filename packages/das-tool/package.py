# Copyright 2013-2021 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

# ----------------------------------------------------------------------------
# If you submit this package back to Spack as a pull request,
# please first remove this boilerplate and all FIXME comments.
#
# This is a template package file for Spack.  We've put "FIXME"
# next to all the things you'll want to change. Once you've handled
# them, you can save this file and test your package like this:
#
#     spack install das-tool
#
# You can edit this file again by typing:
#
#     spack edit das-tool
#
# See the Spack documentation for more information on packaging.
# ----------------------------------------------------------------------------

from spack import *


class DasTool(Package):
    """FIXME: Put a proper description of your package here."""

    # FIXME: Add a proper url for your package's homepage here.
    homepage = "https://www.example.com"
    url      = "https://github.com/cmks/DAS_Tool/archive/refs/tags/1.1.4.tar.gz"

    # FIXME: Add a list of GitHub accounts to
    # notify when the package is updated.
    # maintainers = ['github_user1', 'github_user2']

    version('1.1.4', sha256='8a4324edcca845621b00cef418b5226dc9c7981036d7f28874291fc07faba4da')

    # FIXME: Add dependencies if required.
    depends_on('r@4.1.1')
    depends_on('r-data-table@1.14.2')
    depends_on('r-magrittr@2.0.1')
    depends_on('r-docopt@0.7.1')
#    depends_on('r-stringr')
    depends_on('ruby@3.0.2')
    depends_on('pullseq@1.0.2')
    depends_on('prodigal@2.6.3')
    depends_on('diamond@2.0.11')

    def install(self, spec, prefix):
         install_tree('.', prefix)
 
