# Copyright 2013-2021 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

# ----------------------------------------------------------------------------
# If you submit this package back to Spack as a pull request,
# please first remove this boilerplate and all FIXME comments.
#
# This is a template package file for Spack.  We've put "FIXME"
# next to all the things you'll want to change. Once you've handled
# them, you can save this file and test your package like this:
#
#     spack install py-concoct
#
# You can edit this file again by typing:
#
#     spack edit py-concoct
#
# See the Spack documentation for more information on packaging.
# ----------------------------------------------------------------------------

from spack import *


class PyConcoct(PythonPackage):
    """FIXME: Put a proper description of your package here."""

    # FIXME: Add a proper url for your package's homepage here.
    homepage = "https://www.example.com"
    url      = "https://github.com/BinPro/CONCOCT/archive/refs/tags/1.1.0.tar.gz"

    # FIXME: Add a list of GitHub accounts to
    # notify when the package is updated.
    # maintainers = ['github_user1', 'github_user2']

    version('1.1.0', sha256='00aecacb4b720ac123a63e65072c61e0b5a8690d844c869aaee4dbf287c82888')

    # FIXME: Add dependencies if required. Only add the python dependency
    # if you need specific versions. A generic python dependency is
    # added implicity by the PythonPackage class. 
    depends_on('gsl@2.7')
    depends_on('python@3.6')
    depends_on('py-setuptools', type='build')
    depends_on('py-argparse')
    depends_on('py-bcbio-gff')
    depends_on('py-biopython')
    depends_on('py-nose')
    depends_on('py-numpy@1.18.5:')
    depends_on('py-pandas@0.13.0:')
    depends_on('py-python-dateutil@2.2:')
    depends_on('py-pytz@2013.9:')
    depends_on('py-scikit-learn@0.14.1:')
    depends_on('py-scipy@0.13.3:')
    depends_on('py-cython@0.28.5:')
    depends_on('bedtools2')
    depends_on('mummer')
    depends_on('samtools')

    def build_args(self, spec, prefix):
        # FIXME: Add arguments other than --prefix
        # FIXME: If not needed delete this function
        args = []
        return args
