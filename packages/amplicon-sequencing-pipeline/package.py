# Copyright 2013-2021 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack import *

class AmpliconSequencingPipeline(Package):
    """amplicon-sequencing-pipeline is the Gordon Lab's internal workflow for generating ASV tables from paired-end 16S (V4) amplicon sequence data."""

    homepage = "https://gitlab.com/Gordon_Lab/amplicon_sequencing_pipeline"
    url      = "https://gitlab.com/Gordon_Lab/amplicon_sequencing_pipeline/-/archive/1.3/amplicon_sequencing_pipeline-1.3.tar.gz"

    version('1.3', sha256='c20b1be7834776860bc1a6e2cdca2ace3ac26a80ffc20960060c7372e390e4ad')

    def install(self, spec, prefix):
        install_tree('.', prefix)

    def setup_run_environment(self, env):
        print("echo ; \
               echo [STATUS] Workflow 'amplicon-sequencing-pipeline' loaded.; \
               echo [STATUS] The code, documentation, and a step-by-step protocol for this workflow are available at: https://gitlab.com/Gordon_Lab/amplicon_sequencing_pipeline; \
               echo ;")        
