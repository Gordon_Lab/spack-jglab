# Copyright 2013-2021 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

# ----------------------------------------------------------------------------
# If you submit this package back to Spack as a pull request,
# please first remove this boilerplate and all FIXME comments.
#
# This is a template package file for Spack.  We've put "FIXME"
# next to all the things you'll want to change. Once you've handled
# them, you can save this file and test your package like this:
#
#     spack install r-dada2
#
# You can edit this file again by typing:
#
#     spack edit r-dada2
#
# See the Spack documentation for more information on packaging.
# ----------------------------------------------------------------------------

from spack import *


class RDada2(RPackage):
    """FIXME: Put a proper description of your package here."""

    # FIXME: Add a proper url for your package's homepage here.
    homepage = "https://www.example.com"
    url      = "https://github.com/benjjneb/dada2/archive/refs/tags/v1.26.tar.gz"

    # FIXME: Add a list of GitHub accounts to
    # notify when the package is updated.
    # maintainers = ['github_user1', 'github_user2']

    version('1.26', sha256='48018bd0f39450fd3c6addf11906e63406f1f7b9f33f9a0e921dac095e8bee75')

    # FIXME: Add dependencies if required.
    # depends_on('r-foo', type=('build', 'run'))

    depends_on('r@3.4:', type=('build', 'run'))
    depends_on('r-rcpp@0.12.0:', type=('build', 'run'))
#    depends_on('r-methods@3.4.0:', type=('build', 'run'))
    depends_on('r-biostrings@2.42.1:', type=('build', 'run'))
    depends_on('r-ggplot2@2.1.0:', type=('build', 'run'))
    depends_on('r-reshape2@1.4.1:', type=('build', 'run'))
    depends_on('r-shortread@1.32.0:', type=('build', 'run'))
    depends_on('r-rcppparallel@4.3.0:', type=('build', 'run'))
#    depends_on('r-parallel@3.2.0:', type=('build', 'run'))
    depends_on('r-iranges@2.6.0:', type=('build', 'run'))
    depends_on('r-xvector@0.16.0:', type=('build', 'run'))
    depends_on('r-biocgenerics@0.22.0:', type=('build', 'run'))

    depends_on('gmake', type='build')

