# Copyright 2013-2021 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

# ----------------------------------------------------------------------------
# If you submit this package back to Spack as a pull request,
# please first remove this boilerplate and all FIXME comments.
#
# This is a template package file for Spack.  We've put "FIXME"
# next to all the things you'll want to change. Once you've handled
# them, you can save this file and test your package like this:
#
#     spack install py-trycycler
#
# You can edit this file again by typing:
#
#     spack edit py-trycycler
#
# See the Spack documentation for more information on packaging.
# ----------------------------------------------------------------------------

from spack import *


class PyTrycycler(PythonPackage):
    """FIXME: Put a proper description of your package here."""

    # FIXME: Add a proper url for your package's homepage here.
    homepage = "https://www.example.com"
    url      = "https://github.com/rrwick/Trycycler/archive/refs/tags/v0.5.4.tar.gz"

    # FIXME: Add a list of GitHub accounts to
    # notify when the package is updated.
    # maintainers = ['github_user1', 'github_user2']

    version('0.5.4', sha256='6880e6920a5e4f3764fc3c7dd1dfc7ec4c9106eb5eb2ed90ae292e73c74893df')

    # FIXME: Add dependencies if required. Only add the python dependency
    # if you need specific versions. A generic python dependency is
    # added implicity by the PythonPackage class.
    depends_on('python@3.6:')
    depends_on('edlib')
    depends_on('py-numpy')
    depends_on('py-scipy')
    depends_on('minimap2@2.24')
    depends_on('miniasm@2018-3-30')
    depends_on('mash@2.3') 
    depends_on('r/rbcdt72') # 4.2.0
    depends_on('r-ape')
    depends_on('r-phangorn')
    depends_on('muscle')

   # depends_on('py-setuptools', type='build')
    # depends_on('py-foo',        type=('build', 'run'))

    def build_args(self, spec, prefix):
        # FIXME: Add arguments other than --prefix
        # FIXME: If not needed delete this function
        args = []
        return args
