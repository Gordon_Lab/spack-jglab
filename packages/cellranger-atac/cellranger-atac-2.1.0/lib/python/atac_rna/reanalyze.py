# Copyright (c) 2021 10x Genomics, Inc. All rights reserved.

"""
Reanalyze pipeline library functions
"""

from __future__ import annotations

import re
from io import IOBase
from typing import AnyStr, Generator, Optional, Set, Union

import numpy as np

from atac.tools.ref_manager import ReferenceManager
from atac_rna.aggr import AggrDefs
from cellranger.hdf5 import get_h5_filetype, is_hdf5
from cellranger.io import open_maybe_gzip
from cellranger.library_constants import ATACSEQ_LIBRARY_TYPE, GENE_EXPRESSION_LIBRARY_TYPE
from cellranger.matrix import MATRIX_H5_FILETYPE, CountMatrix, get_gem_group_index
from cellranger.reference import NewGeneIndex

BARCODE_REGEX = b"[ACGT]+-[0-9]+"
MIN_BARCODES = 15


class ReanalyzeError(Exception):
    pass


class InputBarcodes:
    """Barcodes that are supplied to the reanalyze pipeline"""

    def __init__(self, path_or_reader: Optional[Union[bytes, IOBase]] = None):
        """Initialize object using a path to a file containing cell barcodes or a binary reader

        Args:
            path_or_reader (Optional[Union[bytes, IOBase]], optional): File containing cell
            barcodes. Defaults to None.

        Raises:
            TypeError: if object supplied is neither a path nor a reader
        """
        if isinstance(path_or_reader, bytes):
            self.path = path_or_reader
            self.reader = open_maybe_gzip(path_or_reader, "rb")
        elif isinstance(path_or_reader, IOBase):
            self.path = b"Unknown"
            self.reader = path_or_reader
        else:
            print(type(path_or_reader), path_or_reader)
            raise TypeError("incorrect type")
        self.bcs = list(self.iter())
        if self.count == 0:
            raise ReanalyzeError(
                "Error in supplied barcodes file = {}: no barcodes present in file".format(
                    self.path
                )
            )

    def iter(self) -> Generator[bytes]:
        """Iterate over barcodes in the file

        Raises:
            ReanalyzeError: when the file is not correctly formatted or doesn't contain barcodes

        Yields:
            Generator[bytes]: barcodes as a `bytes` object
        """
        self.count = 0
        for i, line in enumerate(self.reader):
            data = line.strip()
            # skip empty lines
            if not data:
                continue
            entry = data.split(b",")[0]
            if re.match(BARCODE_REGEX, entry):
                self.count += 1
                yield entry
            elif i == 0:
                # allow header
                pass
            else:
                raise ReanalyzeError(
                    "Error in supplied barcodes file = {}: entry {} at line {} is not a valid barcode".format(
                        self.path, entry.decode(), i
                    )
                )

    def check_in_matrix(self, matrix_path: bytes):
        """Check that the barcodes specified are contained in the matrix

        Args:
            matrix_path (bytes): path to joint matrix

        Raises:
            ReanalyzeError: when a barcode is not present in the matrix
        """
        mat_bcs_set = set(CountMatrix.load_bcs_from_h5(matrix_path))
        self.check_barcodes(mat_bcs_set)

    def check_barcodes(self, bcs_set: Set[bytes]):
        """Check that barcodes are a subset of the specified set

        Args:
            bcs_set (Set[bytes]): set of barcodes to compare against

        Raises:
            ReanalyzeError: when there is a barcode that is not contained in `bcs_set`
        """

        for bc in self.bcs:
            if not bc in bcs_set:
                raise ReanalyzeError(
                    "Error in supplied barcodes file = {}: barcode {} is not present in the supplied matrix".format(
                        self.path, bc.decode()
                    )
                )

    def check_min_bcs(self, min_count: int = MIN_BARCODES):
        """Check for a minimum number of barcodes."""
        if self.count < min_count:
            raise ReanalyzeError(
                "Too few barcodes specified: {} is < {}. Execution is halted".format(
                    self.count, min_count
                )
            )

    def check_in_whitelist(self, bcs_set: Set[bytes]):
        """Check that barcodes are a subset of the specified set

        Args:
            bcs_set (Set[bytes]): set of barcodes to compare against

        Raises:
            ReanalyzeError: when there is a barcode that is not contained in `bcs_set`
        """
        assert isinstance(bcs_set, set)

        for bc in self.bcs:
            barcode, _ = bc.split(b"-")
            if not barcode in bcs_set:
                raise ReanalyzeError(
                    "Error in supplied barcodes file = {}: barcode {} is not present in the whitelist".format(
                        self.path, bc.decode()
                    )
                )

    def write_atac_cell_bcs(self, path: AnyStr):
        """Write out bcs in atac format always as single species

        Args:
            path (AnyStr): path to output file
        """
        with open(path, "wb") as outfile:
            outfile.write(b"")
            for bc in self.bcs:
                outfile.write(b",")
                outfile.write(bc)
            outfile.write(b"\n")


def check_joint_matrix_h5(
    matrix_h5: bytes, ref_manager: Optional[ReferenceManager], light: bool
):  # pylint: disable=too-many-locals
    """check whether matrix_h5 is a valid joint count matrix for input to reanalyze"""

    err = "Input h5 file {} is not a valid Cell Ranger ARC count matrix".format(matrix_h5)
    if not is_hdf5(matrix_h5):
        raise ReanalyzeError(err)

    h5_filetype = get_h5_filetype(matrix_h5)
    if h5_filetype and h5_filetype != MATRIX_H5_FILETYPE:
        raise ReanalyzeError(err)

    if light:
        return

    try:
        feature_types = CountMatrix.load_library_types_from_h5_file(matrix_h5)
    except (KeyError, TypeError, OSError) as ex:
        raise ReanalyzeError("{}: unable to load feature types".format(err)) from ex

    for ftype in (GENE_EXPRESSION_LIBRARY_TYPE, ATACSEQ_LIBRARY_TYPE):
        if ftype not in feature_types:
            raise ReanalyzeError("{}: matrix does not contain feature type {}".format(err, ftype))

    # check peak features are compatible with primary contigs
    lens = ref_manager.contig_lengths
    fref = CountMatrix.load_feature_ref_from_h5_file(matrix_h5)

    gene_index = NewGeneIndex.load_from_reference(ref_manager.path)

    num_atac = 0
    num_gex = 0
    for fdef in fref.feature_defs:
        if fdef.feature_type == ATACSEQ_LIBRARY_TYPE:
            num_atac += 1
            region = fdef.id.decode()
            contig, rest = region.split(":")
            start, end = rest.split("-")
            start = int(start)
            end = int(end)
            if not ref_manager.is_primary_contig(contig):
                raise ReanalyzeError(
                    "Matrix h5 references a peak = {}, but the contig {} is not a primary "
                    "reference contig. Check that the matrix h5 and the reference are "
                    "compatible.".format(region, contig)
                )
            if end > lens[contig]:
                raise ReanalyzeError(
                    "Matrix h5 references a peak = {}, but the end point of the region is greater "
                    "than the contig length = {}. Check that the matrix h5 and the reference are "
                    "compatible.".format(region, lens[contig])
                )
        elif fdef.feature_type == GENE_EXPRESSION_LIBRARY_TYPE:
            num_gex += 1
            if gene_index.gene_id_to_int(fdef.id) is None:
                raise ReanalyzeError(
                    "Matrix h5 references a gene id {} that is not contained in the provided "
                    "reference. Check that the matrix h5 and the reference are compatible.".format(
                        fdef.id.decode()
                    )
                )
        else:
            raise ReanalyzeError(
                "Matrix h5 contains a feature {} of type {} that is not recognized by "
                "cellranger-arc reanalyze.".format(fdef.id.decode(), fdef.feature_type)
            )

    # check feature counts
    if num_gex <= 10:
        raise ReanalyzeError(
            "There are too few genes {} <= 10 in the matrix to perform reanalysis.".format(num_gex)
        )
    if num_atac <= 15:
        raise ReanalyzeError(
            "There are too few peaks {} <= 15 in the matrix to perform reanalysis.".format(num_atac)
        )

    # check that we have at least 15 barcodes
    (_, bcs, _) = CountMatrix.load_dims_from_h5(matrix_h5)
    if bcs < MIN_BARCODES:
        raise ReanalyzeError(
            "Matrix h5 contains {} < {} barcodes and execution is halted".format(bcs, MIN_BARCODES)
        )


def validate_projection(projection_csv, matrix_h5):
    """check validity of input csv as a reduced dimension matrix for input to reanalyze"""
    if not projection_csv:
        return
    proj_bcs = []
    proj_mat = []
    with open(projection_csv) as fin:
        header = None
        line_count = 0
        for line in fin:
            if line_count == 0:
                header = line.strip().split(",")
                if header is None or header[0] != "Barcode":
                    raise ReanalyzeError(
                        "Invalid projection {}. Supplied projection must be a CSV with first column `Barcode`".format(
                            projection_csv
                        )
                    )
                line_count += 1
                continue
            proj_bcs.append(line.strip().split(",")[0].encode())
            proj_mat.append(line.strip().split(",")[1:])
    expected_bcs = CountMatrix.load_bcs_from_h5(matrix_h5)
    if not np.array_equal(np.array(proj_bcs), expected_bcs):
        raise ReanalyzeError(
            "Invalid projection {}. Supplied projection file must contain the "
            "same barcodes in the same order as the computed feature barcode matrix. Check that the input "
            "barcodes and the projection contain the same barcodes in the same order.".format(
                projection_csv
            )
        )
    try:
        proj_mat = np.asarray(proj_mat, dtype="float")
    except ValueError as err:
        raise ReanalyzeError(
            "Provided custom projection {} is invalid. The entries of the CSV that"
            " specify the projection must be floating point numbers.".format(projection_csv)
        ) from err
    if np.sum(np.isnan(proj_mat)):
        raise ReanalyzeError(
            "Provided custom projection {} is invalid. The entries of the CSV that"
            " specify the projection cannot be NaN.".format(projection_csv)
        )


def check_gg_aggr_csv(aggr_defs: AggrDefs, matrix_h5: AnyStr):
    """Check validity of input aggr defs compared to joint matrix input to reanalyze."""

    # Number of rows in aggr csv should be equal to the number of gem groups in the count matrix
    gg_dict = get_gem_group_index(matrix_h5)
    if aggr_defs.num != len(gg_dict):
        raise ReanalyzeError(
            "The supplied aggr_csv contains {} libraries while the supplied count matrix "
            "contains {} gem groups. There seems to be a mis-match between the supplied matrix "
            "and the aggr_csv. If the input matrix was produced by the 'cellranger-arc aggr' pipeline, you may"
            " pass the same aggregation CSV in order to retain per-library tag information in the "
            "resulting .cloupe file".format(aggr_defs.num, len(gg_dict))
        )
