#
# Copyright (c) 2020 10X Genomics, Inc. All rights reserved.
#

"""
Construct a reference compatible with ATAC and GEX pipelines
"""

from __future__ import absolute_import, print_function

import itertools
import os
import re
import subprocess
import sys
import tempfile

from Bio import motifs
from pybedtools import BedTool
from six import ensure_str

from cellranger.io import get_input_path, get_input_paths
from cellranger.reference import GexReferenceError
from cellranger.reference_builder import ReferenceBuilder


class AtacGexReferenceError(GexReferenceError):
    """Exceptions relating to ARC reference construction"""


class MotifsParseError(Exception):
    """Invalid JASPAR format matrix"""


class MotifsSpaceError(Exception):
    """Motif header contains whitespace"""


def valid_contig_name(contig, genomes):
    """Valid contig names cannot contain : or - or _ characters, unless it is a barnyard reference
    in which case the _ character is OK. The _ character messes up the peak annotation stage
    which is not run for barnyard samples.
    """
    if len(genomes) == 1:
        return re.match(r".*[:].*", contig) is None
    return re.match(r".*[:].*", contig) is None


def check_motifs_format(motifs_infile):
    """Check that the supplied motif file is in the JASPAR format and has no whitespace in the
    motif header field"""
    assert motifs_infile, "motifs_infile cannot be empty"
    if not os.path.exists(motifs_infile):
        raise AtacGexReferenceError("path to motifs file {} does not exist".format(motifs_infile))
    with open(motifs_infile) as motifs_in:
        try:
            motif_obj = motifs.parse(motifs_in, "jaspar")
        except Exception as ex:
            raise MotifsParseError(str(ex)) from ex

        for key, value in motif_obj.to_dict().items():
            if key is None:
                raise MotifsParseError("Found unnamed motif with counts:\n{}".format(value.counts))

        motifs_in.seek(0)
        num_motifs = 0
        for line in motifs_in:
            if line.startswith(">"):
                num_motifs += 1
                name = line.lstrip(">").rstrip()
                fields = re.split(r"\s+", name)
                if len(fields) > 1:
                    raise MotifsSpaceError(
                        "Motifs file {} contains a header line with whitespace:\n{}".format(
                            motifs_infile, line
                        )
                    )
        if num_motifs == 0:
            raise MotifsParseError(
                "Unable to parse any motifs from supplied motifs file {}".format(motifs_infile)
            )


class AtacGexReferenceBuilder(ReferenceBuilder):
    """Sub-class of ReferenceBuilder that adds ATAC functionality. Used to construct the ARC and
    ATAC reference packages."""

    def __init__(self, config, output_dir, ref_version, mkref_version, num_threads, mem_gb):
        """
        config: atac_rna.reference_config.ReferenceConfig object
        output_dir: reference package is created here
        ref_version, mkref_version: stamp the reference with version information
        num_threads: for STAR
        mem_gb: for STAR
        """
        config.validate()
        super().__init__(
            config.genome,
            get_input_paths(config.input_fasta),
            get_input_paths(config.input_gtf),
            output_dir,
            ref_version,
            mkref_version,
            num_threads,
            mem_gb,
            prefixes_as_genomes=True,
        )
        # add properties unique to atac-gex references
        self.in_motifs_fn = config.input_motifs
        self.motifs_path = None
        self.non_nuclear_contigs = config.non_nuclear_contigs
        self.primary_contigs = None
        self.organism = config.organism if config.organism else ""

    def make_bwa_index(self):
        """Generate BWA index for use with ATAC and ARC pipelines"""
        print("Generating bwa index (may take over an hour for a 3Gb genome)...")
        sys.stdout.flush()
        subprocess.check_call(["bwa", "index", self.fasta_path])
        print("done\n")

    # pylint: disable=too-many-locals
    def write_tss_transcripts_bed(self):
        """
        Create files needed by the ATAC and ARC references: tss.bed and transcripts.bed
        Also determine the list of _all_ contigs with genes and return them.

        Note:
        - for each gene, we filter out transcripts that do not carry the tag `basic`. If a
          gene doesn't have any such transcripts, then all transcripts are included.
        - we ignore any transcripts on non nuclear contigs
        - the tss.bed and transcripts.bed file do not contain duplicate rows and are sorted
          as per the faidx file
        """
        assert os.path.exists(self.gtf_path), "tss can be created only after final GTF is written"
        print("Writing TSS and transcripts bed file...")

        regions = os.path.join(self.out_dir, "regions")
        os.makedirs(regions)
        tss_out = os.path.join(regions, "tss.bed")
        tx_out = os.path.join(regions, "transcripts.bed")

        gene_contigs = set()
        with tempfile.NamedTemporaryFile(mode="w") as tss_writer, tempfile.NamedTemporaryFile(
            mode="w"
        ) as tx_writer:
            # item: (fields, is_comment, properties)
            gtf_transcripts = sorted(
                (
                    item
                    for item in self.gtf_reader_iter(self.gtf_path)
                    if not item[1] and item[0][2] == "transcript"
                ),
                key=lambda item: item[2]["gene_id"],
            )
            gtf_transcripts = itertools.groupby(gtf_transcripts, lambda item: item[2]["gene_id"])
            for gene_id, record_iter in gtf_transcripts:
                tx_records = set()
                tss_records = set()
                no_basic = True
                for fields, is_comment, props in record_iter:
                    assert not is_comment

                    chrom = fields[0]
                    gene_contigs.add(chrom)

                    # skip mitochondrial genes
                    if self.non_nuclear_contigs and chrom in self.non_nuclear_contigs:
                        continue

                    start = int(fields[3])
                    end = int(fields[4])
                    strand = fields[6]
                    has_tx = True
                    # extract TSS and transcript coordinates. Note that GTF is 1-based closed,
                    # while BED is 0-based half-open intervals.
                    if strand == "+":
                        tss_start = start - 1
                        tss_end = tss_start + 1
                    elif strand == "-":
                        tss_start = end - 1
                        tss_end = tss_start + 1

                    # only consider transcripts that are "basic" when determining the TSS. If you
                    # don't do this filtering a large number of genes in GRCh38 end up having a very
                    # large TSS >> 1kb due to lower confidence transcripts
                    attr_str = self.format_properties_dict(props)
                    transcript_tags = re.findall('tag\\s["*](\\w+)*["*]', attr_str)

                    is_row_basic = "basic" in transcript_tags
                    if is_row_basic:
                        no_basic = False

                    tss_row = (
                        is_row_basic,
                        ensure_str(chrom),
                        str(tss_start),
                        str(tss_end),
                        ensure_str(gene_id),
                        ".",
                        ensure_str(strand),
                    )
                    tx_row = (
                        is_row_basic,
                        ensure_str(chrom),
                        str(start),
                        str(end),
                        ensure_str(gene_id),
                        ".",
                        ensure_str(strand),
                    )

                    tss_records.add(tss_row)
                    tx_records.add(tx_row)

                # only write tag "basic" transcripts into tss.bed or transcripts.bed. If all
                # transcripts of a gene happen to be not "basic", then use all of them. Filtering
                # them all out would result in a gene missing from the tss.bed/transcripts.bed.
                for tss_rec in sorted(tss_records, key=lambda rec: (int(rec[2]), int(rec[3]))):
                    if no_basic or tss_rec[0]:
                        tss_writer.write("\t".join(tss_rec[1:]))
                        tss_writer.write("\n")
                for tx_rec in sorted(tx_records, key=lambda rec: (int(rec[2]), int(rec[3]))):
                    if no_basic or tx_rec[0]:
                        tx_writer.write("\t".join(tx_rec[1:]))
                        tx_writer.write("\n")
            tss_writer.flush()
            tx_writer.flush()

            # sort tss.bed and transcripts.bed
            faidx = self.fasta_path + ".fai"

            # pylint: disable=unexpected-keyword-arg
            tss_sorted = BedTool.sort(BedTool(tss_writer.name), faidx=faidx)
            tss_sorted.saveas(tss_out)

            # pylint: disable=unexpected-keyword-arg
            tx_sorted = BedTool.sort(BedTool(tx_writer.name), faidx=faidx)
            tx_sorted.saveas(tx_out)

        if not has_tx:
            raise AtacGexReferenceError(
                "Invalid gene annotation input: cannot find 'transcript' type in the 3rd column."
            )

        print("...done\n")
        return gene_contigs

    def compute_primary_contigs(self, gene_contigs):
        """Primary contigs are defined as any contig that contains an annotated gene and is not a
        non-nuclear contig
        """
        primary_contigs = []
        with open(self.fasta_path + ".fai", "r") as faidx:
            for line in faidx:
                chrom = line.split()[0]
                if chrom in gene_contigs and not (
                    self.non_nuclear_contigs and chrom in self.non_nuclear_contigs
                ):
                    primary_contigs.append(chrom)
        return primary_contigs

    def check_contig_names(self):
        """Check that the contig names don't contain : or _ or - since the pipeline breaks"""
        with open(self.fasta_path + ".fai", "r") as faidx:
            for line in faidx:
                chrom = line.split()[0]
                if not valid_contig_name(chrom, self.genomes):
                    raise AtacGexReferenceError(
                        "Supplied FASTA file(s) {} specify a contig {} that contains an invalid "
                        "character. Contig names cannot contain `:` characters.".format(
                            self.in_fasta_fns, chrom
                        )
                    )

    def add_motifs_to_reference(self):
        """Add a JASPAR motifs file to the reference"""
        print("Writing motifs...")
        motifs_inpath = get_input_path(self.in_motifs_fn)
        try:
            check_motifs_format(motifs_inpath)
        except MotifsParseError as ex:
            raise AtacGexReferenceError(
                "The supplied motif file \n{}\nis not in JASPAR format. Parser encountered "
                "error:\n{}".format(motifs_inpath, str(ex))
            ) from ex
        except MotifsSpaceError as ex:
            print(
                "{}Any whitespace characters will be replaced by a single underscore (_)".format(ex)
            )
        regions = os.path.join(self.out_dir, "regions")
        assert os.path.exists(regions)
        self.motifs_path = os.path.join(regions, "motifs.pfm")

        with open(self.motifs_path, "w") as out_motif:
            with open(motifs_inpath, "r") as in_motif:
                for line in in_motif:
                    if line.startswith(">"):
                        name = line.lstrip(">").rstrip()
                        fields = re.split(r"\s+", name)
                        out_motif.write(">{}\n".format("_".join(fields)))
                    else:
                        out_motif.write(line)
        print("...done\n")

    def build_atac_gex_reference(self):
        """Build an ARC-compatible reference"""

        # check motifs since we don't want to wait for the gex reference to get built
        if self.in_motifs_fn:
            motifs_inpath = get_input_path(self.in_motifs_fn)
            try:
                check_motifs_format(motifs_inpath)
            except MotifsParseError as ex:
                raise AtacGexReferenceError(
                    "The supplied motif file \n{}\nis not in JASPAR format. Parser encountered "
                    "error:\n{}".format(motifs_inpath, str(ex))
                ) from ex
            except MotifsSpaceError:
                pass

        if os.path.exists(self.out_dir):
            raise AtacGexReferenceError(
                "Cannot create reference package at {} because the path already exists".format(
                    self.out_dir
                )
            )
        print("Creating new reference folder at %s" % self.out_dir)
        os.mkdir(self.out_dir)
        print("...done\n")

        self.process_fasta()

        # check that contig names are valid, now that we have a fasta index
        self.check_contig_names()

        # validate non-nuclear contigs
        if self.non_nuclear_contigs:
            contig_lengths = self.get_contig_lengths()
            for contig in self.non_nuclear_contigs:
                if contig not in contig_lengths:
                    raise AtacGexReferenceError(
                        "Invalid value for non_nuclear_contigs in supplied config file: {}\n"
                        "Valid contigs in the FASTA file are:\n{}".format(
                            contig, sorted(list(contig_lengths.keys()))
                        )
                    )

        contig_lengths = self.get_contig_lengths()

        # We set no_transcript_fail=True because the ATAC reference relies on the existence of
        # a transcript row for each transcript in the GTF
        self.process_gtf(contig_lengths, no_transcript_fail=True)

        self.validate_gtf()

        self.make_star_index()

        self.gzip_gtf()

        self.compute_metadata()

        self.make_bwa_index()

        gene_contigs = self.write_tss_transcripts_bed()
        primary_contigs = self.compute_primary_contigs(gene_contigs)

        if self.in_motifs_fn:
            self.add_motifs_to_reference()

        extra_data_dict = {
            "primary_contigs": primary_contigs,
            "non_nuclear_contigs": self.non_nuclear_contigs,
            "organism": self.organism,
        }

        self.compute_metadata(extra_data_dict)

    def build_atac_reference(self):
        """Build an ATAC-compatible reference (no STAR index)"""

        # Check motifs
        if self.in_motifs_fn:
            motifs_inpath = get_input_path(self.in_motifs_fn)
            try:
                check_motifs_format(motifs_inpath)
            except MotifsParseError as ex:
                raise AtacGexReferenceError(
                    "The supplied motif file \n{}\nis not in JASPAR format. Parser encountered "
                    "error:\n{}".format(motifs_inpath, str(ex))
                ) from ex
            except MotifsSpaceError:
                pass

        if os.path.exists(self.out_dir):
            raise AtacGexReferenceError(
                "Cannot create reference package at {} because the path already exists".format(
                    self.out_dir
                )
            )
        print("Creating new reference folder at %s" % self.out_dir)
        os.mkdir(self.out_dir)
        print("...done\n")

        self.process_fasta()

        # check that contig names are valid, now that we have a fasta index
        self.check_contig_names()

        # validate non-nuclear contigs
        if self.non_nuclear_contigs:
            contig_lengths = self.get_contig_lengths()
            for contig in self.non_nuclear_contigs:
                if contig not in contig_lengths:
                    raise AtacGexReferenceError(
                        "Invalid value for non_nuclear_contigs in supplied config file: {}\n"
                        "Valid contigs in the FASTA file are:\n{}".format(
                            contig, sorted(list(contig_lengths.keys()))
                        )
                    )

        contig_lengths = self.get_contig_lengths()

        # We set no_transcript_fail=True because the ATAC reference relies on the existence of
        # a transcript row for each transcript in the GTF
        self.process_gtf(contig_lengths, no_transcript_fail=True)

        self.validate_gtf()

        self.gzip_gtf()

        self.compute_metadata()

        self.make_bwa_index()

        gene_contigs = self.write_tss_transcripts_bed()
        primary_contigs = self.compute_primary_contigs(gene_contigs)

        if self.in_motifs_fn:
            self.add_motifs_to_reference()

        extra_data_dict = {
            "primary_contigs": primary_contigs,
            "non_nuclear_contigs": self.non_nuclear_contigs,
            "organism": self.organism,
        }

        self.compute_metadata(extra_data_dict)
