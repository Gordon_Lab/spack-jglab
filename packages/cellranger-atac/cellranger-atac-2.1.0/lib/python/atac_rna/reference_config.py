# Copyright (c) 2020 10x Genomics, Inc. All rights reserved.

"""
Library to parse a reference config file for ATAC or ARC mkref
"""

from __future__ import absolute_import

import os
import re
from typing import List, NamedTuple, Optional, Union, IO
import hjson


class ConfigParseError(Exception):
    """Use for errors generated while parsing config file"""


BaseConfig = NamedTuple(
    "BaseConfig",
    (
        ("organism", str),
        ("genome", List[str]),
        ("input_fasta", List[Union[str, bytes, os.PathLike]]),
        ("input_gtf", List[Union[str, bytes, os.PathLike]]),
        ("non_nuclear_contigs", Optional[List[str]]),
        ("input_motifs", Optional[Union[str, bytes, os.PathLike]]),
    ),
)


def is_invalid_file(path: Union[str, bytes, os.PathLike]):
    """Check if the supplied path is a file and is readable"""
    if not isinstance(path, (str, bytes, os.PathLike)):
        return "'{}' is not a valid string type and is an invalid path".format(path)
    if not os.path.exists(path):
        return "Input file does not exist: {}".format(path)
    if not os.path.isfile(path):
        return "Please provide a file, not a directory: {}".format(path)
    if not os.access(path, os.R_OK):
        return "Input file does not have read permissions: {}".format(path)
    return ""


class ReferenceConfig(BaseConfig):
    """Represents a config file used to define an ATAC or ARC reference for use in mkref"""

    @classmethod
    def from_file(cls, config_fn):
        """Constructor for reading from a config file"""
        if not os.path.exists(config_fn):
            raise ConfigParseError("Config file {} does not exist".format(config_fn))

        with open(config_fn, "r") as in_config:
            obj = cls.from_stream(in_config)
        return obj

    @classmethod
    def from_stream(cls, stream: IO):
        """Constructor for reading from a stream"""
        assert hasattr(stream, "read")
        try:
            config_dict = hjson.load(stream)
        except hjson.HjsonDecodeError as ex:
            raise ConfigParseError(str(ex)) from ex

        obj = cls(*[config_dict.get(key, None) for key in ReferenceConfig._fields])
        obj.validate()

        return obj

    def validate(self):
        """Light-weight validator for the config. Does not parse any FASTA or GTF files."""
        if self.organism and not isinstance(self.organism, str):
            raise ConfigParseError("`organism` must be a string")

        if not isinstance(self.genome, list):
            raise ConfigParseError("`genome` must be a list of one or more genome names")
        for g in self.genome:
            if not isinstance(g, str) or not re.match("^[a-zA-Z0-9_-]+$", g):
                raise ConfigParseError(
                    "Invalid entry {}: each `genome` must only consist of letters (a-z, A-Z), "
                    "numbers (0-9) or a hyphen (-) or an underscore (_)".format(g)
                )
        if not isinstance(self.input_fasta, list):
            raise ConfigParseError("`input_fasta` must be a list of one or more paths")
        if not isinstance(self.input_gtf, list):
            raise ConfigParseError("`input_gtf` must be a list of one or more paths")

        if len(self.input_fasta) != len(self.input_gtf):
            raise ConfigParseError("`input_fasta` and `input_gtf` must be of the same length")

        if len(self.input_fasta) == 0:
            raise ConfigParseError("`input_fasta` cannot be empty")

        if len(self.input_fasta) > 1 and self.input_motifs:
            raise ConfigParseError(
                "`input_motifs` cannot be specified for a multi-genome reference"
            )
        for fn in self.input_fasta:
            msg = is_invalid_file(fn)
            if msg:
                raise ConfigParseError("Error parsing `input_fasta`: {}".format(msg))

        for fn in self.input_gtf:
            msg = is_invalid_file(fn)
            if msg:
                raise ConfigParseError("Error parsing `input_gtf`: {}".format(msg))

        if self.non_nuclear_contigs:
            if not isinstance(self.non_nuclear_contigs, list):
                raise ConfigParseError(
                    "Error parsing `non_nuclear_contigs`: expecting a list but found '{}'".format(
                        self.non_nuclear_contigs
                    )
                )
            for contig in self.non_nuclear_contigs:
                if not isinstance(contig, str):
                    raise ConfigParseError(
                        "Error parsing `non_nuclear_contigs`: expecting a list of strings "
                        "but found '{}'".format(contig)
                    )
        if self.input_motifs:
            msg = is_invalid_file(self.input_motifs)
            if msg:
                raise ConfigParseError("Error parsing `input_motifs`: {}".format(msg))
