# Copyright (c) 2020 10x Genomics, Inc. All rights reserved.
"""
Methods for doing preflight checks for cellranger-arc.
"""

from __future__ import absolute_import, division
import os
import json
import socket

import tenkit.reference as tk_ref
from atac.tools.ref_manager import ReferenceManager
from atac.tools.regions import BedFormatError, bed_format_checker
from atac.preflights import PreflightException as BasePreflightException
from atac_rna.reference import check_motifs_format, valid_contig_name

BED_FILES = [
    "./regions/tss.bed",
    "./regions/transcripts.bed",
]

STAR_FILES = [
    "./star",
    "./star/genomeParameters.txt",
    "./star/chrName.txt",
    "./star/chrStart.txt",
    "./star/chrLength.txt",
    "./star/chrNameLength.txt",
    "./star/exonGeTrInfo.tab",
    "./star/geneInfo.tab",
    "./star/transcriptInfo.tab",
    "./star/exonInfo.tab",
    "./star/sjdbList.fromGTF.out.tab",
    "./star/sjdbInfo.txt",
    "./star/sjdbList.out.tab",
    "./star/Genome",
    "./star/SA",
    "./star/SAindex",
]

OPTIONAL_FILES = [
    "./regions/motifs.pfm",
]

MANDATORY_ATAC_FILES = [
    "./fasta",
    "./fasta/genome.fa",
    "./fasta/genome.fa.fai",
    "./fasta/genome.fa.pac",
    "./fasta/genome.fa.ann",
    "./fasta/genome.fa.amb",
    "./fasta/genome.fa.bwt",
    "./fasta/genome.fa.sa",
    "./genes",
    "./genes/genes.gtf.gz",
    "./reference.json",
    "./regions",
] + BED_FILES

MANDATORY_ARC_FILES = MANDATORY_ATAC_FILES + STAR_FILES

METADATA_KEYS = [
    "input_gtf_files",
    "version",
    "gtf_hash.gz",
    "primary_contigs",
    "genomes",
    "mem_gb",
    "fasta_hash",
    "mkref_version",
    "threads",
    "non_nuclear_contigs",
    "organism",
    "input_fasta_files",
]

DEPRECATED_ATAC_FILES = [
    "./fasta/contig-defs.json",
]

DEPRECATED_ARC_FILES = DEPRECATED_ATAC_FILES


class PreflightException(BasePreflightException):
    """This is the base exception for all preflight errors.

    We only need to catch this base in downstream code.
    """


class ArcReferenceError(PreflightException):
    """Preflight exceptions relating to a bad reference"""

    def __init__(self, reference_path, msg):
        full_msg = (
            "The supplied reference path {ref} is not a valid "
            "10x reference on machine {host}:\n{msg}\n"
            "Please make sure that you have downloaded the correct 10x reference "
            "or generated a custom reference using `mkref`. Cell Ranger ATAC pipelines are "
            "compatible with Cell Ranger ATAC 2.0 or Cell Ranger ARC 1.0+ references. "
            "Cell Ranger ARC pipelines are compatible with a Cell Ranger ARC 1.0+ reference".format(
                ref=reference_path, host=socket.gethostname(), msg=msg
            )
        )
        super().__init__(full_msg)


class ForceCellsError(PreflightException):
    """Force cells argument to pipeline is incorrect"""

    def __init__(self, force_cells, msg):
        full_msg = "The value supplied for `force_cells` = {} is invalid.\n{}".format(
            force_cells, msg
        )
        super().__init__(full_msg)


def check_force_cells(force_cells, species_list):
    """check if force cells is correctly specified as {"genome": {"atac": xxx, "gex": yyy}}"""
    if force_cells and any(force_cells.values()):
        if len(force_cells) != len(species_list):
            if (
                len(force_cells) == 1
                and len(species_list) != 1
                and list(force_cells.keys())[0] == "default"
            ):
                # the genome 'default' is used by the cellranger-arc wrapper
                raise ForceCellsError(
                    force_cells,
                    "The --min-atac-count and --min-gex-count options can only be used with a "
                    "single-species reference genome. The supplied reference package has {} "
                    "genomes: {}".format(len(species_list), species_list),
                )
            raise ForceCellsError(
                force_cells,
                "species_list = {}, force_cells = {} must have the same length".format(
                    species_list, force_cells
                ),
            )
        if len(species_list) > 1 and sorted(force_cells.keys()) != sorted(species_list):
            raise ForceCellsError(
                force_cells,
                "Incorrect genome name(s) supplied, species = {}, force_cells = {}".format(
                    species_list, force_cells
                ),
            )
        # check that each species has a gex and atac min_count specified
        for species, min_counts in force_cells.items():
            for key in ("atac", "gex"):
                val = min_counts.get(key)
                if val is None:
                    raise ForceCellsError(
                        force_cells,
                        "force_cells = {}: does not contain key {} for genome {}".format(
                            force_cells, key, species
                        ),
                    )
                if val <= 0:
                    raise ForceCellsError(
                        force_cells,
                        "force_cells = {}: value for genome {} and assay {} must be positive".format(
                            force_cells, species, key
                        ),
                    )


def path_not_readable(path):
    """Check if a path exists and has read permissions"""
    if not os.path.exists(path):
        return "Path does not exist"
    if not os.access(path, os.R_OK):
        return "Path exists but does not have read permissions"
    return ""


def exists_and_readable(fname, key, isfile=True):
    """check if file exists and is readable"""
    if fname is None:
        return

    if not os.path.exists(fname):
        raise PreflightException("Specified {} file: {} does not exist".format(key, fname))
    if isfile and not os.path.isfile(fname):
        raise PreflightException("Specified {} file: {} is not a valid file".format(key, fname))
    if not os.access(fname, os.R_OK):
        raise PreflightException("{} is not readable, please check file permissions".format(fname))
    if not os.path.getsize(fname) > 0:
        raise PreflightException("{} is empty".format(fname))


def check_valid_arc_reference_heavy(reference_path):
    """Heavier reference checks that involve parsing reference files"""
    _check_valid_reference_heavy(reference_path, MANDATORY_ARC_FILES, DEPRECATED_ARC_FILES)


def check_valid_arc_reference_basic(reference_path):
    """Check that a reference is compatible with cellranger-arc count.

    And return basic reference metadata that is safe to log in preflight. Note: we do not do any
    parsing of large files. The only file parsed is the faidx file which is generally small << 1mb.

    Args:
        reference_path (AnyStr): path to reference

    Returns:
        [Dict[str, Any]]: information about the reference
    """

    return _check_valid_reference_basic(reference_path, MANDATORY_ARC_FILES, DEPRECATED_ARC_FILES)


def check_valid_atac_reference_heavy(reference_path):
    """Heavier reference checks that involve parsing reference files"""
    _check_valid_reference_heavy(reference_path, MANDATORY_ATAC_FILES, DEPRECATED_ATAC_FILES)


def check_valid_atac_reference_basic(reference_path):
    """Check that a reference is compatible with cellranger-atac count.

    And return basic reference metadata that is safe to log in preflight. Note: we do not do any
    parsing of large files. The only file parsed is the faidx file which is generally small << 1mb.

    Args:
        reference_path (AnyStr): path to reference

    Returns:
        [Dict[str, Any]]: information about the reference
    """

    return _check_valid_reference_basic(reference_path, MANDATORY_ATAC_FILES, DEPRECATED_ATAC_FILES)


def _check_valid_reference_heavy(ref, mandatory_files, deprecated_files=None):
    # do basic checks
    _check_valid_reference_basic(ref, mandatory_files, deprecated_files)

    ref_mgr = ReferenceManager(ref)

    # check if motif file is in right format
    if ref_mgr.motifs:
        species = ref_mgr.list_species()
        if len(species) != 1:
            raise ArcReferenceError(
                ref,
                "Cannot specify motifs file for a multi-genome reference. Reference {} has "
                "species {} and a motifs file {}".format(ref, species, ref_mgr.motifs),
            )
        try:
            check_motifs_format(ref_mgr.motifs)
        except Exception as error:  # pylint: disable=broad-except
            raise ArcReferenceError(
                ref,
                "Reference motifs file {} is not in the correct format: {}".format(
                    ref_mgr.motifs, error
                ),
            ) from error

    # checks for valid bed file formats in regions/
    faidx_file = os.path.join(ref, "fasta", "genome.fa.fai")
    for bedfile in BED_FILES:
        path = os.path.join(ref, bedfile)
        try:
            bed_format_checker(path, faidx_file)
        except BedFormatError as err:
            raise ArcReferenceError(ref, "Error parsing BED file {}: {}".format(path, err)) from err


def _check_valid_reference_basic(ref, mandatory_files, deprecated_files=None):
    # Check that reference is readable and that expected files are present and readable
    error = path_not_readable(ref)
    if error:
        raise ArcReferenceError(ref, error)

    if not os.path.isdir(ref):
        raise ArcReferenceError(ref, "Path is not a directory")

    for path in mandatory_files:
        file_path = os.path.join(ref, path)
        error = path_not_readable(file_path)
        if error:
            raise ArcReferenceError(
                ref,
                "Reference package has an issue with a required file {}: {}".format(
                    file_path, error
                ),
            )
    for path in OPTIONAL_FILES:
        file_path = os.path.join(ref, path)
        if os.path.exists(file_path):
            error = path_not_readable(file_path)
            if error:
                raise ArcReferenceError(
                    ref,
                    "Reference package has an issue with a required file {}: {}".format(
                        file_path, error
                    ),
                )

    if deprecated_files is not None:
        for path in deprecated_files:
            file_path = os.path.join(ref, path)
            if os.path.exists(file_path):
                raise ArcReferenceError(
                    ref,
                    "Reference package is outdated, contains deprecated file {}".format(file_path),
                )

    # Check FASTA and contigs
    fasta = tk_ref.open_reference(ref)
    num_contigs = len(fasta)

    for key, seq in fasta.items():
        try:
            _ = seq[:1]
        except Exception as error:  # pylint: disable=broad-except
            raise ArcReferenceError(
                ref,
                "Unable to parse contig {}. Encountered error: {}.\nCheck that fasta "
                "index is up to date.".format(key, error),
            ) from error

    # Check metadata
    with open(os.path.join(ref, "reference.json")) as metadata:
        metadata = json.load(metadata)

    for key in METADATA_KEYS:
        if not key in metadata:
            raise ArcReferenceError(
                ref,
                "{} not present in reference.json. `cellranger-arc count` cannot "
                "run on a cellranger-atac or cellranger reference.".format(key),
            )

    for ctg_name in fasta:
        if not valid_contig_name(ctg_name, metadata["genomes"]):
            raise ArcReferenceError(
                ref,
                "Your reference has a contig {} that contains an invalid character. "
                "Contig names cannot contain `:` characters.".format(ctg_name),
            )

    primary_contigs = metadata["primary_contigs"]
    num_primary_contigs = len(primary_contigs)

    try:
        ref_mgr = ReferenceManager(ref)
    except Exception as error:  # pylint: disable=broad-except
        raise ArcReferenceError(
            ref,
            "Unable to read contig definitions from the reference.json."
            " Encountered error: {}".format(error),
        ) from error

    error = ref_mgr.verify_contig_defs()
    if error:
        raise ArcReferenceError(
            ref,
            "The contig definitions contained within the reference.json are not valid: {}".format(
                error
            ),
        )

    max_len = max(list(ref_mgr.contig_lengths.values()))
    if max_len >= (1 << 29):
        raise ArcReferenceError(
            ref,
            "Reference contains a contig longer than 536.8Mb (2^29 bp), which is "
            "not supported due to limitations of the .bai format. "
            "Please split this contig.",
        )

    info = {}
    info["num_contigs"] = num_contigs
    info["num_primary"] = num_primary_contigs
    info["primary_contig_lengths"] = sorted(list(ref_mgr.contig_lengths.values()))
    info["primary_genome_size_mb"] = sum(list(ref_mgr.contig_lengths.values())) / 1e6
    return info


def check_k_means_max_clusters(kmeans):
    """check whether k_means_max_clusters is a legit integer input for analyzer"""

    if kmeans is not None and (kmeans < 2 or kmeans > 10):
        raise PreflightException(
            "Invalid value for --k-means-max-clusters: {}. Please specify a value satisfying "
            "2 <= value <= 10.".format(kmeans)
        )


def check_feature_linkage_max_dist_mb(max_dist, ref_manager):
    """check whether feature_linkage_max_dist_mb input is legit for feature linkage computer"""

    genome_size_mb = sum(list(ref_manager.contig_lengths.values())) / 1e6
    if max_dist is not None and max_dist <= 0:
        raise PreflightException(
            "Invalid value for --feature-linkage-max-dist-mb: {}. Please specify a positive"
            " number.".format(max_dist)
        )
    if max_dist is not None and max_dist > genome_size_mb:
        raise PreflightException(
            "Invalid value for --feature-linkage-max-dist-mb: {}. The number exceeds the genome "
            "size in megabases = {:.2f}. Note that the window size has units of megabases.".format(
                max_dist, genome_size_mb
            )
        )
