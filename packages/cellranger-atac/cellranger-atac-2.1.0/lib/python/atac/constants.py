# Copyright (c) 2022 10x Genomics, Inc. All rights reserved.
"""
These are constants that are specific to this pipeline.  Other
general constants can be found in tenkit.constants.
"""

######################################################
# DO NOT add new items to this file.
#
# - If a constant is only used from a single module, put it in that module.
# - If a constant is only used in association with a particular module, put it
#   in that module.
# - If a constant is used in a bunch of places, create a module with a more
#   specifically descriptive name to put those constants.
######################################################

from __future__ import division, print_function, absolute_import

# Product name, used in bam header lines
TENX_PRODUCT_NAME = "cellranger-atac"

# These override the default values from tenkit
RAW_BARCODE_TAG = "CR"
PROCESSED_BARCODE_TAG = "CB"
RAW_BARCODE_QUAL_TAG = "CY"

# Downsampled number of reads to report standardized metrics at
DOWNSAMPLED_READS = 30000000
RPC_50K = 50000
RPC_30K = 30000
RPC_10K = 10000

# Limit the total number of cells called for each species to no more than this
MAXIMUM_CELLS_PER_SPECIES = 20000
MINIMUM_COUNT = 5
WHITELIST_CONTAM_RATE = 0.02

# some analysis constants
ALLOWED_FACTORIZATIONS = {"plsa", "pca", "lsa", "bclsa", "bcpca"}
ALLOWED_PROJECTIONS = {"tsne", "umap"}
CLUSTER_FILE_HEAD = {fac: fac + "_kmeans" for fac in ALLOWED_FACTORIZATIONS}
DEFAULT_FACTORIZATION = "lsa"

# aggr
ALLOWED_NORMALIZATIONS = (None, "unique")

# CIGAR codes
CIGAR_SOFTCLIP = 4

# Used for position tagging
MAX_I32 = 2147483647

# Ordered valid DNA bases.  Don't change the order!
VALID_BASES = ["A", "C", "G", "T"]

# Peak calling parameters
WINDOW_SIZE = 400
# Peaks closer than this will be merged into a single peak.
PEAK_MERGE_DISTANCE = 500
PEAK_ODDS_RATIO = 1 / 5
MAX_INSERT_SIZE = 1200

NO_BARCODE = b"NO_BARCODE"

ATACSEQ_LIBRARY_TYPE = "Chromatin Accessibility"
# for preflight checks on fragments
FRAGMENTS_SCAN_SIZE = 100000

# gene types we care about for annotation
TRANSCRIPT_ANNOTATION_GENE_TYPES = [
    "protein_coding",
    "TR_V_gene",
    "TR_D_gene",
    "TR_J_gene",
    "TR_C_gene",
    "IG_V_gene",
    "IG_D_gene",
    "IG_J_gene",
    "IG_C_gene",
]
