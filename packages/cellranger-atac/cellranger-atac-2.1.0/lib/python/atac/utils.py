# Copyright (c) 2019 10x Genomics, Inc. All rights reserved.
"""
Miscellaneous ATAC pipeline utility functions
"""

from __future__ import annotations, division, print_function, absolute_import


from typing import List, Tuple
import numpy as np
from sklearn.utils import sparsefuncs

from atac.tools.ref_manager import ReferenceManager


def normalize_matrix(input_matrix, scale):
    """normalize a matrix with some scale"""
    normed_matrix = input_matrix.copy().astype(np.float64)
    scale = np.median(scale) / scale
    sparsefuncs.inplace_column_scale(normed_matrix, scale)
    return normed_matrix


def generate_genome_tag(ref_path: str) -> List[str]:
    """Replace empty genome name for single genomes with valid genome name"""
    # For a single species reference, use contents of <reference_path>/genome
    ref_contig_manager = ReferenceManager(ref_path)
    genomes = ref_contig_manager.list_species()
    if len(genomes) == 1 and genomes[0] == "" or not genomes:
        genomes = [ref_contig_manager.genome]
    return genomes


def get_purity_info(singlecell_df, species_list):
    """Calculate species barcode purity"""
    assert len(species_list) == 2

    def calculate_purity(self_counts, other_counts):
        return self_counts / (self_counts + other_counts)

    xdata = singlecell_df["passed_filters_{}".format(species_list[0])].values
    ydata = singlecell_df["passed_filters_{}".format(species_list[1])].values
    is_spec_1 = singlecell_df["is_{}_cell_barcode".format(species_list[0])] == 1
    is_spec_2 = singlecell_df["is_{}_cell_barcode".format(species_list[1])] == 1
    spec_1_purity = calculate_purity(xdata[is_spec_1], ydata[is_spec_1])
    spec_2_purity = calculate_purity(ydata[is_spec_2], xdata[is_spec_2])

    return spec_1_purity, spec_2_purity


def get_chunks(element_count, chunk_count) -> List[Tuple[int, int]]:
    """Attempts to generate non-empty slices out of element_count entries."""
    bounds = np.linspace(0, element_count, chunk_count + 1, dtype=int, endpoint=True).tolist()
    return [(s, e) for s, e in zip(bounds, bounds[1:]) if s != e]


def quick_line_count(filename) -> int:
    """
    a fast line counter. It reads the text file as binary chunks and counts the number
    of new line characters.
    :param filename: str of path of the input file
    :return: number of lines
    """

    def _make_gen(reader):
        chunk = reader(1024 * 1024)
        while chunk:
            yield chunk
            chunk = reader(1024 * 1024)

    infile = open(filename, "rb")
    f_gen = _make_gen(infile.read)
    return sum(buf.count(b"\n") for buf in f_gen)
