# Copyright (c) 2019 10x Genomics, Inc. All rights reserved.
"""
Tools for calculating pipestance metrics from the singlecell.csv file.
"""

from __future__ import division, print_function, absolute_import

from six import ensure_str
from six.moves import xrange as range

import numpy as np
import scipy.stats

import atac.utils as utils
from atac.constants import NO_BARCODE
import atac.cell_calling_helpers.exclusions as exclusions
from tenkit.stats import robust_divide


class MetricsError(Exception):
    """Errors when metrics are generated"""


def add_bulk_targeting_metrics(summary_info, singlecell_df, species_list):
    """Take singlecell targeting data and calculate bulk targeting metrics from them."""
    cell_mask = np.zeros(singlecell_df.shape[0], dtype=bool)
    for species in species_list:
        species_cell_mask = singlecell_df["is_%s_cell_barcode" % species] == 1
        cell_mask += species_cell_mask.to_numpy()

        key_suffix = "" if len(species_list) == 1 else "_{}".format(species)

        cell_data = singlecell_df[species_cell_mask]

        pairs = [
            ("on_target_fragments", "targets"),
            ("TSS_fragments", "tss"),
            ("DNase_sensitive_region_fragments", "dnase"),
            ("enhancer_region_fragments", "enhancer"),
            ("promoter_region_fragments", "promoter"),
            ("blacklist_region_fragments", "blacklist"),
            ("peak_region_fragments", "peaks"),
        ]

        total = cell_data["passed_filters"].sum()
        for column_name, suffix in pairs:
            cell_sum = cell_data[column_name].sum()
            summary_key = "frac_fragments_overlapping_{}{}".format(suffix, key_suffix)
            summary_info[summary_key] = robust_divide(cell_sum, total)
    assert cell_mask.dtype == np.bool, "wrong dtype"

    cut_frags_in_peaks = singlecell_df[cell_mask]["peak_region_cutsites"].sum()
    total = singlecell_df[cell_mask]["passed_filters"].sum()
    summary_info["frac_cut_fragments_in_peaks"] = robust_divide(cut_frags_in_peaks, 2 * total)


def add_singlecell_sensitivity_metrics(summary_info, singlecell_df, species_list):
    """Get sensitivity metrics per cell"""
    non_cell_barcodes = np.ones(singlecell_df.shape[0], dtype=bool)
    for species in species_list:
        key_suffix = "" if len(species_list) == 1 else "_{}".format(species)

        cell_mask = (singlecell_df["is_%s_cell_barcode" % species] == 1).to_numpy()
        non_cell_barcodes *= ~cell_mask
        cell_fragments = singlecell_df["passed_filters"][cell_mask].values

        summary_key = "mean_sequenced_fragments_per_cell{}".format(key_suffix)
        summary_info[summary_key] = robust_divide(singlecell_df["total"].sum(), cell_mask.sum())

        summary_key = "mean_fragments_per_cell{}".format(key_suffix)
        summary_info[summary_key] = np.mean(cell_fragments)

        summary_key = "median_fragments_per_cell{}".format(key_suffix)
        summary_info[summary_key] = np.median(cell_fragments)

        summary_key = "min_fragments_per_cell{}".format(key_suffix)
        if cell_fragments.size:
            summary_info[summary_key] = np.min(cell_fragments)
        else:
            summary_info[summary_key] = None

        summary_key = "stdev_fragments_per_cell{}".format(key_suffix)
        summary_info[summary_key] = np.std(cell_fragments)

    # Background noise metric calculations
    valid_barcodes = singlecell_df["barcode"] != ensure_str(NO_BARCODE)
    valid_noncell_mask = valid_barcodes & non_cell_barcodes
    noncell_fragments = singlecell_df["passed_filters"][valid_noncell_mask].values
    summary_info["mean_fragments_per_noncell"] = np.mean(noncell_fragments)
    summary_info["median_fragments_per_noncell"] = np.median(noncell_fragments)


def add_doublet_rate_metrics(summary_info, singlecell_df, species_list):
    """Infer doublet rate from observed doublets"""

    def infer_multiplets_from_observed(n_obs_multiplets, n_cells0, n_cells1):
        """Estimates the number of real multiplets based on the number observed from a
        barnyard (mixed species) experiment
        """
        if n_cells0 == 0 or n_cells1 == 0 or n_obs_multiplets == 0:
            return 0

        # Prior probability of a doublet given counts for each cell type
        # (ignore N_cells > 2)
        total_cells = n_cells0 + n_cells1
        p_obs_multiplet = 2 * (n_cells0 / total_cells) * (n_cells1 / total_cells)

        # Brute force MLE of binomial n
        likelihood = scipy.stats.binom.pmf(
            n_obs_multiplets,
            range(0, total_cells),
            p_obs_multiplet,
        )
        return np.argmax(likelihood)

    if len(species_list) == 1:
        return

    counts = []
    cell_barcodes_dict = {}
    for species in species_list:
        species_cell_mask = singlecell_df["is_%s_cell_barcode" % species] == 1
        barcodes = singlecell_df["barcode"][species_cell_mask].values.tolist()
        cell_barcodes_dict[species] = barcodes
        counts.append(len(barcodes))
    total_unique_cell_barcodes = {bc for barcodes in cell_barcodes_dict.values() for bc in barcodes}
    total_cell_barcodes = sum(counts)
    summary_info["cells_detected"] = len(total_unique_cell_barcodes)

    observed_doublets = total_cell_barcodes - len(total_unique_cell_barcodes)
    observed_doublet_rate = robust_divide(observed_doublets, total_cell_barcodes)
    inferred_doublets = infer_multiplets_from_observed(
        observed_doublets,
        counts[0],
        counts[1],
    )
    inferred_doublet_rate = robust_divide(inferred_doublets, total_cell_barcodes)
    summary_info["observed_doublets"] = observed_doublets
    summary_info["observed_doublet_rate"] = observed_doublet_rate
    summary_info["inferred_doublets"] = inferred_doublets
    summary_info["inferred_doublet_rate"] = inferred_doublet_rate


def add_purity_metrics(summary_info, singlecell_df, species_list):
    """Detect purity of each barcode"""
    if len(species_list) == 2:
        for i, purity in enumerate(utils.get_purity_info(singlecell_df, species_list)):
            summary_key = "median_purity_{}".format(species_list[i])
            summary_info[summary_key] = np.median(purity)


def add_bulk_mapping_metrics(summary_info, singlecell_df, species_list):
    """Waste metric calculations"""

    # singlecell.csv files generated by aggr and reanalyze don't have a `mitochondrial` column
    # because they only have access to the fragments file and hence cannot compute waste metrics
    if "mitochondrial" not in singlecell_df:
        return

    WASTE_KEYS = [
        "no_barcode",
        "non_cell_barcode",
        "mitochondrial",
        "unmapped",
        "lowmapq",
        "chimeric",
        "duplicate",
        "total",
    ]

    no_barcode_row = singlecell_df["barcode"] == ensure_str(NO_BARCODE)
    waste_totals = {}
    cell_mask = (
        singlecell_df[["is_{}_cell_barcode".format(sp) for sp in species_list]].sum(axis=1) > 0
    )
    valid_noncell_mask = (~cell_mask) & (~no_barcode_row)

    for key in WASTE_KEYS:
        if key == "no_barcode":
            waste_totals[key] = (
                0 if all(~no_barcode_row) else singlecell_df["total"].loc[no_barcode_row].values[0]
            )
        elif key == "non_cell_barcode":
            waste_totals[key] = singlecell_df["total"][valid_noncell_mask].sum()
        elif key == "total":
            waste_totals[key] = singlecell_df["total"].sum()
        else:
            waste_totals[key] = singlecell_df[cell_mask][key].sum()
    for key in WASTE_KEYS:
        if key == "total":
            continue
        summary_info["waste_%s_fragments" % key] = waste_totals[key]
        summary_info["frac_waste_%s" % key] = robust_divide(
            waste_totals[key], waste_totals["total"]
        )

    if waste_totals["total"] == 0:
        raise MetricsError(
            "No read pairs were detected in the ATAC library. This is likely because \
                     of incorrect input sequenced data potentially due to bioinformatic \
                     mixup. Further execution will be halted."
        )

    # Special case for frac_waste_no_barcode.
    # We will use its "positive wording" counterpart in the websummary.
    summary_info["frac_valid_barcode"] = 1 - summary_info["frac_waste_no_barcode"]

    cell_mito = singlecell_df[cell_mask]["mitochondrial"].sum()
    cell_unmapped = singlecell_df[cell_mask]["unmapped"].sum()
    cell_lowmapq = singlecell_df[cell_mask]["lowmapq"].sum()
    cell_dup = singlecell_df[cell_mask]["duplicate"].sum()
    cell_passed = singlecell_df[cell_mask]["passed_filters"].sum()
    cell_passed_ontarget = singlecell_df[cell_mask]["peak_region_fragments"].sum()

    summary_info["waste_cell_mito_fragments"] = cell_mito
    summary_info["waste_cell_unmapped_fragments"] = cell_unmapped
    summary_info["waste_cell_lowmapq_fragments"] = cell_lowmapq
    summary_info["waste_cell_dup_fragments"] = cell_dup
    summary_info["total_usable_fragments"] = cell_passed
    summary_info["total_usable_ontarget_fragments"] = cell_passed_ontarget

    summary_info["waste_ratio_mito_cells"] = robust_divide(cell_mito, cell_dup + cell_passed)
    summary_info["waste_ratio_unmapped_cells"] = robust_divide(
        cell_unmapped, cell_dup + cell_passed
    )
    summary_info["waste_ratio_lowmapq_cells"] = robust_divide(cell_lowmapq, cell_dup + cell_passed)
    summary_info["waste_ratio_dup_cells"] = robust_divide(cell_dup, cell_passed)

    noncell_mito = singlecell_df[valid_noncell_mask]["mitochondrial"].sum()
    noncell_unmapped = singlecell_df[valid_noncell_mask]["unmapped"].sum()
    noncell_lowmapq = singlecell_df[valid_noncell_mask]["lowmapq"].sum()
    noncell_dup = singlecell_df[valid_noncell_mask]["duplicate"].sum()
    noncell_passed = singlecell_df[valid_noncell_mask]["passed_filters"].sum()
    summary_info["total_passed_filter_fragments"] = cell_passed + noncell_passed

    summary_info["waste_ratio_mito_noncells"] = robust_divide(
        noncell_mito, noncell_dup + noncell_passed
    )
    summary_info["waste_ratio_unmapped_noncells"] = robust_divide(
        noncell_unmapped, noncell_dup + noncell_passed
    )
    summary_info["waste_ratio_lowmapq_noncells"] = robust_divide(
        noncell_lowmapq, noncell_dup + noncell_passed
    )
    summary_info["waste_ratio_dup_noncells"] = robust_divide(noncell_dup, noncell_passed)

    cell_barcodes_total = singlecell_df["total"][cell_mask].sum()
    noncell_barcodes_total = singlecell_df["total"][valid_noncell_mask].sum()
    total_fragments = waste_totals["total"]
    summary_info["num_fragments"] = total_fragments
    summary_info["frac_waste_overall_nondup"] = robust_divide(
        total_fragments - (cell_dup + cell_passed), total_fragments
    )
    summary_info["frac_waste_cell_nondup"] = robust_divide(
        cell_barcodes_total - (cell_dup + cell_passed), cell_barcodes_total
    )
    summary_info["frac_valid_noncell"] = robust_divide(
        noncell_barcodes_total, noncell_barcodes_total + cell_barcodes_total
    )

    summary_info["frac_fragments_in_cells"] = robust_divide(cell_barcodes_total, total_fragments)

    # Next metric is similar to multi_cdna_pcr_dupe_reads_frac (Percent duplicates,percentage)
    all_pf = noncell_passed + cell_passed
    all_dup = noncell_dup + cell_dup
    summary_info["frac_pcr_dup_passed_filters"] = robust_divide(all_dup, all_dup + all_pf)

    # Next metric is similar to "Reads in Cells" in cellranger.
    summary_info["frac_passed_filter_fragments_in_cells"] = robust_divide(
        cell_passed, summary_info["total_passed_filter_fragments"]
    )

    # Fraction of all reads that contribute to the peak/barcode matrix
    summary_info["frac_fragments_usable_ontarget"] = robust_divide(
        cell_passed_ontarget, total_fragments
    )

    # Next metric is similar to "Reads Mapped Confidently to Genome" in cellranger.
    # Fraction of total reads/fragments with mapq > 30 that are not mitochondrial and not chimeric
    mapped_confidently = noncell_dup + noncell_passed + cell_dup + cell_passed
    summary_info["frac_mapped_confidently"] = robust_divide(mapped_confidently, total_fragments)

    if not np.isnan(summary_info["frac_mapped_confidently"]):
        unmapped_frac = 1 - summary_info["frac_mapped_confidently"]
        if unmapped_frac > 0.90:
            raise MetricsError(
                "{:.1%} of ATAC read pairs were not mapped to the supplied \
reference genome. This is likely the consequence of the incorrect reference \
genome being used, a sample mixup or very low sequencing quality. Further \
execution will be halted.".format(
                    unmapped_frac
                )
            )

    # fraction of total reads with mapq > 30 & have a valid barcode
    summary_info["frac_reads_mapq_gt_30"] = 1 - robust_divide(
        cell_unmapped + noncell_unmapped + cell_lowmapq + noncell_lowmapq,
        total_fragments,
    )

    # fraction of total reads that are unmapped & have a valid barcode
    summary_info["frac_reads_unmapped"] = robust_divide(
        cell_unmapped + noncell_unmapped, total_fragments
    )

    # fraction of total reads that are in mitochondria & mapq > 30 & have a valid barcode
    summary_info["frac_reads_mapq_gt_30_mito"] = robust_divide(
        cell_mito + noncell_mito, total_fragments
    )

    summary_info["waste_total_fragments"] = total_fragments - cell_passed
    summary_info["frac_waste_total"] = robust_divide(
        summary_info["waste_total_fragments"], total_fragments
    )

    # add waste due to exclusions
    summary_info.update(exclusions.calculate_waste_metrics(singlecell_df, species_list))
