# Copyright (c) 2019 10x Genomics, Inc. All rights reserved.
"""
Miscellaneous ATAC pipeline utility functions
"""

from __future__ import division, print_function, absolute_import

from typing import AnyStr, Dict, Set


def load_cell_barcodes_with_species(filename: AnyStr) -> Dict[bytes, Set[bytes]]:
    """Read cell_barcodes.csv and emit barcodes per species."""
    cell_barcodes = {}
    with open(filename, "rb") as infile:
        for line in infile:
            items = line.strip(b"\n").rstrip(b",").split(b",")
            cell_barcodes[items[0]] = (
                set({item for item in items[1:] if item and item != b"null"})
                if len(items) > 1
                else set()
            )
    return cell_barcodes


def load_cell_barcode_set(filename: AnyStr) -> Set[bytes]:
    """Read cell_barcodes.csv and emit barcodes."""
    cell_barcodes = load_cell_barcodes_with_species(filename)
    barcodes = set()
    for bcs in cell_barcodes.values():
        barcodes.update(bcs)
    return barcodes


def load_cell_barcodes(filename, with_species=False):
    """Read cell_barcodes.csv and emit barcodes"""
    if with_species:
        return load_cell_barcodes_with_species(filename)
    return load_cell_barcode_set(filename)
