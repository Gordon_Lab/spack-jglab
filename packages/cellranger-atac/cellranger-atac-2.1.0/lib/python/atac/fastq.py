# Copyright (c) 2020 10x Genomics, Inc. All rights reserved.
"""
Functions for determining input ATAC FASTQs for cellranger-atac and cellranger-arc
"""

from __future__ import annotations

from typing import AnyStr, List, NamedTuple, Optional, Union

from tenkit.fasta import find_input_fastq_files_bcl2fastq_demult as find_fastq


class FastqError(Exception):
    """Errors associated with finding fastq files with specified parameters"""


FastqSet = NamedTuple(
    "FastqSet",
    (("read1", List[bytes]), ("read2", List[bytes]), ("i1", List[bytes]), ("i2", List[bytes])),
)


def find_bcl2fastq_files(
    path: AnyStr, sample_name: AnyStr, lanes: Optional[List[Union[str, int]]]
) -> FastqSet:
    """Find ATAC input FASTQ files demultiplexed by mkfastq/bcl2fastq

    Args:
        path (AnyStr): path to fastqs
        sample_name (AnyStr): bcl2fastq sample name
        lanes (Optional[List[Union[str, int]]]): list of lanes

    Raises:
        FastqError: if naming convention is not consisten

    Returns:
        FastqSet: [description]
    """
    r1 = find_fastq(path, "R1", sample_name, lanes)  # pylint: disable=invalid-name
    r2 = find_fastq(path, "R2", sample_name, lanes)  # pylint: disable=invalid-name
    r3 = find_fastq(path, "R3", sample_name, lanes)  # pylint: disable=invalid-name
    i1 = find_fastq(path, "I1", sample_name, lanes)  # pylint: disable=invalid-name
    i2 = find_fastq(path, "I2", sample_name, lanes)  # pylint: disable=invalid-name

    if r3 and i2:
        raise FastqError(
            "Found both and R3 and I2 file for sample {} and lanes {} in path {}. The FASTQ files"
            " must be named (read1 = R1, read2 = R2, I7 read = I1, I5 read = I2) or "
            "(read1 = R1, read2 = R3, I7 read = I1, I5 read = R2)".format(sample_name, lanes, path)
        )

    if i2:
        return FastqSet(r1, r2, i1, i2)

    # only r1 and r2 are present
    if r1 and r2 and not (i2 or r3):
        return FastqSet(r1, r2, i1, i2)

    return FastqSet(r1, r3, i1, r2)
