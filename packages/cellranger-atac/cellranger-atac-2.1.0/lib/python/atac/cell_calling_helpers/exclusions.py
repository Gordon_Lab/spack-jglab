# Copyright (c) 2020 10X Genomics, Inc. All rights reserved.
"""
Constants and functions for barcode exlusion pipeline
"""

from __future__ import absolute_import, division

from six import ensure_binary, ensure_str, PY3
import numpy as np
from tenkit.stats import robust_divide

if PY3:
    from typing import AnyStr, List  # pylint: disable=import-error,unused-import

# barcodes that don't have sufficiently many fragments on target (dead cells etc)
LOW_TARGETING = "low_targeting"
# a gel bead that has > 1 barcode oligo
BARCODE_MULTIPLET = "whitelist_contam"
# two gel beads and one cell in a partition
GEL_BEAD_DOUBLET = "gel_bead_doublet"

EXCLUSION_REASONS = [GEL_BEAD_DOUBLET, LOW_TARGETING, BARCODE_MULTIPLET]
EXCLUSION_REASON_CODE = {
    "": 0,  # not excluded
    GEL_BEAD_DOUBLET: 1,
    LOW_TARGETING: 2,
    BARCODE_MULTIPLET: 3,
}


def get_species_column(species, species_list):
    # type: (str, List[AnyStr]) -> bytes
    """Column name for excluded barcodes column in singlecell.csv output"""
    if len(species_list) == 1:
        return b"excluded_reason"
    return b"excluded_reason_%s" % (ensure_binary(species))


def get_columns(species_list):
    """All excluded barcodes columns in singlecell.csv output"""
    return [get_species_column(species, species_list) for species in species_list]


def get_waste_key(species, reason, suffix, species_list):
    """Name of metric that captures waste due to excluded barcodes"""
    return (
        "frac_waste_exclusions_{reason}_{suffix}"
        if len(species_list) == 1
        else "frac_waste_exclusions_{reason}_{suffix}_{species}"
    ).format(species=species, reason=reason, suffix=suffix)


def get_num_excluded_key(species, reason, species_list):
    """Number of excluded barcodes in each category with high counts"""
    return (
        "num_high_count_excluded_bcs_{reason}"
        if len(species_list) == 1
        else "num_high_count_excluded_bcs_{reason}_{species}"
    ).format(species=species, reason=reason)


def get_frac_excluded_key(species, reason, species_list):
    """Number of excluded barcodes in each category with high counts"""
    return (
        "frac_high_count_excluded_bcs_{reason}"
        if len(species_list) == 1
        else "frac_high_count_excluded_bcs_{reason}_{species}"
    ).format(species=species, reason=reason)


def define_high_count_barcodes(singlecell, cell_filter, pf_col):
    """
    Define which barcodes are "high count". cell_filter = which barcodes are cells,
    pf_col = passed filters column. Both are species specific.
    """
    num_cells = cell_filter.sum()

    if num_cells > 0:
        n99 = np.percentile(singlecell.loc[cell_filter, pf_col], 99)
    else:
        n99 = np.percentile(singlecell[pf_col], 99)

    threshold = n99 / 50
    high_count_filter = singlecell[pf_col] > threshold
    return high_count_filter, threshold


# pylint: disable=too-many-locals
def calculate_waste_metrics(singlecell, species_list):
    """Calculate waste due to excluded barcodes split by category"""
    summary = {}
    for species in species_list:
        cell_col = (
            "is__cell_barcode" if len(species_list) == 1 else "is_{}_cell_barcode".format(species)
        )
        excl_col = ensure_str(get_species_column(species, species_list))
        total = "total"
        pf_col = "passed_filters" if len(species_list) == 1 else "passed_filters_{}".format(species)

        cell_filter = singlecell[cell_col] == 1
        num_cells = cell_filter.sum()
        high_count_filter, threshold = define_high_count_barcodes(singlecell, cell_filter, pf_col)
        summary["high_count_exclusions_threshold_{}".format(species)] = threshold

        total_reads = singlecell[total].sum()
        for reason in EXCLUSION_REASONS:
            reason_code = EXCLUSION_REASON_CODE[reason]
            exclude_filter = singlecell[excl_col] == reason_code
            both = exclude_filter & high_count_filter

            total_waste = 0
            high_count_waste = 0

            total_waste = singlecell.loc[exclude_filter, total].sum()
            high_count_waste = singlecell.loc[both, total].sum()

            total_key = get_waste_key(species, reason, "total", species_list)
            high_key = get_waste_key(species, reason, "high", species_list)
            count_key = get_num_excluded_key(species, reason, species_list)
            frac_key = get_frac_excluded_key(species, reason, species_list)

            num_high_count = both.sum()

            summary[total_key] = robust_divide(total_waste, total_reads)
            summary[high_key] = robust_divide(high_count_waste, total_reads)
            summary[count_key] = num_high_count
            summary[frac_key] = robust_divide(num_high_count, num_cells)
    return summary
