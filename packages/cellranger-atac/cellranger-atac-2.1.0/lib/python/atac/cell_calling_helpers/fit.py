# Copyright (c) 2019 10x Genomics, Inc. All rights reserved.
"""
Tools for fitting distributions to per barcode measurements.
"""

from __future__ import absolute_import, division, print_function

from collections import namedtuple

import numpy as np
from scipy import stats

from atac.analysis.em_fitting import weighted_mean, MLE_dispersion
from cellranger.cell_calling_helpers import filter_cellular_barcodes_ordmag

MixedModelParams = namedtuple(
    "MixedModelParams",
    ["mu_noise", "alpha_noise", "mu_signal", "alpha_signal", "frac_noise"],
)


def simple_threshold_estimate(count_dict, cutoff=0.01):
    """Does a simple estimate for the threshold count:  pick the threshold such that the lower noise region contains
    cutoff (default 1%) percentage of the total reads.
    """
    count_values = np.array(sorted(k for k in count_dict))
    count_weights = np.array([count_dict[k] for k in count_values])
    read_counts = count_values * count_weights

    cum_frac = read_counts.cumsum() / read_counts.sum()

    threshold = count_values[cum_frac <= cutoff][-1] if any(cum_frac <= cutoff) else count_values[1]
    print(threshold, count_values[1], count_values[-2])
    threshold = max(threshold, count_values[1])
    threshold = min(threshold, count_values[-2])
    return threshold


def find_threshold_using_ordmag(count_data, recovered_cells):
    """Find threshold count to define cells using order-of-magnitude method"""
    top_bc_idx, _, _ = filter_cellular_barcodes_ordmag(count_data, recovered_cells)

    mask = np.ones(count_data.shape[0], np.bool)
    mask[top_bc_idx] = 0
    if sum(mask) < 2:
        rank = count_data.rank(ascending=False)
        top_count = count_data[rank == 1]
        threshold = top_count.values[0] / 10.0
        return threshold
    else:
        threshold = max(count_data[mask])
        return threshold


def normalize_parameters(params):
    """Ensure that the larger of the two distributions is always the signal distribution."""
    if params.mu_noise > params.mu_signal:
        return MixedModelParams(
            params.mu_signal,
            params.alpha_signal,
            params.mu_noise,
            params.alpha_noise,
            1 - params.frac_noise,
        )
    return params


def weighted_parameter_estimates(count_dict, prob_matrix):
    """Given N counts and a 2 by N probability matrix with the likelihood that each count is from signal or noise
    distributions, estimate the maximum likelihood parameters for the distributions"""
    prob_noise = prob_matrix[0, :]
    prob_signal = prob_matrix[1, :]

    noise_mask = prob_noise > 0
    signal_mask = prob_signal > 0

    count_values = np.array(sorted(k for k in count_dict))
    count_weights = np.array([count_dict[k] for k in count_values])

    mu_noise = weighted_mean(
        count_values[noise_mask], count_weights[noise_mask] * prob_noise[noise_mask]
    )
    mu_signal = weighted_mean(
        count_values[signal_mask], count_weights[signal_mask] * prob_signal[signal_mask]
    )
    alpha_noise = MLE_dispersion(
        count_values[noise_mask], count_weights[noise_mask] * prob_noise[noise_mask]
    )
    alpha_signal = MLE_dispersion(
        count_values[signal_mask], count_weights[signal_mask] * prob_signal[signal_mask]
    )
    frac_noise = (prob_noise * count_weights).sum() / (prob_matrix * count_weights).sum()
    return normalize_parameters(
        MixedModelParams(mu_noise, alpha_noise, mu_signal, alpha_signal, frac_noise)
    )


def prob_estimator(k, params):
    """Estimates the probability that a given count was drawn
    from either the non-cell i.e. noise negative binomial distribution
    or cell i.e. signal negative binomial distribution. Since it is a mixture
    of two the fractional multiplication to ensure that the pmf sums to 1.
    """
    signal = np.log(1 - params.frac_noise) + stats.nbinom.logpmf(
        k, 1 / params.alpha_signal, 1 / (1 + params.alpha_signal * params.mu_signal)
    )
    noise = np.log(params.frac_noise) + stats.nbinom.logpmf(
        k, 1 / params.alpha_noise, 1 / (1 + params.alpha_noise * params.mu_noise)
    )
    return signal, noise


def calculate_mixture_pmf(count_dict, params):
    """Returns the normalized probability that each count comes from the signal or the noise distributions,
    given some parameters for those.  Counts where both PMFs are zero are returned with a (non-normalized)
    zero probability of coming from either."""
    count_values = np.array(sorted(k for k in count_dict))

    noise_pmf = stats.nbinom.pmf(
        count_values,
        1 / params.alpha_noise,
        1 / (1 + params.alpha_noise * params.mu_noise),
    )
    signal_pmf = stats.nbinom.pmf(
        count_values,
        1 / params.alpha_signal,
        1 / (1 + params.alpha_signal * params.mu_signal),
    )

    output = np.vstack((params.frac_noise * noise_pmf, (1 - params.frac_noise) * signal_pmf))
    mask = output.sum(axis=0) == 0
    output[:, mask] = 0.5
    output /= np.sum(output, axis=0)
    output[:, mask] = 0.0
    return output


def estimate_parameters(count_dict, epsilon=1e-6, maxiter=250, threshold=None, cutoff=0.01):
    """Uses an expectation-maximization method to estimate the mixed model parameters."""
    if threshold is None:
        threshold = simple_threshold_estimate(count_dict, cutoff)
    print("Initial threshold: {}".format(threshold))

    count_values = np.array(sorted(k for k in count_dict))

    prob_matrix = np.empty((2, len(count_values)), dtype=float)
    prob_matrix[0, :] = count_values <= threshold
    prob_matrix[1, :] = count_values > threshold
    params = weighted_parameter_estimates(count_dict, prob_matrix)
    print("Initial parameters")
    print(params)
    last_params = None
    i = 0
    # pylint: disable=unsubscriptable-object
    while i < maxiter and (
        last_params is None
        or any(abs(last_params[j] - params[j]) / params[j] > epsilon for j in range(len(params)))
    ):
        i += 1
        prob_matrix = calculate_mixture_pmf(count_dict, params)
        last_params = params
        params = weighted_parameter_estimates(count_dict, prob_matrix)
    print("Final parameters")
    print(params)
    return params


def goodness_of_fit(count_data, params):
    """Goodness of fit metric using KS distance

    Count data is a simple array or list of counts
    """

    def mixed_nbinom_cdf(k):
        """CDF of mixture distribution"""
        noise_cdf = stats.nbinom.cdf(
            k, 1 / params.alpha_noise, 1 / (1 + params.alpha_noise * params.mu_noise)
        )
        signal_cdf = stats.nbinom.cdf(
            k, 1 / params.alpha_signal, 1 / (1 + params.alpha_signal * params.mu_signal)
        )
        return params.frac_noise * noise_cdf + (1 - params.frac_noise) * signal_cdf

    gof, _ = stats.kstest(count_data, mixed_nbinom_cdf)
    return gof
