# Copyright (c) 2022 10x Genomics, Inc. All rights reserved.
"""
Methods for doing preflight checks prior to ATAC pipeline execution.
"""

from __future__ import division, print_function, absolute_import

import os
import re
import socket
from numbers import Number
from typing import Dict, List, Tuple, Optional

import numpy as np
import pandas as pd
from six import ensure_str

import tenkit.fasta as tk_fasta
import tenkit.preflight as tk_preflight
from atac.constants import (
    ALLOWED_FACTORIZATIONS,
    FRAGMENTS_SCAN_SIZE,
    NO_BARCODE,
)
import atac.fastq
import atac.tools.csv_utils as atac_csv
from atac.tools.fragments import open_fragment_file
from atac.tools.ref_manager import ReferenceManager
from atac.tools.regions import (
    bed_format_checker,
    is_overlapping,
    count_header_lines_bed,
    BedFormatError,
)
from cellranger.constants import AGG_ID_FIELD, AGG_BATCH_FIELD
from cellranger.io import open_maybe_gzip

ALLOWED_FACTORIZATIONS_FOR_BATCH_CORRECTION = {"pca", "lsa"}


class PreflightException(Exception):
    """Raise expection if checks in preflight fail"""


def check_peak_qval(peak_qval, cmd_str="--peak-qval"):
    """Check for valid q-val for peak calling"""
    if peak_qval is not None:
        if peak_qval <= 0:
            raise PreflightException(
                "Invalid value for {}: {}.  Q-value must be larger than 0.".format(
                    cmd_str, peak_qval
                )
            )
        if peak_qval > 0.25:
            raise PreflightException(
                "Invalid value for {}: {}.  Q-value must be less than 0.25.".format(
                    cmd_str, peak_qval
                )
            )


def check_force_cells(force_cells, ulimit=20000):
    """check if force cells is correctly specified"""
    if force_cells is not None:
        if not force_cells or not hasattr(force_cells, "items"):
            raise PreflightException("force_cells must be a non-empty dictionary.")

        for key, value in force_cells.items():
            if not isinstance(value, Number) or value < 1 or value > ulimit:
                msg = "force-cells[{}] must be a positive integer <= 20000: " "{}".format(
                    key, value
                )
                raise PreflightException(msg)


def check_sample_id(sample_id):
    """check sample id is correctly formatted"""
    if sample_id is not None:
        if not re.match(r"^[\w-]+$", sample_id):
            msg = (
                "Sample name may only contain letters, numbers, "
                "underscores, and dashes: {}".format(sample_id)
            )
            raise PreflightException(msg)
        if len(sample_id) > 64:
            raise PreflightException("`sample_id` must be <= 64 characters")


def exists_and_readable(fname, key, isfile=True):
    """check if file exists and is readable"""
    if not os.path.exists(fname):
        raise PreflightException("Specified {} file: {} does not exist".format(key, fname))
    if isfile and not os.path.isfile(fname):
        raise PreflightException("Specified {} file: {} is not a valid file".format(key, fname))
    if not os.access(fname, os.R_OK):
        raise PreflightException("{} is not readable, please check file permissions".format(fname))
    if not os.path.getsize(fname) > 0:
        raise PreflightException("{} is empty".format(fname))


def check_singlecell_format(fname, species_list, whitelist=None, allow_multi_gem_groups=True):
    """check if the singlecell.csv file has at minimum:
    the barcodes column with barcodes on the whitelist
    and is_{}_cell_barcode columns with right value type and ranges"""
    exists_and_readable(fname, "singlecell")
    singlecell_df = pd.read_csv(ensure_str(fname), sep=",")
    if singlecell_df.isnull().values.any():
        msg = (
            "Nans are not acceptable values in {}. "
            "This file is likely ill-formatted, with missing or empty entries "
            "in some columns for some row(s)".format(fname)
        )
        raise PreflightException(msg)

    # expect barcodes
    if "barcode" not in singlecell_df.columns:
        raise PreflightException("barcode is a required column in {}".format(fname))
    elif singlecell_df["barcode"].dtype != "O":
        raise PreflightException("barcode column has keys of unexpected type in {}".format(fname))

    # barcodes should be on whitelist
    if whitelist is not None:
        assert isinstance(whitelist, set)

    gemgroups = set()

    for raw_barcode in singlecell_df["barcode"]:
        if raw_barcode == ensure_str(NO_BARCODE):
            continue
        barcode, gemgroup = raw_barcode.split("-")
        gemgroups.add(gemgroup)
        if whitelist is not None and barcode.encode() not in whitelist:
            msg = "Found barcode {} in {} that is not on the barcode " "whitelist".format(
                raw_barcode, fname
            )
            raise PreflightException(msg)
    if not allow_multi_gem_groups and len(gemgroups) > 1:
        msg = (
            "Found barcodes from multiple gem groups in {}. "
            "This data is not currently supported on the pipeline".format(fname)
        )
        raise PreflightException(msg)

    # expect cell annotation columns to match reference
    for species in species_list:
        species_cells = "is_{}_cell_barcode".format(species)
        if species_cells not in singlecell_df.columns:
            msg = (
                "Expecting '{}' column in {}. "
                "This could be because of specifying the "
                "wrong reference.".format(species_cells, fname)
            )
            raise PreflightException(msg)
        if singlecell_df[species_cells].dtype != "int64":
            msg = "{} column has keys of unexpected type in " "{}".format(species_cells, fname)
            raise PreflightException(msg)
        if np.sum(singlecell_df[species_cells] > 1) > 0:
            msg = (
                "{} column has keys of unexpected values in {}. "
                "Allowed values are 0 or 1".format(species_cells, fname)
            )
            raise PreflightException(msg)


def check_aggr_csv(
    aggr_csv: str, reference_path: str, cursory: bool = False
) -> Optional[Tuple[str, bool]]:
    """Check aggr csv has correct columns, then progressively stronger checks on
    duplicates and formating of files. These stronger checks are enabled by default,
    unless you want to test the basic minimum, for example in reanalyzer"""
    contig_manager = ReferenceManager(reference_path)

    exists_and_readable(aggr_csv, "aggr_csv")
    nlibs, library_info, msg = atac_csv.parse_aggr_csv(
        aggr_csv, check_cols=[AGG_ID_FIELD] if cursory else None
    )

    if msg is not None:
        raise PreflightException(msg)

    if nlibs == 0:
        msg = "aggregation csv does not include any library. Provide at least two libraries."
        raise PreflightException(msg)

    has_batch_column = AGG_BATCH_FIELD in library_info[1]

    if cursory:
        return None

    # avoid aggr of duplicate files (assessed by filename).
    species_list = contig_manager.list_species()
    keys_to_check = set(atac_csv.ATAC_AGGR_COLUMNS).intersection(library_info[1].keys())
    for aggr_key in keys_to_check:
        files = set()
        for lib_id in library_info:
            fname = library_info[lib_id][aggr_key]
            if fname in files:
                msg = "File {} already specified for a different library " "under {}".format(
                    fname, aggr_key
                )
                raise PreflightException(msg)
            files.add(fname)

            if aggr_key == atac_csv.AGG_CELL_FIELD:
                check_singlecell_format(fname, species_list, allow_multi_gem_groups=False)

            if aggr_key == "peaks":
                raise PreflightException(
                    "Specify a custom peak file using --peaks. Adding a `peaks`"
                    "column in the aggr csv is no longer supported."
                )

            if aggr_key == atac_csv.AGG_FRAGMENT_FIELD:
                validate_fragments_file(fname, contig_manager)

    return (
        "Aggregator should be run on more than one library" if nlibs == 1 else "",
        has_batch_column,
    )


def validate_fragments_file(fname, contig_manager):
    """
    Validates the fragments file - checks that the file:
    * exists and is readable
    * contains fragments mapped to the reference
    * correctly maps to the reference contigs
    * contains only species in the reference
    * contains no more than 1 gem group
    """
    contig_lens = contig_manager.get_contig_lengths()  # type: Dict[str, int]
    species_list = contig_manager.list_species()  # type: List[str]
    observed_gem_groups = set()
    observed_species = set()
    exists_and_readable(fname, atac_csv.AGG_FRAGMENT_FIELD)
    fragments_scanned = 0
    for chrom, start, stop, barcode, _ in open_fragment_file(fname):
        if fragments_scanned >= FRAGMENTS_SCAN_SIZE:
            break
        chrom = ensure_str(chrom)
        species = chrom.split("_")
        observed_species.add(species[0] if species[0] != chrom else "")
        observed_gem_groups.add(barcode.split(b"-")[1])
        if chrom not in contig_lens:
            msg = "fragment {}:{}-{} in {} is mapped to a contig not in " "the reference".format(
                chrom, start, stop, fname
            )
            raise PreflightException(msg)
        if stop > contig_lens[chrom]:
            msg = "fragment {}:{}-{} boundaries exceed contig size " "({} bp)".format(
                chrom, start, stop, contig_lens[chrom]
            )
            raise PreflightException(msg)
        fragments_scanned += 1
    for species in observed_species:
        if species not in species_list:
            msg = (
                "{} contains fragments mapped to species not "
                "recognized in the reference".format(fname)
            )
            raise PreflightException(msg)
    if len(observed_gem_groups) > 1:
        msg = (
            "multiple gem groups present in {}, likely generated in a "
            "previous aggregation run".format(fname)
        )
        raise PreflightException(msg)


def check_filehandle_limit():
    """checks file handles"""
    ok, msg = tk_preflight.check_open_fh()
    if not ok:
        raise PreflightException(msg)


def check_factorization(factorization: str, batch_correction: bool = False) -> None:
    """checks if factorization is valid"""
    if factorization is not None:
        if not factorization:
            raise PreflightException("factorization must be a non-empty list.")
        if not all(elem in ALLOWED_FACTORIZATIONS for elem in factorization):
            raise PreflightException(
                "Unsupported factorization provided. Options are {}.".format(
                    ", ".join(ALLOWED_FACTORIZATIONS)
                )
            )
        if batch_correction and (
            not all(elem in ALLOWED_FACTORIZATIONS_FOR_BATCH_CORRECTION for elem in factorization)
        ):
            msg = (
                "At least one of the provided factorizations are not "
                "compatibile with chemistry batch correction. Options are {}.".format(
                    ", ".join(ALLOWED_FACTORIZATIONS_FOR_BATCH_CORRECTION)
                )
            )
            raise PreflightException(msg)


def check_vmem_for_reference(ref):
    """Finds out vmem required to load a genome reference fully"""
    refpath = os.path.join(ref, "fasta", "genome.fa")
    if not os.path.exists(refpath):
        hostname = socket.gethostname()
        msg = (
            "Your reference does not contain the expected files, "
            "or they are not readable. Please check your reference "
            "folder on {}.".format(hostname)
        )
        raise PreflightException(msg)
    refsize = os.path.getsize(refpath) // 1e9
    vmem_gb = int(np.ceil(refsize)) + 4
    return vmem_gb


def contain_three_columns(in_file, check_top_n=100):
    """Checks that the first n lines of the input file contain exactly 3 columns"""
    header = count_header_lines_bed(in_file)
    with open(in_file) as bediter:
        for i, row in enumerate(bediter):
            if i < header:
                continue
            fields = row.strip().split("\t")
            if len(fields) != 3:
                raise BedFormatError(
                    in_file,
                    "Invalid BED entry at line {}:\n{}\n"
                    "Peak input file must contain tab-separated fields and contain only 3 "
                    "columns".format(i + 1, repr(row)),
                )
            for j in [1, 2]:
                try:
                    int(fields[j])
                except ValueError as err:
                    raise BedFormatError(
                        in_file,
                        "Invalid BED entry at line {} in column {}:\n{}\n{}\n".format(
                            i + 1, j + 1, repr(row), err
                        ),
                    ) from err

            if check_top_n is not None and i > check_top_n + header:
                break


def check_row_count(in_file, min_rows=10):
    """Checks that at least min_rows non-header rows are present in the file"""
    header = count_header_lines_bed(in_file)
    with open(in_file) as bediter:
        total_lines = 0
        for _ in bediter:
            total_lines += 1
            if (total_lines - header) > min_rows:
                return
    raise PreflightException(
        "Only {} peaks found in file, greater than {} peaks must be provided.".format(
            total_lines - header, min_rows
        )
    )


def check_input_peaks_bed(peaks, contig_manager):
    """Check that the supplied peaks file is a valid BED file with three columns and all regions
    are non-overlapping"""
    if not peaks:
        raise IOError("peaks file not provided")
    try:
        exists_and_readable(peaks, "peaks")
        contain_three_columns(peaks, check_top_n=None)
        bed_format_checker(peaks, contig_manager.fasta_index, duplicate_rows_check=True)
        check_row_count(peaks, min_rows=10)
    except BedFormatError as error:
        raise PreflightException("{}".format(error)) from error
    except PreflightException as error:
        raise PreflightException(
            "Error while parsing BED file {}\n{}".format(peaks, error)
        ) from error
    not_ok, msg = is_overlapping(peaks)
    if not_ok:
        raise PreflightException(msg)


def check_subsample_rate(subsample_rate):
    """check whether downsample specification is valid"""

    if subsample_rate is not None:
        bad_value = False
        try:
            float(subsample_rate)
            bad_value = subsample_rate < 1e-12 or subsample_rate > 1.0
        except ValueError:
            bad_value = True
        if bad_value:
            raise PreflightException("Parameter `subsample_rate` must be a float > 0. and <= 1.0")


class FastqError(PreflightException):
    """Errors relating to improperly specified fastqs"""

    def __init__(self, info, msg):
        host = socket.gethostname()
        full_msg = [
            "Error processing ATAC FASTQ files with specified parameters on machine {}:".format(
                host
            )
        ]
        for key, value in info.items():
            full_msg.append("{} = {}".format(key, value))
        full_msg.append(msg)

        super().__init__("\n".join(full_msg))


def check_read_path(read_path, info):
    """Raise an exception if read_path is not an accessible directory of FASTQ files"""
    if not read_path:
        raise FastqError(
            info,
            "Must specify a read_path containing FASTQs in each entry of 'sample_def' argument",
        )
    if not read_path.startswith("/"):
        raise FastqError(info, "Specified FASTQ folder must be an absolute path")
    if not os.path.exists(read_path):
        raise FastqError(info, "Specified FASTQ folder does not exist")
    if os.path.isfile(read_path):
        raise FastqError(info, "Specified FASTQ folder is actually a file")
    if not os.access(read_path, os.X_OK):
        raise FastqError(
            info,
            "We do not have permissions to open specified FASTQ folder",
        )
    if not os.listdir(read_path):
        raise FastqError(info, "Specified FASTQ folder is empty")


def check_barcode_length(bc_files):
    """Check that the first record of each FASTQ file in the list is at least 16 bases"""
    for fname in bc_files:
        with open_maybe_gzip(fname) as fin:
            info = {"FASTQ file": fname}
            length = None
            try:
                for _, seq, _ in tk_fasta.read_generator_fastq(fin, paired_end=False):
                    length = len(seq)
                    break
            except Exception as error:  # pylint: disable=broad-except
                raise FastqError(
                    info, "Unable to open FASTQ file for reading: {}".format(error)
                ) from error
            if length is None:
                raise FastqError(info, "FASTQ file is empty")
            info["I2 read length"] = length

            if length < 16:
                raise FastqError(
                    info,
                    "The I2 read must be at least 16 bases long. This could be caused by "
                    "ATAC and Gene Expression flow cells having been swapped.",
                )


# pylint: disable=too-many-locals
def check_fastq_input(sample_defs, on_cluster=False):
    """check fastq validity from input sample definition"""

    # FASTQ input (sample_def)
    ok, msg = tk_preflight.check_gem_groups(sample_defs)
    if not ok:
        raise FastqError({}, msg)

    for idx, sample_def in enumerate(sample_defs):  # pylint: disable=too-many-nested-blocks
        read_path = sample_def.get("read_path")
        library_id = sample_def.get("library_id")
        lanes = sample_def["lanes"]

        info = {
            "FASTQ path": read_path,
            "Library ID": library_id,
            "Lanes": lanes,
        }

        check_read_path(read_path, info)

        if library_id is not None:
            if not re.match(r"^[\w-]+$", library_id):
                raise FastqError(
                    info,
                    "Library name may only contain letters, numbers, underscores, and dashes",
                )

        if lanes is not None:
            for lane in lanes:
                if not tk_preflight.is_int(lane):
                    raise FastqError(
                        info,
                        "Lanes must be a comma-separated list of numbers.",
                    )

        bc_files = []

        fastq_mode = sample_def.get("fastq_mode")
        if fastq_mode == "BCL_PROCESSOR":
            sample_indices, msg = tk_preflight.check_sample_indices(sample_def)
            if sample_indices is None:
                raise FastqError(info, "Error processing sample indices: {}".format(msg))

            info["Sample Indices"] = sample_indices
            find_func = tk_fasta.find_input_fastq_files_10x_preprocess
            reads = []
            for sample_index in sample_indices:
                # process interleaved reads
                reads.extend(find_func(read_path, "RA", sample_index, lanes))
                bc_files.extend(find_func(read_path, "I2", sample_index, lanes))
            if len(reads) == 0:
                raise FastqError(
                    info,
                    "No input FASTQs were found for the requested parameters.",
                )
            if len(bc_files) == 0:
                raise FastqError(
                    info,
                    "Unable to find any 10x barcode I5 read files for sample indices {}.".format(
                        sample_indices
                    ),
                )
        elif fastq_mode == "ILMN_BCL2FASTQ":
            sample_names = sample_def.get("sample_names", None)
            if sample_names is None:
                raise FastqError(
                    info,
                    "Entry {} in sample_def missing required field: sample_names".format(idx + 1),
                )
            info["Sample Names"] = sample_names
            find_func = tk_fasta.find_input_fastq_files_bcl2fastq_demult
            si_reads = []
            read1s = []
            for sample_name in sample_names:
                try:
                    fastq = atac.fastq.find_bcl2fastq_files(read_path, sample_name, lanes)
                    bc_files.extend(fastq.i2)
                    si_reads.extend(fastq.i1)
                    read1s.extend(fastq.read1)

                    # we are allowed to not have I2 reads as long as the barcode information is present
                    # in the R1/R2 FASTQ header
                    if not fastq.i2:
                        if len(fastq.read1) != len(fastq.read2):
                            msg = [
                                "We do not have an equal number of files for read1 and read2.",
                                "read 1         = {}".format(
                                    ensure_str(
                                        b", ".join([os.path.basename(x) for x in fastq.read1])
                                    )
                                ),
                                "read 2         = {}".format(
                                    ensure_str(
                                        b", ".join([os.path.basename(x) for x in fastq.read2])
                                    )
                                ),
                            ]
                            raise FastqError(info, "\n".join(msg))
                    elif not len(fastq.read1) == len(fastq.read2) == len(fastq.i2):
                        msg = [
                            "We do not have an equal number of files for read1, read2 and I5.",
                            "read 1         = {}".format(
                                ensure_str(b", ".join([os.path.basename(x) for x in fastq.read1]))
                            ),
                            "read 2         = {}".format(
                                ensure_str(b", ".join([os.path.basename(x) for x in fastq.read2]))
                            ),
                            "10x barcode I5 = {}".format(
                                ensure_str(b", ".join([os.path.basename(x) for x in fastq.i2]))
                            ),
                        ]
                        raise FastqError(info, "\n".join(msg))
                    if len(fastq.read1) == 0:
                        raise FastqError(
                            info,
                            "No input FASTQs were found for sample name '{}'".format(sample_name),
                        )
                except atac.fastq.FastqError as err:
                    raise FastqError(info, str(err)) from err

            if len(si_reads) and len(si_reads) != len(read1s):
                raise FastqError(
                    info,
                    "Sample index (I7) reads are present for some sample names and not others. "
                    "There are {} read 1 files and {} sample index files".format(
                        len(read1s), len(si_reads)
                    ),
                )
        else:
            raise PreflightException(
                "Unsupported fastq_mode. Options are ILMN_BCL2FASTQ and BCL_PROCESSOR, provided: {}".format(
                    fastq_mode
                )
            )

        if on_cluster:
            check_barcode_length(bc_files)
