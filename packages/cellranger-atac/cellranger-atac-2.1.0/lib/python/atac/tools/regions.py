# Copyright (c) 2020 10x Genomics, Inc. All rights reserved.
"""
Data structure and functions for region-based file manipulation,
such ad BED, GFF and GTF.
Originally copied from tenkit and simplified.
"""
from __future__ import absolute_import, division, print_function

import bisect
import os
import tempfile
import gzip

from six import ensure_str
from pybedtools import BedTool, MalformedBedLineError
from atac.tools.ref_manager import ReferenceManager

from typing import (
    AnyStr,
    BinaryIO,
    Dict,
    Generator,
    List,
    NamedTuple,
    Optional,
    TYPE_CHECKING,
)

_Region = NamedTuple(
    "Region", [("start", int), ("end", int), ("name", Optional[bytes]), ("strand", Optional[bytes])]
)

if TYPE_CHECKING:
    from pybedtools import Interval


class BedFormatError(Exception):
    """Use when a .bed file is not in the correct format"""

    def __init__(self, filename, msg):
        super(BedFormatError, self).__init__(
            "Error while parsing BED file {}\n{}".format(filename, msg)
        )


class Region(_Region):
    """Overloads a namedtuple to add a few additional methods."""

    def __new__(cls, start: int, end: int, name: Optional[bytes], strand: Optional[bytes]):
        assert isinstance(start, int)
        assert isinstance(end, int)
        assert name is None or isinstance(name, bytes)
        assert strand is None or isinstance(strand, bytes)
        return super(Region, cls).__new__(cls, start, end, name, strand)

    @property
    def width(self):
        # type: () -> int
        return self.end - self.start - 1

    @property
    def is_positive_strand(self):
        # type: () -> bool
        return not self.strand == b"-"

    def get_relative_position(self, point):
        # type: (int) -> int
        if self.is_positive_strand:
            return (point - self.start) - self.width // 2
        else:
            return (self.end - point) - self.width // 2


class Regions:
    """Defines a set of regions.

    Allows for determining whether a point is contained in any of those regions
    or whether another region overlaps any of them.
    """

    def __init__(self, region_list=None):
        # type: (Optional[List[Region]]) -> None
        if region_list:
            sorted_regions = sorted(region_list)  # type: List[Region]
            self.starts = [r[0] for r in sorted_regions]  # type: List[int]
            self.ends = [r[1] for r in sorted_regions]  # type: List[int]
            self.region_list = sorted_regions
        else:
            self.starts = []  # type: List[int]
            self.ends = []  # type: List[int]
            self.region_list = []  # type: List[Region]
        self.current_iter = 0

    def __iter__(self):  # pylint: disable=non-iterator-returned
        return self

    def __next__(self):
        # type: () -> Region
        if self.current_iter >= len(self.region_list):
            self.current_iter = 0
            raise StopIteration
        else:
            self.current_iter += 1
            return self.region_list[self.current_iter]

    def __len__(self):
        return len(self.region_list)

    def contains_point(self, point):
        # type: (int) -> bool
        """Determines whether a point is contained in one of the regions."""
        index = bisect.bisect(self.starts, point) - 1
        return index >= 0 and point >= self.starts[index] and point < self.ends[index]

    def get_region_containing_point(self, point):
        # type: (int) -> Optional[Region]
        """Returns a region containing the point, if one exists."""
        index = bisect.bisect(self.starts, point) - 1
        if index == -1:
            return None

        if point >= self.starts[index] and point < self.ends[index]:
            return self.region_list[index]
        return None

    def get_nearest_distance(self, point):
        # type: (int) -> Optional[int]
        """Finds the closest region to a point and returns the distance to it.
        Points inside a region return a distance of 0.
        """
        index = bisect.bisect(self.starts, point)
        # This index partitions the starts so that all starts in [:index] are
        # less than the point and all starts in [index:] are greater.

        # Check if it's inside a region
        if index > 0 and point >= self.starts[index - 1] and point <= self.ends[index - 1]:
            return 0

        # Check the left region distance
        if index > 0 and point >= self.starts[index - 1]:
            dist_left = point - self.ends[index - 1]
        else:
            dist_left = self.ends[-1]

        # Check the right region distance and return the min
        if index < len(self):
            dist_right = self.starts[index] - point
        else:
            dist_right = self.ends[-1]

        return min(dist_left, dist_right)

    def overlaps_region(self, start, end):
        # type: (int, int) -> bool
        """Determines whether a region described by start and end overlaps
        any of the regions.
        """
        check_index = bisect.bisect_left(self.ends, start)
        if check_index == len(self.starts):
            return False
        if end > self.starts[check_index]:
            return True
        return False


def get_target_regions_dict(targets_file, padding=0):
    # type: (BinaryIO, int) -> Dict[bytes, List[Region]]
    """Gets the target regions from a targets BED file as a chrom-indexed dictionary,
    with every entry given as a list of (start, end, name, strand) tuples
    """
    targets = {}  # type: Dict[bytes, List[Region]]
    for line in targets_file:
        info = line.strip().split(b"\t")
        if any(
            line.startswith(comment)
            for comment in (b"browser", b"track", b"-browser", b"-track", b"#")
        ):
            continue
        if len(line.strip()) == 0:
            continue

        chrom = info[0]
        start = int(info[1]) - padding
        end = int(info[2]) + padding
        name = info[3] if len(info) >= 4 else None
        strand = info[5] if len(info) >= 6 else None
        if strand not in (b"+", b"-"):
            strand = None
        chrom_targs = targets.setdefault(chrom, [])
        chrom_targs.append(Region(start, end, name, strand))
    return targets


def get_target_regions(targets_file, padding=0):
    # type: (BinaryIO, int) -> Dict[bytes, Regions]
    """Gets the target regions from a targets file as a chrom-indexed dictionary,
    with every entry given as a list of Regions objects
    """
    targets_dict = get_target_regions_dict(targets_file, padding)
    target_regions = {}
    for (chrom, region_list) in targets_dict.items():
        target_regions[chrom] = Regions(region_list=region_list)
    return target_regions


def fragment_overlaps_target(contig, start, stop, target_regions):
    # type: (bytes, int, int, Optional[Dict[bytes, Regions]]) -> bool
    if target_regions is None:
        return False
    if contig not in target_regions:
        return False
    return target_regions[contig].overlaps_region(start, stop)


def fragment_distance_to_target(contig, start, stop, target_regions):
    if target_regions is None:
        return 0
    if contig not in target_regions:
        return 0
    dist = target_regions[contig].get_nearest_distance(start)
    if dist == 0:
        return dist
    return min(dist, target_regions[contig].get_nearest_distance(stop))


def is_overlapping(in_peaks_file):
    """Given a sorted bed file of peaks, check if the peaks are non overlapping"""

    assert in_peaks_file is not None
    last_peak = None
    for bed in bed_format_checker_iter(in_peaks_file):
        if last_peak is not None and bed.chrom == last_peak.chrom:
            if bed.start < last_peak.end:

                bedtools_cmd = "path/to/{} pass bedtools merge -i {} > {}".format(
                    os.getenv("TENX_PRODUCT", "cellranger-arc"), in_peaks_file, "[output peak file]"
                )
                msg1 = "{} contains overlapping peak regions.\n".format(in_peaks_file)
                msg2 = "Run the following command to remove overlapping peaks: \n\t{}\n".format(
                    bedtools_cmd
                )
                return True, msg1 + msg2
        last_peak = bed

    return False, None


def bed_format_checker_iter(in_bed_file):
    # type: (AnyStr) -> Generator[Interval, None, None]
    """
    :param in_bed_file:  a file path of a BED/GTF/GFF file
    :return: a generator with BedTool built-in format checker
    """

    bediter = iter(BedTool(ensure_str(in_bed_file)))
    i = count_header_lines_bed(in_bed_file)

    while True:
        try:
            # Loop until this raises StopIteration
            try:
                row = next(bediter)  # type: Interval
            except StopIteration:
                return
            i += 1
            yield row
        except (MalformedBedLineError, IndexError) as e:
            raise BedFormatError(in_bed_file, "Malformed BED entry at line {}:\n{}".format(i, e))


def bed_format_checker(in_bed_file, faidx_file, duplicate_rows_check=False):
    """
    Given a bed file path, examine whether is it a valid bed format
    Examined items include: basic formatting, coordinate overflow, contig name is
    within reference and properly sorted
    :param in_bed_file: file path of input bed file
    :param faidx_file: fasta index file (.fai) file.
    :return:
    """

    def format_msg(line, row, msg):
        return "Malformed BED entry at line {line}:\nLine = {row}\n{msg}".format(**locals())

    if in_bed_file is None:
        return None
    in_bed_file = ensure_str(in_bed_file)

    all_contigs = []  # type: List[str]
    contig_lengths = {}  # Dict[str, int]
    contig_index = {}  # Dict[str, int]
    with open(faidx_file) as faidx:
        for i, line in enumerate(faidx):
            line = line.strip().split()
            contig_name, contig_length = line[0], line[1]

            all_contigs.append(contig_name)
            contig_index[contig_name] = i
            contig_lengths[contig_name] = int(contig_length)

    bediter = bed_format_checker_iter(in_bed_file)

    i = count_header_lines_bed(in_bed_file)

    # Here we assume the fasta index file is located in {ref}/fasta/genome.fa.fai
    ref_dir = os.path.dirname(os.path.dirname(faidx_file))

    prev_contig_index = None
    prev_bed = (None, None)
    for bed in bediter:
        i += 1
        coord = (bed.chrom, bed.start)
        if prev_bed[0] is not None:
            if duplicate_rows_check and prev_bed == coord:
                raise BedFormatError(in_bed_file, format_msg(i, bed, "Duplicate entry found"))
            if prev_bed[0] == coord[0] and prev_bed > coord:
                raise BedFormatError(in_bed_file, format_msg(i, bed, "BED file is not sorted"))
        prev_bed = coord
        if bed.chrom not in contig_lengths:
            raise BedFormatError(
                in_bed_file,
                format_msg(
                    i,
                    bed,
                    "contig name {} is not found in the reference package {}".format(
                        bed.chrom, ref_dir
                    ),
                ),
            )

        if bed.end > contig_lengths[bed.chrom]:
            raise BedFormatError(
                in_bed_file,
                format_msg(
                    i,
                    bed,
                    "End coordinate {} exceeds the length of contig {} = {}".format(
                        bed.end, bed.chrom, contig_lengths[bed.chrom]
                    ),
                ),
            )

        this_contig_index = contig_index[bed.chrom]
        if prev_contig_index is not None:
            if this_contig_index < prev_contig_index:
                raise BedFormatError(
                    in_bed_file,
                    format_msg(
                        i,
                        bed,
                        "Contigs are not sorted in the same order as the FASTA file in the "
                        "reference package {}".format(ref_dir),
                    ),
                )
        prev_contig_index = this_contig_index


def sort_and_uniq_bed(in_bed_file, ref_path, header=None):
    """
    given a path of a bed file, sort bed file and remove duplicate rows
    ref_path is required to extract the chromosome orders from the genome.fa.fai.

    The output bed file is appended with a commented header section derived from
    the input reference and can also inlcude additional data when provided with a
    header argument.
    """

    # test whether file is gzipped
    try:
        with gzip.open(in_bed_file, "r") as fin:
            fin.read(1)
        raise BedFormatError(
            in_bed_file, "File is gzip-compressed. Please decompress and try again."
        )
    except:
        pass

    # chrom are sorted numerically
    faidx_file = os.path.join(ref_path, "fasta", "genome.fa.fai")
    assert os.path.exists(
        faidx_file
    ), "Reference package {} is missing FASTA index expected to be at {}".format(
        ref_path, faidx_file
    )

    input_bed = BedTool(in_bed_file)
    ref = ReferenceManager(ref_path)

    with tempfile.NamedTemporaryFile(dir=os.path.dirname(in_bed_file), delete=False) as tmp:
        # pylint: disable=unexpected-keyword-arg
        # N.B. Bedtools methods are unavailable for introspection by the linter
        sorted_bed = input_bed.sort(g=faidx_file).saveas(tmp.name)

        # remove duplicate lines and write to the original input bed file
        with open(sorted_bed.fn, "rb") as infile, open(in_bed_file, "wb") as outfile:
            if header is not None:
                outfile.writelines(item for item in header)
                outfile.write(b"#\n")
            outfile.writelines(item for item in ref.get_header_string)
            outfile.flush()
            lastline = None
            for line in infile:
                if line != lastline:
                    outfile.write(line)
                lastline = line

        # clean up temporary files
        os.remove(tmp.name)


def count_header_lines_bed(in_file):
    """Count the number of header lines in a bed file
    Header lines are defined as lines starting with "#" in the beginning of the file
    Comment lines with "#" that not in the header will not be counted
    """

    counter = 0
    with open(in_file, "rb") as file_in:
        for line in file_in:
            if line.startswith(b"#"):
                counter += 1
            else:
                break

    return counter


def get_peak_annotation_dict(peak_anno_file):
    # type: (bytes) -> Optional[Dict[str, str]]
    """
    parse peak annotation to a dict with key as the genome coordinate
    an input row of
        chr1_1304723_1311624 ACAP3;INTS11;PUSL1 0;0;0 promoter;distal;promoter
    will be parsed as
    peak_anno["chr1:1304723-1311624"] = "ACAP3_promoter;INTS11_distal;PUSL1_promoter"
    """

    if peak_anno_file is None:
        return None
    peak_anno = {}  # Dict[str, str]
    with open(peak_anno_file) as pa_file:
        for i, line in enumerate(pa_file):

            # skip the header line
            if i == 0:
                continue

            # ignore the distance column
            chrom, start, end, gene, _, peaktype = line.strip().split("\t")
            peak_id = "{}:{}-{}".format(chrom, start, end)
            gene_and_type = "{}_{}".format(gene, peaktype)

            if peak_id in peak_anno:
                peak_anno[peak_id] = "{};{}".format(peak_anno[peak_id], gene_and_type)
            else:
                peak_anno[peak_id] = gene_and_type

    return peak_anno
