# Copyright (c) 2019 10x Genomics, Inc. All rights reserved.
"""
This implements reference "metadata" operations for single species and barnyard references.
The names for the contigs in the reference are formated as either: "chr<XXX>" or
"<species>_chr<XXX>".
This operates on a file, "contigs.json" found in the reference fasta directory that describes
the layout.
"""

from __future__ import absolute_import, division

import json
import os.path
from typing import Dict, List, Optional
from cellranger.constants import REFERENCE_METADATA_FILE

REF_TYPE_ATAC = "atac"
REF_TYPE_ATAC_GEX = "atac_gex"


class ReferenceManager:
    """Class that represents reference metadata. Note that all strings are of type `str`"""

    def __init__(self, path: str):
        self.path = path

        # Load reference metadata
        if self.metadata_file:
            with open(self.metadata_file, "r") as infile:
                self.metadata = json.load(infile)
        else:
            self.metadata = None

        # Load contigs defs
        # Check contigs is a valid JSON first before loading
        try:
            with open(self.contig_definitions, "r") as infile:
                self.contigs = json.load(infile)
                if (
                    "non_nuclear_contigs" in self.contigs
                    and self.contigs["non_nuclear_contigs"] is None
                ):
                    self.contigs["non_nuclear_contigs"] = []

        except ValueError as e:
            raise Exception("Malformed json in '%s': %s" % (self.contig_definitions, e))

        self.all_contigs = []  # type: List[str]
        self.contig_lengths = {}  # type: Dict[str, int]
        with open(self.fasta_index, "r") as infile:
            for line in infile:
                line = line.strip().split()
                name, length = line[0], line[1]
                self.all_contigs.append(name)
                self.contig_lengths[name] = int(length)

    def _filepath_or_error(self, subpaths, err_msg):
        filepath = os.path.join(self.path, *subpaths)
        if not os.path.exists(filepath):
            raise IOError(err_msg)
        return filepath

    def _filepath_or_none(self, subpaths):
        filepath = os.path.join(self.path, *subpaths)
        return filepath if os.path.exists(filepath) else None

    @property
    def metadata_file(self) -> Optional[str]:
        if self._filepath_or_none([REFERENCE_METADATA_FILE]):
            return self._filepath_or_none([REFERENCE_METADATA_FILE])
        else:
            return self._filepath_or_none(["metadata.json"])

    @property
    def genome(self) -> str:
        if self.metadata and "genomes" in self.metadata:
            return "_and_".join(self.metadata["genomes"])
        else:
            filepath = self._filepath_or_error(["genome"], "Reference is missing genome name file")
            with open(filepath) as infile:
                return infile.readline().strip()

    @property
    def contig_definitions(self) -> Optional[str]:
        try:
            return self._filepath_or_error(
                ["fasta", "contig-defs.json"], "Reference is missing contig definition file"
            )
        except IOError:
            return self.metadata_file

    @property
    def fasta(self):
        return self._filepath_or_error(
            ["fasta", "genome.fa"], "Reference is missing genome fasta sequence"
        )

    @property
    def fasta_index(self):
        return self._filepath_or_error(
            ["fasta", "genome.fa.fai"], "Reference is missing fasta index file"
        )

    @property
    def genes(self):
        try:
            return self._filepath_or_error(
                ["genes", "genes.gtf"], "Reference is missing genes.gtf file"
            )
        except IOError:
            return self._filepath_or_error(
                ["genes", "genes.gtf.gz"], "Reference is missing genes.gtf[.gz] file"
            )

    @property
    def motifs(self):
        return self._filepath_or_none(["regions", "motifs.pfm"])

    @property
    def blacklist_track(self):
        return self._filepath_or_none(["regions", "blacklist.bed"])

    @property
    def ctcf_track(self):
        return self._filepath_or_none(["regions", "ctcf.bed"])

    @property
    def dnase_track(self):
        return self._filepath_or_none(["regions", "dnase.bed"])

    @property
    def enhancer_track(self):
        return self._filepath_or_none(["regions", "enhancer.bed"])

    @property
    def promoter_track(self):
        return self._filepath_or_none(["regions", "promoter.bed"])

    @property
    def tss_track(self):
        return self._filepath_or_none(["regions", "tss.bed"])

    @property
    def transcripts_track(self):
        return self._filepath_or_none(["regions", "transcripts.bed"])

    @property
    def all_bed_files(self):
        fpaths = [
            self.tss_track,
            self.transcripts_track,
            self.ctcf_track,
            self.blacklist_track,
            self.dnase_track,
            self.enhancer_track,
            self.promoter_track,
        ]
        return [path for path in fpaths if path]

    @property
    def get_header_string(self):
        header = []
        header.append("# reference_path={}\n".format(self.path).encode())
        if self.metadata is not None:
            header.append(
                "# reference_fasta_hash={}\n".format(self.metadata.get("fasta_hash", "")).encode()
            )
            header.append(
                "# reference_gtf_hash={}\n".format(
                    self.metadata.get("gtf_hash") or self.metadata.get("gtf_hash.gz", "")
                ).encode()
            )
            header.append(
                "# reference_version={}\n".format(self.metadata.get("version", "")).encode()
            )
            header.append(
                "# mkref_version={}\n".format(self.metadata.get("mkref_version", "")).encode()
            )
        else:
            header.append(b"# reference_fasta_hash=\n")
            header.append(b"# reference_gtf_hash=\n")
            header.append(b"# reference_version=\n")
            header.append(b"# mkref_version=\n")
        header.append(b"#\n")

        for contig in self.primary_contigs():
            header.append("# primary_contig={}\n".format(contig).encode())
        return header

    # TODO: make all these functions properties (and update usage)
    def non_nuclear_contigs(self) -> List[str]:
        """Lists non-nuclear contigs (e.g. mitochondria and chloroplasts)"""
        key = "non_nuclear_contigs"
        return self.contigs[key] if key in self.contigs else []

    def get_contig_lengths(self):
        return self.contig_lengths

    def get_vmem_est(self):
        total_contig_len = sum(self.contig_lengths.values())
        return 1.2 * total_contig_len / 1e9

    def primary_contigs(self, species: Optional[str] = None) -> List[str]:
        """List all primary contigs, optionally filter by species."""
        species_prefixes = self.list_species()
        if species is not None:
            assert isinstance(species, str)
            if species not in species_prefixes:
                raise ValueError("Unknown species")
        return [
            x for x in self.contigs["primary_contigs"] if self.is_primary_contig(x, species=species)
        ]

    def list_species(self) -> List[str]:
        """Look for species_prefixes key present in ATAC <= 1.2 references in
        the contig-defs.json

        In newer ARC refences this information is instead in the reference.json
        in the `genomes` key used by cellranger.

        Note however that the `genomes` key is ignored for single-species
        reference and is instead defined as `[""]`
        """
        pres = self.contigs.get("species_prefixes")
        if pres:
            return pres

        pres = self.contigs.get("genomes")
        if pres is None or len(pres) == 1:
            return [""]
        return pres

    def is_primary_contig(self, contig_name: str, species: Optional[str] = None) -> bool:
        assert isinstance(contig_name, str)
        assert species is None or isinstance(species, str)
        if contig_name not in self.contigs["primary_contigs"]:
            return False
        if species is not None and self.species_from_contig(contig_name) != species:
            return False
        return True

    def species_from_contig(self, contig: str) -> str:
        """Given the name of a contig, extract the species."""
        assert isinstance(contig, str)
        species_prefixes = self.list_species()
        if species_prefixes == [""]:
            return ""

        s = contig.split("_")
        if len(s) > 1:
            return s[0]
        else:
            return ""

    def make_chunks(self, max_chunks: int, contigs: Optional[List[str]] = None) -> List[List[str]]:
        """Divide the contigs in a reference into roughly equal-size chunks.

        Note that this is a very simple heuristic and will not always work for various
        combinations of contig sizes and number of chunks. Any empty chunks are dropped.

        Args:
            max_chunks (int): maximum desired number of chunks
            contigs (List[str], optional): Provide a custom list of contigs to partition. If not
            specified, we partition all contigs into chunks. Defaults to None.

        Returns:
            List[List[str]]: A list of chunks, where each chunk is a list of contig names. There
            will be atmost max_chunks chunks.
        """

        assert max_chunks > 0
        if contigs is None:
            contigs = list(self.contig_lengths.keys())
        contigs.sort(key=lambda c: self.contig_lengths[c])
        chunks = [[] for _ in range(max_chunks)]
        index = 0
        reverse = False
        while contigs:
            contig = contigs.pop()
            chunks[index].append(contig)
            index += -1 if reverse else 1
            if not reverse and index == len(chunks):
                reverse = True
                index -= 1
            if reverse and index == -1:
                reverse = False
                index = 0
        return [chunk for chunk in chunks if chunk]

    def verify_contig_defs(self):
        """Verify that the contig defs are correctly formatted"""

        def check_aos(a):
            if not isinstance(a, list):
                return False

            for k in a:
                if not isinstance(k, str):
                    return False
            return True

        # Step 1 does the file exist? -> duplicated functionality in contig_definitions
        if not os.path.exists(self.contig_definitions):
            return "Contig definitions file '%s' is missing" % (self.contig_definitions)

        # Step 2 is it valid JSON? -> implemented before loading during
        # ReferenceManager initiation
        contigs = self.contigs

        # Step 3: species_prefixes must be an array of strings
        species_prefixes = self.list_species()
        if not check_aos(species_prefixes):
            return "Species_prefixes must be an array of strings"
        if len(species_prefixes) == 1 and species_prefixes[0] != "":
            return "Cannot specify species prefix for single species references"

        # Step 4: primary contigs must be an array of strings
        if not check_aos(contigs.get("primary_contigs")):
            return "Primary_contigs must be an array of strings"

        # Step 5: prefix contigs can not be prefixes of themselves, and shouldn't
        # contain underscores
        for p1 in species_prefixes:
            for p2 in species_prefixes:
                if p1 != p2 and p1.startswith(p2):
                    return "Species_prefixes are ambiguous. No prefix may be a prefix of another prefix."
        for p in species_prefixes:
            if "_" in p:
                return "species prefix {} contains and underscore. Underscores are not allowed in species prefixes".format(
                    p
                )

        # step 6: every primary contig and non_nuclear_contigs must be prefixed by a species prefix. Also, every
        # species prefix must be used
        used_prefix = {p: False for p in species_prefixes}
        for c in contigs["primary_contigs"] + contigs["non_nuclear_contigs"]:
            ok = False
            c = str(c)
            for p in species_prefixes:
                if c.startswith(p):
                    used_prefix[p] = True
                    ok = True
                if c == p:
                    return "Species prefix {} matches a contig completely. Species should be a partial prefix to contig".format(
                        p
                    )
            if not ok:
                return "Each primary contig must be prefixed by a species prefix"
        not_used_prefix = [p for p in species_prefixes if not used_prefix[p]]
        if len(not_used_prefix) > 0:
            return (
                "Species prefixes {} are not prefixes for any contigs in the genome fasta".format(
                    ",".join(used_prefix)
                )
            )

        # Step 7: every primary contig and non_nuclear_contigs must exist in the
        # reference
        all_fasta_contigs = []

        for line in open(self.fasta_index):
            fields = line.strip().split()
            all_fasta_contigs.append(fields[0])

        for c in contigs["primary_contigs"] + contigs["non_nuclear_contigs"]:
            if c not in (all_fasta_contigs):
                return (
                    "Contig %s is in the config definition but not in the genome fasta. Please check if the reference is properly built."
                    % (c)
                )

        # Step 8: there must be a primary contig
        if len(contigs["primary_contigs"]) == 0:
            return "At least one contig must be primary."

        return None
