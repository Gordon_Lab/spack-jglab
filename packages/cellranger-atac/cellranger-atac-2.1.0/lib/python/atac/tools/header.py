# Copyright (c) 2021 10X Genomics, Inc. All rights reserved.
"""
Read and validate headers of Cell Ranger ATAC/ARC output files
"""
from __future__ import annotations

import re
from cellranger.io import open_maybe_gzip
from atac.tools.ref_manager import ReferenceManager
from atac.preflights import PreflightException


class HeaderException(PreflightException):
    """Exception for all header-related errors"""


class Header:
    """Class to represent information stored in the header of ATAC
    fragments.tsv and peaks.bed file.
    """

    p = re.compile(rb"^#(.*)(=)([^\\\n]*)")

    def __init__(self, path: bytes):
        """Initialize from a path to a fragments.tsv.gz/peaks.bed file

        Args:
            path (bytes): path to file
        """
        self.path = path

        header_dict = {"primary_contigs": []}
        with open_maybe_gzip(path, "rb") as pfile:
            while True:
                line = pfile.readline()
                if not self._check_format(line):
                    raise HeaderException("Malformed header line in {}: {}".format(path, line))
                if self.p.match(line) is not None:
                    key = self.p.match(line).group(1).decode().strip()
                    value = self.p.match(line).group(3).decode().strip()
                    if key == "primary_contig":
                        header_dict["primary_contigs"] += [value]
                    else:
                        header_dict[key] = value
                if not line.startswith(b"#"):
                    break
        self.fasta_hash = header_dict.get("reference_fasta_hash", "")
        self.gtf_hash = header_dict.get("reference_gtf_hash", "")
        self.primary_contigs = header_dict.get("primary_contigs", [])

    def is_absent(self):
        """ARC 1.0 and ATAC <= 1.2 files are missing headers"""
        return not (self.fasta_hash or self.gtf_hash or self.primary_contigs)

    @classmethod
    def _check_format(cls, line: bytes) -> bool:
        """Checks format of header line."""

        if line.startswith(b"#") and len(line.strip()) > 1 and cls.p.match(line) is None:
            return False
        else:
            return True

    def check_file_compatibility(self, other: Header):
        """Check that two headers are compatible

        Args:
            other (Header): other header we are comparing to

        Raises:
            HeaderException: FASTA hash does not agree
            HeaderException: GTF hash does not agree
            HeaderException: if primary contigs don't match
        """

        if self.fasta_hash != other.fasta_hash:
            raise HeaderException(
                "Reference fasta hash of specified input files are not identical:\n"
                "{}\n{}\n".format(self.path, other.path)
            )

        if self.gtf_hash != other.gtf_hash:
            raise HeaderException(
                "Reference gtf hash of specified input files are not identical:\n"
                "{}\n{}".format(self.path, other.path)
            )

        if self.primary_contigs != other.primary_contigs:
            raise HeaderException(
                "Primary contigs specified in the input files are not identical:\n"
                "{}\n{}\n".format(self.path, other.path)
            )

    def check_ref_compatibility(self, ref: ReferenceManager):
        """Check that header is consistent with a reference

        Note that if no header is present as is the case for ATAC 1.2 or ARC 1.0,
        we declare the header as not compatible.

        Args:
            ref (ReferenceManager): ReferenceManager object corresponding to reference

        Raises:
            HeaderException: FASTA hash does not agree
            HeaderException: GTF hash does not agree
            HeaderException: primary contigs do not match
        """
        ref_fasta_hash = ref.metadata["fasta_hash"] if ref.metadata else ""
        ref_gtf_hash = ref.metadata["gtf_hash.gz"] if ref.metadata else ""

        if self.fasta_hash != ref_fasta_hash:
            raise HeaderException(
                "Reference mismatched with input file: FASTA hash of the "
                "specified reference {} is either missing or not identical to the "
                "reference fasta hash in this input file {}".format(ref.path, self.path)
            )

        if self.gtf_hash != ref_gtf_hash:
            raise HeaderException(
                "Reference mismatched with input file: GTF hash of the "
                "specified reference {} is either missing or not identical to the "
                "reference gtf hash in this input file {}".format(ref.path, self.path)
            )

        if self.primary_contigs != ref.primary_contigs():
            raise HeaderException(
                "Reference mismatched with input file: primary contigs "
                "specified in the reference and this input file "
                "are not identical:\n{}\n{}\n".format(ref.path, self.path)
            )
