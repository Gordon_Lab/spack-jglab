# Copyright (c) 2019 10x Genomics, Inc. All rights reserved.

from __future__ import absolute_import, print_function

import os.path
from collections import Counter

from six import ensure_binary
import pysam
import numpy as np

import tenkit.stats as tk_stats
from cellranger.io import open_maybe_gzip

from typing import (
    AnyStr,
    Iterator,
    List,
    Tuple,
    Optional,
)


class FragmentException(Exception):
    """Raise expection if checks in preflight fail"""


def open_fragment_file(filename):
    with open_maybe_gzip(filename, "rb") as fragment_file:
        for line in fragment_file:
            if line.startswith(b"#"):
                continue
            contig, start, stop, barcode, dup_count = line.strip().split(b"\t")
            assert isinstance(contig, bytes)
            assert isinstance(barcode, bytes)
            yield contig, int(start), int(stop), barcode, int(dup_count)


def parsed_fragments_from_region(
    filename: str,
    contig: AnyStr,
    start: Optional[int],
    stop: Optional[int],
    index: Optional[str] = None,
) -> Iterator[Tuple[bytes, int, int, bytes, int]]:
    """Returns an iterator yielding parsed fragments from a region.

    Expects an index file of the same name as the input file, otherwise supply the file
    """
    fragments = pysam.TabixFile(filename, index=index)
    try:
        for fragment_str in fragments.fetch(contig, start, stop):  # type: str
            fragment: bytes = fragment_str.encode()
            # pretty sure this isn't necessary but just in case
            if fragment.startswith(b"#"):
                continue
            contig_bytes, fstart, fstop, barcode, read_count = fragment.split(b"\t")
            yield contig_bytes, int(fstart), int(fstop), barcode, int(read_count)
    except ValueError:  # in the case when no regions on that contig are present
        pass


def grouped_fragments_from_contig(
    contig: AnyStr,
    filename: str,
    allow_multiple_barcodes: bool = False,
    index: Optional[str] = None,
) -> Iterator[List[Tuple[bytes, int, int, bytes, int]]]:
    """Group fragments by start position

    Args:
        contig (AnyStr): contig
        filename (str): fragments file
        allow_multiple_barcodes (bool, optional): Allow multiple barcodes with the same
        (start, end) in the fragments file. Defaults to False.
        index (Optional[str], optional): path to fragments index. Defaults to None.

    Yields:
        List[Tuple[bytes, int, int, bytes, int]]: list of fragments sharing common start
    """
    current_start = None
    fragment_list = []
    prev_footprint = None
    for fragment in parsed_fragments_from_region(filename, contig, None, None, index):
        start = fragment[1]
        end = fragment[2]
        # Decide whether to allow multiple fragments that share the same footprint = (start, end)
        footprint = (start, end)
        if not allow_multiple_barcodes and footprint == prev_footprint:
            continue
        prev_footprint = footprint
        if current_start is None:
            current_start = start
        if start == current_start:
            fragment_list.append(fragment)
        else:
            yield fragment_list
            current_start = start
            fragment_list = [fragment]
    if fragment_list:
        yield fragment_list


def subsample_fragments(
    infile, rate, outfile, cells=set(), group=None, kind=None, outindex=None, key=None, randomseed=0
):
    """Downsample a fragments infile to outfile (and optionally build the tabindex).

    Downsampling can be of 3 kinds:
    - None: simply copies the input
    - unique: downsamples the number of unique fragments
    - total: downsample the total number of fragments

    If provided cell calls, it computes single cell metrics pre- and post-downsampling.
    If provided group, it relabels the barcodes to be from that gem group.
    If provided outindex, it computes a tabix and compresses the downsampled
    file in process.
    If provided key, it adds a extra column about chromosome order, to be used
    in merging multiple files based on a specific order.

    Different bulk library sensitivity metrics are calculated pre- and post-normalization.
    """
    assert kind in [None, "total", "unique"]
    metrics = {
        "total_pre_normalization": 0,
        "total_post_normalization": 0,
        "unique_pre_normalization": 0,
        "unique_post_normalization": 0,
    }

    np.random.seed(randomseed)

    basefile, _ = os.path.splitext(outfile)
    final_outfile = basefile if outindex is not None else outfile

    np.random.seed(0)

    pre_frags_per_cell = Counter({bc: 0 for bc in cells})
    post_frags_per_cell = Counter({bc: 0 for bc in cells})
    with open(final_outfile, "wb") as f:
        for contig, start, stop, barcode, dup_count in open_fragment_file(infile):
            is_cell = False
            if barcode in cells:
                is_cell = True
            if is_cell:
                pre_frags_per_cell[barcode] += 1
            if group is not None:
                barcode_gg = barcode.split(b"-")[0] + b"-" + ensure_binary(str(group))
            metrics["total_pre_normalization"] += dup_count
            metrics["unique_pre_normalization"] += 1
            if kind == "total":
                new_dup_count = sum(np.random.uniform(size=dup_count) < rate)
                if new_dup_count > 0:
                    if key is None:
                        f.write(
                            b"%s\t%d\t%d\t%s\t%d\n"
                            % (contig, start, stop, barcode_gg, new_dup_count)
                        )
                    else:
                        f.write(
                            b"%s\t%d\t%d\t%d\t%s\t%d\n"
                            % (contig, key[contig], start, stop, barcode_gg, new_dup_count)
                        )
                    metrics["total_post_normalization"] += new_dup_count
                    metrics["unique_post_normalization"] += 1
                    if is_cell:
                        post_frags_per_cell[barcode] += 1
            if kind == "unique" or kind is None:
                if kind is None or np.random.uniform(size=1) < rate:
                    if key is None:
                        f.write(
                            b"%s\t%d\t%d\t%s\t%d\n" % (contig, start, stop, barcode_gg, dup_count)
                        )
                    else:
                        f.write(
                            b"%s\t%d\t%d\t%d\t%s\t%d\n"
                            % (contig, key[contig], start, stop, barcode_gg, dup_count)
                        )
                    metrics["total_post_normalization"] += dup_count
                    metrics["unique_post_normalization"] += 1
                    if is_cell:
                        post_frags_per_cell[barcode] += 1

    if len(+pre_frags_per_cell) != len(cells):
        raise FragmentException(
            "Of the specified {} cell barcodes only {} were observed in the fragment file.".format(
                len(cells), len(+pre_frags_per_cell)
            )
        )

    if len(cells) > 0:
        metrics["pre_norm_median_frags_per_cell"] = np.median(list(pre_frags_per_cell.values()))
        metrics["post_norm_median_frags_per_cell"] = np.median(list(post_frags_per_cell.values()))
        metrics["pre_norm_total_frags_per_cell"] = tk_stats.robust_divide(
            metrics["total_pre_normalization"], len(cells)
        )
        metrics["frac_reads_kept"] = rate

    # convert to gz and tabix
    if outindex is not None:
        pysam.tabix_index(basefile, preset="bed", index=outindex)
    return metrics


def estimate_gem_wells(fragments_file: str, contigs: List[str]):
    """Estimate APPROXIMATE number of gem wells from fragments file

    We only parse the first 100 fragments from each contig
    Args:
        fragments_file (AnyStr): path to fragments
        contigs (List[str]): list of contigs to use
    """
    max_frags = 100
    wells = {1}
    for contig in contigs:
        for i, (_, _, _, bc, _) in enumerate(
            parsed_fragments_from_region(fragments_file, contig, None, None, index=None)
        ):
            if i >= max_frags:
                break
            gg = bc.split(b"-")[1]
            wells.add(int(gg))
    return max(wells)
