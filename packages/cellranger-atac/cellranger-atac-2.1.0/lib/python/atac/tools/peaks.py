# Copyright (c) 2019 10x Genomics, Inc. All rights reserved.
"""
Broadly useful tools for peak detection and transposition site identification.
"""
from __future__ import absolute_import, annotations

from typing import Generator, IO, Iterable, List, NamedTuple, Optional, Union

import tenkit.log_subprocess as tk_subproc

Peak = NamedTuple("Peak", [("chrom", str), ("start", int), ("end", int), ("strand", str)])


def peak_reader(peaks, select: Optional[Iterable[int]] = None) -> Generator[Peak, None, None]:
    """Streaming peak reader with selection mode. Accepts ordered indices.

    Args:
        peaks: file path to the peak bed file
        select: a list of integers corresponding to the line number of the peak to be selected
                select all peaks when None

    Returns:
        An iterator of the peak bed file in which only the selected peaks will be yielded

    """
    if select:
        select = set(select)

    curr = 0
    with open(peaks, "r") as peak_file:
        for line, row in enumerate(peak_file):
            row = row.strip()
            # skip empty lines or comment lines
            if not row or row.startswith("#"):
                continue
            peak = row.split("\t")
            curr += 1

            if len(peak) < 3:
                raise ValueError(
                    "Line {} of peaks file {} = '{}' and is not of the form"
                    " 'chrom\tstart\tend'".format(line, peaks, row)
                )

            if select and (curr - 1) not in select:
                continue

            yield Peak(peak[0], int(peak[1]), int(peak[2]), ".")


def merge_keyed_bed(input_beds: List[str], output_bed: Union[str, bytes, IO[bytes]]):
    """Merge sorted bedfiles retaining their chromosome keys, dropping the key afterwards.

    Note: this function uses 2 threads one for the `sort` and one for the `cut` operation.

    Warning!  sort does not have the --parallel argument on all forms of unix!  As such, we are
    dropping threading support for BED handling through unix sort.
    """
    tk_subproc.pipeline(
        [["sort", "-m", "-k2n", "-k3n", "-k4n"] + input_beds, ["cut", "-f", "1,3-8"]],
        stdout=output_bed,
    )
