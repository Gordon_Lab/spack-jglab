# Copyright (c) 2020 10X Genomics, Inc. All rights reserved.
"""
Write per barcode CSV information
"""

from __future__ import absolute_import, annotations

from typing import Any, Dict, List, Set, Union
import re
from enum import Enum
from six import ensure_binary, ensure_str
import pandas as pd
import atac.cell_calling_helpers.exclusions as exclusions


def write_singlecell(
    species_list: List[str],
    barcodes: List[bytes],
    cell_barcodes: Dict[str, Union[Dict[bytes, int], List[bytes]]],
    barcode_counts_by_species: Dict[str, Dict[bytes, int]],
    targeted_counts_by_species: Dict[str, Dict[bytes, int]],
    excluded_barcodes: Dict[str, Dict[str, List[Any]]],
    out_csv: bytes,
):
    """
    Write singlecell CSV that includes the barcode, whether barcode is a cell, the counts per
    species, the counts overlapping peaks, and excluded barcode information.
    """
    for species in species_list:
        assert isinstance(species, str)
    for key, vdict in targeted_counts_by_species.items():
        assert isinstance(key, str)
        for bc in vdict.keys():
            assert isinstance(bc, bytes)
    for key, vdict in barcode_counts_by_species.items():
        assert isinstance(key, str)
        for bc in vdict.keys():
            assert isinstance(bc, bytes)
    for key in cell_barcodes.keys():
        assert isinstance(key, str)
    species_list_bytes = [ensure_binary(sp) for sp in species_list]
    with open(out_csv, "wb") as outfile:
        header = [b"barcode"]
        header.extend([b"is_%s_cell_barcode" % (species) for species in species_list_bytes])
        header.extend(exclusions.get_columns(species_list))
        if len(species_list) > 1:
            for species in species_list_bytes:
                header.append(b"passed_filters_%s" % (species))
                header.append(b"peak_region_fragments_%s" % (species))
        outfile.write(b",".join(header) + b"\n")
        for barcode in barcodes:
            values = [barcode]
            # is_cell
            values.extend(
                [
                    b"%d" % (species in cell_barcodes and barcode in cell_barcodes[species])
                    for species in species_list
                ]
            )
            # excluded barcodes
            for species in species_list:
                reason = exclusions.EXCLUSION_REASON_CODE[""]
                if excluded_barcodes:
                    ex_sp = excluded_barcodes.get(species)
                    if ex_sp:
                        reason = exclusions.EXCLUSION_REASON_CODE[
                            ex_sp.get(ensure_str(barcode), ["", ""])[0]
                        ]
                values.append(b"%d" % reason)
            if len(species_list) > 1:
                for species in species_list:
                    values.append(b"%d" % (barcode_counts_by_species[species][barcode]))
                    values.append(b"%d" % (targeted_counts_by_species[species][barcode]))

            outfile.write(b",".join(values) + b"\n")


class SingleCellCsv:
    """
    Class to manage different formats of the singlecell.csv file between ATAC, ARC-CS and ARC-PD.
    Rather than extending functionality for this class, we need to unify formats and get rid of it
    altogether.
    """

    class Format(Enum):
        """Possible formats for singlecell csv file"""

        ATAC = "atac"
        ARC_CS = "arc-cs"
        # Note: the format below includes ATAC + GEX and ATAC-only data processed by ARC PD
        ARC_PD = "arc-pd"

    def __init__(self, path: bytes):
        """Initialize from a path to a singlecell.csv/per_barcode_metrics.csv file

        Args:
            path (bytes): path to file
        """
        self.path = path
        species_regex = "[a-zA-Z0-9_-]*"
        self.header = None
        with open(path, "r") as fin:
            for line in fin:
                self.header = line.strip().split(",")
                break
        self.format = None
        self.species = []
        self.data = None
        self.barcode_col = "barcode"
        if "atac_total" in self.header:
            self.format = SingleCellCsv.Format.ARC_PD
            if all(col.startswith("atac_") for col in self.header):
                # this is for ATAC-only data processed by the ARC PD pipeline
                self.barcode_col = "atac_barcode"
            for col in self.header:
                split = re.split("atac_is_({})_cell_barcode".format(species_regex), col)
                if len(split) == 3:
                    self.species.append(split[1])
        elif "duplicate" in self.header:
            self.format = SingleCellCsv.Format.ATAC
            for col in self.header:
                split = re.split("is_({})_cell_barcode".format(species_regex), col)
                if len(split) == 3:
                    self.species.append(split[1])
        elif "atac_raw_reads" in self.header:
            self.format = SingleCellCsv.Format.ARC_CS
            if "is_cell" in self.header:
                self.species = [""]
            else:
                for col in self.header:
                    split = re.split("is_cell_({})".format(species_regex), col)
                    if len(split) == 3:
                        self.species.append(split[1])
        else:
            raise RuntimeError("Unrecognized singlecell.csv format: {}".format(self.path))

    @classmethod
    def from_aggr_def(cls, assay: str, aggr_def: Dict) -> SingleCellCsv:
        """Initialize a SingleCellCsv object given an assay type and an aggr definition dict."""

        if assay == "atac":
            return cls(aggr_def["cells"])
        elif assay == "arc":
            return cls(aggr_def["per_barcode_metrics"])
        else:
            raise ValueError("Invalid assay type {}. Allowed values are atac or arc".format(assay))

    def load(self):
        """
        Actually load the full dataframe into memory
        """
        self.data = pd.read_csv(ensure_str(self.path), converters={self.barcode_col: ensure_binary})
        self.data[self.barcode_col] = self.data[self.barcode_col].astype("S")

    def __getitem__(self, key):
        """Access the underlying pandas dataframe"""
        assert self.data is not None
        return self.data[key]

    def is_cell_column(self, species: str) -> str:
        """
        Get the name of the column in the CSV that indicates whether a barcode is a cell of the
        specified species or not.


        Args:
            species (str): species

        Returns:
            str: name of the column in CSV
        """
        assert species in self.species
        name = ""
        if self.format == SingleCellCsv.Format.ATAC:
            name = "is_{}_cell_barcode".format(species)
        elif self.format == SingleCellCsv.Format.ARC_CS:
            name = "is_cell_{}".format(species) if species else "is_cell"
        elif self.format == SingleCellCsv.Format.ARC_PD:
            name = "atac_is_{}_cell_barcode".format(species)
        assert name in self.header
        return name

    def get_is_cell_columns(self) -> List[str]:
        """Get a list of all the is_cell columns for all species

        Returns:
            List[str]: list of column names
        """
        return [self.is_cell_column(species) for species in self.species]

    def get_all_cell_barcodes(self) -> Set[bytes]:
        """Return a set of all barcodes that are cells of any species

        Returns:
            Set[bytes]: set of barcodes as bytes
        """
        if self.data:
            df = self.data
        else:
            df = pd.read_csv(
                ensure_str(self.path),
                usecols=[self.barcode_col] + self.get_is_cell_columns(),
                converters={self.barcode_col: ensure_binary},
            )
            df[self.barcode_col] = df[self.barcode_col].astype("S")
        is_cell = df[self.get_is_cell_columns()].sum(axis=1) >= 1
        return set(df[self.barcode_col][is_cell])

    def get_cell_barcodes_dict(self) -> Dict[str, Set[bytes]]:
        """For each species detected return a set of cell barcodes

        Returns:
            Dict[str, Set[bytes]]: set of barcodes for each species
        """
        if self.data:
            df = self.data
        else:
            df = pd.read_csv(
                ensure_str(self.path),
                usecols=[self.barcode_col] + self.get_is_cell_columns(),
                converters={self.barcode_col: ensure_binary},
            )
            df[self.barcode_col] = df[self.barcode_col].astype("S")
        data = {}
        for species in self.species:
            is_cell = self.is_cell_column(species)
            data[species] = set(df[self.barcode_col][df[is_cell] == 1])
        return data

    def get_total_column(self):
        """Total number of read pairs column"""
        assert self.data is not None
        if self.format == SingleCellCsv.Format.ATAC:
            return self.data["total"]
        elif self.format == SingleCellCsv.Format.ARC_PD:
            return self.data["atac_total"]
        elif self.format == SingleCellCsv.Format.ARC_CS:
            return self.data["atac_raw_reads"]
        else:
            raise ValueError("Unexpected format")

    def get_passed_filters_column(self):
        """Total number of fragments that pass all filters column"""
        assert self.data is not None
        if self.format == SingleCellCsv.Format.ATAC:
            return self.data["passed_filters"]
        elif self.format == SingleCellCsv.Format.ARC_PD:
            return self.data["atac_passed_filters"]
        elif self.format == SingleCellCsv.Format.ARC_CS:
            return self.data["atac_fragments"]
        else:
            raise ValueError("Unexpected format")

    def get_duplicate_column(self):
        """Number of duplicate read pairs column"""
        assert self.data is not None
        if self.format == SingleCellCsv.Format.ATAC:
            return self.data["duplicate"]
        elif self.format == SingleCellCsv.Format.ARC_PD:
            return self.data["atac_duplicate"]
        elif self.format == SingleCellCsv.Format.ARC_CS:
            return self.data["atac_dup_reads"]
        else:
            raise ValueError("Unexpected format")
