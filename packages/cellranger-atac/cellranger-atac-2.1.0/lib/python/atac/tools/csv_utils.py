# Copyright (c) 2019 10x Genomics, Inc. All rights reserved.

""" Utilities for processing CSV files in the ARC/ATAC pipelines"""

from __future__ import absolute_import, print_function

import re
import csv
from typing import AnyStr, Dict, Optional, OrderedDict, Sequence, Tuple
from six import ensure_str
import pandas as pd
from cellranger.constants import AGG_ID_FIELD

AGG_FRAGMENT_FIELD = "fragments"
AGG_CELL_FIELD = "cells"
ATAC_AGGR_COLUMNS = [AGG_ID_FIELD, AGG_FRAGMENT_FIELD, AGG_CELL_FIELD]


def parse_aggr_csv(
    infile: AnyStr, check_cols: Sequence[str] = None
) -> Tuple[Optional[int], Optional[Dict[int, OrderedDict[str, str]]], Optional[str]]:
    """Parse cellranger-atac aggr definition CSV file"""
    if check_cols is None:
        check_cols = ATAC_AGGR_COLUMNS

    header = None
    library_info = {}
    uniques = set()
    with open(infile, "r", encoding="utf-8-sig", newline=None) as csv_in:
        reader = csv.DictReader(csv_in)
        # 1-indexed gem wells
        gem_well = 1
        for row in reader:
            if header is None:
                header = list(row.keys())
                for col in check_cols:
                    if col not in header:
                        return (
                            None,
                            None,
                            "Supplied CSV {} is missing a required column `{}`.\n"
                            "Detected header: {}".format(infile, col, header),
                        )

            for key, value in row.items():
                # No empty entries
                if not value:
                    return (
                        None,
                        None,
                        "Supplied CSV {} has a missing value for column `{}`".format(infile, key),
                    )

                # library ID must be ASCII and can contain only word characters and dashes
                if key == "library_id" and not re.match(r"^[\w-]+$", value, re.ASCII):
                    return (
                        None,
                        None,
                        "'library_id' = \"{}\" is invalid. Library name can not be empty and "
                        "may only contain ASCII characters that are letters, numbers, "
                        "underscores, and dashes".format(value),
                    )

                # no duplicate entries for any column in check_cols
                if key in check_cols and (key, value) in uniques:
                    return (
                        None,
                        None,
                        "Supplied CSV {} has the value {} for column {} repeated on multiple "
                        "rows".format(infile, value, key),
                    )
                uniques.add((key, value))

            library_info[gem_well] = row
            gem_well += 1

    return len(library_info), library_info, None


def load_insert_sizes(filename, colrange=None, barcode_selection=None, report_sum=False):
    """Memory-efficient loading of the insert_size.csv file.  Uses chunking and loads in as uint32s.

    Optional arguments, all of which improve memory performance:
    - colrange: numerical values indicating columns (insert sizes) to return
    - barcode_selection: set of barcodes to report
    - report_sum: boolean if we want to report out the sum (excluding columns) instead of the dataframe.
    """
    with open(filename, "r") as infile:
        columns = infile.readline().strip().split(",")

    if colrange is None:
        usecols = None
    else:
        validcols = ["Barcode"] + [str(c) for c in colrange]

        def usecols(col):
            return col in validcols

    # Even though the column type of `Barcode` is indicated as `bytes` here pandas does not respect
    # it and creates a column whose entries are of type `str`.
    dtypes = {col: "S" if col == "Barcode" else "uint32" for col in columns}
    reader = pd.read_csv(ensure_str(filename), usecols=usecols, dtype=dtypes, chunksize=10000)
    chunks = []
    for chunk in reader:
        # this casting changes the column type to `bytes` from `str`
        chunk["Barcode"] = chunk["Barcode"].astype("S")
        if barcode_selection is not None:
            cell_mask = chunk.Barcode.apply(lambda bc: bc in barcode_selection)
            chunk = chunk[cell_mask]
        if report_sum:
            # Exclude the barcode from the sum
            chunk = chunk.iloc[:, 1:].sum()
        chunks.append(chunk)

    if report_sum:
        return sum(chunks)
    else:
        return pd.concat(chunks)
