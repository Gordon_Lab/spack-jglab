# Copyright (c) 2020 10x Genomics, Inc. All rights reserved.
"""Annotate peaks based on genomic proximity."""

from __future__ import division, print_function, absolute_import
from typing import AnyStr, List, Tuple
from itertools import groupby
import csv
from six import ensure_str

from pybedtools import BedTool, create_interval_from_list
from atac.constants import TRANSCRIPT_ANNOTATION_GENE_TYPES
from atac.tools.ref_manager import ReferenceManager

DISTANCE_LEFT_OF_TSS = 1000
DISTANCE_RIGHT_OF_TSS = 100
DISTANCE_TO_INTERGENIC = 200000


def get_peak_nearby_genes(peak):
    """parse peaks with closest TSS and overlaped transcript

    :param peak: can be 6 columns when a peak has closest TSS and also overlaps with a transcript:
                 chr1 937133 937621 ISG15 -616 ISG15
                 or just 5 column if the peak doesn't overlap with any transcript
                 chr1 937133 937621 ISG15 -616
                 the 4th and 6th (if present) column is a string in which gene symbols are separated by comma
                 the 5th column is distance values separated by comma
                 column 1 to 5 are required and column 6 is optional
    :return: Interval object created from a list like ['chr1', '6051602', '6053638', 'KCNAB2', '0', 'promoter'].
    If a peak is associated with multiple annotations, an interval object will be created for each annotation.
    """
    genes = list(peak[3].split(","))

    # In a case where an entire chromosome is called a peak and >1000 TSS
    # are identified as closest, BedTools does not report distances
    distances = [] if len(peak.fields) < 5 else list(peak[4].split(","))
    peak_types = []

    # call promoter peaks first
    is_promoter = {}
    for i, dist in enumerate(distances):
        dist = int(dist)
        if -dist <= DISTANCE_LEFT_OF_TSS and dist <= DISTANCE_RIGHT_OF_TSS:
            is_promoter[genes[i]] = True

    # call distal peaks
    is_distal = {}
    for i, dist in enumerate(distances):
        dist = int(dist)
        if -dist <= DISTANCE_LEFT_OF_TSS and dist <= DISTANCE_RIGHT_OF_TSS:
            peak_types.append("promoter")

        elif abs(dist) <= DISTANCE_TO_INTERGENIC:
            # distal peaks, if this peak is already a promoter of the gene being tested, do not annotate it again as a distal peak
            if genes[i] in is_promoter:
                genes[i] = ""
                distances[i] = ""
                peak_types.append("")
            else:
                peak_types.append("distal")
                is_distal[genes[i]] = True
        else:
            genes[i] = ""
            distances[i] = ""
            peak_types.append("")

        if genes.count("") == len(genes):
            genes, distances, peak_types = [], [], []

    # if a peak has an overlapping gene AND it has not been annotated as the promoter peak of that gene , call distal peaks
    if len(list(peak)) > 5:
        for gene in peak[5].split(","):
            if gene not in is_promoter and gene not in is_distal:
                distances.append("0")
                peak_types.append("distal")
                genes.append(gene)

    # if the peak still has not been annotated, it is an intergenic peak
    if peak_types == []:
        peak_types.append("intergenic")
        genes = [""]
        distances = [""]

    # clean up and unify
    assert len(genes) == len(peak_types)
    annos = set(anno for anno in zip(genes, distances, peak_types) if anno != ("", "", ""))
    genes, distances, peak_types = zip(*sorted(annos))

    intervals = []
    for gene, dist, ptype in zip(genes, distances, peak_types):
        i = create_interval_from_list([peak[0], peak[1], peak[2], gene, dist, ptype])
        intervals.append(i[:])
    return intervals


def remove_duplicate_features(old_bed_file, new_bed_file):
    """Remove duplicate features, distances from a BED file

    A BED entry of the form:

    1    100    200    a,a,b    30,30,40

    is turned into:
    1    100    200    a,b    30,40

    Args:
        old_bed_file (AnyStr): BED file with duplicate feature entries
        new_bed_file (AnyStr): BED file with de-duplicated entries
    """
    with open(new_bed_file, "w") as out:
        for line in BedTool(old_bed_file):
            fields = line.fields
            seen = set()
            features = fields[3].split(",")
            dists = fields[4].split(",")
            new_features = []
            new_dists = []
            for f, d in zip(features, dists):  # pylint: disable=invalid-name
                if (f, d) in seen:
                    continue
                new_features.append(f)
                new_dists.append(d)
                seen.add((f, d))
            out_str = "{}\t{}\t{}\t{}\t{}\n".format(
                fields[0], fields[1], fields[2], ",".join(new_features), ",".join(new_dists)
            )
            out.write(out_str)


def annotate_peaks(peaks, ref_path):
    """Annotate peaks.

    peak to gene annotation strategy:
        1. if a peak overlaps with promoter region (-1kb, + 100) of any TSS, call it a promoter peak
        2. if a peak is within 200kb of the closest TSS, AND if it is not a promoter peak, call it a distal peak
        3. if a peak overlaps of a transcript, AND it is not a promoter nor a distal peak of the gene, call it a distal peak
            This step is optional
        4. call it an intergenic peak
    """

    ref_mgr = ReferenceManager(ref_path)
    tss = BedTool(ref_mgr.tss_track)

    # if tss.bed contains the 7th column (gene type), then apply filter. Otherwise use all tss sites
    if tss.field_count() == 7:
        tss_filtered = tss.filter(lambda x: x[6] in TRANSCRIPT_ANNOTATION_GENE_TYPES).saveas()
    else:
        df_tss = tss.to_dataframe()
        df_tss["gene_type"] = "."
        tss_filtered = BedTool.from_dataframe(df_tss).saveas()

    # including transcripts.bed is optional
    if ref_mgr.transcripts_track is None:
        transcripts_filtered = BedTool([])
    else:
        transcripts = BedTool(ref_mgr.transcripts_track)
        if transcripts.field_count() == 7:
            transcripts_filtered = transcripts.filter(
                lambda x: x[6] in TRANSCRIPT_ANNOTATION_GENE_TYPES
            ).saveas()
        else:
            df_tx = transcripts.to_dataframe()
            df_tx["gene_type"] = "."
            transcripts_filtered = BedTool.from_dataframe(df_tx).saveas()

    # run bedtools closest for peaks against filtered tss, group by peaks and summarize annotations from select columns
    # 1=chrom1 (peaks), 2=start1 (peaks), 3=end1 (peaks), 7=name (tss_filtered),
    # 11=distance of closest feature in B (tss_filtered) to feature in A (peaks) using negative distances for upstream features
    peaks_nearby_tss = (
        peaks.closest(tss_filtered, D="b", g=ref_mgr.fasta_index)
        .groupby(g=[1, 2, 3], c=[7, 11], o=["collapse"])
        .saveas("peaks_nearby_tss_dups.bed")
    )
    assert len(peaks_nearby_tss) == len(peaks)

    # remove duplicate transcript, distance entries
    remove_duplicate_features("peaks_nearby_tss_dups.bed", "peaks_nearby_tss.bed")
    peaks_nearby_tss = BedTool("peaks_nearby_tss.bed")

    results = []
    peaks_nearby_tss_butno_tx = (
        peaks_nearby_tss.intersect(  # pylint: disable=too-many-function-args,unexpected-keyword-arg
            transcripts_filtered, v=True
        ).saveas()
    )
    peaks_nearby_tss_yes_tx = (
        peaks_nearby_tss.intersect(  # pylint: disable=too-many-function-args,unexpected-keyword-arg
            transcripts_filtered, u=True
        ).saveas()
    )
    assert len(peaks_nearby_tss_butno_tx) + len(peaks_nearby_tss_yes_tx) == len(peaks)

    # avoid error when no peaks overlap with any transcipts
    # 1=chrom1 (peaks), 2=start1 (peaks), 3=end1 (peaks),
    # 4=closest TSS, 5=distance of closest TSS, 9=name (transcripts_filtered)
    if len(peaks_nearby_tss_yes_tx) > 0:
        peaks_nearby_tss_and_tx = peaks_nearby_tss_yes_tx.intersect(
            transcripts_filtered, wa=True, wb=True
        ).groupby(g=[1, 2, 3, 4, 5], c=[9], o=["distinct"])
        for peak in peaks_nearby_tss_and_tx:
            results.extend(get_peak_nearby_genes(peak))

        # after the intersection with transcripts the peaks_nearby_tss_and_tx
        # may not include all peaks from peaks_nearby_tss_yes_tx
        # this step ensures that those are included in the peak_annotation.
        peaks_with_transcripts = {
            create_interval_from_list(p[0:5]) for p in peaks_nearby_tss_and_tx
        }
        for peak in peaks_nearby_tss_yes_tx:
            if peak not in peaks_with_transcripts:
                results.extend(get_peak_nearby_genes(peak))

    for peak in peaks_nearby_tss_butno_tx:
        results.extend(get_peak_nearby_genes(peak))

    return results


def read_peak_annotation(peak_annotation_tsv: AnyStr) -> List[Tuple[str, str]]:
    """Parses the peak annotation TSV.

    Args:
        peak_annotation_tsv (AnyStr): path to tsv file with six fields per row
        (chrom, start, end, gene, distance, peak_type)

    Returns:
        List[Tuple[str, str]]: annotations associated with each peak in the format
        ["promoter1;promoter2", "nearby_gene1;nearby_gene2"]
    """
    peak_annotation = []
    if peak_annotation_tsv:
        with open(ensure_str(peak_annotation_tsv), "r") as f:
            reader = csv.reader(f, delimiter="\t")
            next(reader)  # skip header
            annotations = []
            for row in reader:
                annotations.append([str(field or "") for field in row])
        for _, group in groupby(annotations, lambda x: (x[0], x[1], x[2])):
            promoter = []
            nearby_gene = []
            for row in group:
                if row[5] == "promoter":
                    promoter.append(row[3])
                nearby_gene.append(row[3])
            peak_annotation.append((";".join(promoter), ";".join(nearby_gene)))
    return peak_annotation
