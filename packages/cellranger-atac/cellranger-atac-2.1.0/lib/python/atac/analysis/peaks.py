# Copyright (c) 2019 10x Genomics, Inc. All rights reserved.
"""
Methods for doing peakcalling.
"""
from __future__ import division, print_function, absolute_import

import numpy as np
from collections import Counter
import pickle
import json

from six import itervalues, iteritems
from six.moves import xrange
from atac.analysis.em_fitting import (
    MixtureModel,
    GeometricComponent,
    NegativeBinomialComponent,
    MixtureModelError,
)
from atac.analysis.em_fitting import weighted_mean, weighted_variance
from atac.tools.peaks import peak_reader
from atac.tools.ref_manager import ReferenceManager

# The default count threshold to use for initializing the parameter estimation
DEFAULT_THRESHOLD = 15

ZERO_NOISE = "zero_noise"
BG_NOISE = "bg_noise"
SIGNAL = "signal"


################################################################################
# PD peak comparison tools
################################################################################


def compare_peak_basecalls(peaks, ref):
    """Compares the input dictionary of peak calls by counting the number
    of bases in each combination of peak files.
    """
    indices = {name: pow(2, i) for i, name in enumerate(peaks)}
    assert len(peaks) < 8
    count_dict = Counter()
    for contig in ref.primary_contigs():
        contig_len = ref.contig_lengths[contig]
        counts = np.zeros(contig_len, dtype=np.uint8)
        for name, peakfile in iteritems(peaks):
            with open(peakfile, "r") as infile:
                for track in infile:
                    if track.startswith("{}\t".format(contig)):
                        track = track.split("\t")
                        start = int(track[1])
                        stop = int(track[2])
                        counts[start:stop] += indices[name]
        mask = counts > 0
        count_dict += Counter(counts[mask])
    print(count_dict)
    # Rewrite the dictionary with readable names rather than bitstrings
    output_dict = {}
    for key, count in iteritems(count_dict):
        names = tuple(sorted([name for name, index in iteritems(indices) if key & index]))
        output_dict[names] = count
    return output_dict


################################################################################


def load_model_from_parameters(params, prefix=None):
    """Load a MixtureModel object from a parameters dictionary"""

    def add_prefix(name, prefix):
        if prefix:
            return "{}_{}".format(prefix, name)
        return name

    if params:
        p_z = params[add_prefix("p_{}".format(ZERO_NOISE), prefix)]
        p_s = params[add_prefix("p_{}".format(SIGNAL), prefix)]
        mean_bg = params[add_prefix("mean_{}".format(BG_NOISE), prefix)]
        alpha_bg = params[add_prefix("alpha_{}".format(BG_NOISE), prefix)]
        mix_prob = [
            params[add_prefix("mix_prob_{}".format(name), prefix)]
            for name in [ZERO_NOISE, BG_NOISE, SIGNAL]
        ]
    else:
        p_z, p_s, mean_bg, alpha_bg = 0.0, 0.0, 0.0, 0.0
        mix_prob = None

    zero_inflation_comp = GeometricComponent(ZERO_NOISE, p_z)
    noise_comp = NegativeBinomialComponent(BG_NOISE, mean_bg, alpha_bg)
    signal_comp = GeometricComponent(SIGNAL, p_s)

    model = MixtureModel([zero_inflation_comp, noise_comp, signal_comp], mix_prob=mix_prob)
    return model


def simple_threshold_estimate(count_dict, cutoff=0.25):
    """Does a simple estimate for the threshold count:
    pick the threshold such that the lower noise region contains
    cutoff (default 25%) percentage of the total reads.
    """
    count_values = np.array(sorted([k for k in count_dict if k > DEFAULT_THRESHOLD]))
    count_weights = np.array([count_dict[k] for k in count_values])
    read_counts = count_values * count_weights
    cum_frac = read_counts.cumsum() / read_counts.sum()

    threshold = count_values[cum_frac <= cutoff][-1] if any(cum_frac <= cutoff) else count_values[1]
    return threshold


def estimate_model_parameters(count_dict, seed_params=None, epsilon=1e-6, maxiter=500):
    """Given a count_dict estimate the mixture model parameters via EM"""
    threshold = simple_threshold_estimate(count_dict)

    count_values = np.array(sorted([k for k in count_dict]))
    log_count_multiplicity = np.log([count_dict[k] for k in count_values])

    model = load_model_from_parameters(seed_params, prefix=None)

    prob_matrix = np.empty((3, len(count_values)), dtype=float)
    prob_matrix[0, :] = count_values <= DEFAULT_THRESHOLD
    prob_matrix[1, :] = (count_values <= threshold) & (count_values > DEFAULT_THRESHOLD)
    prob_matrix[2, :] = count_values > threshold
    prob_matrix += 1e-2
    prob_matrix /= prob_matrix.sum(axis=0)
    model.initialize_responsibilities(np.log(prob_matrix))

    delta_dict = (
        model.estimate_parameters(count_values, log_count_multiplicity) if not seed_params else {}
    )

    for i in xrange(maxiter):
        try:
            model.calculate_responsibilities(count_values)
            delta_dict = model.estimate_parameters(count_values, log_count_multiplicity)
        except MixtureModelError as e:
            print("EM crashed: ", str(e))

        change = max(itervalues(delta_dict)) if delta_dict else 0.0

        zero_inflation_comp = model.get_component(ZERO_NOISE)
        signal_comp = model.get_component(SIGNAL)
        if signal_comp.params.p > zero_inflation_comp.params.p:
            model.swap(ZERO_NOISE, SIGNAL)

        if change < epsilon:
            print("converged after {} iterations".format(i + 1))
            break
    print(model)
    return model


def estimate_threshold(model, odds_ratio, maxval=1000):
    """Estimate threshold for peak calling"""
    xvals = np.arange(maxval)

    yvals_total_bg = model.get_component_contribution(
        ZERO_NOISE, xvals
    ) + model.get_component_contribution(BG_NOISE, xvals)
    yvals_sig = model.get_component_contribution(SIGNAL, xvals)

    for val in xvals[::-1]:
        if yvals_sig[val] / yvals_total_bg[val] < odds_ratio:
            return val
    return None


def estimate_final_threshold(count_dict, odds_ratio, seed_params=None):
    """Provided with a count dictionary, estimate the peak threshold for the count data."""

    # heuristic threshold to avoid fitting using the mixture model when the coverage is too low
    if len(count_dict) < 2 * DEFAULT_THRESHOLD or sum(count_dict.values()) < 1e3:
        return max(count_dict.keys()) / 2, None

    max_tries = 3
    tries = 0

    if seed_params is not None:
        model = estimate_model_parameters(count_dict, seed_params=seed_params)
    if seed_params is None:
        model = estimate_model_parameters(count_dict)
    params_dict = model.get_parameters_dict()
    threshold = estimate_threshold(model, odds_ratio)
    print("threshold = ", threshold)

    p_z = params_dict["p_{}".format(ZERO_NOISE)]
    p_s = params_dict["p_{}".format(SIGNAL)]
    mean_bg = params_dict["mean_{}".format(BG_NOISE)]
    alpha_bg = params_dict["alpha_{}".format(BG_NOISE)]

    zero_mean = 1 / p_z

    # if the zero inflation mean is smaller than negative binom noise mean, swap and redo
    while zero_mean < mean_bg and tries < max_tries:
        print("Try ", tries + 1)
        new_params = {
            "p_{}".format(ZERO_NOISE): p_s,
            "mean_{}".format(BG_NOISE): zero_mean,
            "alpha_{}".format(BG_NOISE): alpha_bg,
            "p_{}".format(SIGNAL): 1 / mean_bg,
            "mix_prob_{}".format(ZERO_NOISE): params_dict["mix_prob_{}".format(SIGNAL)],
            "mix_prob_{}".format(BG_NOISE): params_dict["mix_prob_{}".format(BG_NOISE)],
            "mix_prob_{}".format(SIGNAL): params_dict["mix_prob_{}".format(ZERO_NOISE)],
        }
        model = estimate_model_parameters(count_dict, seed_params=new_params)
        params_dict = model.get_parameters_dict()
        threshold = estimate_threshold(model, odds_ratio)
        print("threshold = ", threshold)

        zero_mean = 1 / params_dict["p_{}".format(ZERO_NOISE)]
        mean_bg = params_dict["mean_{}".format(BG_NOISE)]
        tries += 1

    return threshold, params_dict


def load_count_dict(count_dict_path):
    """Load the cut-site coverage histogram.

    Args:
        count_dict_path (pickle|json): Path to serialized cut-site coverage histogram in either json
        or pickle format.
    """
    if count_dict_path.endswith("pickle"):
        try:
            # python3
            with open(count_dict_path, "rb") as infile:
                # pylint: disable=unexpected-keyword-arg
                return pickle.load(infile, encoding="bytes")
        except TypeError:
            with open(count_dict_path, "rb") as infile:
                return pickle.load(infile)
    elif count_dict_path.endswith("json"):
        with open(count_dict_path) as infile:
            return {int(k): v for (k, v) in iteritems(json.load(infile))}
    else:
        raise ValueError("Unrecognized format for cut-site count histogram")


def calculate_peak_metrics(reference_path, peaks_file):
    """Calculates key peak metrics"""
    peak_metrics = {}
    bases_in_peaks = 0
    num_peaks = 0
    peak_lengths = Counter()
    ctg_mgr = ReferenceManager(reference_path)
    contig_lengths = ctg_mgr.get_contig_lengths()
    primary_contigs = ctg_mgr.primary_contigs()
    genome_size = sum([v for k, v in iteritems(ctg_mgr.contig_lengths) if k in primary_contigs])
    # Only report data for primary contigs
    for peak in peak_reader(peaks_file):
        if peak.chrom not in primary_contigs:
            continue
        peak_lengths[peak.end - peak.start] += 1
        num_peaks += 1
        chrom_length = contig_lengths[peak.chrom]
        assert peak.start <= min(peak.end, chrom_length)
        bases_in_peaks += min(peak.end, chrom_length) - peak.start

    peak_metrics["total_peaks_detected"] = num_peaks
    peak_metrics["bases_in_peaks"] = bases_in_peaks
    peak_metrics["frac_primary_genome_in_peaks"] = bases_in_peaks / genome_size

    peak_metrics["peak_length_distribution"] = dict(peak_lengths)

    weights = np.array([c for l, c in peak_lengths.items()])
    counts = np.array([l for l, c in peak_lengths.items()])
    peak_metrics["peak_length_mean"] = weighted_mean(counts, weights)
    peak_metrics["peak_length_stdev"] = np.sqrt(weighted_variance(counts, weights))
    if num_peaks > 0:
        peak_metrics["frac_very_small_peaks"] = (
            sum(c for l, c in peak_lengths.items() if l <= 5) / num_peaks
        )
        peak_metrics["frac_very_large_peaks"] = (
            sum(c for l, c in peak_lengths.items() if l >= 2000) / num_peaks
        )
    else:
        peak_metrics["frac_very_small_peaks"] = 0
        peak_metrics["frac_very_large_peaks"] = 0

    return peak_metrics


def cut_site_counter_from_bedgraph(cutsites_filename, threshold=1, contig=None):
    """Yield count data at base pairs encoded in bedgraph tracks"""
    if contig:
        assert isinstance(contig, bytes)
    with open(cutsites_filename, "rb") as infile:
        for track in infile:
            chrom, start, stop, count = track.split(b"\t")
            count = int(count)
            if contig is not None and chrom != contig:
                continue
            if count < threshold:
                continue
            for pos in xrange(int(start), int(stop)):
                yield chrom, pos, count
