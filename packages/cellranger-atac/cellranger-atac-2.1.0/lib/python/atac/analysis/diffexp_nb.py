# Copyright (c) 2019 10x Genomics, Inc. All rights reserved.
from __future__ import absolute_import, division, print_function


import sys
from typing import Optional, NamedTuple
import numpy as np
import mkl
import pandas as pd
from scipy.sparse.csc import csc_matrix
from scipy.stats import t, fisher_exact
from scipy.sparse import spmatrix
import cellranger.analysis.diffexp as cr_diffexp
from cellranger.analysis.stats import summarize_columns
from atac.analysis.da_glm_o3 import (  # pylint: disable=no-name-in-module, import-error
    CscMatrix,
    wald_1x1_nb2_py,
    fisher_2x2_py,
    run_nb_glm_py,
)

ALLOWED_NB2_MODELS = ["poisson", "geometric", "nb"]


class MklState(NamedTuple):
    """Turn on/off MKL threading"""

    threads: int
    dynamic: bool

    @staticmethod
    def disable():
        """Disable multi-threading in MKL"""
        state = MklState(mkl.get_max_threads(), mkl.get_dynamic())
        mkl.set_dynamic(False)
        mkl.set_num_threads(1)
        return state

    def restore(self):
        """Restore to state before multithreading was disabled"""
        mkl.set_dynamic(self.dynamic)
        mkl.set_num_threads(self.threads)


def empirical_dispersion(y, threshold=1e-4):
    """Estimate empirical dispersion"""
    assert threshold > 0

    (mu, var) = summarize_columns(y.T)
    alpha_est = np.maximum(
        (var.squeeze() - mu.squeeze()) / (np.square(mu.squeeze() + 1e-100)), threshold
    )
    return np.atleast_1d(alpha_est)


def Wald_1x1_NB2(B, varB, df):
    """Perform Wald test for 1 vs rest grouping with NB2 regression"""
    var_a = varB[:, 0, 0].squeeze()
    var_b = varB[:, 1, 1].squeeze()
    cov_ab = varB[:, 0, 1].squeeze()

    # means and log2fc
    mean_a = np.nan_to_num(np.exp(B[0, :].squeeze()))
    mean_b = np.nan_to_num(np.exp(B[1, :].squeeze()))
    log2fc = (B[0, :].squeeze() - B[1, :].squeeze()) / np.log(2)

    # pval
    var_ab = np.sqrt((var_a + var_b - 2 * cov_ab))
    t_stat = (B[0, :].squeeze() - B[1, :].squeeze()) / var_ab
    p_values = t.sf(np.abs(t_stat), df)

    # main dimension when only one test is performed
    p_values = np.atleast_1d(p_values)
    log2fc = np.atleast_1d(log2fc)
    t_stat = np.atleast_1d(t_stat)
    mean_a = np.atleast_1d(mean_a)
    mean_b = np.atleast_1d(mean_b)

    return log2fc, t_stat, p_values, mean_a, mean_b


def fisher_2x2(binarized_y, group_a, group_b, select_fisher, verbose=False):
    """Perform fisher's exact test on 2 x 2 contigency tables indicating presence or absence frequency of peak in cluster A vs B"""
    Na = len(group_a)
    Nb = len(group_b)

    # binarize for making the contingency tables
    na = binarized_y[:, group_a].sum(axis=1).A.squeeze()
    nc = Na - na
    nb = binarized_y[:, group_b].sum(axis=1).A.squeeze()
    nd = Nb - nb
    log2fc = np.zeros(len(select_fisher))
    p_values = log2fc.copy()

    if len(select_fisher) > 0 and verbose:
        print("Running Fisher's exact test on {} features".format(len(select_fisher)))
        sys.stdout.flush()

    # run fisher test
    for i, nf in enumerate(select_fisher):
        _, p_values[i] = fisher_exact([[na[nf], nb[nf]], [nc[nf], nd[nf]]])
        # add the 1's and 2's to ensure no infs are produced. The log2fc for these
        # cases aren't wholly meaningful anyway
        freq_ratio = (na[nf] + 1) * (Nb + 2) / (Na + 2) / (nb[nf] + 1)
        log2fc[i] = np.log2(freq_ratio)

    return log2fc, p_values


def NBGLM_differential_expression(
    rust_mat, rust_mat_t, group_a, group_b, model, cov=None, alpha=None, threads=1, verbose=False
):
    """Perform NB GLM regression to determine differentially enriched features

    Args:
        rust_mat (CscMatrix): Feature Barcode matrix in rust-pyo3 format
        rust_mat_t (CscMatrix): Transpose of FBM in rust-pyo3 format
        group_a (np.ndarray): cluster 1 indices
        group_b (np.ndarray): non cluster 1 indices
        model (str): nb|poisson|geometric
        cov (Optional[np.ndarray], optional): Any covariates. Defaults to None.
        alpha (Optional[np.ndarray], optional): Initial dispersion estimate. Defaults to None.
        threads (int, optional): number of threads. Defaults to 1.
        verbose (bool, optional): controls verbosity of debug print out. Defaults to False.

    Raises:
        KeyError: invalid model string

    Returns:
        pd.DataFrame: differential enrichment results
    """

    p = 2
    q = 0
    Na = len(group_a)
    Nb = len(group_b)
    M, N = rust_mat.shape()
    assert rust_mat_t.shape() == (N, M)
    assert N >= p
    assert Na >= 1
    assert Nb >= 1
    assert N == (Na + Nb)

    # add covariates if supplied
    if cov:
        X = np.zeros((N, p + len(cov)))
        X[group_a, 0] = 1
        X[group_b, 1] = 1
        for var in cov:
            X[:, p] = var.squeeze()
            p += 1
    else:
        X = np.zeros((N, p))
        X[group_a, 0] = 1
        X[group_b, 1] = 1

    # init alpha
    # NOTE: Can use shrinkage estimators on empirical MoMs
    if model == "nb":
        assert alpha is not None
        alpha_est = alpha.copy()
        alpha_est[alpha_est <= 0] = 1e-6  # set zero dispersion to nonzero small value
    elif model == "poisson":
        alpha_est = np.full(M, 1e-4)
    elif model == "geometric":
        alpha_est = np.full(M, 1.0)
    else:
        raise KeyError("Valid values for 'model' are 'nb', 'poisson' or 'geometric'")

    # estimation happens in pyo3-rust library
    # set MKL threads to 1 since we observe threads spinning waiting for work
    # the for loop over features is parallelized
    mkl_state = MklState.disable()
    B, varB, alpha_est = run_nb_glm_py(rust_mat_t, X, alpha_est, threads)
    # restore
    mkl_state.restore()

    # get pvals:
    log2fc, t_stat, p_values, mean_a, mean_b = wald_1x1_nb2_py(B, varB, N - p - q)

    # apply bh correction
    adj_p_values = p_values.copy()
    adj_p_values = cr_diffexp.adjust_pvalue_bh(p_values)

    # adjust low counts by fisher's test
    select_fisher = np.where(adj_p_values > 0.49)[0]
    log2fc_fisher, p_values_fisher = fisher_2x2_py(
        rust_mat,
        group_a.astype(np.uint64),
        group_b.astype(np.uint64),
        select_fisher.astype(np.uint64),
    )
    log2fc[select_fisher] = log2fc_fisher
    p_values[select_fisher] = p_values_fisher

    # adjust pvals
    adj_p_values = p_values.copy()
    adj_p_values = cr_diffexp.adjust_pvalue_bh(p_values)

    de_result = pd.DataFrame(
        {
            "tested": np.full(len(mean_a), True),
            "mean_a": mean_a,
            "mean_b": mean_b,
            "log2_fold_change": log2fc,
            "p_value": p_values,
            "adjusted_p_value": adj_p_values,
            "t_stats": t_stat,
            "alpha": alpha_est,
        }
    )

    return de_result


def run_differential_expression(
    mm: spmatrix,
    mm_t: csc_matrix,
    clusters: np.ndarray,
    model: str,
    impute_rest: bool = False,
    cov: Optional[np.ndarray] = None,
    alpha: Optional[np.ndarray] = None,
    threads: int = 1,
    verbose: bool = False,
) -> cr_diffexp.DifferentialExpression:
    """Compute differential expression for each cluster vs other cells

    Args:
        mm (spmatrix): Feature barcode matrix with features as columns, cells as rows
        mm_t (spmatrix): Transpose of FBM above with dtype float64
        clusters (np.ndarray): cluster label for each cell (1-based)
        model (str): GLM being fitted (poisson|nb|geometric)
        impute_rest (bool, optional): Impute for special case of 2 clusters. Defaults to False.
        cov (Optional[np.ndarray], optional): Any covariates. Defaults to None.
        alpha (Optional[np.ndarray], optional): Initial dispersion estimate. Defaults to None.
        threads (int, optional): Number of threads. Defaults to 1.
        verbose (bool, optional): print number of iterations. Defaults to False.

    Raises:
        ValueError: if invalid model is specified

    Returns:
        cr_diffexp.DifferentialExpression: DE results
    """

    if model not in ALLOWED_NB2_MODELS:
        raise ValueError("{} model is not supported".format(model))

    n_clusters = np.max(clusters)
    n_feats, n_cells = mm.shape
    assert mm_t.shape == (n_cells, n_feats), "mm_t must be a transpose of mm"
    assert isinstance(mm_t, csc_matrix), "mm_t must be csc_matrix"

    # Create a numpy array with 3*K columns;
    # each group of 3 columns is mean, log2, pvalue for cluster i
    all_de_results = np.zeros((n_feats, 3 * n_clusters))

    # construct rust-pyo3 versions of matrices
    rust_mat = CscMatrix(mm.data.astype(np.float64), mm.indices, mm.indptr, n_feats, n_cells)
    rust_mat_t = CscMatrix(
        mm_t.data.astype(np.float64), mm_t.indices, mm_t.indptr, n_cells, n_feats
    )

    # compute empirical dispersion if not supplied
    if alpha is None:
        alpha = empirical_dispersion(mm)
    de_result = None
    for cluster in range(1, 1 + n_clusters):
        print("*" * 80)
        print("Computing DE for cluster %d..." % cluster)
        sys.stdout.flush()

        # number of cells >= 2 + number of covariates. For peaks we use one covariate - depth; for
        # motifs we use depth and gc content
        if n_clusters == 1 or n_cells < 4:
            return cr_diffexp.DifferentialExpression(all_de_results)

        if n_clusters == 2 and cluster == 2 and impute_rest:
            # pylint: disable=unsubscriptable-object
            # NOTE: under Wald test, the pvals should be symmetric
            # so one can derive the values for the "rest" group using previous de result
            all_de_results[:, 0 + 3 * (cluster - 1)] = de_result["mean_b"]
            all_de_results[:, 1 + 3 * (cluster - 1)] = -1.0 * de_result["log2_fold_change"]
            all_de_results[:, 2 + 3 * (cluster - 1)] = 1.0 - de_result["adjusted_p_value"]
            return cr_diffexp.DifferentialExpression(all_de_results)

        in_cluster = clusters == cluster
        group_a = np.flatnonzero(in_cluster)
        group_b = np.flatnonzero(np.logical_not(in_cluster))

        # compute DE

        de_result = NBGLM_differential_expression(
            rust_mat, rust_mat_t, group_a, group_b, model, cov, alpha, threads, verbose
        )
        all_de_results[:, 0 + 3 * (cluster - 1)] = de_result["mean_a"]
        all_de_results[:, 1 + 3 * (cluster - 1)] = de_result["log2_fold_change"]
        all_de_results[:, 2 + 3 * (cluster - 1)] = de_result["adjusted_p_value"]

    return cr_diffexp.DifferentialExpression(all_de_results)
