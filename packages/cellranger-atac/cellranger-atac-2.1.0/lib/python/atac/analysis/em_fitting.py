# Copyright (c) 2020 10x Genomics, Inc. All rights reserved.
"""
Tools for parameter estimation, used for EM model fitting for discrete mixture models
"""

from __future__ import absolute_import, annotations, print_function, division

from abc import ABCMeta, abstractmethod
from typing import NamedTuple

import numpy as np
import scipy.stats
from scipy.special import logsumexp


class MixtureComponent(metaclass=ABCMeta):
    """
    Class to represent a single component of a mixture model. Takes as input:
    name: string, name for the component
    params: a namedtuple of parameters for the distribution

    Additionally, the following methods must be defined for any subclass
    new: a constructor
    validate: assert parameter ranges

    pmf: a function pmf(count vector, params) -> probability vector
    fit: a parameter estimator function (count vector, log-weight vector) that returns the
         relative difference between the previously stored parameters. The log-weight vector
         represents the log of multiplicity of each count in the count vector.
         count data of [1, 2, 3, 3] would be represented as
         count vector = [1, 2, 3]
         log weight vector = [0, 0, log(2)]
    """

    __slots__ = ("name", "params")

    def __init__(self, name, *params):
        self.name = None
        self.params = None
        self.new(name, *params)
        if not hasattr(self.params, "_asdict"):
            raise TypeError("params must be a namedtuple or implement an _asdict() method")
        self.validate()

    def to_dict(self):
        """convert the component parameter info into a dictionary"""
        param_dict = {}
        for k, v in self.params._asdict().items():
            param_dict["{}_{}".format(k, self.name)] = v
        return param_dict

    @abstractmethod
    def new(self, name, *params):
        pass

    @abstractmethod
    def validate(self):
        pass

    @abstractmethod
    def logpmf(self, counts):
        pass

    @abstractmethod
    def pmf(self, counts):
        pass

    @abstractmethod
    def fit(self, counts, log_weights):
        pass


class GeometricComponent(MixtureComponent):
    class Params(NamedTuple):
        p: float

    def new(self, name, *params):
        self.name = name
        self.params = GeometricComponent.Params(*params)

    def __str__(self):
        return "GeometricComponent(name={}, p={})".format(self.name, self.params.p)

    def validate(self):
        assert self.name
        assert self.params.p >= 0

    def pmf(self, counts):
        assert counts.min() >= 0
        return scipy.stats.geom.pmf(counts, self.params.p)

    def logpmf(self, counts):
        assert counts.min() >= 0
        return scipy.stats.geom.logpmf(counts, self.params.p)

    def fit(self, counts, log_weights):
        assert log_weights.shape == counts.shape
        assert counts.min() >= 0

        log_p = logsumexp(log_weights) - logsumexp(log_weights + np.log(counts))
        p = np.exp(log_p)

        delta = abs(p - self.params.p) / self.params.p if self.params.p > 0 else 0.0

        self.params = GeometricComponent.Params(p)
        return {"delta_p": delta}


class NegativeBinomialComponent(MixtureComponent):
    class Params(NamedTuple):
        mean: float
        alpha: float

    def new(self, name, *params):
        mean, alpha = params
        self.name = name
        self.params = NegativeBinomialComponent.Params(mean=mean, alpha=alpha)

    def __str__(self):
        return "NegativeBinomialComponent(name={}, mean={}, alpha={})".format(
            self.name, self.params.mean, self.params.alpha
        )

    def validate(self):
        assert self.name
        assert self.params.mean >= 0
        assert self.params.alpha >= 0

    def pmf(self, counts):
        assert counts.min() >= 0
        if self.params.alpha == 0:
            return scipy.stats.poisson.pmf(counts, self.params.mean)
        n = 1 / self.params.alpha
        p = 1 / (1 + self.params.mean * self.params.alpha)
        return scipy.stats.nbinom.pmf(counts, n, p)

    def logpmf(self, counts):
        assert counts.min() >= 0
        if self.params.alpha == 0:
            return scipy.stats.poisson.logpmf(counts, self.params.mean)
        n = 1 / self.params.alpha
        p = 1 / (1 + self.params.mean * self.params.alpha)
        return scipy.stats.nbinom.logpmf(counts, n, p)

    def fit(self, counts, log_weights):
        weights = np.exp(log_weights)
        assert weights.shape == counts.shape
        assert counts.min() >= 0
        assert weights.min() >= 0
        mean = weighted_mean(counts, weights)
        alpha = MLE_dispersion(counts, weights)

        delta_mean = (
            abs(mean - self.params.mean) / self.params.mean if self.params.mean > 0 else 0.0
        )
        delta_alpha = (
            abs(alpha - self.params.alpha) / self.params.alpha if self.params.alpha > 0 else 0.0
        )
        self.params = NegativeBinomialComponent.Params(mean, alpha)
        return {"delta_mean": delta_mean, "delta_alpha": delta_alpha}


class MixtureModelError(Exception):
    pass


class MixtureModel(object):
    """Representation of a mixture model consisting of multiple mixture components. Has functions
    that can be used to easily write an EM.

    Data members:
    - ncomps: number of components
    - mix_prob: vector of mixture probabilities
    - components: vector of MixtureComponent objects
    - index: map from component name to component index
    - resp: the responsibility of each component in explaining some count data
    """

    def __init__(self, components, mix_prob=None):
        """
        Create a mixture model.
        components: vector of MixtureComponent objects
        mix_prob: None or a vector of mixture probabilities
        """
        self.components = components
        self.ncomps = len(self.components)
        assert len({comp.name for comp in components}) == self.ncomps
        self.index = {comp.name: i for i, comp in enumerate(components)}
        if mix_prob:
            self.mix_prob = np.array(mix_prob, dtype=np.float64)
            assert (self.mix_prob >= 0).all()
            self.mix_prob /= np.sum(self.mix_prob)
        else:
            self.mix_prob = None
        # log responsibilities of each component to explain each data point
        # log P(comp=i | count=j)
        self.resp = None

    def __str__(self):
        pieces = []
        for i in range(self.ncomps):
            pieces.append("Component {}:\n{}".format(i, str(self.components[i])))
            pieces.append("Mixture prob = {}".format(self.mix_prob[i]))
            pieces.append("-" * 80)
        return "\n".join(pieces)

    def get_component_contribution(self, name, xvals):
        """Given some counts xvals and a component specified by name, evaluate
            Prob(xvals | z) x pi
        where pi = mixture probability, z represents the parameters of the component
        """

        ci = self.get_component_index(name)
        assert ci is not None
        comp = self.components[ci]
        prob = comp.pmf(xvals)
        prob *= self.mix_prob[ci]
        return prob

    def swap(self, name1, name2):
        """
        Swap two mixture components. Assumes that they are the same type of MixtureComponent
        but does not check it.
        """
        i1 = self.index.get(name1, -1)
        i2 = self.index.get(name2, -1)
        assert i1 >= 0
        assert i2 >= 0
        assert i1 != i2
        assert type(self.components[i1]) == type(self.components[i2])
        params1 = self.components[i1].params
        params2 = self.components[i2].params
        self.components[i1].params, self.components[i2].params = params2, params1
        self.mix_prob[i1], self.mix_prob[i2] = self.mix_prob[i2], self.mix_prob[i1]

    def get_component_index(self, name):
        ci = self.index.get(name, None)
        return ci

    def get_component(self, name):
        ci = self.get_component_index(name)
        assert ci is not None
        return self.components[ci]

    def is_initialized(self):
        return (self.resp is not None) and (self.mix_prob is not None)

    def initialize_responsibilities(self, resp):
        assert resp.shape[0] == self.ncomps
        self.resp = resp
        self.resp -= logsumexp(self.resp, axis=0)

    def get_parameters_dict(self):
        """Store all the information about the mixture model in a dictionary"""
        params = {}
        for ci in range(self.ncomps):
            comp = self.components[ci]
            params.update(comp.to_dict())
            params["mix_prob_{}".format(comp.name)] = self.mix_prob[ci]
        return params

    def calculate_responsibilities(self, counts):
        """
        Given the current values of the parameters evaluate the log responsibilities, or the
        posterior probability log P(component | counts) (E step)
        """
        if self.resp is None:
            self.resp = np.zeros((self.ncomps, len(counts)), dtype=np.float64)
        assert self.mix_prob is not None
        for ci in range(self.ncomps):
            comp = self.components[ci]
            self.resp[ci, :] = comp.logpmf(counts) + np.log(self.mix_prob[ci])
        lognorm = logsumexp(self.resp, axis=0)
        nz_prob = ~np.isinf(lognorm)
        if nz_prob.sum() == 0:
            raise MixtureModelError("All responsibilities are zero (E step failed)")
        self.resp[:, nz_prob] -= lognorm[nz_prob]

    def estimate_parameters(self, counts, log_multiplicity):
        """
        Given counts and the log multiplicity estimate the mixture probs and the parameters
        of the mixture components. Assumes that the responsibilities have been initialized.
        (M step)
        """
        assert counts.shape[-1] == log_multiplicity.shape[-1] == self.resp.shape[-1]
        assert self.resp is not None

        temp = log_multiplicity + self.resp

        log_new_mix_prob = logsumexp(temp, axis=1)
        log_new_mix_prob_norm = logsumexp(log_new_mix_prob)
        if np.isinf(log_new_mix_prob_norm):
            raise MixtureModelError(
                "Mixture probabilities {} cannot be normalized (M step failed)".format(
                    log_new_mix_prob
                )
            )
        log_new_mix_prob -= log_new_mix_prob_norm
        new_mix_prob = np.exp(log_new_mix_prob)

        delta_dict = {}
        for ci in range(self.ncomps):
            zero_filter = ~np.isinf(self.resp[ci])
            if zero_filter.sum() == 0:
                comp = self.components[ci]
                raise MixtureModelError(
                    "Responsibility for component {} (params: {}) "
                    "is zero for all count values (M step failed)".format(comp.name, comp.params)
                )
            delta_comp = self.components[ci].fit(counts[zero_filter], temp[ci, zero_filter])
            delta_dict.update(delta_comp)

        if self.is_initialized():
            for i in range(self.ncomps):
                new_mix = new_mix_prob[i]
                old_mix = self.mix_prob[i]
                delta_dict["mix_prob_{}".format(i)] = (
                    abs(new_mix - old_mix) / old_mix if old_mix > 0.0 else 0.0
                )

        self.mix_prob = new_mix_prob
        return delta_dict


def weighted_mean(counts: np.ndarray, weights=None):
    """The mean of the input counts.  If weights are provided, returns the weighted mean."""
    if weights is None:
        return counts.mean()
    return (counts * weights).sum() / weights.sum()


def weighted_variance(counts, weights=None):
    """The variance of the input counts.  If weights are provided, returns the weighted variance."""
    if weights is None:
        return counts.var()
    sum_of_squares = (weighted_mean(counts, weights) - counts) ** 2
    return (sum_of_squares * weights).sum() / weights.sum()


def MOM_dispersion(counts, weights=None):
    """Simple --- and biased --- estimator of the dispersion parameter of a negative binomial distribution
    using the method of moments.
    """
    mu = weighted_mean(counts, weights)
    var = weighted_variance(counts, weights)
    alpha = (var - mu) / np.power(mu, 2)
    return max(1e-6, alpha)


def MLE_dispersion(counts, weights=None, maxiter=10000, epsilon=1e-6, errtol="relative"):
    """
    Borrowed from RiboPy (https://bitbucket.org/caethan/ribopy/src):
    Fixed point estimation of the dispersion (alpha) parameter for a negative binomial distribution.
    Provides the maximum likelihood estimate for alpha.

    :param counts: numpy array of raw count or counter keys of the raw count
    :param weights: numpy array of counter values of the raw count.
    """

    def abs_check(new, old):
        return abs(new - old) < epsilon

    def rel_check(new, old):
        return 2 * abs(new - old) / (new + alpha) < epsilon

    assert errtol in ["relative", "absolute"]
    check = rel_check if errtol == "relative" else abs_check

    def G_mle(alpha, mu, v, indices, weights=None, weight_sum=None):
        """This is the fixed point for the maximum likelihood estimate of alpha (the NB dispersion parameter).

        alpha - float in (0, inf), the dispersion parameter
        mu - float, the fitted mean of the sample data

        returns a single float that is a new estimate of alpha
        """
        tmp = alpha * v
        subfunc_cumsum = np.cumsum((tmp) / (1 + tmp))
        est_mean = (
            subfunc_cumsum[indices].mean()
            if weights is None
            else (subfunc_cumsum[indices] * weights).sum() / weight_sum
        )
        return np.log(1 + alpha * mu) / (mu - est_mean)

    if (counts <= 0).all():
        return 1e-6
    alpha = MOM_dispersion(counts, weights)
    # If max(counts) is too big, this will be slow; fall back on method-of-moments
    if counts.max() > 1e8:
        return alpha

    v = np.arange(counts.max())
    indices = counts - 1
    indices[indices < 0] = 0

    weight_sum = None if weights is None else weights.sum()
    mu = weighted_mean(counts, weights)
    var = weighted_variance(counts, weights)
    if var <= mu:
        return alpha
    for _ in range(maxiter):
        new = G_mle(alpha, mu, v, indices, weights, weight_sum)
        if check(new, alpha):
            break
        alpha = new
    return new


def MLE_geometric_parameter(counts, weights=None, exclude_zeros=True):
    """Returns the maximum likelihood estimate for a geometric fit to the data, with or without including zeros
    in the count data."""
    decrement = 1 if exclude_zeros else 0
    if weights is None:
        N = len(counts)
        k_total = sum(counts) - N * decrement
    else:
        N = sum(weights)
        k_total = sum(counts * weights) - N * decrement
    return N / (N + k_total)
