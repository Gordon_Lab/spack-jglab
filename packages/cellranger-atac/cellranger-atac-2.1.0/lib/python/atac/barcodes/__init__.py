# Copyright (c) 2019 10x Genomics, Inc. All rights reserved.
"""
Barcode handling functions, including query functions that override tenkit query
functions (Due to changes in tags used to store barcodes)
"""

from __future__ import absolute_import, division

import os
import json
from typing import List, Union, Set, Counter

from six import ensure_binary
from atac.constants import RAW_BARCODE_TAG, RAW_BARCODE_QUAL_TAG, PROCESSED_BARCODE_TAG
from cellranger.io import open_maybe_gzip
from tenkit.constants import HIGH_CONF_MAPQ, MIN_MATE_OFFSET_DUP_FILTER


def stringent_read_filter(bam_read, require_barcode):
    """Test for a very high-quality read. Only reads satisfying this predicate
    are used when computing summary dup rates to avoid spurious duplicates.
    Reads must have a high MAPQ, a perfect cigar, with a fragment size somewhat
    longer than the read length"""

    if bam_read.mapq < HIGH_CONF_MAPQ:
        return False

    if bam_read.is_secondary:
        return False

    if require_barcode and get_read_barcode(bam_read) is None:
        return False

    if len(bam_read.cigar) != 1:
        return False

    mate_diff_contig = bam_read.tid != bam_read.rnext
    fragment_length = abs(bam_read.pos - bam_read.mpos)
    long_fragment = fragment_length >= MIN_MATE_OFFSET_DUP_FILTER

    return long_fragment or mate_diff_contig


def whitelist_mem_gb(filename):
    """Return memory consumption in loading the whitelist file"""
    path = barcode_whitelist_path(filename)
    return os.path.getsize(path) / 1e9


def barcode_whitelist_path(filename):
    """Barcode whitelist is just a text file of valid barcodes, one per line.
    Lines containing the '#' character are ignored"""
    if filename is None:
        return None
    code_path = os.path.dirname(os.path.abspath(__file__))
    if os.path.exists(filename):
        return filename
    elif os.path.exists(os.path.join(code_path, "{}.txt".format(filename))):
        return os.path.join(code_path, "{}.txt".format(filename))
    elif os.path.exists(os.path.join(code_path, "{}.txt.gz".format(filename))):
        return os.path.join(code_path, "{}.txt.gz".format(filename))
    else:
        raise ValueError("Whitelist {} not found".format(filename))


def load_barcode_whitelist(filename, ordered=False) -> Union[Set[bytes], List[bytes]]:
    """Barcode whitelists are text files of valid barcodes, one per line.
    Lines containing the '#' character are ignored
    """
    full_path = barcode_whitelist_path(filename)
    if full_path is None:
        return None
    with open_maybe_gzip(full_path, "rb") as infile:
        if ordered:
            barcodes = [line.strip() for line in infile if b"#" not in line]
        else:
            barcodes = {line.strip() for line in infile if b"#" not in line}
    return barcodes


def get_barcode_gem_group(barcode):
    """Checks a barcode sequence for gem group.  If none is present, returns None."""
    split = barcode.split(b"-")
    if len(split) == 2:
        barcode, gem_group = split
    else:
        gem_group = None
    return gem_group


def split_barcode_parts(barcode):
    """Splits a barcode sequence into part A, part C, part B excluding gem well"""
    barcode = barcode.split(b"-")[0]
    assert len(barcode) == 16
    part_a, part_c, part_b = barcode[:7], barcode[7:9], barcode[9 : 9 + 7]
    return part_a, part_c, part_b


def split_barcode_parts_gem_well(barcode):
    """Splits a barcode sequence into part A, part C, and part B sequences."""
    barcode, gem_well = barcode.split(b"-")
    assert len(barcode) == 16
    part_a, part_c, part_b = barcode[:7], barcode[7:9], barcode[9 : 9 + 7]
    return part_a, part_c, part_b, gem_well


def merge_barcode(part_a, part_c, part_b, gem_group=None):
    """Recreates a barcode from separate part a, b, and c pieces, and optionally
    gem group as well."""
    barcode = part_a + part_c + part_b
    if gem_group is not None:
        if isinstance(gem_group, str):
            return barcode + b"-" + ensure_binary(gem_group)
        elif isinstance(gem_group, bytes):
            return barcode + b"-" + gem_group
        else:
            return barcode + b"-" + ensure_binary(str(gem_group))
    return barcode


def query_barcode_subsequences(valid_barcodes):
    """Breaks down a list of valid barcode sequences into unique part A, B, and C
    subsequences, as well as possible gem groups."""
    part_a_seqs = {}
    part_b_seqs = {}
    part_c_seqs = set()
    gem_group_seqs = set()

    for barcode in valid_barcodes:
        part_a, part_c, part_b, gem_group = split_barcode_parts_gem_well(barcode)
        part_c_seqs.add(part_c)
        gem_group_seqs.add(gem_group)
        if part_c not in part_a_seqs:
            part_a_seqs[part_c] = set()
        if part_c not in part_b_seqs:
            part_b_seqs[part_c] = set()
        part_a_seqs[part_c].add(part_a)
        part_b_seqs[part_c].add(part_b)

    part_c_seqs = sorted(part_c_seqs)
    for part_c in part_c_seqs:
        part_a_seqs[part_c] = sorted(part_a_seqs[part_c])
        part_b_seqs[part_c] = sorted(part_b_seqs[part_c])

    return part_a_seqs, part_c_seqs, part_b_seqs, gem_group_seqs


def split_barcode_and_gem_group(barcode):
    """Splits a barcode sequence into a barcode and a gem group."""
    split = barcode.split(b"-")
    if len(split) == 2:
        # Remove the gem group from the barcode
        barcode, gem_group = split
    else:
        gem_group = None
    assert len(barcode) == 16
    return barcode, gem_group


def merge_barcode_and_gem_group(barcode, gem_group):
    """Merges sequence with gem group"""
    if gem_group is not None:
        if isinstance(gem_group, str):
            return barcode + b"-" + ensure_binary(gem_group)
        elif isinstance(gem_group, bytes):
            return barcode + b"-" + gem_group
        else:
            return barcode + b"-" + ensure_binary(str(gem_group))
    return barcode


def query_barcodes_and_gem_groups(valid_barcodes):
    """Breaks down a list of valid barcode sequences into barcodes - gem group
    subsequences."""
    barcode_seqs = {}
    gem_group_seqs = set()

    for barcode in valid_barcodes:
        assert b"-" in barcode
        barcode_seq, gem_group = barcode.split(b"-")
        gem_group_seqs.add(gem_group)
        if gem_group not in barcode_seqs:
            barcode_seqs[gem_group] = set()
        barcode_seqs[gem_group].add(barcode_seq)

    gem_group_seqs = sorted(gem_group_seqs)
    for gem_group in gem_group_seqs:
        barcode_seqs[gem_group] = sorted(barcode_seqs[gem_group])

    return barcode_seqs, gem_group_seqs


def _get_tag_if_exists_else_default(read, tag, default=None):
    return ensure_binary(read.get_tag(tag)) if read.has_tag(tag) else default


def get_read_raw_barcode(read, default=None):
    return _get_tag_if_exists_else_default(read, RAW_BARCODE_TAG, default)


def get_read_barcode_qual(read, default=None):
    tag = _get_tag_if_exists_else_default(read, RAW_BARCODE_QUAL_TAG, b"")
    return default if tag == b"" else tag


def get_read_barcode(read, default=None):
    """Returns the corrected barcode sequence for a read, or None if
    none exists."""
    tag = _get_tag_if_exists_else_default(read, PROCESSED_BARCODE_TAG, b"")
    return default if tag == b"" else tag


def load_barcode_counts(bc_counts_json) -> Counter[bytes]:
    """Load barcode counts JSON

    Args:
        bc_counts_json (bytes): Path to barcode counts JSON

    Returns:
        collections.Counter: Counter of barcode: count, where barcode is of type `bytes`
    """
    with open(bc_counts_json, "rb") as infile:
        barcode_counts = Counter({bc.encode(): count for bc, count in json.load(infile).items()})
    return barcode_counts
