# Copyright (c) 2019 10x Genomics, Inc. All rights reserved.
"""
Miscellaneous ATAC pipeline utility functions
"""

from __future__ import division, print_function, absolute_import

from collections import Counter, namedtuple
import numpy as np
from tenkit.reference import open_reference
from atac.constants import VALID_BASES as JASPAR_MOTIF_BASE_ORDER
from atac.tools.peaks import peak_reader

LOW_GC = 0.33
HIGH_GC = 0.7
NBINS = 25

GCInfo = namedtuple("GCInfo", ("peaks", "counter", "total_bases"))


def get_barcode_gc(ref_f, peaks_f, matrix):
    """Get mean GC% of peaks in a barcode"""
    genome_fa = open_reference(ref_f)
    peak_gc = np.array([get_peak_gc_counts(peak, genome_fa)[0] for peak in peak_reader(peaks_f)])
    barcode_gc = ((peak_gc * matrix.m) / np.array(matrix.m.sum(axis=0))).squeeze()
    return barcode_gc


def get_peak_gc_counts(peak, genome_fa):
    """Get GC% in base seq in a peak and nucleotide counts."""

    seq = genome_fa[peak.chrom][peak.start : peak.end].seq.upper()
    base_counter = {}
    base_counter["A"] = seq.count("A") + seq.count("a")
    base_counter["G"] = seq.count("G") + seq.count("g")
    base_counter["C"] = seq.count("C") + seq.count("c")
    base_counter["T"] = seq.count("T") + seq.count("t")
    base_counter["N"] = seq.count("N") + seq.count("n")
    peak_gc = (base_counter["G"] + base_counter["C"]) / sum(base_counter.values()) if seq else 0.0
    return peak_gc, base_counter


# pylint: disable=too-many-locals
def get_gcbinned_peaks_and_bg(peaks_file, reference_path, pseudocount=1.0):
    """Calculate the G-C and A-T % within the peaks for each bin."""

    def findbin(peakgc, gcbins):
        """Expected sorted GCbins"""
        for gc_val in gcbins:
            if gc_val[0] <= peakgc < gc_val[1]:
                return gc_val
        if abs(peakgc - gcbins[-1][1]) < 1e-5:
            return gcbins[-1]
        return None

    # get peak-GC distribution
    genome_fa = open_reference(reference_path)
    gcdist = [get_peak_gc_counts(peak, genome_fa)[0] for peak in peak_reader(peaks_file)]

    if not gcdist:
        return None

    # compute base background from peaks in bins
    # merge extreme GC bins with adjoining ones if they're too narrow for motif scanner to work
    # correctly
    gcbounds = []
    nbins = NBINS
    for n, gc_val in enumerate(
        np.percentile(gcdist, np.linspace(0, 100, nbins + 1, endpoint=True), interpolation="lower")
    ):
        if n in (0, nbins):
            gcbounds += [gc_val]
            continue
        if LOW_GC <= gc_val < HIGH_GC:
            gcbounds += [gc_val]
    gcbins = sorted(list(set(zip(gcbounds, gcbounds[1:]))))  # uniqify
    gcdict = {}
    base_counter = {}
    for gc_val in gcbins:
        gcdict[gc_val] = GCInfo([], Counter({base: 0 for base in JASPAR_MOTIF_BASE_ORDER}), None)

    for num, peak in enumerate(peak_reader(peaks_file)):
        peakgc, base_counter = get_peak_gc_counts(peak, genome_fa)
        gc_val = findbin(peakgc, gcbins)
        gcdict[gc_val].peaks.append(num)
        gcdict[gc_val].counter.update(base_counter)

    for gc_val in gcbins:
        cur_val = gcdict[gc_val]
        total_bases = sum(cur_val.counter.values())
        freq = {}
        for base in JASPAR_MOTIF_BASE_ORDER:
            freq[base] = np.divide(
                cur_val.counter[base] + pseudocount,
                total_bases + 4 * pseudocount,
                dtype=float,
            )

        # the freq is computed using only the "+" strand. To get the freq of both strand, average
        # A&T and G&C
        background = {}
        background["A"] = (freq["A"] + freq["T"]) / 2
        background["C"] = (freq["G"] + freq["C"]) / 2
        background["G"] = background["C"]
        background["T"] = background["A"]
        gcdict[gc_val] = GCInfo(
            cur_val.peaks, [background[base] for base in JASPAR_MOTIF_BASE_ORDER], total_bases
        )

    return gcdict
