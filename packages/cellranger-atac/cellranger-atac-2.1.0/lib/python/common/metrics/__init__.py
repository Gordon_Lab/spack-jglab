# Copyright (c) 2019 10x Genomics, Inc. All rights reserved.
"""
Tools for managing metrics with metadata attached.
"""

from __future__ import division, print_function, absolute_import

import os
import sys
import numbers

from collections import namedtuple
import pandas as pd
from six import ensure_str, text_type, binary_type


class MetricAnnotations(object):
    """Interfaces with a structured CSV file to generate metadata associated with
    specific metrics defined in the file.
    """

    def __init__(self, filename=None, species_list=None, species_is_library=False):
        """Load in metric information from the associated csv file.

        Key is the base metric handle as defined in the individual assays. Subkey refers
        to the modified handle by either prefixing an assay_type as atac_/rna_ and/or making
        the key species-specific. The subkey is what is output in the summary.json

        The metric_data dictionary is keyed on a tuple of both key and subkey to allow
        for instances where metric names i.e. keys overlap between the two assays.
        """
        MetricKey = namedtuple("MetricKey", ["key", "subkey", "include_species"])

        if filename is None:
            filename = "atac_metrics.csv"
        file_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), ensure_str(filename))
        metric_data = pd.read_csv(file_path, float_precision="high")
        # Makes empty values None instead of NaN
        metric_data = metric_data.where(pd.notnull(metric_data), None)

        def get_key_value(key, add_species_name, metric_type):
            print(key, add_species_name, metric_type)
            if add_species_name:
                if metric_type is None:
                    # Species suffix, no atac/rna prefix
                    return "{}_{}".format(key, add_species_name)
                else:
                    # atac/rna prefix
                    if metric_type == "atac":
                        # atac uses species suffixes
                        return "{}_{}_{}".format(metric_type, key, add_species_name)
                    elif metric_type == "rna":
                        # rna uses species prefixes
                        return "{}_{}_{}".format(metric_type, add_species_name, key)
            elif metric_type is None:
                return key
            return "{}_{}".format(metric_type, key)

        self.metric_data = {}

        for row in metric_data.itertuples():
            try:
                # Try to query the "type" column of the CSV, which should contain which kind of data
                # this key is meant for.  If it doesn't exist or is null for this metric, fall back
                # to None
                metric_type = row.type if pd.notnull(row.type) else None
            except AttributeError:
                metric_type = None
            if metric_type is not None and metric_type not in ["atac", "rna"]:
                raise ValueError(
                    "Only 'atac' or 'rna' metric types are supported. Invalid type: {}".format(
                        metric_type
                    )
                )
            if row.is_species_specific:
                if species_list is None or not species_list:
                    raise ValueError("Must provide a species list for species-specific metrics")
                for species in species_list:
                    # Species suffixes/prefixes are added for: RNA metrics, aggr samples and barnyard expts
                    include_species = (
                        species
                        if ((len(species_list) > 1) or species_is_library or (metric_type == "rna"))
                        else False
                    )
                    subkey = get_key_value(row.key, include_species, metric_type)
                    self.metric_data[
                        MetricKey(
                            key=row.key,
                            subkey=subkey,
                            include_species="" if not include_species else species,
                        )
                    ] = metric_data.iloc[row.Index]
            else:
                subkey = get_key_value(row.key, False, metric_type)
                self.metric_data[
                    MetricKey(key=row.key, subkey=subkey, include_species="")
                ] = metric_data.iloc[row.Index]

    def gen_metric(self, key, value, subkey=None, species=None, debug=True):
        """Returns a single metric object for the given key and value.  Alerts are
        raised when the metric falls outside normal range, which depends on debug
        status.
        """
        metric_keys = [key_tuple for key_tuple in self.metric_data if key_tuple.key == key]

        if subkey is not None:
            metric_keys = [key_tuple for key_tuple in metric_keys if key_tuple.subkey == subkey]

        if len(metric_keys) > 1:
            sys.stderr.write(
                "{} is duplicated in the registered metrics, please also provide the subkey\n".format(
                    key
                )
            )
            return None

        metric_info = self.metric_data[metric_keys[0]]
        name = metric_info.full_name
        alert_name = metric_info.alert_name
        if species is not None and species:
            key += "_{species}".format(**locals())
            name += " ({species})".format(**locals())
            # alert_name may not be specified for some metrics, in which case an alert
            # can't be raised, therefore no need to worry about its species specific
            # name format.
            if alert_name is not None:
                alert_name += " ({species})".format(**locals())

        description = metric_info.description if debug else metric_info.description_cs
        acceptable = metric_info.acceptable if debug else metric_info.acceptable_cs
        targeted = metric_info.targeted if debug else metric_info.targeted_cs

        return Metric(
            key,
            name,
            value,
            description=description,
            acceptable=acceptable,
            targeted=targeted,
            evaluate_type=metric_info.evaluate_type,
            format_type=metric_info.format_type,
            category=metric_info.category,
            alert_name=alert_name,
            subkey=metric_keys[0].subkey,
        )

    def compile_summary_metrics(self, value_dict, keys=None, species_list=None):
        """Processes a metrics dictionary and select summary metrics based on 2nd
        column in metrics.csv for keys provided or all registered keys in metrics.csv
        """
        metrics = []
        if keys is not None:
            for key in keys:
                keys_tuple = [key_tuple for key_tuple in self.metric_data if key_tuple.key in keys]
                if len(keys_tuple) == 0:
                    sys.stderr.write("{} not found in registered metrics\n".format(key))
                    continue
                for key_tuple in keys_tuple:
                    metric_info = self.metric_data[key_tuple]
                    if not metric_info.summary:
                        continue
                    if key_tuple.subkey in value_dict:
                        if species_list is not None and len(species_list) > 1:
                            species = (
                                None
                                if len(key_tuple.include_species) == 0
                                else key_tuple.include_species
                            )
                            metrics += [
                                self.gen_metric(
                                    key_tuple.key,
                                    value_dict[key_tuple.subkey],
                                    key_tuple.subkey,
                                    species,
                                )
                            ]
                        else:
                            metrics += [
                                self.gen_metric(
                                    key_tuple.key, value_dict[key_tuple.subkey], key_tuple.subkey
                                )
                            ]
        else:
            for key_tuple, metric_info in self.metric_data.items():
                if not metric_info.summary:
                    continue
                if key_tuple.subkey in value_dict:
                    if species_list is not None and len(species_list) > 1:
                        species = (
                            None
                            if len(key_tuple.include_species) == 0
                            else key_tuple.include_species
                        )
                        metrics += [
                            self.gen_metric(
                                key_tuple.key,
                                value_dict[key_tuple.subkey],
                                key_tuple.subkey,
                                species,
                            )
                        ]
                    else:
                        metrics += [
                            self.gen_metric(
                                key_tuple.key, value_dict[key_tuple.subkey], key_tuple.subkey
                            )
                        ]
        return metrics

    def gen_metric_helptext(self, keys, metric_type=None):
        """Processes a metrics dictionary and generates helptext for keys if present in
        metrics.csv. In case of overlapping metric keys between assays, the metric_type flag
        can be used to restrict to keys from a single assay
        """
        keys_tuple = [key_tuple for key_tuple in self.metric_data if key_tuple.key in keys]
        if metric_type is not None:
            keys_tuple = [
                key_tuple
                for key_tuple in keys_tuple
                if self.metric_data[key_tuple].get("type") == metric_type
            ]

        # sort to match the input order of metric keys
        key_order = {k: i for i, k in enumerate(keys)}
        keys_tuple = sorted(keys_tuple, key=lambda x: (key_order.get(x.key), x.subkey))

        output = []
        for key_tuple in keys_tuple:
            metric_info = self.metric_data[key_tuple]
            if metric_info.help_description is not None:
                output.append([metric_info.full_name, [metric_info.help_description]])
            else:
                sys.stderr.write("{} not found in registered metrics\n".format(key_tuple.key))
        return output

    # pylint: disable=too-many-arguments
    def gen_metric_list(
        self,
        value_dict,
        keys,
        species_list=None,
        metric_type=None,
        debug=True,
        species_is_library=False,
    ):
        """Returns a list of metric objects for the provided keys, using the value
        dictionary to get values for each metric.  When metrics are species-specific, a
        list of species is required. Species_is_library is used when multiple libraries
        are aggregated and the library ID is used as a species list to differentiate
        between the metrics.

        In case of overlapping metric keys between assays, the metric_type flag can be used to
        restrict to keys from a single assay.

        Alerts are raised when metrics fall outside normal ranges, which depends on debug status.
        """
        keys_tuple = [key_tuple for key_tuple in self.metric_data if key_tuple.key in keys]
        if metric_type is not None:
            keys_tuple = [
                key_tuple
                for key_tuple in keys_tuple
                if self.metric_data[key_tuple].get("type") == metric_type
            ]

        # sort to match the input order of metric keys
        key_order = {k: i for i, k in enumerate(keys)}
        if species_list is not None and len(species_list) > 1:
            sp_order = {sk: i for i, sk in enumerate(species_list)}
        else:
            sp_order = {}
        keys_tuple = sorted(
            keys_tuple, key=lambda x: (key_order.get(x.key), sp_order.get(x.include_species))
        )

        output = []
        for key_tuple in keys_tuple:
            if self.metric_data[key_tuple].is_species_specific:
                if species_list is None or not species_list:
                    raise ValueError("Must provide a species list for species-specific metrics")
                species = None if len(key_tuple.include_species) == 0 else key_tuple.include_species
                if key_tuple.subkey in value_dict:
                    output.append(
                        self.gen_metric(
                            key_tuple.key,
                            value_dict[key_tuple.subkey],
                            key_tuple.subkey,
                            species if ((len(species_list) > 1) or species_is_library) else None,
                            debug,
                        )
                    )
                else:
                    sys.stderr.write("{} not found in metrics\n".format(key_tuple.subkey))
            else:
                if key_tuple.subkey in value_dict:
                    output.append(
                        self.gen_metric(
                            key_tuple.key,
                            value_dict[key_tuple.subkey],
                            key_tuple.subkey,
                            debug=debug,
                        )
                    )
                else:
                    sys.stderr.write("{} not found in metrics\n".format(key_tuple.subkey))
        return output


# pylint: disable=too-many-instance-attributes
class Metric(object):
    """Contains metadata about a single metric along with methods for evaluating its
    value with respect to that metadata.
    """

    # pylint: disable=too-many-arguments
    def __init__(
        self,
        key,
        name,
        value,
        description=None,
        acceptable=None,
        targeted=None,
        evaluate_type=None,
        format_type=None,
        category=None,
        alert_name=None,
        subkey=None,
    ):
        self.key = key
        self.name = name
        self.value = value
        self.description = description
        try:
            self.acceptable = float(acceptable)
        except (ValueError, TypeError):
            self.acceptable = acceptable
        try:
            self.targeted = float(targeted)
        except (ValueError, TypeError):
            self.targeted = targeted
        self.alert_name = alert_name

        eval_function_map = {
            None: self._always_true,
            "lt": self._less_than,
            "gt": self._greater_than,
            "range": self._in_range,
            "exists": self._exists,
            "is_true": self._is_true,
            "is_false": self._is_false,
            "is_value": self._is_value,
            "is_not_value": self._is_not_value,
        }
        if evaluate_type not in eval_function_map:
            raise ValueError("Unknown evaluation type: {}".format(evaluate_type))

        self.evaluate_type = evaluate_type
        self.evaluation_function = eval_function_map[evaluate_type]

        if format_type is None:
            format_type = "flat"
        if format_type not in ["flat", "float", "percentage", "int", "ppm"]:
            raise ValueError("Unknown format type: {}".format(format_type))
        self.format_type = format_type

        if category is None:
            category = "General"
        self.category = category

        if subkey is None:
            subkey = key
        self.subkey = subkey

    def gen_metric_dict(self, default_threshold=None):
        """Returns metric information in the form needed for hero metric display."""
        translate_dict = {"VALID": "pass", "WARN": "warn", "ERROR": "error"}
        if default_threshold is not None:
            assert default_threshold in ["pass", "warn", "error"]
            threshold = default_threshold
        else:
            threshold = translate_dict[self.threshold_type]
        return {"threshold": threshold, "metric": self.value_string, "name": self.name}

    @property
    def alarm_dict(self):
        """Format metric information as needed for raising a websummary alert."""
        threshold = self.threshold_type
        if threshold == "VALID":
            return {}
        return {
            "raw_value": self.value,
            "formatted_value": self.value_string,
            "raised": True,
            "parent": self.key,
            "title": self.alert_name,
            "message": self.description,
            "level": threshold,
            "test": "",
            "id": self.key,
        }

    # Evaluation methods
    @staticmethod
    def _less_than(value, target):
        return value < target

    @staticmethod
    def _greater_than(value, target):
        return value > target

    @staticmethod
    def _in_range(value, target):
        return (value > target[0]) and (value < target[1])

    @staticmethod
    def _exists(value, target):  # pylint: disable=unused-argument
        return value is not None and str(value).lower() != "nan"

    @staticmethod
    def _is_true(value, target):  # pylint: disable=unused-argument
        return value is True

    @staticmethod
    def _is_false(value, target):  # pylint: disable=unused-argument
        return value is False

    @staticmethod
    def _is_value(value, target):
        return value == target

    @staticmethod
    def _is_not_value(value, target):
        return value != target

    @staticmethod
    def _always_true(value, target):  # pylint: disable=unused-argument
        return True

    @property
    def threshold_type(self):  # pylint: disable=too-many-return-statements
        """Checks the metric value against the given metric thresholds."""
        if self.targeted is None and self.acceptable is None:
            return "VALID"
        elif self.targeted is None:
            # Only acceptable - error if we don't meet them.
            if self._exists(self.value, None) and self.evaluation_function(
                self.value, self.acceptable
            ):
                return "VALID"
            return "ERROR"
        elif self.acceptable is None:
            # Only targets - warn if we don't meet them.
            if self._exists(self.value, None) and self.evaluation_function(
                self.value, self.targeted
            ):
                return "VALID"
            return "WARN"

        # Both set - error/warn depending on which we meet.
        if self._exists(self.value, None) and self.evaluation_function(self.value, self.targeted):
            return "VALID"
        elif self._exists(self.value, None) and self.evaluation_function(
            self.value, self.acceptable
        ):
            return "WARN"
        return "ERROR"

    @property
    def color(self):
        """Display colors based on whether the metric meets thresholds."""
        if self.targeted is None and self.acceptable is None:
            # No targeting, return black
            return "black"
        threshold = self.threshold_type
        if threshold == "VALID":
            return "green"
        elif threshold == "WARN":
            return "orange"
        return "red"

    @property
    def acceptable_string(self):
        return self._format_target_value(self.acceptable)

    @property
    def targeted_string(self):
        return self._format_target_value(self.targeted)

    @property
    def value_string(self):
        return self._format_value(self.value)

    # pylint: disable=too-many-return-statements
    def _format_target_value(self, value):
        """Formats the targeting values for display."""
        if value is None or value == "NaN":
            return ""

        if self.evaluate_type == "exists":
            return "Exists"
        if self.evaluate_type == "is_true":
            return "True"
        if self.evaluate_type == "is_false":
            return "False"

        if self.evaluate_type == "lt":
            return "< {}".format(self._format_value(value))
        if self.evaluate_type == "gt":
            return "> {}".format(self._format_value(value))
        if self.evaluate_type == "range":
            return "{} - {}".format(self._format_value(value[0]), self._format_value(value[1]))
        return ""

    def _format_value(self, value):
        """Formats the metric value for display."""
        if value is None or value == "NaN":
            return "None"

        if self.format_type == "flat":
            return "{}".format(value)
        elif self.format_type == "float":
            return "{:,.2f}".format(value)
        elif self.format_type == "percentage":
            # if value is infinite type(value) = 'unicode'
            if isinstance(value, numbers.Number):
                return "{:.1%}".format(value)
            else:
                return "{}%".format(value)
        elif self.format_type == "int":
            return "{:,.0f}".format(value)
        elif self.format_type == "ppm":
            return "{:,.1f} ppm".format(value * 1e6)
        return ""


def format_metric_print_value(val):
    """Format metric values for printing in summary CSV

    Args:
        val (Any): metric value to be formatted (int/float/str)

    Returns:
        str: formatted value
    """
    if val is None:
        return "nan"
    if isinstance(val, numbers.Integral):
        return str(val)
    if isinstance(val, numbers.Real):
        return "{:.4f}".format(val)
    if isinstance(val, (binary_type, text_type)):
        return ensure_str(val)
    raise TypeError("Unexpected type supplied")
