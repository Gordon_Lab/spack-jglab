#!/usr/bin/env python
#
# Copyright (c) 2014 10X Genomics, Inc. All rights reserved.
#

from __future__ import absolute_import, print_function

import re
from six import iteritems
from tenkit.regions import Regions
from tenkit.constants import (
    SAMPLE_INDEX_TAG,
    SAMPLE_INDEX_QUAL_TAG,
)


def get_locus_info(locus):
    """Returns chrom, start and stop from locus string.
    Enforces standardization of how locus is represented.
    chrom:start_stop (start and stop should be ints or 'None')
    """
    chrom, start_stop = locus.split(":")
    if chrom == "None":
        chrom = None

    start, stop = re.split(r"\.\.|-", start_stop)
    if start == "None":
        start = None
    else:
        start = int(float(start))
    if stop == "None":
        stop = None
    else:
        stop = int(float(stop))
    return (str(chrom), start, stop)


def create_locus_info(chrom, start, stop):
    """Inverse to above"""
    return str(chrom) + ":" + str(start) + ".." + str(stop)


def get_read_sample_index(read):
    index = None
    for (label, value) in read.tags:
        if label == SAMPLE_INDEX_TAG:
            index = value
    return index


def get_read_sample_index_qual(read):
    index_qual = None
    for (label, value) in read.tags:
        if label == SAMPLE_INDEX_QUAL_TAG:
            index_qual = value
    return index_qual


def get_target_regions_dict(targets_file, feature_name=None):
    """Gets the target regions from a targets file as a chrom-indexed dictionary,
    with every entry given as a list of (start, end) tuples
    """
    targets = {}
    for line in targets_file:
        info = line.strip().split(b"\t")
        if (
            line.startswith(b"browser")
            or line.startswith(b"track")
            or line.startswith(b"-browser")
            or line.startswith(b"-track")
            or line.startswith(b"#")
        ):
            continue
        if len(line.strip()) == 0:
            continue
        if feature_name is not None:
            if len(info) < 4 or info[3] != feature_name:
                continue
        chrom = info[0]
        start = int(info[1])
        end = int(info[2])
        chrom_targs = targets.setdefault(chrom, [])
        chrom_targs.append((start, end))
    return targets


def get_target_regions(targets_file, feature_name=None):
    """Gets the target regions from a targets file as a chrom-indexed dictionary,
    with every entry given as a list of Regions objects
    """
    targets_dict = get_target_regions_dict(targets_file, feature_name)
    target_regions = {}
    for (chrom, starts_ends) in iteritems(targets_dict):
        chrom_regions = Regions(regions=starts_ends)
        target_regions[chrom] = chrom_regions
    return target_regions
