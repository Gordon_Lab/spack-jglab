#!/usr/bin/env python
#
# Copyright (c) 2019 10X Genomics, Inc. All rights reserved.
#
# pylint: disable=too-few-public-methods


from __future__ import annotations

import copy

import cellranger.rna.library as rna_library
from cellranger.targeted.targeted_constants import TARGETING_METHOD_HC
from cellranger.webshim.common import load_sample_data
from cellranger.webshim.constants.shared import CELLRANGER_COMMAND_NAME, PIPELINE_AGGR
from cellranger.websummary.analysis_tab_aux import (
    antibody_histogram_plot,
    barnyard_table,
    median_gene_plot,
    seq_saturation_plot,
    targeted_table,
)
from cellranger.websummary.analysis_tab_core import analysis_by_clustering, umi_on_tsne_plot
from cellranger.websummary.metrics import (
    LTMetricAnnotations,
    MetricAnnotations,
    TargetedMetricAnnotations,
)
from cellranger.websummary.react_components import WebSummaryData, write_html_file
from cellranger.websummary.sample_properties import CountSampleProperties, SampleDataPaths
from cellranger.websummary.summary_tab import (
    ANTIBODY_CELL_CALLING_METRIC_KEYS,
    CELL_CALLING_ALARM_KEYS,
    CELL_CALLING_METRIC_KEYS,
    FEATURE_BARCODINGS,
    FILTERED_BCS_TRANSCRIPTOME_UNION,
    TARGETED_CELL_CALLING_ALARM_KEYS,
    TARGETED_CELL_CALLING_METRIC_KEYS,
    add_data,
    aggregation_table,
    batch_correction_table,
    cell_or_spot_calling_table,
    feature_barcode_aggregation_table,
    feature_barcode_application_table,
    feature_barcode_sequencing_table,
    hero_metrics,
    mapping_table,
    pipeline_info_table,
    sequencing_table,
)


def _build_summary_tab_common(
    metadata,
    sample_data,
    species_list,
    sample_properties,
    pipeline,
    ws_data,
    zoom_images,
):
    """Code to create summary tab shared by spatial and single cell, updates ws_data"""
    alarm_list = ws_data.alarms
    summary_tab = ws_data.summary_tab

    add_data(summary_tab, alarm_list, hero_metrics(metadata, sample_data, species_list))
    add_data(
        summary_tab,
        alarm_list,
        pipeline_info_table(sample_data, sample_properties, pipeline, metadata, species_list),
    )
    if pipeline != PIPELINE_AGGR:
        add_data(
            summary_tab,
            alarm_list,
            sequencing_table(
                metadata, sample_data, species_list, is_targeted=sample_data.is_targeted()
            ),
        )
        add_data(
            summary_tab,
            alarm_list,
            mapping_table(metadata, sample_data, species_list),
        )
    # TODO: Detecting antibody-only/GEX-Less was done by two different conditions in two different
    # stages before.  In `count` the check was if 'cells' not in web_sum_data.summary_tab
    # and in the check was `aggr` was if len(species_list) == 0:
    # currently trying the condition below which should count for both.

    # Default for Single Cell or Spatial with or without antibody. Targeted TL with or without antibody
    if FILTERED_BCS_TRANSCRIPTOME_UNION not in sample_data.summary:
        metric_keys = ANTIBODY_CELL_CALLING_METRIC_KEYS
    else:
        metric_keys = CELL_CALLING_METRIC_KEYS
    alarm_keys = CELL_CALLING_ALARM_KEYS

    # Targeted HC with or without antibody
    if sample_data.is_targeted() and sample_data.targeting_method == TARGETING_METHOD_HC:
        metric_keys = TARGETED_CELL_CALLING_METRIC_KEYS + ANTIBODY_CELL_CALLING_METRIC_KEYS
        alarm_keys = TARGETED_CELL_CALLING_ALARM_KEYS

    add_data(
        ws_data.summary_tab,
        ws_data.alarms,
        cell_or_spot_calling_table(
            metadata,
            sample_data,
            sample_properties,
            species_list,
            metric_keys,
            alarm_keys,
            zoom_images=zoom_images,
        ),
    )
    return


def _build_analysis_tab_common(sample_data, species_list, sample_properties, ws_data):
    """Code to create analysis tab shared by spatial and single cell"""
    alarm_list = ws_data.alarms
    analysis_tab = ws_data.analysis_tab
    add_data(
        analysis_tab,
        alarm_list,
        seq_saturation_plot(sample_data, sample_properties),
    )
    add_data(
        analysis_tab,
        alarm_list,
        median_gene_plot(sample_data, sample_properties, species_list),
    )
    ws_data.clustering_selector = analysis_by_clustering(
        sample_data, spatial=sample_properties.is_spatial
    )
    return


def _build_antibody_tab_common(sample_data, sample_properties, ws_data):
    """Code to create antibody tab shared by spatial and single cell"""
    alarm_list = ws_data.alarms
    antibody_tab = ws_data.antibody_tab
    add_data(
        antibody_tab,
        alarm_list,
        antibody_histogram_plot(sample_data),
    )
    ws_data.antibody_clustering_selector = analysis_by_clustering(
        sample_data,
        spatial=sample_properties.is_spatial,
        library_type=rna_library.ANTIBODY_LIBRARY_TYPE,
    )


def _build_diagnostic_values(sample_data, ws_data):
    """Some metrics are useful for debugging but not yet ready to be displayed to the customer,
    we hide these in the web summary json inside a "diagnostics" field if we encounter them."""
    # assert isinstance(sample_data, SampleData)
    assert isinstance(ws_data, WebSummaryData)
    diagnostic_metrics = [
        "tso_frac",
        "i1_bases_with_q30_frac",
        "i2_bases_with_q30_frac",
        "low_support_umi_reads_frac",
    ]
    mets = {}
    for met in diagnostic_metrics:
        if met in sample_data.summary:
            mets[met] = sample_data.summary[met]
    ws_data.diagnostics = mets


def build_web_summary_data_common(
    sample_properties,
    sample_data,
    pipeline,
    metadata,
    command,
    zoom_images=None,
):
    """Produce common data shared by both spatial and cell ranger, VDJ is currently independent"""
    is_spatial = command != CELLRANGER_COMMAND_NAME
    wsd = WebSummaryData(sample_properties, command, pipeline)
    species_list = sample_properties.genomes
    _build_summary_tab_common(
        metadata, sample_data, species_list, sample_properties, pipeline, wsd, zoom_images
    )
    if FILTERED_BCS_TRANSCRIPTOME_UNION in sample_data.summary:
        # we don't need the GEX tab in antibody-only case
        _build_analysis_tab_common(sample_data, species_list, sample_properties, wsd)
    _build_antibody_tab_common(sample_data, sample_properties, wsd)
    _build_diagnostic_values(sample_data, wsd)

    for fb in FEATURE_BARCODINGS:
        # Not all three of theses will be present

        # feature barcoding sequencing info (Count)
        add_data(
            wsd.summary_tab,
            wsd.alarms,
            feature_barcode_sequencing_table(metadata, sample_data, species_list, fb),
        )
        # feature barcoding application metric (Count)
        add_data(
            wsd.summary_tab,
            wsd.alarms,
            feature_barcode_application_table(metadata, sample_data, species_list, fb),
        )
        # aggregation metrics (AGGR)
        add_data(
            wsd.summary_tab,
            wsd.alarms,
            feature_barcode_aggregation_table(metadata, sample_data, sample_properties, fb),
        )

    # Targeting
    add_data(
        wsd.analysis_tab,
        wsd.alarms,
        targeted_table(metadata, sample_data, species_list, is_spatial=is_spatial),
    )

    return wsd


def build_web_summary_html_sc_and_aggr(
    sample_properties, sample_data_paths, pipeline, metadata, command_name
):
    """
    Produce the main websummary
    """
    assert isinstance(sample_properties, CountSampleProperties)
    assert isinstance(sample_data_paths, SampleDataPaths)
    sample_data = load_sample_data(sample_properties, sample_data_paths)

    species_list = sample_properties.genomes
    web_sum_data = build_web_summary_data_common(
        sample_properties,
        sample_data,
        pipeline,
        metadata,
        command_name,
    )
    # Single cell specific stuff
    add_data(
        web_sum_data.summary_tab,
        web_sum_data.alarms,
        aggregation_table(metadata, sample_data, sample_properties),
    )
    add_data(
        web_sum_data.summary_tab,
        web_sum_data.alarms,
        batch_correction_table(metadata, sample_data, species_list),
    )

    # t-SNE plot appears as a constant left plot in the Cell Ranger
    # clustering selector
    if web_sum_data.clustering_selector:
        web_sum_data.clustering_selector.left_plots = umi_on_tsne_plot(
            sample_data, spatial=sample_properties.is_spatial
        )
    if web_sum_data.antibody_clustering_selector:
        web_sum_data.antibody_clustering_selector.left_plots = umi_on_tsne_plot(
            sample_data,
            spatial=sample_properties.is_spatial,
            library_type=rna_library.ANTIBODY_LIBRARY_TYPE,
        )
    # Barnyard
    b_table = barnyard_table(metadata, sample_data, sample_properties, species_list)
    if b_table:
        web_sum_data.analysis_tab["barnyard"] = b_table

    return web_sum_data


def build_web_summary_html_sc(filename, sample_properties, sample_data_paths, pipeline):
    if sample_properties.is_lt:
        metadata = LTMetricAnnotations(intron_mode_alerts=sample_properties.include_introns)
    elif sample_properties.is_targeted:
        metadata = TargetedMetricAnnotations()
    else:
        metadata = MetricAnnotations(intron_mode_alerts=sample_properties.include_introns)
    command = CELLRANGER_COMMAND_NAME
    web_sum_data = build_web_summary_html_sc_and_aggr(
        sample_properties, sample_data_paths, pipeline, metadata, command
    )

    # to circumvent self._check_valid
    data = copy.deepcopy(web_sum_data)
    data = data.to_dict_for_json()

    write_html_file(filename, web_sum_data)
    return data
