#!/usr/bin/env python
#
# Copyright (c) 2018 10X Genomics, Inc. All rights reserved.
#


from __future__ import annotations

import os

from six import ensure_binary, ensure_str

import cellranger.library_constants as lib_constants
from cellranger.feature_ref import FeatureDef, FeatureReference


def save_features_bed_path(feature_ref, fullpath):
    base_dir = os.path.dirname(fullpath)
    filename = os.path.basename(fullpath)
    save_features_bed(feature_ref, base_dir, filename)


def save_features_bed(feature_ref, base_dir, filename="peaks.bed", compress=False):
    out_features_fn = os.path.join(base_dir, filename)
    with open(out_features_fn, "wb") as f:
        for feature_def in feature_ref.feature_defs:
            fields = ensure_binary(feature_def.name).split(b":")
            f.write(
                fields[0]
                + b"\t"
                + fields[1].split(b"-")[0]
                + b"\t"
                + fields[1].split(b"-")[1]
                + b"\n"
            )


def save_motifs_tsv(feature_ref, base_dir, compress=False):
    out_features_fn = os.path.join(base_dir, "motifs.tsv")
    with open(out_features_fn, "wb") as f:
        for feature_def in feature_ref.feature_defs:
            f.write(feature_def.id + b"\t" + ensure_binary(feature_def.name) + b"\n")


# A parallel implementation "get_genome_from_str" available via utils.py
def get_genome_from_contig(s, genomes):
    assert len(genomes) > 0

    if s is None:
        return None

    if len(genomes) == 1:
        return genomes[0]

    for genome in genomes:
        if s.startswith(ensure_binary(genome)):
            return genome

    raise Exception("%s does not have valid associated genome" % s)


def from_peaks_bed(peaks_path, genomes):
    """
    Create a FeatureReference from a peaks bed file

    Args:
        peaks_path (str): Path to peaks bed file. Can be None.
        genomes: This should be the genomes on which peaks were defined. These would be prefixes in peak names
    """

    # Load peaks info
    feature_defs = []
    all_tag_keys = ["genome", "derivation"]

    if peaks_path:
        # Stuff relevant fields of peak tuple into FeatureDef
        with open(peaks_path, "rb") as pf:
            for line in pf:
                if line.startswith(b"#"):
                    continue
                peak = b"%s:%s-%s" % tuple(line.strip(b"\n").split(b"\t"))
                genome = get_genome_from_contig(peak, genomes)
                feature_defs.append(
                    FeatureDef(
                        index=len(feature_defs),
                        id=peak,
                        name=ensure_str(peak),
                        feature_type=lib_constants.ATACSEQ_LIBRARY_TYPE,
                        tags={"genome": genome, "derivation": ""},
                    )
                )

    return FeatureReference(feature_defs, all_tag_keys)


def get_genome_from_motif(s, genomes):
    """Genome name is extracted only in single species case"""

    assert len(genomes) == 1, "Motif analysis not done for multispecies samples or None species"
    if s is None:
        return None
    return genomes[0]


def from_motif_list(motif_list, genomes):
    """
    Create a FeatureReference from a motifs list.

    Args:
        motif_list (list): list of motif names. Can be None.
    """

    # Load peaks info
    feature_defs = []
    all_tag_keys = ["genome", "derivation"]

    if motif_list:
        # Stuff into FeatureDef
        for motif in motif_list:
            genome = get_genome_from_motif(motif, genomes)
            feature_defs.append(
                FeatureDef(
                    index=len(feature_defs),
                    id=ensure_binary(motif),
                    name=ensure_str(motif.split("_")[0]),
                    feature_type=lib_constants.ATACSEQ_LIBRARY_DERIVED_TYPE,
                    tags={"genome": genome, "derivation": "POOL"},
                )
            )

    return FeatureReference(feature_defs, all_tag_keys)
