#!/usr/bin/env python
#
# Copyright (c) 2019 10X Genomics, Inc. All rights reserved.
#
#
# Determine the locations of the cell barcode, cDNA sequence, UMI, sample index.
#

from __future__ import annotations

import json
import os
from collections import OrderedDict

# NOTE: The rust code in `cr_types` also loads chemistry definitions from the same file.
CHEMISTRY_DEFS = json.load(open(os.path.join(os.path.dirname(__file__), "chemistry_defs.json")))


# Single Cell 3' chemistries
CHEMISTRY_SC3P_V1 = CHEMISTRY_DEFS["SC3Pv1"]
CHEMISTRY_SC3P_V2 = CHEMISTRY_DEFS["SC3Pv2"]
CHEMISTRY_SC3P_V3 = CHEMISTRY_DEFS["SC3Pv3"]

# Single Cell V(D)J
CHEMISTRY_SCVDJ = CHEMISTRY_DEFS["SCVDJ"]
# Single Cell V(D)J, transcript on R2 only
CHEMISTRY_SCVDJ_R2 = CHEMISTRY_DEFS["SCVDJ-R2"]

# Single Cell 5' Gene Expression, paired-end
CHEMISTRY_SC5P_PE = CHEMISTRY_DEFS["SC5P-PE"]
# Single Cell 5' Gene Expression, transcript on R2 only
CHEMISTRY_SC5P_R2 = CHEMISTRY_DEFS["SC5P-R2"]
# Single Cell 5' Gene Expression, transcript on R1 only
CHEMISTRY_SC5P_R1 = CHEMISTRY_DEFS["SC5P-R1"]

# Single Cell Feature-Barcoding only
CHEMISTRY_SC_FB = CHEMISTRY_DEFS["SC-FB"]

# Spatial Chemistry
CHEMISTRY_SPATIAL3P_V1 = CHEMISTRY_DEFS["SPATIAL3Pv1"]

# ARC chemistry
CHEMISTRY_ARC_V1 = CHEMISTRY_DEFS["ARC-v1"]

SC3P_CHEMISTRIES = [CHEMISTRY_SC3P_V1, CHEMISTRY_SC3P_V2, CHEMISTRY_SC3P_V3]

SC5P_CHEMISTRIES = [CHEMISTRY_SC5P_PE, CHEMISTRY_SC5P_R2, CHEMISTRY_SC5P_R1]


DEFINED_CHEMISTRIES = list(CHEMISTRY_DEFS.values())


# Aliases for human usage
CHEMISTRY_ALIASES = OrderedDict([("threeprime", "SC3P_auto"), ("fiveprime", "SC5P_auto")])

# User-defined chemistry (use the various read_type/offset/length arguments passed to the pipeline)
CUSTOM_CHEMISTRY_NAME = "custom"


def get_chemistry(name):
    """Returns a chemistry definition dict for a given name"""

    name = CHEMISTRY_ALIASES.get(name, name)

    chemistries = [n for n in DEFINED_CHEMISTRIES if n["name"] == name]
    if len(chemistries) == 0:
        raise ValueError("Could not find chemistry named %s" % name)
    if len(chemistries) > 1:
        raise ValueError("Found multiple chemistries with name %s" % name)
    return chemistries[0]
