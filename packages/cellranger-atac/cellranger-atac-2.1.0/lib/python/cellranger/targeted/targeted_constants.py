#!/usr/bin/env python
#
# Copyright (c) 2020 10X Genomics, Inc. All rights reserved.
#
"""
Constants used for the targeting pipeline.
"""


from __future__ import annotations

# two types of targeted assays
TARGETING_METHOD_TL = "templated_ligation"
TARGETING_METHOD_TL_FILE_NAME = "probe set"
TARGETING_METHOD_TL_FILE_FORMAT = "probe_set_file_format"
TARGETING_METHOD_TL_OLIGO_NAME = "probe"

TARGETING_METHOD_HC = "hybrid_capture"
TARGETING_METHOD_HC_FILE_NAME = "target panel"
TARGETING_METHOD_HC_FILE_FORMAT = "target_panel_file_format"
TARGETING_METHOD_HC_OLIGO_NAME = "bait"

TARGETING_METHOD_NAMES = {
    TARGETING_METHOD_TL: TARGETING_METHOD_TL_FILE_NAME,
    TARGETING_METHOD_HC: TARGETING_METHOD_HC_FILE_NAME,
}

OLIGO_NAME = {
    TARGETING_METHOD_TL: TARGETING_METHOD_TL_OLIGO_NAME,
    TARGETING_METHOD_HC: TARGETING_METHOD_HC_OLIGO_NAME,
}

ALL_TARGETING_METHODS = list(TARGETING_METHOD_NAMES.keys())

# List of gene/probe ID prefixes that are excluded from the filtered_feature_bc_matrix.
PROBE_ID_IGNORE_PREFIXES = (b"DEPRECATED", b"IGNORE")
