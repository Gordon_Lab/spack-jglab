#!/usr/bin/env python
#
# Copyright (c) 2017 10X Genomics, Inc. All rights reserved.
#


from __future__ import annotations

import collections
import os
import traceback

import martian
import numpy as np
import umap

import cellranger.analysis.constants as analysis_constants
import cellranger.analysis.io as analysis_io

UMAP = collections.namedtuple(
    "UMAP",
    [
        "transformed_umap_matrix",
        "name",  # Human readable form
        "key",  # Machine queryable form, must be unique
    ],
)


def run_umap(
    transformed_pca_matrix: np.ndarray,
    name: str = "UMAP",
    key: str = "UMAP",
    input_pcs: int = None,
    n_neighbors: int = None,
    min_dist: float = None,
    metric: str = None,
    umap_dims: int = None,
    random_state: int = None,
    n_jobs: int = None,
):

    if umap_dims is None:
        umap_dims = analysis_constants.UMAP_N_COMPONENTS

    if n_neighbors is None:
        n_neighbors = analysis_constants.UMAP_DEFAULT_N_NEIGHBORS

    if min_dist is None:
        min_dist = analysis_constants.UMAP_MIN_DIST

    if metric is None:
        metric = analysis_constants.UMAP_DEFAULT_METRIC

    if random_state is None:
        random_state = analysis_constants.RANDOM_STATE

    if n_jobs is None:
        n_jobs = -1

    if input_pcs is not None:
        transformed_pca_matrix = transformed_pca_matrix[:, :input_pcs]

    # Update disconnection_distance to 2 for correlation and cosine
    # this avoids disconnecting vertices where the distances are
    # defaulted out to 1 because an observation (rotated or not) has
    # zero counts.  NOTE that this is now the default in newer versions
    # of the package.
    distance_overrides = {"correlation": 2, "cosine": 2}
    disconnection_distance = distance_overrides.get(metric, None)

    try:
        umap_reducer = umap.UMAP(
            n_neighbors=n_neighbors,
            min_dist=min_dist,
            n_components=umap_dims,
            metric=metric,
            random_state=np.random.RandomState(random_state),
            n_jobs=n_jobs,
            disconnection_distance=disconnection_distance,
        )

        transformed_umap_matrix = umap_reducer.fit_transform(transformed_pca_matrix)
    except:
        try:
            martian.log_info(
                "Failed to run UMAP with default setting on this data. Trying random initialization."
            )

            umap_reducer = umap.UMAP(
                n_neighbors=n_neighbors,
                min_dist=min_dist,
                n_components=umap_dims,
                metric=metric,
                random_state=np.random.RandomState(random_state),
                disconnection_distance=disconnection_distance,
                init="random",
            )

            transformed_umap_matrix = umap_reducer.fit_transform(transformed_pca_matrix)
        except:
            martian.log_info(f"Caught exception:\n{traceback.format_exc()}")
            martian.log_info("Failed to run UMAP on this data. Filling in 0s as UMAP embeddings.")
            transformed_umap_matrix = np.zeros(
                (transformed_pca_matrix.shape[0], umap_dims), dtype=np.float32
            )

    return UMAP(transformed_umap_matrix, name=name, key=key)


def save_umap_csv(umap_obj, barcodes, base_dir):
    """Save a UMAP object to CSV"""
    # Preserve backward compatibility with pre-3.0 CSV files
    #   where the CSV directory was named "2_components" and the HDF5 dataset was named "_2"
    key = umap_obj.key + "_components"

    umap_dir = os.path.join(base_dir, key)
    os.makedirs(umap_dir, exist_ok=True)

    matrix_fn = os.path.join(umap_dir, "projection.csv")
    n_umap_components = umap_obj.transformed_umap_matrix.shape[1]
    matrix_header = ["Barcode"] + ["UMAP-%d" % (i + 1) for i in range(n_umap_components)]
    analysis_io.save_matrix_csv(
        matrix_fn, umap_obj.transformed_umap_matrix, matrix_header, barcodes
    )


def save_umap_h5(umap_obj, fname):
    """Save a UMAP object to HDF5"""
    umap_map = {umap_obj.key: umap_obj}
    analysis_io.save_dimension_reduction_h5(
        umap_map, fname, analysis_constants.ANALYSIS_H5_UMAP_GROUP
    )
