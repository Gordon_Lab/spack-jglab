# Copyright (c) 2020 10X Genomics, Inc. All rights reserved.
"""Type definitions for various analysis types.

This allows others to use them without taking a dependency on
the actual analysis code.
"""

from __future__ import annotations

from typing import TYPE_CHECKING, NamedTuple

if TYPE_CHECKING:
    import numpy.typing as npt

# pylint: disable=invalid-name


class PCA(NamedTuple):
    """Holds information related to PCA dimensionality reduction."""

    transformed_pca_matrix: npt.NDArray
    components: npt.NDArray
    variance_explained: npt.NDArray
    dispersion: npt.NDArray
    features_selected: npt.NDArray


class LSA(NamedTuple):
    """Hold information related to LSA dimensionality reduction"""

    transformed_lsa_matrix: npt.NDArray
    components: npt.NDArray
    variance_explained: npt.NDArray
    dispersion: npt.NDArray
    features_selected: npt.NDArray


class TSNE(NamedTuple):
    """Hold information related to TSNE projection"""

    transformed_tsne_matrix: npt.NDArray
    name: str  # Human readable form
    key: str  # Machine queryable form, must be unique


class DifferentialExpression(NamedTuple):
    """Hold information related to differential expression analysis"""

    data: npt.NDArray


class PLSA(NamedTuple):
    """Hold information related to PLSA dimensionality reduction"""

    transformed_plsa_matrix: npt.NDArray
    components: npt.NDArray
    variance_explained: npt.NDArray
    dispersion: npt.NDArray
    features_selected: npt.NDArray
