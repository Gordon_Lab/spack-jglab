#!/usr/bin/env python
#
# Copyright (c) 2015 10X Genomics, Inc. All rights reserved.
#

"""Assorted grab-bag of miscellaneous helper methods.

Do not add to this module.  Instead, find or create a module with a name
that indicates to a potential user what sorts of methods they might find
in that module.
"""

from __future__ import annotations

import collections
import csv
import json
import os
import random
from typing import (
    IO,
    TYPE_CHECKING,
    AnyStr,
    Collection,
    Dict,
    Generator,
    Iterable,
    List,
    Optional,
    Sequence,
    Set,
    Tuple,
    Union,
    overload,
)

import numpy as np

import cellranger.constants as cr_constants
import tenkit.constants as tk_constants
import tenkit.log_subprocess as tk_subproc
import tenkit.seq as tk_seq

if TYPE_CHECKING:
    from pysam import AlignedSegment


def get_gem_group_from_barcode(barcode: Optional[bytes]) -> Optional[int]:
    """Method to get just the gem group from a barcode.

    Args:
        barcode (bytes): a barcode, a la "ACGTACTAGAC-1"

    Returns:
        The Gem group as an int or None if not present.
    """
    if barcode is None:
        return None
    gem_group = barcode.index(b"-")
    if gem_group > 0:
        return int(barcode[gem_group + 1 :])
    else:
        return None


def pos_sort_key(read: AlignedSegment) -> tuple:
    return read.tid, read.pos


def format_barcode_seq(barcode: bytes, gem_group: Optional[int] = None) -> bytes:
    if gem_group is not None:
        barcode += b"-%d" % gem_group
    return barcode


def format_barcode_seqs(
    barcode_seqs: Collection[bytes], gem_groups: Optional[Iterable[int]]
) -> List[bytes]:
    """Format a sequence of barcodes as seqs.

    Args:
        barcode_seqs (List[bytes]): _description_
        gem_groups (Optional[Iterable[int]]): _description_

    Returns:
        List[bytes]: _description_
    """
    if gem_groups is None:
        return barcode_seqs
    return [format_barcode_seq(bc, gg) for gg in sorted(set(gem_groups)) for bc in barcode_seqs]


@overload
def split_barcode_seq(
    barcode: None,
) -> Tuple[None, None]:
    ...


@overload
def split_barcode_seq(
    barcode: bytes,
) -> Tuple[bytes, Optional[int]]:
    ...


def split_barcode_seq(barcode):
    """Split a barcode-gem_group.

    Args:
        barcode (bytes): A barcode with an optional gem group suffix.

    Returns:
        _type_: _description_
    """
    if barcode is None:
        return None, None

    assert isinstance(barcode, bytes)
    barcode_parts = barcode.split(b"-", maxsplit=2)

    barcode = barcode_parts[0]
    if len(barcode_parts) > 1:
        gem_group = int(barcode_parts[1])
    else:
        gem_group = None

    return barcode, gem_group


def bcs_suffices_to_names(
    bcs: Iterable[bytes], gg_id_map: Dict[int, Union[str, bytes]]
) -> List[Union[str, bytes]]:
    """Turn list of barcodes into corresponding list of mapped values.

    Args:
        bcs (list): aggr barcodes with suffix corresponding to gem group id
        gg_id_map (dict): mapping gem-group-ids/barcode-suffixes (int) to
            a desired named value (typically `aggr_id` for the library id of that
            one sample, or `batch_id` for the name of a batch it is a part of.)
    """
    mapped_ids = [gg_id_map[split_barcode_seq(bc)[1]] for bc in bcs]
    return mapped_ids


def get_genome_from_str(search: Optional[AnyStr], genomes: Sequence[AnyStr]):
    """Return the first genome which is a prefix for the given search string.

    Args:
        search: _description_
        genomes: _description_

    Raises:
        Exception: _description_

    Returns:
        _type_: _description_
    """
    assert len(genomes) > 0

    if search is None:
        return None

    if len(genomes) == 1:
        return genomes[0]

    for genome in genomes:
        if search.startswith(genome):
            return genome

    raise Exception(f"{search} does not have valid associated genome")


def remove_genome_from_str(seq, genomes, prefixes_as_genomes=False):
    """_summary_

    Args:
        seq (_type_): _description_
        genomes (_type_): _description_
        prefixes_as_genomes (bool, optional): _description_. Defaults to False.

    Raises:
        Exception: _description_

    Returns:
        _type_: _description_
    """
    assert len(genomes) > 0
    if seq is None:
        return None
    if len(genomes) == 1:
        return seq

    # Gene names/ids/chroms are padded with N underscores to achieve the same prefix length
    #   for all genomes, e.g., GRCh38_* and mm10___*
    max_len = max(len(g) for g in genomes)

    for genome in genomes:
        if seq.startswith(genome):
            # Strip genome and subsequent underscores
            if prefixes_as_genomes:
                return seq[(1 + len(genome)) :]
            else:
                return seq[(1 + max_len) :]

    raise Exception(f"{seq} does not have valid associated genome")


def get_genome_from_read(read, chroms, genomes):
    """_summary_

    Args:
        read (_type_): _description_
        chroms (_type_): _description_
        genomes (_type_): _description_

    Returns:
        _type_: _description_
    """
    assert len(genomes) > 0

    if read.is_unmapped:
        return None

    if len(genomes) == 1:
        return genomes[0]

    return get_genome_from_str(chroms[read.tid], genomes)


def get_read_barcode(read: AlignedSegment):
    return _get_read_tag(read, cr_constants.PROCESSED_BARCODE_TAG)


def get_read_raw_barcode(read: AlignedSegment):
    return _get_read_tag(read, cr_constants.RAW_BARCODE_TAG)


def get_read_barcode_qual(read: AlignedSegment):
    return _get_read_tag(read, cr_constants.RAW_BARCODE_QUAL_TAG)


def get_read_umi_qual(read: AlignedSegment):
    return _get_read_tag(read, cr_constants.UMI_QUAL_TAG)


def get_read_umi(read: AlignedSegment):
    return _get_read_tag(read, cr_constants.PROCESSED_UMI_TAG)


def get_read_raw_umi(read: AlignedSegment):
    return _get_read_tag(read, cr_constants.RAW_UMI_TAG)


def get_read_library_index(read: AlignedSegment):
    return _get_read_tag(read, cr_constants.LIBRARY_INDEX_TAG)


def get_read_gene_ids(read: AlignedSegment):
    """_summary_

    Args:
        read (AlignedSegment): _description_

    Returns:
        _type_: _description_
    """
    id_str: Optional[str] = _get_read_tag(read, cr_constants.FEATURE_IDS_TAG)
    if id_str is None:
        return None
    assert isinstance(id_str, str)
    return tuple(id_str.split(";"))


def get_read_transcripts_iter(read: Optional[bytes]):
    """Iterate over all transcripts compatible with the given read.

    We do this by iterating over the TX tag entries that are of the form
    `TX:<transcript id>,<strand><position>,<cigar>`.

    Note:
        When intronic alignment is turned on, the `TX` tag is set to
        `TX:<gene id>,<strand>` for intronic  reads or exonic reads that are
        not compatible with annotated splice junctions. We ignore these.
    """
    # Method used to work directly with BAM records, since moved to rust/pyo3
    # s = _get_read_tag(read, cr_constants.TRANSCRIPTS_TAG)
    if not read:
        return

    for x in read.split(b";"):
        if len(x) == 0:
            continue

        parts = x.split(b",")
        # len(parts) = 3 for transcriptomic alignments, while for intronic alignments when
        # intron counting mode is enabled len(parts)=2
        if len(parts) != 3:
            continue

        chrom = parts[0] if parts[0] else None
        if parts[1]:
            strand = parts[1][0:1]
            pos = int(parts[1][1:])
        else:
            strand = None
            pos = None
        cigarstring = parts[2] if parts[2] else None
        assert strand in cr_constants.STRANDS or strand is None

        yield chrom, strand, pos, cigarstring


def set_read_tags(read: AlignedSegment, tags) -> None:
    read_tags = read.tags
    read_tags.extend(tags)
    read.tags = read_tags


def _get_read_tag(read: AlignedSegment, tag):
    try:
        r = read.opt(tag)
        if not r:
            r = None
        return r
    except KeyError:
        return None


def get_read_extra_flags(read: AlignedSegment):
    return _get_read_tag(read, cr_constants.EXTRA_FLAGS_TAG) or 0


# EXTRA_FLAGS_CONF_MAPPED_TXOME = 1
EXTRA_FLAGS_LOW_SUPPORT_UMI = 2
# EXTRA_FLAGS_GENE_DISCORDANT = 4
EXTRA_FLAGS_UMI_COUNT = 8
EXTRA_FLAGS_CONF_MAPPED_FEATURE = 16
EXTRA_FLAGS_FILTERED_TARGET_UMI = 32


def is_read_low_support_umi(read: AlignedSegment) -> bool:
    return (get_read_extra_flags(read) & EXTRA_FLAGS_LOW_SUPPORT_UMI) > 0


def is_read_filtered_target_umi(read: AlignedSegment) -> bool:
    return (get_read_extra_flags(read) & EXTRA_FLAGS_FILTERED_TARGET_UMI) > 0


def is_read_umi_count(read: AlignedSegment) -> bool:
    return (get_read_extra_flags(read) & EXTRA_FLAGS_UMI_COUNT) > 0


def is_read_conf_mapped_to_feature(read: AlignedSegment) -> bool:
    return (get_read_extra_flags(read) & EXTRA_FLAGS_CONF_MAPPED_FEATURE) > 0


def is_read_dupe_candidate(
    read, high_conf_mapq: int, use_corrected_umi: bool = True, use_umis: bool = True
) -> bool:
    """_summary_

    Args:
        read: _description_
        high_conf_mapq: _description_
        use_corrected_umi: _description_. Defaults to True.
        use_umis: _description_. Defaults to True.

    Returns:
        bool: _description_
    """
    if use_corrected_umi:
        umi = get_read_umi(read)
    else:
        umi = get_read_raw_umi(read)

    return (
        not read.is_secondary
        and (umi is not None or not use_umis)
        and (get_read_barcode(read) is not None)
        and not is_read_low_support_umi(read)
        and not is_read_filtered_target_umi(read)
        and (
            is_read_conf_mapped_to_transcriptome(read, high_conf_mapq)
            or is_read_conf_mapped_to_feature(read)
        )
    )


def is_read_conf_mapped(read: AlignedSegment, high_conf_mapq: int) -> bool:
    """_summary_

    Args:
        read (AlignedSegment): _description_
        high_conf_mapq (int): _description_

    Returns:
        bool: _description_
    """
    if read.is_unmapped:
        return False
    elif read.mapq < high_conf_mapq:
        return False
    return True


def is_read_conf_mapped_to_transcriptome(read: AlignedSegment, high_conf_mapq: int) -> bool:
    """_summary_

    Args:
        read (AlignedSegment): _description_
        high_conf_mapq (int): _description_

    Returns:
        bool: _description_
    """
    if is_read_conf_mapped(read, high_conf_mapq):
        gene_ids = get_read_gene_ids(read)
        return (gene_ids is not None) and (len(gene_ids) == 1)
    return False


def compress_seq(seq: bytes, bits: int = 64):
    """Pack a DNA sequence (no Ns!) into a 2-bit format, into a python int.

    Args:
        seq (str): A DNA sequence.
        bits: the bit size of the integer to pack into.

    Returns:
        int: The sequence packed into the bits of an integer.
    """
    assert len(seq) <= (bits // 2 - 1)
    result = 0
    for i in range(len(seq)):
        nuc = seq[i : i + 1]
        assert nuc in tk_seq.NUCS_INVERSE
        result = result << 2
        result = result | tk_seq.NUCS_INVERSE[nuc]
    return result


def set_tag(read, key, old_value, new_value):
    """Set a bam tag for a read, overwriting the previous value"""
    tags = read.tags
    tags.remove((key, old_value))
    tags.append((key, new_value))
    read.tags = tags


def merge_jsons_as_dict(in_filenames: Iterable[Union[str, bytes, os.PathLike]]) -> dict:
    """Merge a list of json files and return the result as a dictionary"""
    result = {}
    for filename in in_filenames:
        if filename is None:
            continue
        try:
            with open(filename) as f:
                data = json.load(f)
                result.update(data)
        except OSError:
            continue
    return result


def format_barcode_summary_h5_key(
    library_prefix: str, genome: str, region: str, read_type: str
) -> str:
    """Formats the barcode summary into a key to access the `barcode_summary.h5` outs.

    Here, we need to accommodate both accessing old and new versions of the h5 file
    compatible with both v2 and v3 of the matrix.

    Args:
        library_prefix (str): Name of the library, used as a prefix
        genome         (str): Name of the genome used for alignment
        region         (str): Name of the subset of the genome we are looking at (e.g. transcriptome, regulome,
                            epigenome, ...). This should be a controlled vocabulary in `cellranger.constants`
        read_type      (str): Name of the read types we are trying to extract (e.g. `conf_mapped_deduped_barcoded`, ...).
                            It should be also controlled in `cellranger.constants`.

    Returns:
        output_key     (str): A string constant with the suffix `reads` appended.
    """
    # Clean the input for non-found or undefined keys. Will break badly if they are not controlled, but will let us
    # keep compatible across v2 and v3 developments.
    str_list = [library_prefix, genome, region, read_type]
    # Append reads
    str_list.append("reads")
    # join
    output_key = "_".join(str_list)
    return output_key


def downsample(rate: Optional[float]) -> bool:
    if rate is None or rate == 1.0:
        return True
    return random.random() <= rate


def numpy_groupby(values, keys) -> Generator[Tuple[tuple, tuple], None, None]:
    """Group a collection of numpy arrays by key arrays.

    Yields `(key_tuple, view_tuple)` where `key_tuple` is the key grouped
    on and `view_tuple` is a tuple of views into the value arrays.

    Args:
        values (tuple of arrays): tuple of arrays to group.
        keys (tuple): tuple of sorted, numeric arrays to group by.

    Returns:
        sequence of tuple: Sequence of (`key_tuple`, `view_tuple`).
    """

    if len(values) == 0:
        return
    if len(values[0]) == 0:
        return

    for key_array in keys:
        assert len(key_array) == len(keys[0])
    for value_array in values:
        assert len(value_array) == len(keys[0])

    # The indices where any of the keys differ from the previous key become group boundaries
    # pylint: disable=no-member
    key_change_indices = np.logical_or.reduce(
        tuple(np.concatenate(([1], np.diff(key))) != 0 for key in keys)
    )
    group_starts = np.flatnonzero(key_change_indices)
    group_ends = np.roll(group_starts, -1)
    group_ends[-1] = len(keys[0])

    for group_start, group_end in zip(group_starts, group_ends):
        yield tuple(key[group_start] for key in keys), tuple(
            value[group_start:group_end] for value in values
        )


def get_fasta_iter(f: IO[bytes]) -> Generator[Tuple[bytes, bytes], None, None]:
    """Iterate through sequences in a fasta file.

    Args:
        f (IO[bytes]): The input file object.

    Yields:
        Generator[tuple[bytes, bytes], None, None]: _description_
    """
    hdr = b""
    seq = b""
    for line in f:
        line = line.strip()
        if line.startswith(b">"):
            if hdr:
                yield hdr, seq
            hdr = line[1:]
            seq = b""
        else:
            seq += line
    if hdr:
        yield hdr, seq


__CIGAR_CATEGORIES = b"MIDNSHP=X"


def _cigar_numeric_to_category_map(i: int) -> str:
    """Pysam numeric codes to meaningful categories."""
    return chr(__CIGAR_CATEGORIES[i])


def get_cigar_summary_stats(read: AlignedSegment, strand: bytes) -> Dict[str, int]:
    """Get number of mismatches, insertions, deletions, ref skip, soft clip, hard clip bases from a read.

    Returns a dictionary by the element's CIGAR designation. Adds additional
    fields to distinguish between three and five prime soft-clipping for `R1`
    and `R2`: `R1_S_three_prime` and `R1_S_five_prime`, etc. to account for
    soft-clipped local alignments.

    Args:
        read (pysam.AlignedRead): aligned read object
        strand (string): + or - to indicate library orientation (MRO argument strand, for example)

    Returns:
        dict of str,int: Key of base type to base counts for metrics. Adds
                         additional fields to distinguish between three and
                         five prime soft-clipping: `S_three_prime` and
                         `S_five_prime`.
    """

    statistics: Dict[str, int] = {}
    cigar_tuples = read.cigar

    for i, (category, count) in enumerate(cigar_tuples):
        # Convert numeric code to category
        category = _cigar_numeric_to_category_map(category)
        count = int(count)

        # Detect 5 prime soft-clipping
        if i == 0:

            if strand == cr_constants.REVERSE_STRAND:
                metric = "R1_S_five_prime" if read.is_read1 else "R2_S_three_prime"
            else:
                metric = "R2_S_five_prime" if read.is_read1 else "R1_S_three_prime"

            if category == "S":
                statistics[metric] = count
            else:
                statistics[metric] = 0

        # Tally up all standard categories from BAM
        statistics[category] = statistics.get(category, 0) + count

        # Detect 3 prime soft-clipping
        if i == len(cigar_tuples):
            if strand == cr_constants.REVERSE_STRAND:
                metric = "R2_S_five_prime" if read.is_read2 else "R1_S_three_prime"
            else:
                metric = "R1_S_five_prime" if read.is_read2 else "R2_S_three_prime"

            if category == "S":
                statistics[metric] = count
            else:
                statistics[metric] = 0

    return statistics


def get_full_alignment_base_quality_scores(
    read: AlignedSegment,
) -> Tuple[int, np.ndarray[int, np.dtype[np.byte]]]:
    """Returns base quality scores for the full read alignment.

    Inserts zeroes for deletions and removing inserted and soft-clipped bases.
    Therefore, only returns quality for truly aligned sequenced bases.

    Args:
        read (pysam.AlignedSegment): read to get quality scores for

    Returns:
        np.array: numpy array of quality scores
    """

    # pylint: disable=no-member
    quality_scores = np.fromstring(read.qual, dtype=np.byte) - tk_constants.ILLUMINA_QUAL_OFFSET

    start_pos = 0

    for operation, length in read.cigar:
        operation = _cigar_numeric_to_category_map(operation)

        if operation == "D":
            quality_scores = np.insert(quality_scores, start_pos, [0] * length)
        elif operation in ("I", "S"):
            quality_scores = np.delete(quality_scores, np.s_[start_pos : start_pos + length])

        if not operation in ("I", "S"):
            start_pos += length

    return start_pos, quality_scores


def get_unmapped_read_count_from_indexed_bam(bam_file_name: Union[str, bytes, os.PathLike]) -> int:
    """Get number of unmapped reads from an indexed BAM file.

    Args:
        bam_file_name (str): Name of indexed BAM file.

    Returns:
        int: number of unmapped reads in the BAM

    Note:
        BAM must be indexed for lookup using samtools.
    """

    index_output = tk_subproc.check_output(["samtools", "idxstats", bam_file_name])
    return int(index_output.strip().rsplit(b"\n", maxsplit=1)[-1].rsplit(maxsplit=1)[-1])


def load_barcode_csv(barcode_csv: Union[str, bytes, os.PathLike]) -> Dict[bytes, List[bytes]]:
    """Load a csv file of (genome,barcode)"""

    bcs_per_genome: collections.defaultdict[bytes, List[bytes]] = collections.defaultdict(list)
    with open(barcode_csv) as f:
        reader = csv.reader(f)
        for row in reader:
            if len(row) != 2:
                raise ValueError("Bad barcode file: %s" % barcode_csv)
            (genome, barcode) = row
            bcs_per_genome[genome.encode()].append(barcode.encode())
    return bcs_per_genome


def get_cell_associated_barcode_set(
    barcode_csv_filename: Union[str, bytes, os.PathLike], genome: Optional[bytes] = None
) -> Set[bytes]:
    """Get set of cell-associated barcode strings.

    Args:
      barcode_csv_filename: TODO
      genome (bytes): Only get cell-assoc barcodes for this genome. If None, disregard genome.

    Returns:
      set of bytes: Cell-associated barcode strings (seq and gem-group).
    """
    if isinstance(genome, str):
        genome = genome.encode()
    cell_bcs_per_genome = load_barcode_csv(barcode_csv_filename)  # type: Dict[bytes, List[bytes]]
    cell_bcs = set()  # type: Set[bytes]
    for g, bcs in cell_bcs_per_genome.items():
        if genome is None or g == genome:
            cell_bcs.update(bcs)
    return cell_bcs


def string_is_ascii(input_string: Union[bytes, str]) -> bool:
    """Returns true if the string can be encoded as ascii.

    Input strings are often stored as ascii in numpy arrays, and we need
    to check that this conversion works.
    """
    if isinstance(input_string, bytes):
        try:
            input_string = input_string.decode(errors="strict")
        except UnicodeDecodeError:
            return False
    if isinstance(input_string, str):
        try:
            input_string.encode("ascii", errors="strict")
        except UnicodeEncodeError:
            return False
        return True
    # Not str or bytes
    return False
