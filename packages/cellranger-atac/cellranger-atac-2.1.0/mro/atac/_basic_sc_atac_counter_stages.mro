#
# Copyright (c) 2020 10x Genomics, Inc. All rights reserved.
#

filetype tsv.gz;
filetype tsv.gz.tbi;
filetype bed;
filetype h5;
filetype json;

struct FastqDef(
    path   read1,
    path   read2,
    bool   reads_interleaved,
    path   barcode,
    path   sample_index,
    int    gem_group,
    float  subsample_rate,
    string read_group,
    map    chemistry,
    string fastq_mode,
)

# SETUP_CHUNKS chunks up the input fastq data into sets of matched R1, R2, SI, and BC fastq files
stage ATAC_SETUP_CHUNKS(
    in  string     sample_id               "id of the sample",
    in  map[]      sample_def              "list of dictionary specifying input data",
    in  float      subsample_rate          "fraction of reads to preserve",
    in  string     barcode_whitelist       "List of valid barcodes",
    out FastqDef[] chunks,
    out path       barcode_whitelist_path  "Path to valid barcodes",
    src py         "stages/processing/setup_chunks",
)

stage GENERATE_PEAK_MATRIX(
    in  path   reference_path,
    in  tsv.gz fragments,
    in  bed    peaks,
    # optional: fragment barcode histogram. If provided we avoid a full pass of the fragments file
    # in the split,
    in  json   frag_bc_counts,
    out h5     raw_matrix,
    out path   raw_matrix_mex,
    src py     "stages/processing/generate_peak_matrix",
) split (
    in  file   barcodes,
) using (
    mem_gb   = 4,
    # N.B. we don't explicitly need the fragment index
    volatile = strict,
)
