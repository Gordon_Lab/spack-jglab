#
# Copyright (c) 2019 10x Genomics, Inc. All rights reserved.
#

filetype csv;
filetype bed;
filetype tsv.gz;
filetype tsv.gz.tbi;

stage ATAC_COUNTER_PREFLIGHT(
    in  string   sample_id,
    in  map[]    sample_def,
    in  path     reference_path,
    in  map      force_cells,
    in  float    peak_qval,
    in  string[] factorization,
    in  float    subsample_rate,
    in  bed      custom_peaks,
    in  bool     check_executables,
    src py       "stages/preflight/atac_counter",
) split (
) using (
    volatile = strict,
)

stage ATAC_AGGR_PREFLIGHT(
    in  string   sample_id,
    in  path     reference_path,
    in  csv      aggr_csv,
    in  string   normalization,
    in  string[] factorization,
    in  bed      custom_peaks,
    in  bool     check_executables,
    src py       "stages/preflight/atac_aggr",
) split (
) using (
    volatile = strict,
)

stage ATAC_REANALYZER_PREFLIGHT(
    in  string     sample_id,
    in  path       reference_path,
    in  string     barcode_whitelist,
    in  bed        peaks,
    in  csv        parameters,
    in  map        force_cells,
    in  csv        cell_barcodes,
    in  tsv.gz     fragments,
    in  tsv.gz.tbi fragments_index,
    in  csv        aggregation_csv,
    in  bool       check_executables,
    src py         "stages/preflight/atac_reanalyzer",
) split (
) using (
    volatile = strict,
)
