#
# Copyright (c) 2019 10x Genomics, Inc. All rights reserved.
#

@include "_common_analysis_metric_stages.mro"
@include "_cr_atac_stages.mro"
@include "_peak_caller.mro"
@include "_sc_atac_metric_collector_stages.mro"
@include "_library_aggregator_stages.mro"

filetype csv;
filetype bed;
filetype tsv.gz;
filetype tsv.gz.tbi;
filetype pickle;

pipeline _LIBRARY_AGGREGATOR(
    in  string     sample_id,
    in  string     sample_desc,
    in  string     assay,
    in  csv        aggr_csv,
    in  string     normalization,
    in  path       reference_path,
    in  bed        custom_peaks,
    out bed        peaks,
    out tsv.gz     fragments,
    out tsv.gz.tbi fragments_index,
    out csv        cell_barcodes,
    out csv        metrics_csv,
    out json       metrics,
    out json       gem_group_index_json,
    out json       library_info,
    out csv        singlecell,
    out json       frag_bc_counts,
)
{
    call SETUP_AGGR_SAMPLES(
        aggr_csv       = self.aggr_csv,
        normalization  = self.normalization,
        reference_path = self.reference_path,
        assay          = self.assay,
    )

    call NORMALIZE_LIBRARIES(
        library_info   = SETUP_AGGR_SAMPLES.library_info,
        reference_path = self.reference_path,
        assay          = self.assay,
    )

    call _PEAK_CALLER(
        sample_id       = self.sample_id,
        sample_desc     = self.sample_desc,
        assay           = self.assay,
        reference_path  = self.reference_path,
        fragments       = NORMALIZE_LIBRARIES.fragments.fragments,
        fragments_index = NORMALIZE_LIBRARIES.fragments.index,
        custom_peaks    = self.custom_peaks,
        qval            = null,
        disable_bigwig  = true,
    ) using (
        disabled = NORMALIZE_LIBRARIES.skip_peakcalling,
    )

    call COMPUTE_FRAGMENT_METRICS(
        fragments       = NORMALIZE_LIBRARIES.fragments.fragments,
        fragments_index = NORMALIZE_LIBRARIES.fragments.index,
        reference_path  = self.reference_path,
        peaks           = _PEAK_CALLER.peaks,
        cell_barcodes   = NORMALIZE_LIBRARIES.cell_barcodes,
        targeting_only  = false,
    )

    call SUMMARIZE_SINGLECELL_METRICS(
        reference_path = self.reference_path,
        singlecell     = COMPUTE_FRAGMENT_METRICS.singlecell,
    )

    call MERGE_ANALYSIS_METRICS(
        reference_path = self.reference_path,
        library_info   = SETUP_AGGR_SAMPLES.library_info,
        metrics        = [
            NORMALIZE_LIBRARIES.normalization_metrics,
            _PEAK_CALLER.peak_metrics,
            SUMMARIZE_SINGLECELL_METRICS.summary,
        ],
    )

    return (
        peaks                = _PEAK_CALLER.peaks,
        fragments            = NORMALIZE_LIBRARIES.fragments.fragments,
        fragments_index      = NORMALIZE_LIBRARIES.fragments.index,
        cell_barcodes        = COMPUTE_FRAGMENT_METRICS.cell_barcodes,
        gem_group_index_json = SETUP_AGGR_SAMPLES.gem_group_index_json,
        metrics              = MERGE_ANALYSIS_METRICS.metrics,
        metrics_csv          = MERGE_ANALYSIS_METRICS.metrics_csv,
        library_info         = SETUP_AGGR_SAMPLES.library_info,
        singlecell           = COMPUTE_FRAGMENT_METRICS.singlecell,
        frag_bc_counts       = COMPUTE_FRAGMENT_METRICS.frag_bc_counts,
    )
}
