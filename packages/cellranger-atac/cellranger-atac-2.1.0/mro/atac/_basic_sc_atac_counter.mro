#
# Copyright (c) 2019 10x Genomics, Inc. All rights reserved.
#

@include "_atac_matrix_computer.mro"
@include "_produce_cell_barcodes.mro"
@include "_sc_atac_postprocess_cells_stages.mro"
@include "_basic_sc_atac_counter_stages.mro"

pipeline _BASIC_SC_ATAC_COUNTER(
    in  string     sample_id,
    in  string     sample_desc,
    in  map[]      sample_def,
    in  path       reference_path,
    in  string     barcode_whitelist            "name of barcode whitelist file",
    in  float      subsample_rate,
    in  float      peak_qval,
    in  map        force_cells,
    in  bed        custom_peaks                 "Override for peak caller",
    out bam        possorted_bam                "bam file sorted by position",
    out bam.bai    possorted_bam_index          "position-sorted bam bai index",
    out tsv.gz     fragments,
    out tsv.gz.tbi fragments_index,
    out csv        cell_barcodes,
    out json       excluded_barcodes,
    out json       cell_calling_summary,
    out bed        peaks,
    out bigwig     cut_sites,
    out json       count_dict,
    out csv        singlecell_mapping,
    out csv        singlecell_cells,
    out json       peak_metrics,
    out h5         raw_peak_bc_matrix,
    out path       raw_peak_bc_matrix_mex,
    out h5         filtered_peak_bc_matrix,
    out path       filtered_peak_bc_matrix_mex,
    out csv        insert_sizes,
    out json       basic_summary,
    out json       frag_bc_counts,
)
{
    call ATAC_SETUP_CHUNKS(
        sample_id         = self.sample_id,
        sample_def        = self.sample_def,
        subsample_rate    = self.subsample_rate,
        barcode_whitelist = self.barcode_whitelist,
    ) using (
        volatile = true,
    )

    call _ATAC_MATRIX_COMPUTER(
        chunks                 = ATAC_SETUP_CHUNKS.chunks,
        barcode_whitelist_path = ATAC_SETUP_CHUNKS.barcode_whitelist_path,
        reference_path         = self.reference_path,
        assay                  = "atac",
        custom_peaks           = self.custom_peaks,
        peak_qval              = self.peak_qval,
        no_bam                 = false,
        sample_id              = self.sample_id,
        sample_desc            = self.sample_desc,
    )

    call _PRODUCE_CELL_BARCODES(
        fragments         = _ATAC_MATRIX_COMPUTER.fragments,
        fragments_index   = _ATAC_MATRIX_COMPUTER.fragments_index,
        peaks             = _ATAC_MATRIX_COMPUTER.peaks,
        force_cells       = self.force_cells,
        reference_path    = self.reference_path,
        barcode_whitelist = self.barcode_whitelist,
    )

    call FILTER_PEAK_MATRIX(
        cell_barcodes    = _PRODUCE_CELL_BARCODES.cell_barcodes,
        raw_matrix       = _ATAC_MATRIX_COMPUTER.raw_peak_bc_matrix,
        num_analysis_bcs = null,
        random_seed      = null,
    )

    return (
        possorted_bam               = _ATAC_MATRIX_COMPUTER.possorted_bam,
        possorted_bam_index         = _ATAC_MATRIX_COMPUTER.possorted_bam_index,
        singlecell_mapping          = _ATAC_MATRIX_COMPUTER.singlecell_mapping,
        singlecell_cells            = _PRODUCE_CELL_BARCODES.singlecell,
        cell_barcodes               = _PRODUCE_CELL_BARCODES.cell_barcodes,
        excluded_barcodes           = _PRODUCE_CELL_BARCODES.excluded_barcodes,
        cell_calling_summary        = _PRODUCE_CELL_BARCODES.cell_calling_summary,
        peak_metrics                = _ATAC_MATRIX_COMPUTER.peak_metrics,
        cut_sites                   = _ATAC_MATRIX_COMPUTER.cut_sites,
        count_dict                  = _ATAC_MATRIX_COMPUTER.count_dict,
        peaks                       = _ATAC_MATRIX_COMPUTER.peaks,
        fragments                   = _ATAC_MATRIX_COMPUTER.fragments,
        fragments_index             = _ATAC_MATRIX_COMPUTER.fragments_index,
        raw_peak_bc_matrix          = _ATAC_MATRIX_COMPUTER.raw_peak_bc_matrix,
        raw_peak_bc_matrix_mex      = _ATAC_MATRIX_COMPUTER.raw_peak_bc_matrix_mex,
        filtered_peak_bc_matrix     = FILTER_PEAK_MATRIX.filtered_matrix,
        filtered_peak_bc_matrix_mex = FILTER_PEAK_MATRIX.filtered_matrix_mex,
        insert_sizes                = _ATAC_MATRIX_COMPUTER.insert_sizes,
        basic_summary               = _ATAC_MATRIX_COMPUTER.basic_summary,
        frag_bc_counts              = _ATAC_MATRIX_COMPUTER.frag_bc_counts,
    )
}
