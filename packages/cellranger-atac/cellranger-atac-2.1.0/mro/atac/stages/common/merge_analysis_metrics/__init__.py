# Copyright (c) 2019 10x Genomics, Inc. All rights reserved.
"""
aggregates metrics
"""

from __future__ import absolute_import

# Imports
import json
import numbers
import martian
import numpy as np
import tenkit.safe_json
from common.metrics import MetricAnnotations
from atac.tools.ref_manager import ReferenceManager

# MRO docstring
__MRO__ = """
stage MERGE_ANALYSIS_METRICS(
    in  path   reference_path,
    in  json   library_info,
    in  json[] metrics,
    out json   metrics,
    out csv    metrics_csv,
    src py     "stages/common/merge_analysis_metrics",
) using (
    volatile = strict,
)
"""


def write_dict_to_csv(fn, dictionary, sort=False):
    """Write a single row CSV with header starting with a dictionary of (key, value) pairs. Float
        values are represented in general format with 12 significant decimal digits. Use this only
        for terminal pipeline outputs that are customer facing and not for consumption by other
        stages.

    Args:
        fn (bytes): Output CSV filename
        dictionary (Dict[Any, Any]): Dictionary of metrics to write
        sort (bool, optional): Sort column header lexicographically. Defaults to False.
    """
    with open(fn, "w") as f:
        keys = dictionary.keys() if not sort else sorted(dictionary.keys())
        f.write(",".join([str(k) for k in keys]) + "\n")
        for i, k in enumerate(keys):
            if i > 0:
                f.write(",")
            value = dictionary[k]
            if isinstance(value, numbers.Integral):
                f.write(str(value))
            elif isinstance(value, numbers.Real):
                f.write("{:.12g}".format(value))
            else:
                f.write(str(value))
        f.write("\n")


def main(args, outs):
    metrics = {}
    for fname in args.metrics:
        if fname is not None:
            with open(fname, "r") as f:
                metrics.update(json.load(f))

    # Normalize "NaN" values
    for key, value in metrics.items():
        if str(value) == "NaN" or (isinstance(value, float) and np.isnan(value)):
            metrics[key] = None

    # add version info
    metrics["cellranger-atac_version"] = martian.get_pipelines_version()

    if len(metrics) > 0:
        martian.log_info("Writing out summary_metrics")
        with open(outs.metrics, "w") as outfile:
            outfile.write(tenkit.safe_json.safe_jsonify(metrics, pretty=True))

    ctg_mgr = ReferenceManager(args.reference_path)
    species_list = ctg_mgr.list_species()

    # compile summary.csv metrics
    # load library info and fake libraries as species
    metrics_csv_dict = {}
    if args.library_info is not None:
        with open(args.library_info, "r") as f:
            library_info = json.load(f)
        # sort in numeric gem group order
        library_list = [library_info[n]["library_id"] for n in sorted(library_info.keys(), key=int)]
        metadata_per_lib = MetricAnnotations(species_list=library_list, species_is_library=True)
        metrics_subset = metadata_per_lib.compile_summary_metrics(
            metrics, species_list=library_list
        )
        metrics_csv_dict.update({m.subkey: m.value for m in metrics_subset})

    # load species level metrics
    metadata = MetricAnnotations(species_list=species_list)
    metrics_subset = metadata.compile_summary_metrics(metrics, species_list=species_list)
    metrics_csv_dict.update({m.subkey: m.value for m in metrics_subset})
    write_dict_to_csv(outs.metrics_csv, metrics_csv_dict, sort=True)
