# Copyright (c) 2019 10x Genomics, Inc. All rights reserved.
"""
Merges data from other stages into comprehensive outputs
"""

from __future__ import absolute_import, division

import json

import martian
import numpy as np
from atac.tools.ref_manager import ReferenceManager

import tenkit.safe_json
from common.metrics import MetricAnnotations, format_metric_print_value

__MRO__ = """
stage SUMMARIZE_REPORTS_SINGLECELL(
    in  string sample_id,
    in  string reference_path,
    in  json   complexity_summary,
    in  json   cell_calling_summary,
    in  json   peak_results,
    in  json   basic_results,
    in  json   insert_summary,
    in  json   singlecell_results,
    in  json   enrichment_results,
    out json   analysis_params,
    out json   summary,
    out csv    summary_csv,
    src py     "stages/reporter/summarize_reports_singlecell",
) using (
    mem_gb = 4,
)
"""


def split(args):
    raise NotImplementedError("No split")


def join(args, outs, chunk_defs, chunk_outs):
    raise NotImplementedError("No split")


def write_analysis_parameters(analysis_params_outfn):
    with open(analysis_params_outfn, "w") as analysis_params_out:
        analysis_params = {
            "analysis_version": martian.get_pipelines_version(),
            # Dropping meowmix version -- we're moving to putting special reference datasets into main repo
            "meowmix_version": "99.9.9",
            # Lena needs this set, even though we're not trimming
            "lead_trim": 0,
        }
        analysis_params_out.write(tenkit.safe_json.safe_jsonify(analysis_params))


def simple_load_metrics(summary_metrics, metrics_fn):
    with open(metrics_fn, "r") as infile:
        metrics = json.load(infile)
    summary_metrics.update(metrics)
    return summary_metrics


def write_cs_summary_csv(summary_info, ref, out_csv):
    """Generate the customer facing summary_csv metrics for single-species samples"""
    species_list = ref.list_species()
    metric_registry = MetricAnnotations(species_list=species_list)
    metrics = metric_registry.compile_summary_metrics(summary_info, species_list=species_list)

    header = [m.name for m in metrics]
    value = [m.value for m in metrics]

    csv_dict = dict(zip(header, value))
    # Add additional metrics
    csv_dict.update(
        {
            "Sample ID": summary_info.get("sample_id", ""),
            "Genome": ref.genome,
            "Pipeline version": summary_info.get("cellranger-atac_version", ""),
        }
    )
    print(csv_dict)
    # set column order
    with open(out_csv, "w") as out:
        first_cols = [
            "Sample ID",
            "Genome",
            "Pipeline version",
        ]
        first_cols.extend(
            [m.name for m in metrics if m.name.startswith("Estimated number of cells")]
        )
        values = []
        for i, col in enumerate(first_cols):
            if i > 0:
                out.write(",")
            out.write(col)
            # use None for any missing metric
            values.append(format_metric_print_value(csv_dict.get(col)))
        for col in sorted(csv_dict.keys()):
            if col not in first_cols:
                out.write(",")
                out.write(col)
                # use None for any missing metric
                values.append(format_metric_print_value(csv_dict.get(col)))
        out.write("\n")
        for i, value in enumerate(values):
            if i > 0:
                out.write(",")
            out.write(value)
        out.write("\n")


def main(args, outs):
    reference = ReferenceManager(args.reference_path)

    martian.log_info("Writing analysis parameters")
    write_analysis_parameters(outs.analysis_params)

    martian.log_info("Initializing summary metrics")
    summary_metrics = {}
    summary_metrics = simple_load_metrics(summary_metrics, args.basic_results)
    summary_metrics["cellranger-atac_version"] = martian.get_pipelines_version()
    summary_metrics["sample_id"] = args.sample_id

    if args.singlecell_results is not None:
        martian.log_info("Loading single cell results")
        summary_metrics = simple_load_metrics(summary_metrics, args.singlecell_results)

    if args.insert_summary is not None:
        martian.log_info("Loading insert summary")
        summary_metrics = simple_load_metrics(summary_metrics, args.insert_summary)

    if args.complexity_summary is not None:
        martian.log_info("Loading complexity summary")
        summary_metrics = simple_load_metrics(summary_metrics, args.complexity_summary)

    if args.peak_results is not None:
        martian.log_info("Loading peak results")
        summary_metrics = simple_load_metrics(summary_metrics, args.peak_results)

    if args.enrichment_results is not None:
        martian.log_info("Loading TSS and CTCF scores")
        summary_metrics = simple_load_metrics(summary_metrics, args.enrichment_results)

    if args.cell_calling_summary is not None:
        martian.log_info("Loading cell calling parameters")
        summary_metrics = simple_load_metrics(summary_metrics, args.cell_calling_summary)

    # Normalize "NaN" values
    for key, value in summary_metrics.items():
        if str(value) == "NaN" or (isinstance(value, float) and np.isnan(value)):
            summary_metrics[key] = None

    if reference.metadata:
        # If we have reference metadata - copy over the data to summary.json
        for (key, value) in reference.metadata.items():
            summary_metrics["reference_" + key] = value

    martian.log_info("Writing out summary_metrics")
    with open(outs.summary, "w") as outfile:
        outfile.write(tenkit.safe_json.safe_jsonify(summary_metrics, pretty=True))

    # compile summary.csv metrics
    write_cs_summary_csv(summary_metrics, reference, outs.summary_csv)
