# Copyright (c) 2019 10x Genomics, Inc. All rights reserved.
"""
Produces the final customer-facing web_summary.
"""

from __future__ import absolute_import, division

import json
import os

import h5py
import martian
import numpy as np
import pandas as pd
from six import ensure_str

from tenkit.safe_json import json_sanitize
import cellranger.analysis.constants as cr_analysis_constants
import cellranger.analysis.graphclust as cr_graphclust
import cellranger.matrix as cr_matrix
from atac.constants import RPC_30K, RPC_50K, RPC_10K
from atac.plots import (
    generate_ctcf_enrichment_plot,
    generate_tss_enrichment_plot,
    generate_targeting_plot,
    generate_clustering_plot,
    generate_knee_plot,
    generate_fragment_counts_plot,
    generate_wasted_data_plot,
    generate_insertsize_plot,
    generate_purity_plot,
    generate_barnyard_plot,
    generate_assay_efficiency_plot,
    generate_singlecell_complexity_plot,
    generate_bulk_complexity_plot,
)
from atac.tools.ref_manager import ReferenceManager
from common.metrics import MetricAnnotations
from common.plots.plotly_tools import PLOT_CONFIG_KWARGS
from websummary import summarize

__MRO__ = """
stage CREATE_WEBSUMMARY(
    in  path   reference_path,
    in  string barcode_whitelist,
    in  json   summary_results,
    in  json   bulk_complexity,
    in  json   singlecell_complexity,
    in  string sample_id,
    in  string sample_desc,
    in  map[]  sample_def,
    in  bool   debug,
    in  float  peak_qval,
    in  csv    singlecell,
    in  csv    insert_sizes,
    in  csv    tss_relpos,
    in  csv    ctcf_relpos,
    in  h5     filtered_peak_bc_matrix,
    in  h5     analysis,
    in  json   excluded_barcodes,
    out html   web_summary,
    out json   data,
    src py     "stages/reporter/create_websummary",
) using (
    mem_gb   = 16,
    volatile = strict,
)
"""

DOWNSAMPLE_BARNYARD = True


def get_hero_metric_data(metadata, summary_data, species_list, debug):
    """Adds a few metrics of primary importance into the header of the web summary:
    - Annotated cell counts (split by species if barnyard)
    - Median fragments per cell (split)
    - Non-dup wasted data
    Alerts raised depend on debug status.
    """
    data = {}
    if summary_data is None:
        return data

    for key in [
        "annotated_cells",
        "median_fragments_per_cell",
        "frac_fragments_overlapping_peaks",
    ]:
        metrics = metadata.gen_metric_list(
            summary_data, [key], species_list=species_list, debug=debug
        )
        for metric in metrics:
            key = metric.key
            if len(species_list) > 1:
                for i, species in enumerate(species_list):
                    key = key.replace(species, "sp{}".format(i + 1))
            data[key] = metric.gen_metric_dict()

    # Alerts.
    alarm_keys = [
        "annotated_cells",
        "median_fragments_per_cell",
        "frac_fragments_overlapping_peaks",
        "frac_high_count_excluded_bcs_whitelist_contam",
        "cell_calling_method",
    ]
    alarms = metadata.gen_metric_list(
        summary_data, alarm_keys, species_list=species_list, debug=debug
    )
    new_alarms = [metric.alarm_dict for metric in alarms if metric.alarm_dict]
    if new_alarms:
        data["alarms"] = new_alarms

    # low number of annotated_cells is a special alert case.
    # greater than case is handled automatically with the metrics.csv ranges;
    # less than case is handled here.

    for species in species_list:
        suffix = "_{}".format(species) if len(species_list) > 1 else ""
        suffix_withpar = " ({})".format(species) if len(species_list) > 1 else ""
        sufkey = "annotated_cells{}".format(suffix)
        if sufkey in summary_data:
            if summary_data[sufkey] < 500:
                # create alarm
                alarm_dict = {
                    "raw_value": summary_data["annotated_cells{}".format(suffix)],
                    "formatted_value": "{:,.0f}".format(
                        summary_data["annotated_cells{}".format(suffix)]
                    ),
                    "raised": True,
                    "parent": "annotated_cells{}".format(suffix),
                    "title": "Estimated number of cells is low{}".format(suffix_withpar),
                    "message": "Number of cells detected is expected to be higher than 500.  This usually \
                                    indicates poor cell, library, or sequencing quality.",
                    "level": "WARN",
                    "test": "",
                    "id": "annotated_cells{}".format(suffix),
                }
                if "alarms" in data:
                    data["alarms"].append(alarm_dict)
                else:
                    data["alarms"] = [alarm_dict]

    return data


def get_barnyard_data(metadata, summary_data, species_list, singlecell_df, debug, downsample):
    """Adds a barnyard plot for multi-species samples.  Alerts raised depend on debug status."""
    data = {}
    if summary_data is None:
        return data

    metric_keys = ["observed_doublet_rate", "inferred_doublet_rate", "median_purity"]
    metrics = metadata.gen_metric_list(summary_data, metric_keys, species_list, debug=debug)
    data["barnyard_metrics_table"] = {
        "rows": [[metric.name, metric.value_string] for metric in metrics]
    }

    helptext_metrics = metadata.gen_metric_helptext(metric_keys)
    helptext_plots = [
        [
            "Plots",
            [
                "(left) Barnyard scatter plot, where each dot represents a barcode and its coordinates indicate "
                "number of fragments, from each species, assigned to the barcode. Groups are estimated "
                "computationally.",
                "(right) Histograms of per barcode purities estimated for barcodes in each species.",
            ],
        ]
    ]
    helptext = helptext_metrics + helptext_plots
    data["barnyard_helptext"] = {"title": "Barnyard", "data": helptext}

    if singlecell_df is None:
        return data

    data["plot_barnyard"] = generate_barnyard_plot(singlecell_df, species_list, downsample)
    data["plot_purity"] = generate_purity_plot(singlecell_df, species_list)

    return data


def get_cell_metrics_data(
    metadata, summary_data, species_list, singlecell_df, excluded_barcodes, debug
):
    """Adds a section with per-cell sensitivity metrics, and fragments per cell histograms."""
    data = {}
    if summary_data is None:
        return data

    metric_keys = [
        "annotated_cells",
        "mean_sequenced_fragments_per_cell",
        "frac_passed_filter_fragments_in_cells",
        "frac_cut_fragments_in_peaks",
        "median_fragments_per_cell",
    ]

    if debug:
        metric_keys.append("median_fragments_per_noncell")
        metric_keys.append("estimated_gelbead_doublet_rate")
    metrics = metadata.gen_metric_list(summary_data, metric_keys, species_list, debug=debug)
    data["cell_metrics_table"] = {
        "rows": [[metric.name, metric.value_string] for metric in metrics]
    }

    helptext_metrics = metadata.gen_metric_helptext(metric_keys)
    helptext_plots = [
        [
            "Plots",
            [
                "(left) Knee plot of number of fragments overlapping peaks for all the barcodes in the library. "
                "This number is used to call cells.",
                "(right) Histograms of number of fragments per cell barcode for non-cells and cells.",
            ],
        ]
    ]
    helptext = helptext_metrics + helptext_plots
    data["cell_helptext"] = {"title": "Cells", "data": helptext}

    if singlecell_df is None:
        return data

    for i, species in enumerate(species_list):
        key_suffix = "_spec{}".format(i + 1) if species else ""
        data["fragments_per_cell_plot{}".format(key_suffix)] = generate_fragment_counts_plot(
            singlecell_df, species
        )
        data["barcode_knee_plot{}".format(key_suffix)] = generate_knee_plot(
            singlecell_df, excluded_barcodes, species
        )

    # Alerts.
    alarm_keys = [
        "mean_sequenced_fragments_per_cell",
        "frac_passed_filter_fragments_in_cells",
        "frac_cut_fragments_in_peaks",
    ]
    add_alarms(data, metadata, summary_data, alarm_keys, species_list, debug)

    return data


def get_wasted_data(metadata, summary_data, singlecell_df, species_list, debug):
    """Add metrics on wasted data fractions and ratios along with a
    Sankey flow diagram of fragments.  Alerts raised depend on debug status.
    """
    data = {}
    if summary_data is None:
        return data

    metric_keys = [
        "num_fragments",
        "total_usable_fragments",
        "total_usable_ontarget_fragments",
        "frac_fragments_usable_ontarget",
        "frac_valid_barcode",
        "frac_valid_noncell",
        "frac_waste_overall_nondup",
        "waste_ratio_mito_cells",
    ]
    metrics = metadata.gen_metric_list(summary_data, metric_keys, species_list, debug=debug)
    data["wasted_data_table"] = {"rows": [[metric.name, metric.value_string] for metric in metrics]}

    helptext_metrics = metadata.gen_metric_helptext(metric_keys)
    helptext_plots = [
        [
            "Plots",
            [
                "Sankey flow diagram showing the final label assigned to each of the read pairs sequenced in the library."
            ],
        ],
    ]
    helptext = helptext_metrics + helptext_plots
    data["wasted_helptext"] = {"title": "Wasted Data", "data": helptext}
    if singlecell_df is None:
        return data

    data["sankey_fragments_plot"] = generate_wasted_data_plot(singlecell_df, species_list)
    return data


def get_pipeline_info(args, summary, debug):
    """Generates a table of general pipeline information."""
    reference = ReferenceManager(args.reference_path)
    metadata = reference.metadata

    rows = [
        ["Sample ID", args.sample_id],
        ["Sample description", args.sample_desc],
        ["Pipeline version", martian.get_pipelines_version()],
        ["Reference path", args.reference_path],
        # ATAC data generated by the joint ATAC+RNA assay can also be processed through the ATAC pipeline
        ["Chemistry", "ARC-v1" if args.barcode_whitelist == "737K-arc-v1" else "ATAC"],
        ["Organism", metadata.get("organism")],
    ]

    if summary.get("cell_calling_method", None) == "force_cells":
        rows.extend(
            [
                ["Cell caller", "Force cells"],
            ]
        )
    if summary.get("peakcaller_method", None) == "custom":
        rows.extend(
            [
                ["Peaks", "User defined"],
            ]
        )

    if args.peak_qval:
        rows.extend(
            [
                ["Custom peak q-value", args.peak_qval],
            ]
        )

    if debug:
        rows.append(["Barcode Whitelist", args.barcode_whitelist])

    data = {"pipeline_info_table": {"rows": rows}}
    data["pipeline_helptext"] = {"title": "Sample", "data": []}
    return data


def get_sequencing_info(metadata, summary_data, species_list, debug):
    """Alerts raised depend on debug status."""
    data = {}
    if summary_data is None:
        return data

    metric_keys = [
        "num_fragments",
        "frac_valid_barcode",
        "bc_q30_bases_frac",
        "r1_q30_bases_frac",
        "r2_q30_bases_frac",
        "si_q30_bases_frac",
    ]
    metrics = metadata.gen_metric_list(summary_data, metric_keys, species_list, debug=debug)
    data["sequencing_info_table"] = {
        "rows": [[metric.name, metric.value_string] for metric in metrics]
    }

    helptext = metadata.gen_metric_helptext(metric_keys)
    data["sequencing_helptext"] = {"title": "Sequencing", "data": helptext}

    # Alerts.
    alarm_keys = [
        "frac_valid_barcode",
        "bc_q30_bases_frac",
        "r1_q30_bases_frac",
        "r2_q30_bases_frac",
        "peakcaller_method",
    ]
    add_alarms(data, metadata, summary_data, alarm_keys, species_list, debug)

    return data


def get_mapping_data(metadata, summary_data, singlecell_df, insertsize_fn, species_list, debug):
    data = {}
    if summary_data is None:
        return data

    metric_keys = [
        "frac_reads_mapq_gt_30",
        "frac_reads_unmapped",
        "frac_reads_mapq_gt_30_mito",
        "frac_fragments_nfr",
        "frac_fragments_nuc",
    ]

    if debug:
        metric_keys.append("insert_twist_period")
        metric_keys.append("insert_nucleosome_period")

    metrics = metadata.gen_metric_list(summary_data, metric_keys, species_list=species_list)
    data["mapping_table"] = {"rows": [[metric.name, metric.value_string] for metric in metrics]}

    helptext_metrics = metadata.gen_metric_helptext(metric_keys)
    if debug:
        helptext_plots = [
            [
                "Plots",
                [
                    "(left) Insert size distribution in log scale.",
                    "(right) Insert size distribution in linear scale.",
                ],
            ]
        ]
    else:
        helptext_plots = [["Plots", ["Insert size distribution in linear scale."]]]

    helptext = helptext_metrics + helptext_plots
    data["mapping_helptext"] = {"title": "Mapping", "data": helptext}

    if singlecell_df is None or insertsize_fn is None:
        return data

    if debug:
        data["insert_size_plot"] = generate_insertsize_plot(
            singlecell_df, insertsize_fn, species_list, "log"
        )
    data["non_log_insert_size_plot"] = generate_insertsize_plot(
        singlecell_df, insertsize_fn, species_list, "linear"
    )

    # Alerts.
    add_alarms(data, metadata, summary_data, ["frac_reads_mapq_gt_30"], species_list, debug)

    return data


def get_targeting_data(
    metadata,
    summary_data,
    species_list,
    singlecell_df,
    tss_relpos,
    ctcf_relpos,
    debug,
    excluded_barcodes,
):
    data = {}
    if summary_data is None:
        return data

    metric_keys = [
        "total_peaks_detected",
        "frac_primary_genome_in_peaks",
        "tss_enrichment_score",
        "frac_fragments_overlapping_tss",
        "frac_fragments_overlapping_peaks",
    ]

    if debug:
        metric_keys += [
            "frac_fragments_overlapping_targets",
            "ctcf_enrichment_score",
        ]
    metrics = metadata.gen_metric_list(
        summary_data, metric_keys, species_list=species_list, debug=debug
    )
    data["targeting_table"] = {"rows": [[metric.name, metric.value_string] for metric in metrics]}
    helptext_targeting = metadata.gen_metric_helptext(metric_keys)

    helptext_plots = (
        [
            [
                "Plots",
                [
                    "(left) TSS profile, as described above.",
                    (
                        "(right) Scatter plot that represents each barcode as a point. The position on the "
                        "x-axis reflects the number fragments associated with that barcode, while the position "
                        "on the y-axis corresponds to the percentage of those fragments that overlap peaks. "
                        "Non-cell and cell barcodes are represented with different colors."
                    ),
                ],
            ],
        ]
        if not debug
        else [
            [
                "Plots",
                [
                    "(1st row, left) TSS profile, as described above.",
                    "(1st row, right) CTCF profile, as described above.",
                    "(all other rows) Targeting scatter plots. Each dot represents a barcode. Horizontal axis is the barcode's number of fragments, vertical axis is the percentage of those fragments that overlap different sets of functional genomic regions. Each panel corresponds to a different kind of region. Non-cell and cell groups are represented with different colors.",
                ],
            ],
        ]
    )

    helptext = helptext_targeting + helptext_plots

    data["targeting_helptext"] = {"title": "Targeting", "data": helptext}
    if singlecell_df is None:
        return data

    if tss_relpos is not None:
        tss_file_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), "tss_ref.csv")
        data["tss_enrichment_plot"] = generate_tss_enrichment_plot(tss_relpos, tss_file_path, debug)

    if ctcf_relpos is not None and debug:
        ctcf_file_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), "ctcf_ref.csv")
        data["ctcf_enrichment_plot"] = generate_ctcf_enrichment_plot(
            ctcf_relpos, ctcf_file_path, debug
        )

    data["peak_targeting_plot"] = generate_targeting_plot(singlecell_df, species_list, "Peaks")
    if debug:
        data["tss_targeting_plot"] = generate_targeting_plot(singlecell_df, species_list, "TSS")
        # Peak targeting plot showing specially excluded barcodes
        data["peak_excluded_targeting_plot"] = generate_targeting_plot(
            singlecell_df, species_list, "Peaks", excluded_barcodes
        )

    # Alerts.
    metric_keys = ["frac_primary_genome_in_peaks", "tss_enrichment_score"]
    add_alarms(data, metadata, summary_data, metric_keys, species_list, debug)

    return data


def get_peakcalling_data(metadata, summary_data, species_list, debug):
    data = {}
    if summary_data is None:
        return data

    metric_keys = {
        "total_peaks_detected",
        "frac_primary_genome_in_peaks",
        "frac_cut_fragments_in_peaks",
    }
    metrics = metadata.gen_metric_list(summary_data, metric_keys, species_list, debug=debug)

    data["peak_calling_table"] = {
        "rows": [[metric.name, metric.value_string] for metric in metrics]
    }
    helptext = metadata.gen_metric_helptext(metric_keys)
    data["peakcalling_helptext"] = {"title": "Peak Calling", "data": helptext}

    return data


def get_peak_length_plot(summary_data):
    """Generate a plot showing the length distribution of called peaks."""
    lengths, counts = zip(
        *sorted(
            [
                (int(length), int(count))
                for length, count in summary_data["peak_length_distribution"].items()
            ],
            key=lambda x: x[0],
        )
    )
    yvals, xvals = np.histogram(lengths, weights=counts, bins=350)

    data = {
        "peak_length_plot": {
            "layout": {
                "xaxis": {
                    "type": "linear",
                    "title": "Peak Size (bp)",
                },
                "yaxis": {
                    "type": "log",
                    "title": "counts",
                },
                "title": "Peak Size Distribution",
                "hovermode": "closest",
            },
            "data": [
                {
                    "name": "Cellranger Peaks",
                    "x": [float(x) for x in xvals],
                    "y": [int(y) for y in yvals],
                    "type": "scatter",
                    "connectgaps": True,
                    "fill": "tozeroy",
                }
            ],
        }
    }
    return data


def get_complexity_data(
    metadata, summary_data, bulk_complexity, singlecell_complexity, species_list, debug
):
    data = {}
    if summary_data is None:
        return data

    metric_keys = [
        "frac_pcr_dup_passed_filters",
    ]
    metrics = metadata.gen_metric_list(summary_data, metric_keys, species_list, debug=debug)
    data["bulk_complexity_table"] = {
        "rows": [[metric.name, metric.value_string] for metric in metrics]
    }

    helptext_metrics = metadata.gen_metric_helptext(metric_keys)
    helptext_plots = (
        [
            [
                "Plots",
                [
                    "(top, left) Observed and fitted bulk library complexity as a function of downsampling rate.",
                    "(top, right) Observed median per cell unique fragments as a function of downsampled mean read pairs per cell.",
                    "(bottom) Observed per cell complexity as a function of downsampling rate in mean read pairs per cell, but only sampling high quality read pairs.",
                ],
            ],
        ]
        if debug
        else [
            [
                "Plots",
                [
                    "Observed median per cell unique fragments as a function of downsampled mean read pairs per cell."
                ],
            ],
        ]
    )
    helptext = helptext_metrics + helptext_plots
    data["complexity_helptext"] = {"title": "Library Complexity", "data": helptext}

    if singlecell_complexity is not None:
        with open(singlecell_complexity, "r") as infile:
            complexity_data = json.load(infile)
        data["assay_efficiency_plot"] = generate_assay_efficiency_plot(complexity_data)
        if debug:
            data["singlecell_complexity_plot"] = generate_singlecell_complexity_plot(
                complexity_data, summary_data
            )

    if debug and bulk_complexity is not None:
        with open(bulk_complexity, "r") as infile:
            complexity_data = json.load(infile)
        data["bulk_complexity_plot"] = generate_bulk_complexity_plot(complexity_data, summary_data)

    return data


def get_clustering_plots(
    analysis, filtered_peak_bc_matrix, species_list, singlecell_df, is_barnyard
):
    data = {}
    if filtered_peak_bc_matrix is None or analysis is None:
        return data

    tsne_results = np.array(
        h5py.File(analysis, "r")[cr_analysis_constants.ANALYSIS_H5_TSNE_GROUP]["_2"][
            "transformed_tsne_matrix"
        ]
    )
    # N.B.: these cluster labels are 1-indexed
    cluster_labels = cr_graphclust.load_graphclust_from_h5(analysis).clusters
    barcodes = [
        bc.decode() for bc in cr_matrix.CountMatrix.load_bcs_from_h5(filtered_peak_bc_matrix)
    ]
    data["graph_cluster_plot"] = generate_clustering_plot(
        singlecell_df, tsne_results, cluster_labels, barcodes, species_list, method="cluster"
    )

    data["depth_cluster_plot"] = generate_clustering_plot(
        singlecell_df, tsne_results, cluster_labels, barcodes, species_list, method="depth"
    )

    if len(species_list) == 2:
        # Add an additional plot with cells colored by species for barnyard samples
        data["barnyard_cluster_plot"] = generate_clustering_plot(
            singlecell_df, tsne_results, cluster_labels, barcodes, species_list, method="species"
        )

    helptext = [
        [
            "Plots",
            [
                (
                    "(left) The plot represents cell-associated barcodes in a 2-D embedding produced "
                    "by the t-SNE algorithm on dimensionality reduced ATAC counts. The colors show an automated "
                    "graph clustering analysis which groups together cells that have similar chromatin "
                    "accessibility profiles."
                ),
                (
                    "(right) The same 2D projection is shown, but cell coloring depicts the number of "
                    "high-quality fragments associated with each barcode."
                ),
            ],
        ]
    ]

    if is_barnyard:
        helptext[0][1] += [
            "(bottom) Scatter plot of barcodes annotated as cells, colored by species."
        ]
    data["clustering_helptext"] = {"title": "Cell Clustering", "data": helptext}

    return data


def get_master_table(metadata, summary_data, species_list, is_barnyard, debug):
    data = {}
    if summary_data is None:
        return data
    metric_keys = [
        "annotated_cells",
        "cell_threshold",
        "median_fragments_per_cell",
        "median_fragments_per_noncell",
        "num_fragments",
        "total_usable_fragments",
        "frac_valid_barcode",
        "frac_valid_noncell",
        "frac_waste_overall_nondup",
        "waste_ratio_mito_cells",
        "r1_q30_bases_frac",
        "r2_q30_bases_frac",
        "bc_q30_bases_frac",
        "si_q30_bases_frac",
        "insert_twist_period",
        "insert_nucleosome_period",
        "frac_fragments_nfr",
        "frac_fragments_nuc",
        "frac_fragments_overlapping_targets",
        "frac_fragments_overlapping_tss",
        "frac_fragments_overlapping_dnase",
        "frac_fragments_overlapping_enhancer",
        "frac_fragments_overlapping_promoter",
        "frac_fragments_overlapping_blacklist",
        "frac_fragments_overlapping_peaks",
        "tss_enrichment_score",
        "ctcf_enrichment_score",
        "total_peaks_detected",
        "frac_cut_fragments_in_peaks",
        "bulk_total_library_complexity",
        "bulk_estimated_saturation",
        "frac_waste_duplicate",
        "frac_waste_mitochondrial",
        "median_per_cell_unique_fragments_at_{}_HQ_RPC".format(RPC_10K),
        "median_per_cell_unique_fragments_at_{}_HQ_RPC".format(RPC_30K),
        "median_per_cell_unique_fragments_at_{}_HQ_RPC".format(RPC_50K),
        "median_per_cell_unique_fragments_at_{}_RRPC".format(RPC_10K),
        "median_per_cell_unique_fragments_at_{}_RRPC".format(RPC_30K),
        "median_per_cell_unique_fragments_at_{}_RRPC".format(RPC_50K),
    ]
    if debug:
        metric_keys.append("estimated_gelbead_doublet_rate")
    if is_barnyard:
        metric_keys.extend(
            [
                "observed_doublet_rate",
                "inferred_doublet_rate",
            ]
        )
    metrics = metadata.gen_metric_list(summary_data, metric_keys, species_list, debug=debug)
    rows = [
        [
            metric.category,
            metric.name,
            metric.value_string,
            metric.acceptable_string,
            metric.targeted_string,
        ]
        for metric in metrics
    ]
    data["master_table"] = {
        "header": ["Category", "Metric", "Value", "Acceptable", "Targeted"],
        "rows": sorted(rows, key=lambda r: (r[0], r[1])),
    }

    data["summary_helptext"] = {"title": "Summary Metrics", "data": []}
    return data


def add_data(websummary_data, input_data):
    """Adds data to global dictionary, returns content"""
    if "alarms" in input_data:
        websummary_data["alarms"]["alarms"].extend(input_data["alarms"])
        del input_data["alarms"]
    websummary_data.update(input_data)


def add_alarms(websummary_data, metadata, summary_info, alarm_keys, species_list, debug):
    """Adds alarms to websummary dictionary, returns content"""
    alarms = metadata.gen_metric_list(summary_info, alarm_keys, species_list, debug=debug)
    new_alarms = [metric.alarm_dict for metric in alarms if metric.alarm_dict]
    if new_alarms:
        websummary_data.update({"alarms": new_alarms})


def find_template(debug=False):
    """Pulls up the correct template information"""
    template_path = os.path.dirname(os.path.abspath(__file__))
    if debug:
        template_file = os.path.join(template_path, "templates_pd", "websummary.html")
    else:
        template_file = os.path.join(template_path, "templates", "websummary.html")

    with open(template_file, "r") as infile:
        template = infile.read()
    return (template, template_path)


def truncate_title(sample_id, sample_desc, cutoff=40):
    """Generate a plot sub-title of fixed character length"""

    title = "{} - {}".format(sample_id, sample_desc)
    title = (title[: cutoff - 3] + "...") if len(title) > cutoff else title
    return title


def main(args, outs):
    reference = ReferenceManager(args.reference_path)
    species_list = reference.list_species()
    is_barnyard = len(species_list) > 1 and args.singlecell is not None

    singlecell_df = None
    if args.singlecell:
        singlecell_df = pd.read_csv(ensure_str(args.singlecell))
        singlecell_df.set_index("barcode", inplace=True, drop=False)

    summary_data = None
    if args.summary_results:
        with open(args.summary_results, "r") as infile:
            summary_data = json.load(infile)

    if args.excluded_barcodes is not None:
        with open(args.excluded_barcodes, "r") as infile:
            excluded_barcodes = json.load(infile)
    else:
        excluded_barcodes = None

    metadata = MetricAnnotations(species_list=species_list)
    websummary_data = {
        "alarms": {"alarms": []},
        "sample": {
            "id": args.sample_id,
            "description": args.sample_desc,
            "pipeline": "Cell Ranger ATAC",
        },
        "is_barnyard": int(is_barnyard),
    }

    # Pull out all the general-purpose information
    add_data(
        websummary_data, get_hero_metric_data(metadata, summary_data, species_list, args.debug)
    )
    add_data(websummary_data, get_pipeline_info(args, summary_data, args.debug))
    add_data(websummary_data, get_sequencing_info(metadata, summary_data, species_list, args.debug))
    add_data(
        websummary_data,
        get_cell_metrics_data(
            metadata, summary_data, species_list, singlecell_df, excluded_barcodes, args.debug
        ),
    )
    add_data(
        websummary_data,
        get_clustering_plots(
            args.analysis, args.filtered_peak_bc_matrix, species_list, singlecell_df, is_barnyard
        ),
    )
    add_data(
        websummary_data,
        get_mapping_data(
            metadata, summary_data, singlecell_df, args.insert_sizes, species_list, args.debug
        ),
    )
    add_data(
        websummary_data,
        get_targeting_data(
            metadata,
            summary_data,
            species_list,
            singlecell_df,
            args.tss_relpos,
            args.ctcf_relpos,
            args.debug,
            excluded_barcodes,
        ),
    )
    add_data(
        websummary_data,
        get_complexity_data(
            metadata,
            summary_data,
            args.bulk_complexity,
            args.singlecell_complexity,
            species_list,
            args.debug,
        ),
    )

    # For barnyard samples only
    if is_barnyard:
        add_data(
            websummary_data,
            get_barnyard_data(
                metadata, summary_data, species_list, singlecell_df, args.debug, DOWNSAMPLE_BARNYARD
            ),
        )

    # For PD runs only
    if args.debug:
        add_data(
            websummary_data, get_peakcalling_data(metadata, summary_data, species_list, args.debug)
        )
        # pylint: disable=too-many-function-args
        add_data(websummary_data, get_peak_length_plot(summary_data))
        add_data(
            websummary_data,
            get_wasted_data(metadata, summary_data, singlecell_df, species_list, args.debug),
        )
        add_data(
            websummary_data,
            get_master_table(metadata, summary_data, species_list, is_barnyard, args.debug),
        )

    # Modify the titles of plots to add consistent plot styling sample ID/descriptions
    for subdata in websummary_data.values():
        try:
            if "layout" in subdata:
                subtitle = truncate_title(args.sample_id, args.sample_desc)
                if "<br><sup>" in subdata["layout"]["title"]:
                    title_line1_nchars = subdata["layout"]["title"].find("<br><sup>")
                    title_line1 = subdata["layout"]["title"][:title_line1_nchars]
                    subdata["layout"]["title"] = title_line1
                subdata["layout"]["title"] += "<br><sup>{}</sup>".format(subtitle)
                subdata["layout"]["hovermode"] = "closest"
                subdata["config"] = PLOT_CONFIG_KWARGS
                if "xaxis" in subdata["layout"] and "yaxis" in subdata["layout"]:
                    subdata["layout"]["xaxis"]["fixedrange"] = False
                    subdata["layout"]["yaxis"]["fixedrange"] = False
        except TypeError:
            pass

    websummary_data = json_sanitize(websummary_data)

    template, template_path = find_template(args.debug)
    print(template_path)
    with open(outs.web_summary, "w") as outfile:
        summarize.generate_html_summary(websummary_data, template, template_path, outfile)

    with open(outs.data, "w") as out:
        json.dump(websummary_data, out, sort_keys=True, indent=4)
