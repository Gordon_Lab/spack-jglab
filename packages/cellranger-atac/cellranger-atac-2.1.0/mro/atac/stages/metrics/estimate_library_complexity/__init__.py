# Copyright (c) 2019 10x Genomics, Inc. All rights reserved.
"""
Estimates total library complexity -- both bulk and on a single cell basis -- using total and unique fragments.
"""

from collections import Counter, defaultdict
import json
from typing import DefaultDict, Set, Dict
from six import ensure_binary

import numpy as np
import pandas as pd
from scipy import optimize

from atac.constants import DOWNSAMPLED_READS, RPC_30K, RPC_50K, RPC_10K
import atac.barcode_loading as bc_loading
from atac.tools.fragments import open_fragment_file
from atac.tools.regions import get_target_regions, fragment_overlaps_target
from atac.utils import get_chunks
from cellranger.csv_utils import combine_csv
import tenkit.stats as tk_stats
from tenkit.safe_json import json_sanitize
import martian

__MRO__ = """
stage ESTIMATE_LIBRARY_COMPLEXITY(
    in  json   sequencing_summary,
    in  tsv.gz fragments,
    in  csv    cell_barcodes,
    in  json   frag_bc_counts,
    in  bed    peaks,
    out json   bulk_complexity,
    out json   complexity_summary,
    out json   singlecell_complexity,
    src py     "stages/metrics/estimate_library_complexity",
) split (
    in  file   barcodes,
    out file   singlecell_targeted_complexity,
) using (
    mem_gb   = 2,
    volatile = strict,
)
"""


def get_unique_and_total_fragments(dup_counts):
    """
    Takes as input a dictionary of duplicate counts:  for each copy number of duplicates,
    it reports how many unique fragments with that copy number were observed.

    This function collapses the dictionary, returning both the number of unique fragments
    observed as well as the total number of fragments.
    """
    unique = 0
    total = 0
    for copy_num, fragment_count in dup_counts.items():
        unique += fragment_count
        total += fragment_count * int(copy_num)
    return unique, total


def interpolate_downsampled_counts(sampled_rates, unique_array, requested_rate):
    """
    Takes paired downsampled arrays for the total and unique number of fragments at varying
    downsampling rates between 0 and 1.  The last entry in both array is guaranteed to be the
    non-downsampled count of total or unique fragments.

    Given a requested downsampling rate, it finds the pair of entries bracketing the desired
    downsampling rate and linearly interpolates to estimate the number of unique fragments
    at that rate.
    """
    if requested_rate >= 1:
        return unique_array[:, -1]

    assert len(sampled_rates) > 1
    assert len(sampled_rates) == unique_array.shape[1]
    assert sampled_rates[-1] == 1

    for i, rate in enumerate(sampled_rates):
        if requested_rate < rate:
            lower_index = i - 1
            upper_index = i
            break

    frac_through_interval = (requested_rate - sampled_rates[lower_index]) / (
        sampled_rates[upper_index] - sampled_rates[lower_index]
    )
    return (
        frac_through_interval * (unique_array[:, upper_index] - unique_array[:, lower_index])
        + unique_array[:, lower_index]
    )


def downsample_counts(dup_counts, sampling_rates=None):
    """
    Takes as input a dictionary of duplicate counts:  for each copy number of duplicates,
    it reports how many unique fragments with that copy number were observed.
    We also take a list of sampling rates to downsample the total and unique reads to.

    Returns numpy arrays of the unique and total fragments at each sampling rate provided,
    with the final entry being additionally at zero downsampling.
    """
    if sampling_rates is None:
        sampling_rates = [1.0]
    sampling_rates = [rate for rate in sampling_rates if rate <= 1.0]

    unique, total = get_unique_and_total_fragments(dup_counts)

    unique_array = np.empty(len(sampling_rates) + 1, dtype=float)
    total_array = np.empty(len(sampling_rates) + 1, dtype=float)
    unique_array[-1] = unique
    total_array[-1] = total

    def estimate_unique_loss(rate, copy_num):
        """Estimates the fraction of unique reads with a given number of copies that will become lost
        at a given downsampling rate
        """
        assert rate <= 1.0
        return 1 - np.power(1 - rate, copy_num)

    max_dup = int(max(dup_counts, key=int))
    duplicate_histogram = np.zeros(max_dup + 1)
    for copy_num, read_count in dup_counts.items():
        duplicate_histogram[int(copy_num)] = read_count

    for i, rate in enumerate(sampling_rates):
        total_array[i] = total * rate
        unique_array[i] = (
            estimate_unique_loss(rate, np.arange(max_dup + 1)) * duplicate_histogram
        ).sum()

    return unique_array, total_array


def mean_unique_from_total_dual(observed, pool1_complexity, pool2_complexity, frac_pool1):
    """
    Estimates the total complexity of the fragment pool when sampling with replacement from a sample
    library with two independent pools of known complexity, where a known fraction of the fragments
    are sampled from each pool.  See reference here, section 1.1:
    http://lh3lh3.users.sourceforge.net/download/samtools.pdf

    We use this dual-pool model for scATAC sampling because we've found experimentally that more
    accessible regions of the genome tend to have a limited library complexity that is very
    effectively sampled while the background genome signal has much higher library complexity
    that is sampled much slower.

    By combining these pools, we effectively estimate the total complexity of all fragments.
    """
    pool1_unique = pool1_complexity * (1 - np.exp(-(observed * frac_pool1) / pool1_complexity))
    pool2_unique = pool2_complexity * (
        1 - np.exp(-(observed * (1 - frac_pool1)) / pool2_complexity)
    )
    return pool1_unique + pool2_unique


def mean_unique_from_total_safe(complexity_data):
    """
    Wrapper for mean_unique_from_total_dual
    """
    if not np.isnan(complexity_data["fraction_pool1"]):
        return mean_unique_from_total_dual(
            np.array(complexity_data["plot_total"]),
            complexity_data["estimated_complexity1"],
            complexity_data["estimated_complexity2"],
            complexity_data["fraction_pool1"],
        )
    else:
        return np.nan


def estimate_complexity_dual(sequenced, unique):
    """Uses least squares fitting to estimate the total number of unique fragments found in a library given
    sequenced versus unique fragment counts for overall and downsampled data.
    (Estimates relative populations of a 2-pool model)
    """

    def error(xdata, pool1_complexity, pool2_complexity, frac_pool1):
        """
        Compute the fitted unique reads given the input parameters to minimize parameters
        to match the observed unique reads.
        """
        return mean_unique_from_total_dual(xdata, pool1_complexity, pool2_complexity, frac_pool1)

    invalid = np.array([np.nan, np.nan, np.nan])

    try:
        popt, cov = optimize.curve_fit(  # pylint: disable=unbalanced-tuple-unpacking
            error, sequenced, unique, p0=[max(unique) / 2, max(unique) / 2, 0.0]
        )
    except RuntimeError:
        return invalid

    # If the parameters are out of range or the CV is too bad, return NaNs instead
    if popt[0] < 0 or popt[1] < 0 or popt[2] < 0 or popt[2] > 1:
        return invalid
    coeff_of_var = np.sqrt(np.diag(cov)) / popt
    if coeff_of_var[0] > 0.1 or coeff_of_var[1] > 0.1:
        return invalid
    return popt


def estimate_library_complexity_safe(dup_data, method=None):
    """Wrapper around the 2-pool model"""
    sampling_rates = np.linspace(1e-5, 0.95, 100)

    if method == "compressed":
        dup_hist = dup_data
    elif method == "expanded":
        dup_hist = {int(k): sum(count for _, count in v) for k, v in dup_data.items()}
    else:
        raise ValueError("Invalid format for duplicate data")

    failed_estimates = {
        "total": None,
        "unique": None,
        "estimated_complexity1": 0,
        "estimated_complexity2": 0,
        "fraction_pool1": np.nan,
        "estimated_complexity": 0,
    }

    if len(dup_hist) == 0:
        return failed_estimates
    unique, total = downsample_counts(dup_hist, sampling_rates)
    estimated_complexity1, estimated_complexity2, fraction_pool1 = estimate_complexity_dual(
        total, unique
    )

    if np.isnan(fraction_pool1):
        return {
            "total": total.tolist(),
            "unique": unique.tolist(),
            "estimated_complexity1": 0,
            "fraction_pool1": np.nan,
            "estimated_complexity2": 0,
            "estimated_complexity": 0,
        }
    else:
        return {
            "total": total.tolist(),
            "unique": unique.tolist(),
            "estimated_complexity1": estimated_complexity1,
            "fraction_pool1": fraction_pool1,
            "estimated_complexity2": estimated_complexity2,
            "estimated_complexity": estimated_complexity1 + estimated_complexity2,
        }


def compute_bulk_complexity_metrics(bulk_histogram, bulk_out):
    """Computes metrics using the full duplicate count data."""
    unique, total = get_unique_and_total_fragments(bulk_histogram)
    bulk_complexity = estimate_library_complexity_safe(bulk_histogram, "compressed")
    metrics = {}

    if (total * 2) >= DOWNSAMPLED_READS:
        ds_rate = DOWNSAMPLED_READS / (total * 2)
        downsampled_unique, _ = downsample_counts(bulk_histogram, [ds_rate])
        ds_fragment_count = downsampled_unique[0]
        metrics["bulk_unique_fragments_at_{}_reads".format(DOWNSAMPLED_READS)] = ds_fragment_count
    else:
        metrics["bulk_unique_fragments_at_{}_reads".format(DOWNSAMPLED_READS)] = None

    # if complexity estimation is successful, compute some metrics and fitted plot
    if not np.isnan(bulk_complexity["fraction_pool1"]):
        bulk_complexity["plot_total"] = list(
            np.linspace(1e5, max(bulk_complexity["total"]) * 1.2, 100)
        )
        bulk_complexity["plot_estimated_unique"] = list(
            mean_unique_from_total_safe(bulk_complexity)
        )
        metrics.update(
            {
                "bulk_total_library_complexity": np.round(
                    bulk_complexity["estimated_complexity"], 1
                ),
                "bulk_estimated_saturation": np.round(
                    tk_stats.robust_divide(unique, bulk_complexity["estimated_complexity"]), 4
                ),
            }
        )
    else:
        metrics["bulk_total_library_complexity"] = None
        metrics["bulk_estimated_saturation"] = None

    with open(bulk_out, "w") as outfile:
        json.dump(bulk_complexity, outfile, indent=4)

    return metrics


def merge_chunk_data(chunk_outs, singlecell_complexity, peaks):
    """Load and merge chunk data:
    - pandas DataFrame: per-cell-barcode complexity data across multiple downsampling rates
    - dict:  per-cell-barcode total (non-unique) fragments
    - dict:  histogram of how many unique fragments were observed with each dup count across all
              barcodes
    """
    if peaks is None:
        ds_unique_targeted_fragments = None
    else:
        combine_csv(
            [
                chunk_out.singlecell_targeted_complexity
                for chunk_out in chunk_outs
                if chunk_out.singlecell_targeted_complexity is not None
            ],
            singlecell_complexity,
            header_lines=1,
        )
        ds_unique_targeted_fragments = pd.read_csv(
            singlecell_complexity, converters={"Barcode": ensure_binary}, index_col="Barcode"
        )

    combine_csv(
        [
            chunk_out.singlecell_complexity
            for chunk_out in chunk_outs
            if chunk_out.singlecell_complexity is not None
        ],
        singlecell_complexity,
        header_lines=1,
    )
    ds_unique_fragments = pd.read_csv(
        singlecell_complexity, converters={"Barcode": ensure_binary}, index_col="Barcode"
    )

    total_fragments_per_cell: dict[bytes, float] = {}
    bulk_histogram = Counter()
    for chunk_out in chunk_outs:
        with open(chunk_out.complexity_summary, "r") as infile:
            for barcode, total_frags in json.load(infile).items():
                total_fragments_per_cell[barcode.encode()] = total_frags
        with open(chunk_out.bulk_complexity, "r") as infile:
            bulk_histogram += Counter(json.load(infile))

    return (
        total_fragments_per_cell,
        bulk_histogram,
        ds_unique_fragments,
        ds_unique_targeted_fragments,
    )


def split(args):
    if args.fragments is None or args.sequencing_summary is None:
        return {"chunks": [], "join": {}}

    if args.frag_bc_counts:
        # It's much faster to load barcodes from the barcode histogram file
        with open(args.frag_bc_counts, "r") as infile:
            barcodes = json.load(infile)
            barcodes = sorted((bc.encode() for bc in barcodes.keys()))
    else:
        # If it's not available, do the much much slower option of getting all the
        # barcodes from the fragments file.
        barcodes = sorted({bc for _, _, _, bc, _ in open_fragment_file(args.fragments)})

    # Chunk by barcode, into 20 unique sets of barcodes to be handled in each split
    barcode_chunks = get_chunks(len(barcodes), 20)
    chunks = []
    for num, bc_chunk in enumerate(barcode_chunks):
        bc_path = martian.make_path("barcode_{}.txt".format(num))
        with open(bc_path, "wb") as outfile:
            for i in range(bc_chunk[0], bc_chunk[1]):
                if i > bc_chunk[0]:
                    outfile.write(b"\n")
                outfile.write(barcodes[i])
        chunks.append({"__mem_gb": 4, "barcodes": bc_path})
    return {"chunks": chunks, "join": {"__mem_gb": 8}}


def main(args, outs):
    args.coerce_strings()
    outs.coerce_strings()

    if args.fragments is None or args.sequencing_summary is None:
        outs.singlecell_complexity = None
        outs.complexity_summary = None
        outs.bulk_complexity = None
        return

    cell_barcodes: Set[bytes]
    cell_barcodes = (
        set()
        if args.cell_barcodes is None
        else bc_loading.load_cell_barcode_set(args.cell_barcodes)
    )

    if args.peaks is None:
        peaks = None
    else:
        with open(args.peaks, "rb") as infile:
            peaks = get_target_regions(infile)

    # For each barcode, keep track of the number of fragments with each duplicate count
    # for downsampling purposes.
    singlecell_histogram: DefaultDict[bytes, Counter[int]] = defaultdict(Counter)
    singlecell_targeted_histogram: DefaultDict[bytes, Counter[int]] = defaultdict(Counter)
    bulk_histogram: Counter[int] = Counter()
    with open(args.barcodes, "rb") as infile:
        chunk_barcodes: Set[bytes] = {bc.strip(b"\n") for bc in infile}
    for contig, start, stop, barcode, dup_count in open_fragment_file(args.fragments):
        if barcode in chunk_barcodes:
            singlecell_histogram[barcode][dup_count] += 1
            bulk_histogram[dup_count] += 1
            if fragment_overlaps_target(contig, start, stop, peaks):
                singlecell_targeted_histogram[barcode][dup_count] += 1

    # Write out the singlecell complexity series of uniques at fixed downsampling rates
    downsample_rates = np.linspace(1e-5, 0.99, 100)
    total_fragments_per_cell = {}
    with open(outs.singlecell_complexity, "wb") as outfile:
        # Header output:  1st column is the barcode sequence, then comma-separated downsampling
        # rates.  We add on as the last column the non-downsampled data.
        outfile.write(b"Barcode,")
        outfile.write(b",".join(["{:.8f}".format(rate).encode() for rate in downsample_rates]))
        outfile.write(b",1.0\n")
        for barcode in singlecell_histogram:
            if barcode in cell_barcodes:
                _, total_fragments_per_cell[barcode] = get_unique_and_total_fragments(
                    singlecell_histogram[barcode]
                )
                unique_at_ds, _ = downsample_counts(singlecell_histogram[barcode], downsample_rates)
                outfile.write(barcode)
                for x in unique_at_ds:
                    outfile.write(b",")
                    outfile.write("{:.8f}".format(x).encode())
                outfile.write(b"\n")

    with open(outs.singlecell_targeted_complexity, "wb") as outfile:
        outfile.write(b"Barcode,")
        outfile.write(b",".join(["{:.8f}".format(rate).encode() for rate in downsample_rates]))
        outfile.write(b",1.0\n")
        for barcode in singlecell_targeted_histogram:
            if barcode in cell_barcodes:
                unique_at_ds, _ = downsample_counts(
                    singlecell_targeted_histogram[barcode], downsample_rates
                )
                outfile.write(barcode)
                for x in unique_at_ds:
                    outfile.write(b",")
                    outfile.write("{:.8f}".format(x).encode())
                outfile.write(b"\n")

    # NOTE: repurpose the summary file and bulk complexity files to avoid creating unneeded new outputs
    with open(outs.complexity_summary, "w") as outfile:
        json.dump(json_sanitize(total_fragments_per_cell), outfile, indent=4)
    with open(outs.bulk_complexity, "w") as outfile:
        json.dump(json_sanitize(bulk_histogram), outfile, indent=4)


def join(args, outs, chunk_defs, chunk_outs):
    # pylint: disable=too-many-locals,too-many-nested-blocks
    if args.fragments is None or args.sequencing_summary is None:
        outs.singlecell_complexity = None
        outs.complexity_summary = None
        outs.bulk_complexity = None
        return

    with open(args.sequencing_summary, "r") as infile:
        # This is the total number of raw reads sequenced, with no filtering at all
        num_reads = json.load(infile)["num_reads"]
    summary_data = {}

    (
        total_fragments_per_cell,
        bulk_histogram,
        downsampled_unique_fragments,
        downsampled_unique_targeted_fragments,
    ) = merge_chunk_data(chunk_outs, outs.singlecell_complexity, args.peaks)

    downsample_rates = downsampled_unique_fragments.columns.values.astype(float)
    num_cells = downsampled_unique_fragments.values.shape[0]
    if num_cells == 0:
        outs.singlecell_complexity = None
    else:
        median_total_array = downsample_rates * np.median(list(total_fragments_per_cell.values()))
        median_unique_array = np.median(downsampled_unique_fragments.values, axis=0)

        # Raw reads per cell at all sampling rates
        total_depth_per_cell_array = downsample_rates * num_reads / num_cells

        with open(outs.singlecell_complexity, "w") as outfile:
            json.dump(
                {
                    "total_depth": total_depth_per_cell_array.tolist(),
                    "total": median_total_array.tolist(),
                    "unique": median_unique_array.tolist(),
                },
                outfile,
                indent=4,
            )

        pool1, pool2, frac_in_pool1 = estimate_complexity_dual(
            median_total_array, median_unique_array
        )
        if np.isnan(frac_in_pool1):
            summary_data["median_per_cell_estimated_saturation"] = None
            summary_data["median_per_cell_total_library_complexity"] = None
        else:
            summary_data["median_per_cell_total_library_complexity"] = np.round(pool1 + pool2, 1)
            summary_data["median_per_cell_estimated_saturation"] = np.round(
                tk_stats.robust_divide(median_unique_array[-1], pool1 + pool2), 4
            )

        # cell_barcodes here is a dictionary of species, with a set of all cell barcodes for that species.
        cell_barcodes: Dict[bytes, Set[bytes]] = {}
        if args.cell_barcodes:
            cell_barcodes = bc_loading.load_cell_barcodes_with_species(args.cell_barcodes)

        for species, bcs in cell_barcodes.items():
            median_total_high_quality = np.median([total_fragments_per_cell[bc] for bc in bcs])
            species_prefix = "_{}".format(species.decode()) if len(cell_barcodes) > 1 else ""

            for dataframe, label in zip(
                [downsampled_unique_fragments, downsampled_unique_targeted_fragments],
                ["unique_fragments", "targeted_unique_fragments"],
            ):
                unique_array = pd.DataFrame(dataframe, index=bcs).to_numpy()
                for reads_per_cell in [RPC_50K, RPC_30K, RPC_10K, None]:
                    if reads_per_cell is not None:
                        # Target downsampling rate for high quality reads
                        ds_rate = reads_per_cell / (median_total_high_quality * 2)
                        if ds_rate < 1 and dataframe is not None:
                            summary_data[
                                "median_per_cell_{}_at_{}_HQ_RPC{}".format(
                                    label, reads_per_cell, species_prefix
                                )
                            ] = np.median(
                                interpolate_downsampled_counts(
                                    downsample_rates, unique_array, ds_rate
                                )
                            )
                        else:
                            summary_data[
                                "median_per_cell_{}_at_{}_HQ_RPC{}".format(
                                    label, reads_per_cell, species_prefix
                                )
                            ] = None

                        # Target downsampling rate for raw reads
                        ds_rate = reads_per_cell / (num_reads / num_cells)
                        if ds_rate < 1 and dataframe is not None:
                            summary_data[
                                "median_per_cell_{}_at_{}_RRPC{}".format(
                                    label, reads_per_cell, species_prefix
                                )
                            ] = np.median(
                                interpolate_downsampled_counts(
                                    downsample_rates, unique_array, ds_rate
                                )
                            )
                        else:
                            summary_data[
                                "median_per_cell_{}_at_{}_RRPC{}".format(
                                    label, reads_per_cell, species_prefix
                                )
                            ] = None
                    else:
                        # Non-downsampled full complexity-metrics
                        summary_data["median_per_cell_{}".format(label)] = np.median(
                            interpolate_downsampled_counts(downsample_rates, unique_array, 1.0)
                        )

    bulk_metrics = compute_bulk_complexity_metrics(bulk_histogram, outs.bulk_complexity)
    summary_data.update(bulk_metrics)

    if summary_data:
        with open(outs.complexity_summary, "w") as outfile:
            json.dump(summary_data, outfile, indent=4)
    else:
        outs.complexity_summary = None
