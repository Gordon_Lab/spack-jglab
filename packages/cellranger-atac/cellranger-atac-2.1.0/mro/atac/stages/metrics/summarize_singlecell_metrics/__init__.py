# Copyright (c) 2021 10x Genomics, Inc. All rights reserved.

"""
Generate summary metrics using the per-barcode metrics in the singlecell.csv file
"""

import pandas as pd
from six import ensure_str
import martian
from tenkit.safe_json import dump_numpy
from atac.tools.ref_manager import ReferenceManager
from atac.metrics.generate_metrics import (
    MetricsError,
    add_bulk_mapping_metrics,
    add_bulk_targeting_metrics,
    add_doublet_rate_metrics,
    add_purity_metrics,
    add_singlecell_sensitivity_metrics,
)

__MRO__ = """
stage SUMMARIZE_SINGLECELL_METRICS(
    in  csv  singlecell,
    in  path reference_path,
    out json summary,
    src py   "stages/metrics/summarize_singlecell_metrics",
) using (
    mem_gb   = 4,
    volatile = strict,
)
"""


def main(args, outs):
    ref = ReferenceManager(args.reference_path)
    species_list = ref.list_species()

    # Note: `barcode` column is kept as `str` here
    scdf = pd.read_csv(ensure_str(args.singlecell))

    summary_info = {}
    try:
        add_bulk_targeting_metrics(summary_info, scdf, species_list)
        add_bulk_mapping_metrics(summary_info, scdf, species_list)
        add_doublet_rate_metrics(summary_info, scdf, species_list)
        add_purity_metrics(summary_info, scdf, species_list)
        add_singlecell_sensitivity_metrics(summary_info, scdf, species_list)
    except MetricsError as err:
        martian.exit(str(err))
    for species in species_list:
        key_suffix = "" if len(species_list) == 1 else "_{}".format(species)
        summary_info["annotated_cells{}".format(key_suffix)] = scdf[
            "is_{}_cell_barcode".format(species)
        ].sum()

    with open(outs.summary, "w") as summary_file:
        dump_numpy(summary_info, summary_file, pretty=True)
