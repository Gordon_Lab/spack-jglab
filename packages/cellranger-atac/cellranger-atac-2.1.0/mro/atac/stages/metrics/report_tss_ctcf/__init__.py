# Copyright (c) 2019 10x Genomics, Inc. All rights reserved.
"""
Compute TSS and CTCF scores of an ATAC-seq library.
"""

from __future__ import absolute_import, division

import json
import numpy as np
from atac.metrics import (
    calculate_ctcf_score_and_profile,
    calculate_tss_score_and_profile,
)

__MRO__ = """
stage REPORT_TSS_CTCF(
    in  csv  tss_relpos,
    in  csv  ctcf_relpos,
    out json summary_metrics,
    src py   "stages/metrics/report_tss_ctcf",
)
"""


def main(args, outs):
    if args.tss_relpos is None and args.ctcf_relpos is None:
        return

    summary_metrics = {}

    if args.tss_relpos:
        tss, _, _ = calculate_tss_score_and_profile(relative_positions=args.tss_relpos)
        summary_metrics["tss_enrichment_score"] = np.round(tss, 4)

    if args.ctcf_relpos:
        ctcf, _, _ = calculate_ctcf_score_and_profile(relative_positions=args.ctcf_relpos)
        summary_metrics["ctcf_enrichment_score"] = np.round(ctcf, 4)

    with open(outs.summary_metrics, "w") as outfile:
        json.dump(summary_metrics, outfile)
