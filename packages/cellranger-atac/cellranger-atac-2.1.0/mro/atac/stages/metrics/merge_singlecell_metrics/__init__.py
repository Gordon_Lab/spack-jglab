# Copyright (c) 2019 10x Genomics, Inc. All rights reserved.
"""
Pools single-cell analyses from other stages and calculates bulk metrics from the pooled dataset.
"""
from __future__ import absolute_import, division


import pandas as pd
from six import ensure_str

from atac.tools.ref_manager import ReferenceManager
import atac.cell_calling_helpers.exclusions as exclusions


__MRO__ = """
stage MERGE_SINGLECELL_METRICS(
    in  string reference_path,
    in  csv    singlecell_mapping,
    in  csv    singlecell_targets,
    in  csv    singlecell_cells,
    out csv    singlecell,
    src py     "stages/metrics/merge_singlecell_metrics",
) using (
    mem_gb = 8,
)
"""


def main(args, outs):
    if (
        args.singlecell_mapping is None
        or args.singlecell_targets is None
        or args.singlecell_cells is None
    ):
        outs.singlecell = None
        outs.summary = None
        return

    ref = ReferenceManager(args.reference_path)
    species_list = ref.list_species()

    # Merge the input singlecell data into a single dataframe and write it out
    mapping = pd.read_csv(ensure_str(args.singlecell_mapping), float_precision="high")
    cells = pd.read_csv(ensure_str(args.singlecell_cells), float_precision="high")
    targeting = pd.read_csv(ensure_str(args.singlecell_targets), float_precision="high")

    merged = mapping.merge(cells, how="left", on="barcode", sort=False, validate="one_to_one")
    for column in merged.columns:
        if (
            column.endswith("_cell_barcode")
            or column.startswith("passed_filters_")
            or column.startswith("peak_region_fragments_")
        ):
            merged[column] = merged[column].fillna(0).astype(int)

    merged = merged.merge(targeting, how="left", on="barcode", sort=False, validate="one_to_one")
    keys = (
        [
            "{}_fragments".format(region)
            for region in [
                "TSS",
                "DNase_sensitive_region",
                "enhancer_region",
                "promoter_region",
                "on_target",
                "blacklist_region",
                "peak_region",
            ]
        ]
        + ["peak_region_cutsites"]
        + [ensure_str(col) for col in exclusions.get_columns(species_list)]
    )
    for column in keys:
        merged[column] = merged[column].fillna(0).astype(int)
    merged.to_csv(outs.singlecell, index=None)
