# Copyright (c) 2019 10x Genomics, Inc. All rights reserved.
"""
Compute insert size summary statistics
"""

from __future__ import absolute_import, division

import json
import numpy as np
from scipy import signal, stats

__MRO__ = """
stage REPORT_INSERT_SIZES(
    in  csv    insert_sizes,
    out json   insert_summary,
    src py     "stages/metrics/report_insert_sizes",
) using (
    volatile = strict,
)
"""

MAX_INSERT_SIZE = 1000
NFR_THRESHOLD = 124
NUC1_THRESHOLD = 296


def gen_yvals(xvals, counts, norm_linear=False):
    yvals = np.array([counts[x - 1] for x in xvals])
    if not norm_linear:
        return yvals
    slope, intercept, _, _, _ = stats.linregress(xvals, yvals)
    return yvals - (slope * xvals + intercept)


def find_peak(xvals, counts, target, maxdist):
    """Look for a frequency peak around a given target"""
    try:
        freqs = np.fft.rfftfreq(len(xvals))
        amps = abs(np.fft.rfft(gen_yvals(xvals, counts, True)))
        peak_freqs = freqs[signal.find_peaks_cwt(amps, np.arange(1, 25))]
        diffs = abs((1 / peak_freqs) - target)
        idx = np.argmin(diffs)
        return 1 / peak_freqs[idx] if diffs[idx] <= maxdist else None
    except IndexError:
        # In the case that counts are zero for given xvals
        return None


def main(args, outs):
    if args.insert_sizes is None:
        outs.insert_summary = None
        return

    # compute total
    total = np.zeros(MAX_INSERT_SIZE)
    for i, line in enumerate(open(args.insert_sizes, "rb")):
        if i == 0:
            continue
        fields = np.array([float(x) for x in line.strip().split(b",")[1:-1]])
        total += fields

    # NOTE: this assumes MAX_INSERT_SIZE > 701
    assert MAX_INSERT_SIZE > 701
    xvals = {
        "full": np.arange(1, MAX_INSERT_SIZE + 1),
        "twist": np.arange(50, 401),
        "nucleosome": np.arange(150, 701),
    }

    # Calculate metrics
    twist_period = find_peak(xvals["twist"], total, 12, 5)
    nucleosome_period = find_peak(xvals["nucleosome"], total, 180, 30)

    yvals = gen_yvals(xvals["full"], total)
    non_nucleosomal = yvals[xvals["full"] < NFR_THRESHOLD].sum()
    nuc1 = yvals[(xvals["full"] >= NFR_THRESHOLD) & (xvals["full"] <= NUC1_THRESHOLD)].sum()

    summary_data = {
        "insert_twist_period": twist_period,
        "insert_nucleosome_period": nucleosome_period,
        "frac_fragments_nfr": non_nucleosomal / yvals.sum(),
        "frac_fragments_nuc": nuc1 / yvals.sum(),
        "frac_fragments_nfr_or_nuc": (non_nucleosomal + nuc1) / yvals.sum(),
    }
    with open(outs.insert_summary, "w") as outfile:
        json.dump(summary_data, outfile, indent=4)
