# Copyright (c) 2020 10x Genomics, Inc. All rights reserved.

from __future__ import absolute_import
import json
from six import ensure_binary
from atac_rna.preflight import (
    ArcReferenceError,
    check_valid_atac_reference_basic,
    check_valid_atac_reference_heavy,
)

import martian
from atac.constants import FRAGMENTS_SCAN_SIZE
from atac.preflights import (
    PreflightException,
    check_vmem_for_reference,
    check_filehandle_limit,
    check_sample_id,
    check_aggr_csv,
    check_force_cells,
    exists_and_readable,
    check_input_peaks_bed,
)
from atac.barcodes import load_barcode_whitelist
from atac.tools.fragments import open_fragment_file, parsed_fragments_from_region
from atac.tools.ref_manager import ReferenceManager
from atac_rna.reanalyze import InputBarcodes, ReanalyzeError

__MRO__ = """
stage ATAC_REANALYZER_PREFLIGHT(
    in  string     sample_id,
    in  string     reference_path,
    in  string     barcode_whitelist,
    in  bed        peaks,
    in  csv        parameters,
    in  map        force_cells,
    in  csv        cell_barcodes,
    in  tsv.gz     fragments,
    in  tsv.gz.tbi fragments_index,
    in  csv        aggregation_csv,
    in  bool       check_executables,
    src py         "stages/preflight/atac_reanalyzer",
) split (
)
"""


def split(args):
    if args.check_executables:
        vmem_gb = check_vmem_for_reference(args.reference_path)
        return {"chunks": [], "join": {"__vmem_gb": vmem_gb}}
    return {"chunks": []}


def run_light_checks(args):
    # Sample ID / pipestance name
    check_sample_id(args.sample_id)

    # force_cells
    check_force_cells(
        args.force_cells, ulimit=10000000
    )  # allow arbitrarily large limit for reanalyzer

    # # Reference
    # ref directory structure and timestamps
    info = check_valid_atac_reference_basic(args.reference_path)
    martian.log_info(json.dumps(info, indent=4))

    # check parameters files
    if args.parameters is not None:
        exists_and_readable(args.parameters, "parameters")

    if args.fragments is None:
        martian.exit("fragments file not provided")
    exists_and_readable(args.fragments, "fragments")

    # fragments index is synced with fragments
    if args.fragments_index is None:
        martian.exit("fragments index file not provided")
    exists_and_readable(args.fragments_index, "fragments index")

    # aggr csv checks
    if args.aggregation_csv is not None:
        exists_and_readable(args.aggregation_csv, "aggregation_csv")
        msg = check_aggr_csv(args.aggregation_csv, args.reference_path, cursory=True)
        if msg:
            martian.log_info(msg)


def run_heavy_checks(args):
    check_valid_atac_reference_heavy(args.reference_path)

    contig_manager = ReferenceManager(args.reference_path)
    # peaks format check and nonoverlapping
    # pylint: disable=broad-except
    try:
        check_input_peaks_bed(args.peaks, contig_manager)
    except Exception as e:
        martian.exit(str(e))

    # fragments checks
    whitelist_barcodes = load_barcode_whitelist(args.barcode_whitelist)
    species_list = contig_manager.list_species()
    observed_gem_groups = set()
    observed_species = set()
    contig_lens = contig_manager.get_contig_lengths()
    # check bounds and matching contigs in reference and species
    for chrom, start, stop, bc, _ in open_fragment_file(args.fragments):
        chrom = chrom.decode()
        if len(species_list) > 1:
            observed_species.add(chrom.split("_")[0])
        barcode, gem_group = bc.split(b"-")
        observed_gem_groups.add(gem_group)
        if barcode not in whitelist_barcodes:
            martian.exit("{} is not a valid whitelist barcode".format(barcode))
        if chrom not in contig_lens:
            martian.exit("contig {} not present in reference".format(chrom))
        if stop > contig_lens[chrom]:
            martian.exit(
                "fragment {}:{}-{} boundaries exceed contig size ({} bp)".format(
                    chrom, start, stop, contig_lens[chrom]
                )
            )

    # ensure fragments are on the correct reference
    for species in observed_species:
        if species not in species_list:
            martian.exit(
                "{} contains fragments mapped to species not recognized in the reference".format(
                    args.fragments
                )
            )
    if len(observed_gem_groups) > 1:
        martian.log_info(
            "multiple gem groups present in {}, likely generated in a previous aggregation run".format(
                args.fragments
            )
        )

    try:
        all_contigs = contig_manager.primary_contigs()
        for contig in all_contigs:
            en = 0
            for _ in parsed_fragments_from_region(
                args.fragments, contig, None, None, index=args.fragments_index
            ):
                if en >= FRAGMENTS_SCAN_SIZE:
                    break
                en += 1
    except:
        martian.exit("fragments index is not in sync with the fragments file")

    # cell barcode checks
    if args.cell_barcodes:
        exists_and_readable(args.cell_barcodes, "singlecell.csv")
        bcs = InputBarcodes(ensure_binary(args.cell_barcodes))
        bcs.check_min_bcs()
        bcs.check_in_whitelist(whitelist_barcodes)


def join(args, outs, chunk_defs, chunk_outs):
    try:
        run_light_checks(args)
    except (PreflightException, SystemExit, ArcReferenceError) as err:
        martian.exit(str(err))

    if args.check_executables:
        # Open file handles limit
        check_filehandle_limit()

        try:
            run_heavy_checks(args)
        except (PreflightException, SystemExit, ArcReferenceError, ReanalyzeError) as err:
            martian.exit(str(err))
