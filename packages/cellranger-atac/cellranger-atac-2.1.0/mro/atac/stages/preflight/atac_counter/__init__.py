# Copyright (c) 2020 10x Genomics, Inc. All rights reserved.
"""
Methods for doing preflight checks for cellranger atac.
"""

from __future__ import absolute_import
import json

import martian
import atac.preflights as atac_prefl
from atac.tools.ref_manager import ReferenceManager
from atac_rna.preflight import (
    ArcReferenceError,
    check_valid_atac_reference_basic,
    check_valid_atac_reference_heavy,
)

__MRO__ = """
stage ATAC_COUNTER_PREFLIGHT(
    in  string   sample_id,
    in  map[]    sample_def,
    in  path     reference_path,
    in  map      force_cells,
    in  float    peak_qval,
    in  string[] factorization,
    in  float    subsample_rate,
    in  bed      custom_peaks,
    in  bool     check_executables,
    src py       "stages/preflight/atac_counter",
) split (
) using (
    volatile = strict,
)
"""


def run_preflight_checks(args):
    """package all preflight checks for unified error handlling"""

    # Sample ID / pipestance name
    atac_prefl.check_sample_id(args.sample_id)

    # force_cells
    atac_prefl.check_force_cells(args.force_cells)

    # downsample
    atac_prefl.check_subsample_rate(args.subsample_rate)

    # Peak qvalue check
    atac_prefl.check_peak_qval(args.peak_qval, "--peak-qval")

    # FASTQ input (sample_def)
    atac_prefl.check_fastq_input(args.sample_def)

    # factorization.
    atac_prefl.check_factorization(args.factorization)

    # # Reference
    # ref directory structure and timestamps
    info = check_valid_atac_reference_basic(args.reference_path)
    martian.log_info(json.dumps(info, indent=4))

    # these checks run on the cluster
    if args.check_executables:
        # Open file handles limit
        atac_prefl.check_filehandle_limit()

        # reference check
        check_valid_atac_reference_heavy(args.reference_path)

        # validate custom peaks if specified (not locally)
        ref_manager = ReferenceManager(args.reference_path)
        if args.custom_peaks:
            try:
                atac_prefl.check_input_peaks_bed(args.custom_peaks, ref_manager)
            except atac_prefl.PreflightException as err:
                martian.exit("Error parsing supplied `--peaks`: {}".format(err))


def split(args):
    if args.check_executables:
        vmem_gb = atac_prefl.check_vmem_for_reference(args.reference_path)
        return {"chunks": [], "join": {"__vmem_gb": vmem_gb}}
    return {"chunks": []}


def join(args, outs, chunk_defs, chunk_outs):
    try:
        run_preflight_checks(args)
    except (atac_prefl.PreflightException, ArcReferenceError, SystemExit) as err:
        martian.exit(err)
