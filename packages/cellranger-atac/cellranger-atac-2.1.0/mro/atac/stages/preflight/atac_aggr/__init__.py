# Copyright (c) 2022 10x Genomics, Inc. All rights reserved.

from __future__ import absolute_import
import json
from atac_rna.preflight import (
    ArcReferenceError,
    check_valid_atac_reference_basic,
    check_valid_atac_reference_heavy,
)
import martian
from atac.constants import ALLOWED_NORMALIZATIONS
from atac.tools.ref_manager import ReferenceManager
from atac.preflights import (
    check_vmem_for_reference,
    check_factorization,
    check_filehandle_limit,
    check_aggr_csv,
    check_sample_id,
    PreflightException,
    check_input_peaks_bed,
)
from atac_rna.aggr import AggrDefs, AggrDefError

__MRO__ = """
stage ATAC_AGGR_PREFLIGHT(
    in  string   sample_id,
    in  string   reference_path,
    in  csv      aggr_csv,
    in  string   normalization,
    in  string[] factorization,
    in  bed      custom_peaks,
    in  bool     check_executables,
    src py       "stages/preflight/atac_aggr",
) split (
)
"""


def split(args):
    if args.check_executables:
        mem_gb = 6  # HACK: reserve a basic chunk when aggr list is long
        vmem_gb = mem_gb + check_vmem_for_reference(args.reference_path)
        return {"chunks": [], "join": {"__mem_gb": mem_gb, "__vmem_gb": vmem_gb}}
    else:
        return {"chunks": []}


def run_light_checks(args):
    # Sample ID / pipestance name
    check_sample_id(args.sample_id)

    # factorization.
    check_factorization(args.factorization)

    # normalization checks
    if args.normalization not in ALLOWED_NORMALIZATIONS:
        martian.exit('Unsupported normalization method provided. Options are `null` or `"unique"`.')

    # Reference
    # ref directory structure and timestamps
    info = check_valid_atac_reference_basic(args.reference_path)
    martian.log_info(json.dumps(info, indent=4))

    # aggr csv checks
    if not args.aggr_csv:
        martian.exit("aggregation csv must be provided")

    msg = check_aggr_csv(args.aggr_csv, args.reference_path, cursory=True)
    if msg:
        martian.log_info(msg)


def run_heavy_checks(args):
    check_valid_atac_reference_heavy(args.reference_path)
    ref_manager = ReferenceManager(args.reference_path)

    try:
        aggr_defs = AggrDefs.from_atac_aggr_csv(args.aggr_csv, ref_manager)
        aggr_defs.validate(ref_manager)
    except AggrDefError as err:
        martian.exit("Error parsing aggr library definitions: {}".format(err))

    msg, has_batch_column = check_aggr_csv(args.aggr_csv, args.reference_path, cursory=False)
    if msg:
        martian.log_info(msg)

    # factorization.
    check_factorization(args.factorization, has_batch_column)

    if args.custom_peaks:
        check_input_peaks_bed(args.custom_peaks, ref_manager)


def join(args, outs, chunk_defs, chunk_outs):

    try:
        run_light_checks(args)

        # these checks run on the cluster
        if args.check_executables:
            check_filehandle_limit()
            run_heavy_checks(args)

    except (PreflightException, ArcReferenceError) as err:
        martian.exit(str(err))
