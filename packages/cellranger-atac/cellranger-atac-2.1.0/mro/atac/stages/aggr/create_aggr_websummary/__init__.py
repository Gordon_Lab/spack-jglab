# Copyright (c) 2021 10x Genomics, Inc. All rights reserved.
"""
Produces the final customer-facing web_summary.
"""

from __future__ import absolute_import, division

import json
import os

import h5py
import martian
import numpy as np
import pandas as pd
from six import ensure_str, iteritems
from typing import Dict, Any

import cellranger.analysis.constants as cr_analysis_constants
import cellranger.analysis.graphclust as cr_graphclust
import cellranger.matrix as cr_matrix
from atac.plots import generate_clustering_plot
from atac.tools.ref_manager import ReferenceManager
from common.metrics import MetricAnnotations
from common.plots.plotly_tools import PLOT_CONFIG_KWARGS
from websummary import summarize

__MRO__ = """
stage CREATE_AGGR_WEBSUMMARY(
    in  path       reference_path,
    in  json       summary_results,
    in  json       library_info,
    in  string     sample_id,
    in  string     sample_desc,
    in  csv        singlecell,
    in  bool       debug,
    in  h5         filtered_peak_bc_matrix,
    in  h5         analysis,
    in  map<float> batch_scores,
    out html       web_summary,
    out json       data,
    src py         "atac/stages/aggr/create_aggr_websummary",
) using (
    mem_gb   = 16,
    volatile = strict,
)
"""


def get_hero_metric_data(metadata, summary_data, species_list, debug):
    """Adds a few metrics of primary importance into the header of the web summary:
    - Annotated cell counts (split by species if barnyard)
    - Median fragments per cell (split)
    - Non-dup wasted data
    Alerts raised depend on debug status.
    """
    data = {}
    if summary_data is None:
        return data

    for key in [
        "annotated_cells",
        "median_fragments_per_cell",
        "frac_cut_fragments_in_peaks",
    ]:
        metrics = metadata.gen_metric_list(
            summary_data, [key], species_list=species_list, debug=debug
        )
        for metric in metrics:
            key = metric.key
            if len(species_list) > 1:
                for i, species in enumerate(species_list):
                    key = key.replace(species, "spec{}".format(i + 1))
            data[key] = metric.gen_metric_dict(default_threshold="pass")

    return data


def get_pipeline_info(args, reference, debug):
    """Generates a table of general pipeline information."""
    data = {}
    metadata = reference.metadata

    rows = [
        ["Sample ID", args.sample_id],
        ["Sample description", args.sample_desc],
        ["Pipeline version", martian.get_pipelines_version()],
        ["Reference path", args.reference_path],
    ]

    if metadata:
        rows.extend(
            [
                ["Organism", metadata.get("organism", "")],
            ]
        )

    data = {"pipeline_info_table": {"rows": rows}}
    return data


def get_aggr_metrics_data(metadata, summary_data, library_list):
    """Get aggr sections"""
    data = {}
    if summary_data is None:
        return data

    # aggr
    metric_keys = [
        "post_norm_median_frags_per_cell_Library",
        "pre_norm_median_frags_per_cell_Library",
        "total_post_normalization_Library",
        "total_pre_normalization_Library",
        "unique_post_normalization_Library",
        "unique_pre_normalization_Library",
    ]
    metrics = metadata.gen_metric_list(
        summary_data, metric_keys, species_list=library_list, species_is_library=True
    )
    data["aggr_table"] = {"rows": [[metric.name, metric.value_string] for metric in metrics]}
    helptext = metadata.gen_metric_helptext(metric_keys)
    data["aggr_metrics_helptext"] = {"title": "Aggregation", "data": helptext}

    return data


def get_batch_scores(metadata: MetricAnnotations, batch_scores: Dict[str, float]) -> Dict[str, Any]:
    """Get aggr sections"""
    data = {}

    metric_keys = ["batch_effect_score_before_correction", "batch_effect_score_after_correction"]
    metrics = metadata.gen_metric_list(batch_scores, metric_keys)
    data["batch_score_table"] = {"rows": [[metric.name, metric.value_string] for metric in metrics]}
    helptext = metadata.gen_metric_helptext(metric_keys)
    data["batch_score_helptext"] = {"title": "Chemistry Batch Correction", "data": helptext}

    return data


def get_sequencing_info(metadata, summary_data, species_list, debug):
    """Alerts raised depend on debug status."""
    data = {}
    if summary_data is None:
        return data

    metric_keys = [
        "total_post_normalization",
        "total_pre_normalization",
    ]
    metrics = metadata.gen_metric_list(summary_data, metric_keys, species_list, debug=debug)
    data["sequencing_info_table"] = {
        "rows": [[metric.name, metric.value_string] for metric in metrics]
    }

    helptext = metadata.gen_metric_helptext(metric_keys)
    data["sequencing_helptext"] = {"title": "Sequencing", "data": helptext}

    return data


def get_clustering_plots(
    analysis, filtered_peak_bc_matrix, species_list, library_list, singlecell_df, is_barnyard
):
    data = {}
    if filtered_peak_bc_matrix is None or analysis is None:
        return data
    tsne_results = np.array(
        h5py.File(analysis, "r")[cr_analysis_constants.ANALYSIS_H5_TSNE_GROUP]["_2"][
            "transformed_tsne_matrix"
        ]
    )
    # N.B.: these cluster labels are 1-indexed
    cluster_labels = cr_graphclust.load_graphclust_from_h5(analysis).clusters
    barcodes = [
        bc.decode() for bc in cr_matrix.CountMatrix.load_bcs_from_h5(filtered_peak_bc_matrix)
    ]
    data["graph_cluster_plot"] = generate_clustering_plot(
        singlecell_df, tsne_results, cluster_labels, barcodes, species_list, method="cluster"
    )

    data["depth_cluster_plot"] = generate_clustering_plot(
        singlecell_df, tsne_results, cluster_labels, barcodes, species_list, method="depth"
    )

    data["aggr_cluster_plot"] = generate_clustering_plot(
        singlecell_df,
        tsne_results,
        cluster_labels,
        barcodes,
        species_list,
        library_list=library_list,
        method="library",
    )

    if len(species_list) == 2:
        # Add an additional plot with cells colored by species for barnyard samples
        data["barnyard_cluster_plot"] = generate_clustering_plot(
            singlecell_df, tsne_results, cluster_labels, barcodes, species_list, method="species"
        )

    helptext = [
        [
            "Plots",
            [
                "(top left) Scatter plot of barcodes annotated as cells, colored by automatically computed clusters via graph clustering.",
                "(top right) Scatter plot of barcodes annotated as cells, colored by number of fragments in the barcode.",
                "(bottom left) Scatter plot of barcodes annotated as cells, colored by library id.",
            ],
        ]
    ]
    if is_barnyard:
        helptext[0][1] += [
            "(bottom right) Scatter plot of barcodes annotated as cells, colored by species."
        ]

    data["clustering_helptext"] = {"title": "Cell Clustering", "data": helptext}
    return data


def add_data(websummary_data, input_data):
    """Adds data to global dictionary, returns content"""
    if "alarms" in input_data:
        websummary_data["alarms"]["alarms"].extend(input_data["alarms"])
        del input_data["alarms"]
    websummary_data.update(input_data)


def main(args, outs):
    reference = ReferenceManager(args.reference_path)
    species_list = reference.list_species()
    is_barnyard = len(species_list) > 1 and args.singlecell is not None

    summary_data = None
    if args.summary_results:
        with open(args.summary_results, "r") as infile:
            summary_data = json.load(infile)

    with open(args.library_info, "r") as f:
        library_info = json.load(f)
    # sort by numeric gem group
    library_list = [library_info[n]["library_id"] for n in sorted(library_info.keys(), key=int)]

    # Pull up the correct template information
    template_path = os.path.dirname(os.path.abspath(__file__))
    template_file = os.path.join(
        template_path,
        "{}{}.html".format("barnyard" if is_barnyard else "single", "_debug" if args.debug else ""),
    )
    with open(template_file, "r") as infile:
        template = infile.read()

    metadata = MetricAnnotations(species_list=species_list)
    metadata_per_lib = MetricAnnotations(species_list=library_list, species_is_library=True)

    websummary_data = {
        "alarms": {"alarms": []},
        "sample": {
            "id": args.sample_id,
            "description": args.sample_desc,
            "pipeline": "Cell Ranger ATAC Aggregator",
        },
    }

    singlecell_df = (
        pd.read_csv(ensure_str(args.singlecell)) if args.singlecell is not None else None
    )
    singlecell_df.set_index("barcode", inplace=True)

    add_data(
        websummary_data, get_hero_metric_data(metadata, summary_data, species_list, args.debug)
    )

    add_data(websummary_data, get_pipeline_info(args, reference, args.debug))

    add_data(websummary_data, get_sequencing_info(metadata, summary_data, species_list, args.debug))

    add_data(websummary_data, get_aggr_metrics_data(metadata_per_lib, summary_data, library_list))

    if args.batch_scores is not None:
        add_data(websummary_data, get_batch_scores(metadata, args.batch_scores))

    add_data(
        websummary_data,
        get_clustering_plots(
            args.analysis,
            args.filtered_peak_bc_matrix,
            species_list,
            library_list,
            singlecell_df,
            is_barnyard,
        ),
    )

    # Modify the titles of plots to add consistent plot styling sample ID/descriptions
    for _, subdata in iteritems(websummary_data):
        if "layout" in subdata:
            subdata["layout"]["title"] += "<br><sup>Sample {} - {}</sup>".format(
                args.sample_id, args.sample_desc
            )
            subdata["layout"]["hovermode"] = "closest"
            subdata["config"] = PLOT_CONFIG_KWARGS

    with open(outs.web_summary, "w") as outfile:
        summarize.generate_html_summary(websummary_data, template, template_path, outfile)

    with open(outs.data, "w") as out:
        json.dump(websummary_data, out, sort_keys=True, indent=4)
