# Copyright (c) 2019 10x Genomics, Inc. All rights reserved.
"""
Perform normalization of libraries
"""

from __future__ import absolute_import

# Imports

import json
import os
from collections import defaultdict
import pysam
from six import ensure_str
import martian
from atac.tools.fragments import subsample_fragments, FragmentException
from atac.tools.peaks import merge_keyed_bed
from atac.tools.ref_manager import ReferenceManager
from atac.tools.singlecell import SingleCellCsv


# MRO docstring
__MRO__ = """
stage NORMALIZE_LIBRARIES(
    in  json      library_info,
    in  path      reference_path,
    in  string    assay,
    out Fragments fragments,
    out csv       cell_barcodes,
    out bool      skip_peakcalling,
    out json      normalization_metrics,
    src py        "stages/aggr/normalize_libraries",
) split (
    in  string    n,
    in  map       library_chunk,
    out tsv       fragments_chunk,
) using (
    volatile = strict,
)
"""


def split(args):
    """Each chunk gets a library to downsample, along with normalization parameters"""
    with open(args.library_info, "r") as f:
        library_info = json.load(f)
    return {
        "chunks": [
            {"n": group, "library_chunk": chunk, "__mem_gb": 12}
            for group, chunk in library_info.items()
        ],
        # sort & cut in merge_keyed_bed run on independent threads
        "join": {"__mem_gb": 12, "__threads": 2},
    }


def main(args, outs):
    """Downsample each fragments file to produce a sorted file, while computing the pre and post complexity metrics"""

    # read cells
    singlecell = SingleCellCsv.from_aggr_def(args.assay, args.library_chunk)
    cell_barcodes = singlecell.get_all_cell_barcodes()

    # get chrom key from fasta index
    chrom_order = {}
    ctg_mgr = ReferenceManager(args.reference_path)
    with open(ctg_mgr.fasta_index, "rb") as f:
        for en, line in enumerate(f):
            chrom = line.split(b"\t")[0]
            chrom_order[chrom] = en

    if args.assay == "atac":
        infile = args.library_chunk["fragments"]
    elif args.assay == "arc":
        infile = args.library_chunk["atac_fragments"]
    else:
        raise ValueError("Invalid assay type {}".format(args.assay))

    try:
        downsampling_metrics = subsample_fragments(
            infile=infile,
            rate=args.library_chunk["rate"],
            outfile=outs.fragments_chunk,
            group=int(args.n),
            cells=cell_barcodes,
            kind=args.library_chunk["kind"],
            key=chrom_order,
        )
    except FragmentException as err:
        martian.exit(
            "{} There is a mismatch between {} and other inputs to this row of the "
            "aggr CSV. Please check that the input files originate from the same pipestance.".format(
                err, infile
            )
        )

    with open(outs.normalization_metrics, "w") as f:
        json.dump(downsampling_metrics, f, indent=4)


def join(args, outs, chunk_defs, chunk_outs):
    """Merge sorted, downsampled fragments from each chunk.

    Emit pre and post normalization sensitivity metrics per library.
    """

    with open(args.library_info, "r") as f:
        library_info = json.load(f)

    if args.assay == "atac":
        key = "cells"
    elif args.assay == "arc":
        key = "per_barcode_metrics"
    else:
        raise ValueError("Invalid assay type {}".format(args.assay))

    # Merge cell_barcodes
    cell_barcodes = defaultdict(set)
    for group in library_info:
        singlecell = SingleCellCsv(library_info[group][key])
        cell_barcodes_group = singlecell.get_cell_barcodes_dict()

        for species, barcodes in cell_barcodes_group.items():
            cell_barcodes[species].update(
                bc.split(b"-")[0] + b"-" + group.encode() for bc in barcodes
            )
    with open(outs.cell_barcodes, "wb") as f:
        for species in cell_barcodes:
            f.write(species.encode())
            for bc in sorted(cell_barcodes[species]):
                f.write(b",")
                f.write(bc)
            f.write(b"\n")

    # override library name when aggring 1 library:
    if len(library_info) == 1:
        library_info["1"]["library_info"] = ""

    # merge the metrics
    normalization_metrics = {}
    for cdef, cout in zip(chunk_defs, chunk_outs):
        with open(cout.normalization_metrics, "r") as f:
            chunk_metrics = json.load(f)
            for key in chunk_metrics:
                normalization_metrics[
                    "{}_Library_{}".format(key, library_info[cdef.n]["library_id"])
                ] = chunk_metrics[key]
                # aggregate some metrics across all libraries
                if key in ["total_pre_normalization", "total_post_normalization"]:
                    if key not in normalization_metrics:
                        normalization_metrics[key] = 0
                    normalization_metrics[key] += chunk_metrics[key]
    with open(outs.normalization_metrics, "w") as f:
        json.dump(normalization_metrics, f, indent=4)

    outs.fragments = {
        "fragments": martian.make_path("fragments.tsv.gz"),
        "index": martian.make_path("fragments.tsv.gz.tbi"),
    }
    # merge the fragments
    base_file, extension = os.path.splitext(outs.fragments["fragments"])
    if extension != b".gz":
        raise ValueError("Expecting compressed file output")
    input_tsvs = [str(chunk.fragments_chunk) for chunk in chunk_outs]
    merge_keyed_bed(input_tsvs, base_file)

    # index the fragments
    if os.path.getsize(base_file) == 0:
        outs.fragments = None
        outs.fragments_index = None
    else:
        # N.B. tabix_index will automatically compress the input file, adding the .gz suffix
        pysam.tabix_index(
            ensure_str(base_file), preset="bed", index=ensure_str(outs.fragments["index"])
        )
