# Copyright (c) 2019 10x Genomics, Inc. All rights reserved.
"""
Generate a matrix of barcodes by peaks, with the counts of the # of transposition sites in each
"""

from __future__ import absolute_import, division

import os
import json
from collections import OrderedDict, Counter
from typing import AnyStr
import numpy as np
from scipy.sparse import csc_matrix, coo_matrix, hstack

import atac.utils as utils
import tenkit.bio_io as tk_bio
import cellranger.matrix as cr_matrix
import cellranger.atac.feature_ref as atac_feature_ref
import cellranger.atac.matrix as atac_matrix
import cellranger.library_constants as cr_lib_constants

from atac.tools.fragments import open_fragment_file
from atac.tools.regions import count_header_lines_bed
import martian

__MRO__ = """
stage GENERATE_PEAK_MATRIX(
    in  path   reference_path,
    in  tsv.gz fragments,
    in  bed    peaks,
    # optional: fragment barcode histogram. If provided we avoid a full pass of the fragments file
    # in the split,
    in  json   frag_bc_counts,
    out h5     raw_matrix,
    out path   raw_matrix_mex,
    src py     "stages/processing/generate_peak_matrix",
) split (
    in  file   barcodes,
) using (
    mem_gb   = 4,
    # N.B. we don't explicitly need the fragment index
    volatile = strict,
)
"""


def est_mem_gb(fragments_path: AnyStr):
    """Estimate chunk & join mem_gb based on size of fragments file

    Assume linear scaling and fit fragments size vs mem gb for a large number of pipestances. Add
    a padding of 1.5x for the slope and 1 for the intercept.

    Args:
        fragments_path (AnyStr): path to fragments
    """
    frags_gb = os.path.getsize(fragments_path) / 1e9
    # See PR-6563
    chunk_gb = 1.5 * 0.1797 * frags_gb + 0.2236 + 1.0
    join_gb = 1.75 * 0.8795 * frags_gb + 0.3327 + 2.0
    return chunk_gb, join_gb


def split(args):
    if args.fragments is None:
        return {"chunks": [], "join": {}}

    chunk_gb, join_gb = est_mem_gb(args.fragments)

    if args.frag_bc_counts:
        # if barcode histogram is available load barcodes
        with open(args.frag_bc_counts, "r") as fin:
            barcodes = json.load(fin)
            barcodes = sorted((bc.encode() for bc in barcodes.keys()))
    else:
        # This is EXPENSIVE so always pass in the barcode counts
        # as the fragments file is not sorted by barcodes, we iterate through the files to get a list of
        # ordered bcs
        barcodes = list({bc for _, _, _, bc, _ in open_fragment_file(args.fragments)})
        barcodes.sort()

    # chunk on barcodes
    barcode_chunks = utils.get_chunks(len(barcodes), 30)
    chunks = []
    for num, bc_chunk in enumerate(barcode_chunks):
        bc_path = martian.make_path("barcode_{}.txt".format(num))
        with open(bc_path, "wb") as f:
            for i in range(bc_chunk[0], bc_chunk[1]):
                if i > bc_chunk[0]:
                    f.write(b"\n")
                f.write(barcodes[i])
        chunks.append({"barcodes": bc_path, "__mem_gb": chunk_gb})
    return {"chunks": chunks, "join": {"__mem_gb": join_gb}}


def join(args, outs, chunk_defs, chunk_outs):
    if args.fragments is None:
        outs.raw_matrix = None
        outs.raw_matrix_mex = None
        return

    # Hstack barcodes to generate full peak matrix
    barcodes = []
    sp_matrix = None
    for i, chunk in enumerate(chunk_outs):
        if chunk.raw_matrix is not None and os.path.exists(chunk.raw_matrix):
            cpm = cr_matrix.CountMatrix.load_h5_file(chunk.raw_matrix)
            if i == 0:
                sp_matrix = cpm.m
            else:
                sp_matrix = hstack([sp_matrix, cpm.m])
            barcodes.extend(cpm.bcs)

    genomes = utils.generate_genome_tag(args.reference_path)
    peaks_def = atac_feature_ref.from_peaks_bed(args.peaks, genomes)
    raw_matrix = cr_matrix.CountMatrix(peaks_def, barcodes, sp_matrix)
    raw_matrix.save_h5_file(outs.raw_matrix, sw_version=martian.get_pipelines_version())
    if not os.path.exists(outs.raw_matrix_mex):
        os.mkdir(outs.raw_matrix_mex)
    atac_matrix.save_mex(
        raw_matrix,
        outs.raw_matrix_mex,
        cr_lib_constants.ATACSEQ_LIBRARY_TYPE,
        martian.get_pipelines_version(),
    )


def main(args, outs):
    args.coerce_strings()
    outs.coerce_strings()

    outs.raw_matrix_mex = None
    if args.fragments is None:
        outs.raw_matrix = None
        return

    with open(args.peaks, "rb") as infile:
        full_peaks = tk_bio.get_target_regions(infile)

    i = count_header_lines_bed(args.peaks)
    with open(args.peaks, "rb") as pfile:
        for _ in range(i):
            next(pfile)
        peaks_dict = OrderedDict(
            (b"%s:%s-%s" % tuple(peak.strip(b"\n").split(b"\t")), num)
            for num, peak in enumerate(pfile)
        )

    with open(args.barcodes, "rb") as barcode_file:
        barcodes_dict = OrderedDict((bc.strip(b"\n"), num) for num, bc in enumerate(barcode_file))

    if len(barcodes_dict) == 0:
        outs.raw_matrix = None
        return

    # get matrix counts
    peak_bc_counts = Counter()
    key_set = set(full_peaks.keys())
    for contig, start, stop, barcode, _ in open_fragment_file(args.fragments):
        if barcode not in barcodes_dict:
            continue
        for pos in (start, stop):
            if contig in key_set:
                peak = full_peaks[contig].get_region_containing_point(pos)
                if peak is not None:
                    peak_bc_counts[
                        barcodes_dict[barcode],
                        peaks_dict[b"%s:%d-%d" % (contig, peak[0], peak[1])],
                    ] += 1

    data = np.zeros(len(peak_bc_counts), dtype=np.int32)
    row = np.zeros(len(peak_bc_counts), dtype=np.int32)
    col = np.zeros(len(peak_bc_counts), dtype=np.int32)

    if len(peak_bc_counts) > 0:
        for i, (key, val) in enumerate(peak_bc_counts.items()):
            data[i] = val
            col[i], row[i] = key
    sp_matrix = csc_matrix(
        coo_matrix((data, (row, col)), shape=(len(peaks_dict), len(barcodes_dict)), dtype=int)
    )

    # save as a CountMatrix
    genomes = utils.generate_genome_tag(args.reference_path)
    peaks_def = atac_feature_ref.from_peaks_bed(args.peaks, genomes)
    raw_matrix = cr_matrix.CountMatrix(
        peaks_def,
        barcodes_dict.keys(),
        sp_matrix,
    )
    raw_matrix.save_h5_file(outs.raw_matrix, sw_version=martian.get_pipelines_version())
