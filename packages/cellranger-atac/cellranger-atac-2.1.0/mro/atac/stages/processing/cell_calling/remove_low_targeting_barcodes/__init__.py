# Copyright (c) 2019 10x Genomics, Inc. All rights reserved.
"""
Identifies barcodes with low targeting (highly likely to be from dead cells), with no
limitation to accessible regions.

Outputs a json of species-specific barcodes to be excluded from later cell calling.
"""
from __future__ import absolute_import, division

import os
import pickle
from collections import Counter
import martian
import tenkit.safe_json as safe_json
from atac.tools.ref_manager import ReferenceManager
from atac.tools.fragments import estimate_gem_wells, parsed_fragments_from_region
from atac.tools.regions import get_target_regions, fragment_overlaps_target

__MRO__ = """
stage REMOVE_LOW_TARGETING_BARCODES(
    in  bed        peaks,
    in  tsv.gz     fragments,
    in  tsv.gz.tbi fragments_index,
    in  path       reference_path,
    out json       barcode_counts,
    out json       low_targeting_barcodes,
    out json       low_targeting_summary,
    out json       fragment_lengths,
    out json       covered_bases,
    src py         "stages/processing/cell_calling/remove_low_targeting_barcodes",
) split (
    in  string[]   contigs,
    out pickle[]   fragment_counts,
    out pickle[]   targeted_counts,
    out pickle[]   chunk_fragment_lengths,
    out pickle[]   chunk_covered_bases,
    out int[]      peak_coverage,
) using (
    mem_gb   = 4,
    volatile = strict,
)
"""

DISTANCE = 250
PADDING_VALUES = [0, 50000]


def count_covered_bases(start_iter, stop_iter, contig_len, padding=1000):
    """
    Loop through an input file, counting up the per-barcode total coverage and maximum
    coverage
    """
    occupancy = 0
    last_start = None
    last_stop = None

    for start, stop in zip(start_iter, stop_iter):
        start = max(0, start - padding)
        stop = min(contig_len, stop + padding)

        if last_start is None:
            last_start = start
            last_stop = stop
        else:
            if start < last_stop:
                last_stop = max(stop, last_stop)
            else:
                occupancy += last_stop - last_start
                last_start = start
                last_stop = stop

    if last_start is not None:
        occupancy += last_stop - last_start

    return occupancy


def split(args):
    if args.fragments is None:
        return {"chunks": [], "join": {}}

    ref = ReferenceManager(args.reference_path)
    contig_chunks = ref.make_chunks(30, ref.primary_contigs())

    # the chunks/join construct dict's keyed by barcode so we could blow up with a large number
    # of gem wells. This should be captured by the size of fragments file but probably not linearly
    # NOTE: the scaling with gem groups is a complete guess. When we have more data we can fit the
    # parameter.
    frags_gb = os.path.getsize(args.fragments) / 1e9
    num_gem_wells = estimate_gem_wells(args.fragments, ref.primary_contigs())
    join_gb = max(0.24 * frags_gb + 2.3 + 0.5 * (num_gem_wells - 1), 4)
    chunk_gb = max(0.12 * frags_gb + 1.3 + 0.1 * (num_gem_wells - 1), 2)

    chunks = []
    for contigs in contig_chunks:
        chunks.append({"contigs": contigs, "__mem_gb": chunk_gb})

    return {"chunks": chunks, "join": {"__mem_gb": join_gb}}


def merge_chunk_fragment_results(chunk_outs):
    """Merges the (padded) total fragment length and base coverge data on a per
    -barcode basis across all chunks."""
    fragment_lengths = {padding: Counter() for padding in PADDING_VALUES}
    covered_bases = {padding: Counter() for padding in PADDING_VALUES}

    for chunk_out in chunk_outs:
        for fl_pickle, cb_pickle in zip(
            chunk_out.chunk_fragment_lengths, chunk_out.chunk_covered_bases
        ):
            with open(fl_pickle, "rb") as infile:
                data = pickle.load(infile)
                for padding in PADDING_VALUES:
                    fragment_lengths[padding] += data[padding]

            with open(cb_pickle, "rb") as infile:
                data = pickle.load(infile)
                for padding in PADDING_VALUES:
                    covered_bases[padding] += Counter(data[padding])
    return fragment_lengths, covered_bases


def merge_chunk_coverage(ref, chunk_defs, chunk_outs):
    """Merge chunk results for fraction coverage of genome in peaks."""
    species_list = ref.list_species()

    peak_bp_by_species = {species: 0 for species in species_list}
    genome_bp_by_species = {species: 0 for species in species_list}

    for chunk_in, chunk_out in zip(chunk_defs, chunk_outs):
        for contig, peak_coverage in zip(chunk_in.contigs, chunk_out.peak_coverage):
            species = ref.species_from_contig(contig)

            peak_bp_by_species[species] += peak_coverage
            genome_bp_by_species[species] += ref.contig_lengths[contig]

    return {
        species: peak_bp_by_species[species] / genome_bp_by_species[species]
        for species in species_list
    }


def merge_chunk_counts(ref, chunk_defs, chunk_outs):
    """Merge fragment counts and targeted counts from separate chunks into
    single dictionaries.
    """

    species_list = ref.list_species()

    barcode_counts_by_species = {species: Counter() for species in species_list}
    targeted_counts_by_species = {species: Counter() for species in species_list}

    for chunk_in, chunk_out in zip(chunk_defs, chunk_outs):
        for contig, fc_pickle, tc_pickle in zip(
            chunk_in.contigs, chunk_out.fragment_counts, chunk_out.targeted_counts
        ):
            species = ref.species_from_contig(contig)

            with open(fc_pickle, "rb") as infile:
                barcode_counts_by_species[species] += pickle.load(infile)
            with open(tc_pickle, "rb") as infile:
                targeted_counts_by_species[species] += pickle.load(infile)

    return barcode_counts_by_species, targeted_counts_by_species


def join(args, outs, chunk_defs, chunk_outs):
    args.coerce_strings()
    outs.coerce_strings()
    if args.fragments is None:
        outs.low_targeting_barcodes = None
        outs.low_targeting_summary = None
        return

    # Merge the chunk inputs
    ref = ReferenceManager(args.reference_path)
    species_list = ref.list_species()

    barcode_counts_by_species, targeted_counts_by_species = merge_chunk_counts(
        ref, chunk_defs, chunk_outs
    )
    frac_genome_in_peaks_by_species = merge_chunk_coverage(ref, chunk_defs, chunk_outs)
    fragment_lengths, covered_bases = merge_chunk_fragment_results(chunk_outs)

    # Identify barcodes that have lower fraction of reads overlapping peaks than the
    # genomic coverage of the peaks
    low_targeting_barcodes = {
        "label": "low_targeting",
        "data": {species: {} for species in species_list},
    }
    for species in species_list:
        for barcode, total_count in barcode_counts_by_species[species].items():
            barcode_frac_peaks = targeted_counts_by_species[species][barcode] / total_count
            if barcode_frac_peaks < frac_genome_in_peaks_by_species[species]:
                low_targeting_barcodes["data"][species][barcode] = barcode_frac_peaks

    # Sum up the total fragment counts per barcode across all species
    total_barcode_counts = Counter()
    for species, barcode_counts in barcode_counts_by_species.items():
        total_barcode_counts += barcode_counts
    with open(outs.barcode_counts, "w") as outfile:
        outfile.write(safe_json.safe_jsonify(total_barcode_counts, pretty=True))

    summary_data = {}
    for species in species_list:
        key_suffix = "" if len(species_list) == 1 else "_{}".format(species)
        summary_data["number_of_low_targeting_barcodes{}".format(key_suffix)] = len(
            low_targeting_barcodes["data"][species]
        )
        summary_data[
            "fraction_of_genome_within_{}bp_of_peaks{}".format(DISTANCE, key_suffix)
        ] = frac_genome_in_peaks_by_species[species]
    with open(outs.low_targeting_summary, "w") as outfile:
        safe_json.dump_numpy(summary_data, outfile, pretty=True)
    with open(outs.low_targeting_barcodes, "w") as outfile:
        outfile.write(safe_json.safe_jsonify(low_targeting_barcodes, pretty=True))
    with open(outs.fragment_lengths, "w") as outfile:
        outfile.write(safe_json.safe_jsonify(fragment_lengths, pretty=True))
    with open(outs.covered_bases, "w") as outfile:
        outfile.write(safe_json.safe_jsonify(covered_bases, pretty=True))


def main(args, outs):  # pylint: disable=too-many-locals
    outs.fragment_counts = []
    outs.targeted_counts = []
    outs.chunk_fragment_lengths = []
    outs.chunk_covered_bases = []
    outs.peak_coverage = []

    if args.fragments is None:
        return

    ref = ReferenceManager(args.reference_path)

    with open(args.peaks, "rb") as infile:
        peak_regions = get_target_regions(infile)

    for str_contig in args.contigs:
        contig_len = ref.get_contig_lengths()[str_contig]
        contig = str_contig.encode()  # type: bytes
        fragment_counts = Counter()
        targeted_counts = Counter()

        cumulative_fragment_length = {padding: Counter() for padding in PADDING_VALUES}
        covered_bases = {padding: {} for padding in PADDING_VALUES}

        for _, start, stop, barcode, _ in parsed_fragments_from_region(
            args.fragments, contig, None, None, args.fragments_index
        ):
            fragment_counts[barcode] += 1
            if fragment_overlaps_target(contig, start, stop, peak_regions):
                targeted_counts[barcode] += 1

            for padding in PADDING_VALUES:
                adj_start = max(0, start - padding)
                adj_stop = min(contig_len, stop + padding)

                cumulative_fragment_length[padding][barcode] += adj_stop - adj_start

                if barcode not in covered_bases[padding]:
                    # Total # of covered bases, current start/stop of active region
                    covered_bases[padding][barcode] = 0, None, None

                current_covered, active_start, active_stop = covered_bases[padding][barcode]
                if active_start is None:
                    active_start = adj_start
                    active_stop = adj_stop
                else:
                    if adj_start < active_stop:
                        active_stop = max(adj_stop, active_stop)
                    else:
                        current_covered += active_stop - active_start
                        active_start = adj_start
                        active_stop = adj_stop
                covered_bases[padding][barcode] = current_covered, active_start, active_stop

        # Reformat the results for output and check for uncompleted regions
        for padding in PADDING_VALUES:
            for barcode in covered_bases[padding]:
                current_covered, active_start, active_stop = covered_bases[padding][barcode]
                if active_start is None:
                    covered_bases[padding][barcode] = current_covered
                else:
                    covered_bases[padding][barcode] = current_covered + active_stop - active_start

        fc_pickle = martian.make_path(b"fragment_counts_%s.pickle" % contig)
        tc_pickle = martian.make_path(b"targeted_counts_%s.pickle" % contig)
        fl_pickle = martian.make_path(b"fragment_lengths_%s.pickle" % contig)
        cb_pickle = martian.make_path(b"covered_bases_%s.pickle" % contig)
        with open(fc_pickle, "wb") as outfile:
            pickle.dump(fragment_counts, outfile, protocol=pickle.HIGHEST_PROTOCOL)
        with open(tc_pickle, "wb") as outfile:
            pickle.dump(targeted_counts, outfile, protocol=pickle.HIGHEST_PROTOCOL)
        with open(fl_pickle, "wb") as outfile:
            pickle.dump(cumulative_fragment_length, outfile, protocol=pickle.HIGHEST_PROTOCOL)
        with open(cb_pickle, "wb") as outfile:
            pickle.dump(covered_bases, outfile, protocol=pickle.HIGHEST_PROTOCOL)
        outs.fragment_counts.append(fc_pickle)
        outs.targeted_counts.append(tc_pickle)
        outs.chunk_fragment_lengths.append(fl_pickle)
        outs.chunk_covered_bases.append(cb_pickle)
        outs.peak_coverage.append(
            count_covered_bases(
                peak_regions[contig].starts,
                peak_regions[contig].ends,
                ref.contig_lengths[str_contig],
                padding=DISTANCE,
            )
            if contig in peak_regions
            else 0
        )
