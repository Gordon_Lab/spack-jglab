# Copyright (c) 2019 10x Genomics, Inc. All rights reserved.
"""
Annotate barcodes which contain cells using total number of reads.

Uses an EM model to fit two negative binomial distributions to the reads-per-barcode
data, one modeling the background barcodes and one modeling the signal (cell-containing)
barcodes.
"""
from __future__ import absolute_import, division, print_function

import json
import pickle
from collections import namedtuple, Counter
from typing import Any, Dict, List
import martian
import numpy as np
from scipy import stats
from six import ensure_str, ensure_binary

import atac.cell_calling_helpers.fit as cc_help
from atac.barcodes import load_barcode_whitelist
from atac.constants import (
    MAXIMUM_CELLS_PER_SPECIES,
    NO_BARCODE,
    MINIMUM_COUNT,
    WHITELIST_CONTAM_RATE,
)
from atac.tools.fragments import parsed_fragments_from_region
from atac.tools.ref_manager import ReferenceManager
from atac.tools.regions import get_target_regions, fragment_overlaps_target
from atac.tools.singlecell import write_singlecell
from tenkit.stats import robust_divide
import tenkit.safe_json as tk_json


__MRO__ = """
stage DETECT_CELL_BARCODES(
    in  tsv.gz     fragments,
    in  tsv.gz.tbi fragments_index,
    in  string     barcode_whitelist,
    in  json       excluded_barcodes,
    in  map        force_cells,
    in  path       reference_path,
    in  bed        peaks,
    out csv        cell_barcodes,
    out csv        singlecell,
    out json       cell_calling_summary,
    src py         "stages/processing/cell_calling/detect_cell_barcodes",
) split (
    in  string[]   contigs,
    out pickle[]   barcode_counts,
    out pickle[]   targeted_counts,
    out int[]      fragment_depths,
) using (
    mem_gb   = 4,
    volatile = strict,
)
"""
CELL_CALLING_THRESHOLD = 100000
MixedModelParams = namedtuple(
    "MixedModelParams", ["mu_noise", "alpha_noise", "mu_signal", "alpha_signal", "frac_noise"]
)


def split(args):
    if args.fragments is None:
        return {"chunks": [], "join": {}}

    ref = ReferenceManager(args.reference_path)
    chunk_contigs = ref.make_chunks(30, ref.primary_contigs())

    chunks = []
    for contigs in chunk_contigs:
        chunks.append({"contigs": contigs, "__mem_gb": 5})

    return {"chunks": chunks, "join": {"__mem_gb": 5}}


def estimate_threshold(params, odds_ratio=20):
    """Estimate the separation threshold between signal and noise using the fits - look
    for the lowest count that gives an appropriate odds ratio in favor of signal
    """

    def ratio(k):
        """Computes the odds ratio of signal to noise for a given k"""
        signal = (1 - params.frac_noise) * stats.nbinom.pmf(
            k, 1 / params.alpha_signal, 1 / (1 + params.alpha_signal * params.mu_signal)
        )
        noise = params.frac_noise * stats.nbinom.pmf(
            k, 1 / params.alpha_noise, 1 / (1 + params.alpha_noise * params.mu_noise)
        )
        return signal / noise

    test_count = -1
    for test_count in np.arange(int(params.mu_noise), int(params.mu_signal) + 1):
        if ratio(test_count) >= odds_ratio:
            return test_count
    return test_count


# pylint: disable=too-many-branches,too-many-statements,too-many-locals
def join(args, outs, chunk_defs, chunk_outs):
    args.coerce_strings()
    outs.coerce_strings()
    if args.fragments is None:
        outs.cell_barcodes = None
        outs.cell_calling_summary = None
        outs.singlecell = None
        return

    excluded_barcodes = None
    if args.excluded_barcodes is not None:
        with open(args.excluded_barcodes, "r") as infile:
            excluded_barcodes = json.load(infile)  # type: Dict[str, Dict[str, List[Any]]]

    # Merge the chunk inputs
    ref = ReferenceManager(args.reference_path)
    species_list = ref.list_species()

    barcode_counts_by_species = {species: Counter() for species in species_list}
    targeted_counts_by_species = {species: Counter() for species in species_list}
    fragment_depth = 0
    for chunk_in, chunk_out in zip(chunk_defs, chunk_outs):
        for i, contig in enumerate(chunk_in.contigs):
            species = ref.species_from_contig(contig)
            with open(chunk_out.barcode_counts[i], "rb") as infile:
                barcode_counts_by_species[species] += pickle.load(infile)
            with open(chunk_out.targeted_counts[i], "rb") as infile:
                targeted_counts_by_species[species] += pickle.load(infile)
            fragment_depth += chunk_out.fragment_depths[i]
    print("Total fragments across all chunks: {}".format(fragment_depth))

    barcodes = list({bc for species in species_list for bc in barcode_counts_by_species[species]})
    non_excluded_barcodes = {
        species: [bc for bc in barcodes if ensure_str(bc) not in excluded_barcodes[species]]
        for species in species_list
    }
    print("Total barcodes observed: {}".format(len(barcodes)))

    retained_counts = {}
    for species in species_list:
        if excluded_barcodes is None:
            retained_counts[species] = np.array(
                [targeted_counts_by_species[species][bc] for bc in barcodes]
            )
        else:
            retained_counts[species] = np.array(
                [
                    targeted_counts_by_species[species][bc]
                    for bc in barcodes
                    if ensure_str(bc) not in excluded_barcodes[species]
                ]
            )
            print(
                "Barcodes excluded for species {}: {}".format(
                    species, len(excluded_barcodes[species])
                )
            )
            print(
                "Barcodes remaining for species {}: {}".format(
                    species, len(non_excluded_barcodes[species])
                )
            )

    parameters = {}

    whitelist_length = len(load_barcode_whitelist(args.barcode_whitelist))
    count_shift = max(MINIMUM_COUNT, int(fragment_depth * WHITELIST_CONTAM_RATE / whitelist_length))
    print("Count shift for whitelist contamination: {}".format(count_shift))

    for (species, count_data) in retained_counts.items():
        print("Analyzing species {}".format(species))
        # Subtract MINIMUM_COUNT from all counts to remove the effects of whitelist contamination
        shifted_data = count_data[count_data >= count_shift] - count_shift
        print("Number of barcodes analyzed: {}".format(len(shifted_data)))
        count_dict = Counter(shifted_data)
        parameters[species] = {}

        forced_cell_count = None
        if args.force_cells is not None:
            if species in args.force_cells:
                forced_cell_count = int(args.force_cells[species])
            elif "default" in args.force_cells:
                forced_cell_count = int(args.force_cells["default"])
            elif isinstance(args.force_cells, dict) and len(args.force_cells) == 1:
                forced_cell_count = int(list(args.force_cells.values())[0])
            if forced_cell_count > MAXIMUM_CELLS_PER_SPECIES:
                forced_cell_count = MAXIMUM_CELLS_PER_SPECIES
                martian.log_info(
                    "Attempted to force cells to {}.  Overriding to maximum allowed cells.".format(
                        forced_cell_count
                    )
                )

        # Initialize parameters to empty
        parameters[species]["noise_mean"] = None
        parameters[species]["noise_dispersion"] = None
        parameters[species]["signal_mean"] = None
        parameters[species]["signal_dispersion"] = None
        parameters[species]["fraction_noise"] = None
        parameters[species]["cell_threshold"] = None
        parameters[species]["goodness_of_fit"] = None
        parameters[species]["estimated_cells_present"] = 0
        parameters["cell_calling_method"] = "default"

        if not count_dict:
            # if count_dict is empty we exit
            martian.exit(
                "No transposition events within peaks for species {} after filtering."
                " Unable to perform cell calling.".format(species)
            )
        elif len(count_dict) < 10:
            # if the count dict is small => this is a pathological edge case, just declare the max
            # count barcode(s) as cells and set an appropriate cell_threshold
            max_count = max(count_dict.keys())
            parameters[species]["cells_detected"] = count_dict[max_count]
            parameters[species]["cell_threshold"] = max_count + count_shift - 0.1
            forced_cell_count = None
        elif forced_cell_count is None:
            print("Estimating parameters")
            fitted_params = cc_help.estimate_parameters(count_dict)
            signal_threshold = (
                estimate_threshold(fitted_params, CELL_CALLING_THRESHOLD) + count_shift
            )
            print("Primary threshold: {}".format(signal_threshold))
            parameters[species]["noise_mean"] = fitted_params.mu_noise
            parameters[species]["noise_dispersion"] = fitted_params.alpha_noise
            parameters[species]["signal_mean"] = fitted_params.mu_signal
            parameters[species]["signal_dispersion"] = fitted_params.alpha_signal
            parameters[species]["fraction_noise"] = fitted_params.frac_noise
            parameters[species]["cell_threshold"] = signal_threshold
            parameters[species]["goodness_of_fit"] = cc_help.goodness_of_fit(
                shifted_data, fitted_params
            )
            called_cell_count = np.sum(count_data >= signal_threshold)
            parameters[species]["cells_detected"] = called_cell_count
            parameters[species]["estimated_cells_present"] = int(
                (1 - fitted_params.frac_noise) * len(shifted_data)
            )
            if called_cell_count > MAXIMUM_CELLS_PER_SPECIES:
                # Abort the model fitting and instead force cells to the maximum
                forced_cell_count = MAXIMUM_CELLS_PER_SPECIES

        if forced_cell_count is not None:
            parameters["cell_calling_method"] = "force_cells"
            print("Forcing cells to {}".format(forced_cell_count))

            if forced_cell_count <= 0:
                raise ValueError("Force cells must be positive")
            else:
                adj_data = shifted_data[shifted_data > 0]
                print("Total barcodes considered for forcing cells: {}".format(len(adj_data)))
                parameters[species]["cell_threshold"] = (
                    min(adj_data)
                    if forced_cell_count >= len(adj_data)
                    else sorted(adj_data, reverse=True)[forced_cell_count - 1]
                )
                parameters[species]["cell_threshold"] += count_shift
                parameters[species]["cells_detected"] = np.sum(
                    count_data >= parameters[species]["cell_threshold"]
                )

    # For barnyard samples, mask out the noise distribution and re-fit to get cleaner separation
    if len(retained_counts) == 2 and (args.force_cells is None or not args.force_cells):
        print("Estimating secondary thresholds")
        sp1, sp2 = species_list

        sp1_threshold = (
            -1 if parameters[sp1]["cell_threshold"] is None else parameters[sp1]["cell_threshold"]
        )
        sp2_threshold = (
            -1 if parameters[sp2]["cell_threshold"] is None else parameters[sp2]["cell_threshold"]
        )

        if parameters[sp1]["cell_threshold"] is not None:
            sp1_counts = np.array(
                [
                    targeted_counts_by_species[sp1][bc]
                    for bc in non_excluded_barcodes[sp1]
                    if (
                        (targeted_counts_by_species[sp1][bc] > sp1_threshold)
                        or (targeted_counts_by_species[sp2][bc] > sp2_threshold)
                    )
                ]
            )
            sp1_params = cc_help.estimate_parameters(Counter(sp1_counts), threshold=sp1_threshold)
            if not np.isnan(sp1_params.frac_noise):
                parameters[sp1]["cell_threshold"] = max(
                    sp1_threshold, estimate_threshold(sp1_params, 20)
                )
            parameters[sp1]["cells_detected"] = np.sum(
                retained_counts[sp1] >= parameters[sp1]["cell_threshold"]
            )
        else:
            parameters[sp1]["cells_detected"] = 0

        if parameters[sp2]["cell_threshold"] is not None:
            sp2_counts = np.array(
                [
                    targeted_counts_by_species[sp2][bc]
                    for bc in non_excluded_barcodes[sp2]
                    if (
                        (targeted_counts_by_species[sp1][bc] > sp1_threshold)
                        or (targeted_counts_by_species[sp2][bc] > sp2_threshold)
                    )
                ]
            )
            sp2_params = cc_help.estimate_parameters(Counter(sp2_counts), threshold=sp2_threshold)
            if not np.isnan(sp2_params.frac_noise):
                parameters[sp2]["cell_threshold"] = max(
                    sp2_threshold, estimate_threshold(sp2_params, 20)
                )
            parameters[sp2]["cells_detected"] = np.sum(
                retained_counts[sp2] >= parameters[sp2]["cell_threshold"]
            )
        else:
            parameters[sp2]["cells_detected"] = 0

        print("Secondary threshold ({}): {}".format(sp1, parameters[sp1]["cell_threshold"]))
        print("Secondary threshold ({}): {}".format(sp2, parameters[sp2]["cell_threshold"]))

    print("Writing out cell barcodes")
    cell_barcodes = {}
    for (species, count_data) in retained_counts.items():
        threshold = parameters[species]["cell_threshold"]
        cell_barcodes[species] = {}
        print("Cell threshold for species {}: {}".format(species, threshold))
        if threshold is not None:
            for count, barcode in zip(count_data, non_excluded_barcodes[species]):
                if count >= threshold:
                    print(
                        "{} - Total {}, Targeted {}, Count {}, Threshold {}".format(
                            barcode,
                            barcode_counts_by_species[species][barcode],
                            targeted_counts_by_species[species][barcode],
                            count,
                            threshold,
                        )
                    )
                    cell_barcodes[species][barcode] = count
        if len(cell_barcodes[species]) != parameters[species]["cells_detected"]:
            print(len(cell_barcodes[species]), parameters[species]["cells_detected"])
            raise ValueError("Mismatch in called cells identified - failure in threshold setting")
        print("Selected {} barcodes of species {}".format(len(cell_barcodes[species]), species))

    with open(outs.cell_barcodes, "wb") as outfile:
        # low mem reduce op to merge-sort bcs across species
        for species, species_barcodes in cell_barcodes.items():
            outfile.write(ensure_binary(species))
            for bc in species_barcodes:
                outfile.write(b",")
                outfile.write(bc)
            outfile.write(b"\n")

    write_barcodes = [NO_BARCODE] + sorted(barcodes)
    write_singlecell(
        species_list,
        write_barcodes,
        cell_barcodes,
        barcode_counts_by_species,
        targeted_counts_by_species,
        excluded_barcodes,
        outs.singlecell,
    )

    # process parameters into summary metrics
    summary_info = generate_cell_calling_metrics(parameters, cell_barcodes, species_list)
    with open(outs.cell_calling_summary, "w") as outfile:
        json.dump(tk_json.json_sanitize(summary_info), outfile, indent=4)


def generate_cell_calling_metrics(parameters, cell_barcodes, species_list):
    """Generates a small dictionary of key species-specific metrics for cell calling."""
    summary_info = {}
    for species in species_list:
        key_suffix = "" if len(species_list) == 1 else "_{}".format(species)

        # Cell calling metrics
        summary_info["fitted_mean_noise{}".format(key_suffix)] = parameters[species]["noise_mean"]
        summary_info["fitted_dispersion_noise{}".format(key_suffix)] = parameters[species][
            "noise_dispersion"
        ]
        summary_info["fitted_mean_signal{}".format(key_suffix)] = parameters[species]["signal_mean"]
        summary_info["fitted_dispersion_signal{}".format(key_suffix)] = parameters[species][
            "signal_dispersion"
        ]
        summary_info["fraction_cell_calling_noise{}".format(key_suffix)] = parameters[species][
            "fraction_noise"
        ]
        summary_info["cell_threshold{}".format(key_suffix)] = parameters[species]["cell_threshold"]
        summary_info["goodness_of_fit{}".format(key_suffix)] = parameters[species][
            "goodness_of_fit"
        ]
        summary_info["estimated_cells_present{}".format(key_suffix)] = parameters[species][
            "estimated_cells_present"
        ]

        summary_info["annotated_cells{}".format(key_suffix)] = len(cell_barcodes[species])
        summary_info["estimated_fraction_cells_annotated{}".format(key_suffix)] = robust_divide(
            len(cell_barcodes[species]), parameters[species]["estimated_cells_present"]
        )

    summary_info["cells_detected"] = len(
        {bc for barcodes in cell_barcodes.values() for bc in barcodes}
    )
    summary_info["cell_calling_method"] = parameters["cell_calling_method"]

    return summary_info


def main(args, outs):
    with open(args.peaks, "rb") as infile:
        peak_regions = get_target_regions(infile)

    outs.fragment_depths = []
    outs.barcode_counts = []
    outs.targeted_counts = []
    for contig_str in args.contigs:
        barcode_counts = Counter()  # type: Counter[bytes]
        targeted_counts = Counter()  # type: Counter[bytes]
        fragment_depth = 0
        contig = contig_str.encode()  # type: bytes
        for _, start, stop, barcode, _ in parsed_fragments_from_region(
            args.fragments, contig_str, None, None, args.fragments_index
        ):
            barcode_counts[barcode] += 1
            fragment_depth += 1
            if fragment_overlaps_target(contig, start, stop, peak_regions):
                targeted_counts[barcode] += 1

        bc_counts = martian.make_path(b"bc_counts_%s.pickle" % contig)
        tgt_counts = martian.make_path(b"tgt_counts_%s.pickle" % contig)
        with open(bc_counts, "wb") as outfile:
            pickle.dump(barcode_counts, outfile, protocol=pickle.HIGHEST_PROTOCOL)
        with open(tgt_counts, "wb") as outfile:
            pickle.dump(targeted_counts, outfile, protocol=pickle.HIGHEST_PROTOCOL)
        outs.fragment_depths.append(fragment_depth)
        outs.barcode_counts.append(bc_counts)
        outs.targeted_counts.append(tgt_counts)
