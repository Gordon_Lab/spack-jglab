# Copyright (c) 2019 10x Genomics, Inc. All rights reserved.
"""
Detect open chromatin regions in the genome as measured from ATAC-seq fragment ends.
"""
from __future__ import absolute_import, division

import json
from collections import namedtuple, Counter
from typing import Optional, List

import martian
import numpy as np
from scipy.signal import cwt, ricker, find_peaks, peak_widths
from scipy.stats import nbinom, poisson, beta
from atac.tools.fragments import parsed_fragments_from_region
from atac.tools.regions import sort_and_uniq_bed
from atac.tools.ref_manager import ReferenceManager
from atac.analysis.em_fitting import (
    MLE_dispersion,
    weighted_mean,
    MixtureModel,
    MixtureModelError,
    GeometricComponent,
    NegativeBinomialComponent,
)
from atac.analysis.peaks import (
    load_count_dict,
    calculate_peak_metrics,
)
from atac.preflights import check_input_peaks_bed
from cellranger.csv_utils import combine_csv

__MRO__ = """
stage DETECT_PEAKS(
    in  string     sample_id,
    in  string     sample_desc,
    in  string     assay,
    in  bedgraph   cut_sites,
    in  path       reference_path,
    in  json       count_dict,
    in  bed        custom_peaks,
    in  float      qval,
    in  tsv.gz     fragments,
    in  tsv.gz.tbi fragments_index,
    out bed        peaks,
    out json       peak_metrics,
    src py         "stages/processing/detect_peaks",
) split (
    in  string[]   contigs,
    in  float      threshold,
) using (
    mem_gb   = 6,
    volatile = strict,
) retain (
    peaks,
)
"""

# Combined, these parameters tune how aggressive the caller is.  Smaller qvalues
# or higher signal to noise ratios will be more stringent in calling peaks.
DEFAULT_GLOBAL_QVAL = 5e-2
DEFAULT_SIGNAL_NOISE_RATIO = 1.5

MAX_FRAGMENT_LENGTH = 1000
SHORT_FRAGMENT_LENGTH = 147
WAVELET_PADDING = 5000
WAVELET_WIDTH = 300


class Region(namedtuple("Region", ["contig", "start", "stop"])):
    """Simple region class for better working with peak regions."""

    @property
    def length(self):
        return self.stop - self.start

    def __repr__(self):
        return "{}:{}-{}".format(self.contig, self.start, self.stop)


################################################################################
# I/O functions


def load_signal_track_from_bedgraph(signal, ref, contig):
    """
    Opens up a bedgraph file containing the signal track and loads the signal
    into a numpy array for the chosen contig.

    Also produces a count dictionary with the number of times each count is
    seen across that contig.
    """
    contig_len = ref.contig_lengths[contig]
    counts = np.zeros(contig_len, dtype=int)
    count_dict = Counter()
    with open(signal, "r") as infile:
        for track in infile:
            chrom, start, stop, count = track.split("\t")
            if chrom == contig:
                start, stop, count = int(start), int(stop), int(count)
                counts[start:stop] = count
                if count > 0:
                    count_dict[count] += stop - start
    return counts, count_dict


def write_peaks(peak_iter, out_peaks):
    """Writes out peaks from an iterator to an output file."""
    with open(out_peaks, "a") as outfile:
        for peak in peak_iter:
            outfile.write("{}\t{}\t{}\n".format(peak.contig, peak.start, peak.stop))


def get_pipeline_name(assay: str) -> str:
    """Get the pipeline name associated with the assay.

    Args:
        assay (str): Either 'atac' or 'arc'

    Returns:
        str: pipeline name
    """
    if assay == "atac":
        return "cellranger-atac"
    elif assay == "arc":
        return "cellranger-arc"
    elif assay is None:
        return ""
    else:
        raise ValueError("{} is not a valid assay".format(assay))


def make_pipeline_header(sample_id: str, sample_desc: str, assay: str) -> List[bytes]:
    """Generates comment strings related to the pipeline parameters.

    Args:
        sample_id (str): sample identifier
        sample_desc (str): sample description
        assay (str): assay identifier

    Returns:
        List[bytes]: a list of header lines
    """
    rows = []
    rows.append("# id={}\n".format(sample_id or "").encode())
    rows.append("# description={}\n".format(sample_desc or "").encode())
    rows.append(b"#\n")
    rows.append("# pipeline_name={}\n".format(get_pipeline_name(assay)).encode())
    rows.append("# pipeline_version={}\n".format(martian.get_pipelines_version()).encode())
    return rows


################################################################################
# Peak generation and modulation functions


def threshold_peaks_from_signal(signal, threshold, contig, indices=None):
    """Takes a signal track, applies a fixed threshold to it, and yields as peaks
    all contiguous regions that are above that threshold.

    Note that peak outputs are 0-indexed and do not include the stop position,
    as in the BED file format.
    """
    mask = signal >= threshold
    if indices is None:
        indices = np.arange(len(signal))
    start = None
    stop = None
    for index in indices[mask]:
        if start is None:
            start = index
            stop = index + 1
        elif stop == index:
            stop = index + 1
        else:
            yield Region(contig, start, stop)
            start = index
            stop = index + 1
    if start is not None:
        yield Region(contig, start, stop)


def extend_peak_length(peak, fragments: str, index: Optional[str]):
    """Post-processes peaks by extending each peak so that partial short fragments
    (with only one end inside the peak) are included.
    """

    def is_partial(fragment, peak_start, peak_stop, upstream=True):
        if upstream:
            return (fragment.start < peak_start < fragment.stop) and (fragment.stop < peak_stop)
        else:
            return (fragment.start > peak_start) and (fragment.start < peak_stop < fragment.stop)

    def is_cutsite_inside(fragment, peak_start, peak_stop):
        if fragment.start >= peak_start and fragment.start < peak_stop:
            return True
        if fragment.stop > peak_start and fragment.stop <= peak_stop:
            return True
        return False

    overlapping_fragments = [
        Region(f[0], f[1], f[2])
        for f in parsed_fragments_from_region(
            fragments,
            peak.contig,
            peak.start - MAX_FRAGMENT_LENGTH,
            peak.stop + MAX_FRAGMENT_LENGTH,
            index,
        )
    ]

    peak_start = peak.start
    peak_stop = peak.stop

    # Filter out peaks without many short fragments inside them
    count = 0
    for f in overlapping_fragments:
        if (f.length <= SHORT_FRAGMENT_LENGTH) and is_cutsite_inside(f, peak_start, peak_stop):
            count += 1
    if count < 5:
        return None

    # Extend upstream in the 5' direction
    while True:
        partial_frags = [
            f for f in overlapping_fragments if is_partial(f, peak_start, peak_stop, upstream=True)
        ]
        short_frags = [f for f in partial_frags if f.length < SHORT_FRAGMENT_LENGTH]
        if not short_frags or len(short_frags) < (len(partial_frags) // 2):
            break
        peak_start = min([f.start for f in short_frags])
    # Extend downstream in the 3' direction
    while True:
        partial_frags = [
            f for f in overlapping_fragments if is_partial(f, peak_start, peak_stop, upstream=False)
        ]
        short_frags = [f for f in partial_frags if f.length < SHORT_FRAGMENT_LENGTH]
        if not short_frags or len(short_frags) < (len(partial_frags) // 2):
            break
        peak_stop = max([f.stop for f in short_frags])
    return Region(peak.contig, peak_start, peak_stop)


def wavelet_peak_caller(signal, candidate, contig, snr_threshold=2, global_threshold=1, qval=0.05):
    """Calls peaks using a wavelet transform to identify regions with high signal
    to noise ratio.
    """
    # pylint: disable=too-many-locals
    start = max(0, candidate.start - WAVELET_PADDING)
    stop = min(len(signal), candidate.stop + WAVELET_PADDING)
    local_signal = signal[start:stop]

    wavelet_transform = cwt(local_signal, ricker, [WAVELET_WIDTH])[0, :]
    wavelet_peaks, _ = find_peaks(wavelet_transform, height=1)
    wavelet_peaks = [peak for peak in wavelet_peaks if local_signal[peak] > (global_threshold // 2)]
    _, _, waveleft, waveright = peak_widths(
        wavelet_transform, wavelet_peaks, rel_height=0.95, wlen=1000
    )
    peak_mask = np.ones(len(local_signal), dtype=bool)
    for left, right in zip(waveleft, waveright):
        left, right = int(left), int(right)
        peak_mask[left:right] = False

    for left, right in zip(waveleft, waveright):
        left, right = int(left), int(right)
        # Calculate signal to noise ratio
        peak_signal = np.median(local_signal[left:right])
        peak_width = right - left
        noise = 0
        for padding in [peak_width, peak_width * 3, peak_width * 5]:
            minval = max(0, left - padding)
            maxval = min(right + padding, len(local_signal))
            noise = max(
                noise,
                np.median(local_signal[minval:maxval][peak_mask[minval:maxval]]),
            )
        # Uses a dynamic signal-to-noise ratio based on a beta distribution to better
        # filter out very low-signal chaff
        martian.log_info("Putative peak: {}:{}-{}".format(contig, start + left, start + right))
        martian.log_info("Peak signal: {}\tPeak noise: {}".format(peak_signal, noise))
        if beta.cdf(snr_threshold / (1 + snr_threshold), peak_signal + 2, noise + 2) <= qval:
            martian.log_info("Passes SNR filter")
            yield Region(contig, start + left, start + right)


def merge_peaks(sorted_peaks, mergedist=100, smallsize=25, minsize=100):
    """Takes an iterator of sorted peaks and merges adjacent or overlapping
    peaks into a single peak.

    Also merges small peaks very close to other peaks.
    """
    active = None
    for peak in sorted_peaks:
        if active is None:
            active = peak
            small_active_peak = True if smallsize is None else active.length < smallsize
        else:
            same_contig = active.contig == peak.contig
            is_overlapping = active.stop >= peak.start
            is_nearby = (active.stop < peak.start) & (
                False if mergedist is None else (peak.start - active.stop) < mergedist
            )
            is_small_peak = small_active_peak | (
                True if smallsize is None else peak.length < smallsize
            )

            if same_contig and (is_overlapping or (is_nearby and is_small_peak)):
                active = Region(
                    active.contig,
                    min(active.start, peak.start),
                    max(active.stop, peak.stop),
                )
                small_active_peak = True if smallsize is None else peak.length < smallsize
            else:
                if active.length >= minsize:
                    yield active
                active = peak
                small_active_peak = True if smallsize is None else active.length < smallsize
    if active is not None:
        if active.length > minsize:
            yield active


################################################################################
# Threshold generation functions


def fit_mixture_model(count_dict, threshold=None):
    """Fits a mixture model of three components to count dictionary data.

    Composed of:
    GeometricComponent: for the zero-inflated background
    NegativeBinomialComponent: for the normal background
    NegativeBinomialComponent: for the signal
    """
    # pylint: disable=too-many-locals

    def converge_model(model, counts, logweights, maxiter=500, epsilon=1e-6):
        """Takes an initialized mixture model and iteratively applies
        expectation-maximization with given count and logweight data.

        TODO: this function should probably be a method of MixtureModel
        """
        for i in range(maxiter):
            try:
                model.calculate_responsibilities(counts)
                delta_dict = model.estimate_parameters(counts, logweights)
            except MixtureModelError as error:
                martian.log_info("EM crashed: {}".format(error))
                raise MixtureModelError(error) from error
            change = max(delta_dict.values()) if delta_dict else 0.0
            if change < epsilon:
                martian.log_info("Converged model after {} iterations".format(i + 1))
                martian.log_info(model)
                break
        return model

    def get_model_pmf(model, xvals):
        """Takes a multi-component model and generates its full probability distribution.

        TODO: This function should be a method of MixtureModel
        """
        pmfs = None
        for name in model.index:
            if pmfs is None:
                pmfs = model.get_component_contribution(name, xvals)
            else:
                pmfs += model.get_component_contribution(name, xvals)
        return pmfs

    # If the count-dict is all zeros (e.g., for very small contigs) then we abort
    # and return None
    if 0 in count_dict and count_dict[0] == np.sum(count_dict.values()):
        return None

    origcounts, origweights = (np.array(array) for array in zip(*sorted(count_dict.items())))
    # Determine a threshold for which data to include in the initial background fit.
    counts = np.copy(origcounts)
    weights = np.copy(origweights)
    if threshold is None:
        cumulative = np.cumsum(origweights) / np.sum(origweights)
        for count, frac in zip(counts, cumulative):
            if frac > 0.5:
                threshold = max(count, 5)
                break
    martian.log_info("Using threshold for count data of {}".format(threshold))
    weights[counts >= threshold] = 0
    counts = counts[weights > 0]
    weights = weights[weights > 0]
    logweights = np.log(weights)

    # Initialize background components:
    # - initial fit of the background data to a single geometric distribution
    # - find residual counts that are underestimated by the geometric distribution
    # - fit residuals to a negative binomial distribution
    # - EM converge dual component mixture to the background data
    zero_inflation = GeometricComponent("zero_inflation", 0.0)
    zero_inflation.fit(counts, logweights)
    martian.log_info(zero_inflation)

    fitted_weights = zero_inflation.pmf(counts) * weights.sum()
    residual_weights = weights - fitted_weights
    residual_weights[residual_weights < 0] = 0
    zero_mix_prob = 1 - residual_weights.sum() / weights.sum()

    background = NegativeBinomialComponent("background", 0.0, 0.0)
    if np.any(residual_weights > 0):
        background.fit(counts[residual_weights > 0], np.log(residual_weights[residual_weights > 0]))
    else:
        background.fit(counts, logweights)

    mix_prob = [zero_mix_prob, 1 - zero_mix_prob]
    model = MixtureModel([zero_inflation, background], mix_prob=mix_prob)
    model = converge_model(model, counts, logweights)

    # Finalize three-component mixture:
    # - converged dual component mixture provides starting point for background components
    # - find residual counts underestimated by the background components
    # - fit residuals to a negative binomial distribution
    # - EM converge to the full set of data
    zero_inflation = GeometricComponent(
        "zero_inflation", *model.get_component("zero_inflation").params
    )
    background = NegativeBinomialComponent("background", *model.get_component("background").params)
    counts, weights, logweights = origcounts, origweights, np.log(origweights)
    fitted_weights = get_model_pmf(model, counts) * weights.sum()
    residual_weights = weights - fitted_weights
    residual_weights[residual_weights < 0] = 0
    signal_prob = residual_weights.sum() / origweights.sum()

    signal = NegativeBinomialComponent("signal", 0.0, 0.0)
    signal.fit(origcounts[residual_weights > 0], np.log(residual_weights[residual_weights > 0]))

    mix_prob = []
    for p in model.mix_prob:
        mix_prob.append(p * (1 - signal_prob))
    mix_prob.append(signal_prob)

    model = MixtureModel([zero_inflation, background, signal], mix_prob=mix_prob)
    model = converge_model(model, counts, logweights)

    return model


def get_threshold_from_model(model, max_count, qval=0.05):
    """Takes a mixture model with background components and produces a qvalue-based
    threshold from the data.
    """
    # Special case where the model wasn't fit because no signal
    if model is None or max_count <= 1:
        return 1

    # Generate the PMF from just the background components
    # TODO: PMFs should be a method of MixtureModel
    xvals = np.arange(max_count)
    pmfs = model.get_component_contribution("zero_inflation", xvals)
    pmfs += model.get_component_contribution("background", xvals)
    pmfs /= model.mix_prob[:2].sum()

    total, x = 0, 1
    for x, pmf in zip(xvals, pmfs):
        total += pmf
        if total >= (1 - qval):
            break
    return x


def negative_binomial_fit_threshold(count_dict, qval=0.05):
    """Takes a count dictionary and does maximum likelihood fitting of a negative binomial,
    returning a tail threshold on the fitted distribution.
    """
    # Special-case where the count-dict is all zeros:
    if not count_dict or (0 in count_dict and count_dict[0] == np.sum(count_dict.values())):
        return 1

    counts, weights = (np.array(array) for array in zip(*count_dict.items()))

    mean = weighted_mean(counts, weights)
    dispersion = MLE_dispersion(counts, weights, errtol="absolute")
    if dispersion == 0:
        threshold = poisson.isf(qval, mean)
    else:
        n = 1 / dispersion
        p = 1 / (1 + mean * dispersion)
        threshold = nbinom.isf(qval, n, p)
    return int(threshold)


################################################################################


def split(args):
    if args.cut_sites is None:
        return {"chunks": [], "join": {}}

    # TODO: better estimation of these requirements
    chunk_mem_gb = 8
    join_mem_gb = 5
    if args.custom_peaks:
        return {"chunks": [], "join": {"__mem_gb": join_mem_gb}}

    global_qval = DEFAULT_GLOBAL_QVAL if args.qval is None else args.qval
    count_dict = load_count_dict(args.count_dict)
    if count_dict is not None:
        global_model = fit_mixture_model(count_dict)
        global_threshold = min(
            [
                get_threshold_from_model(global_model, max(count_dict.keys()), global_qval),
                negative_binomial_fit_threshold(count_dict, qval=global_qval),
            ]
        )
        global_threshold = max(global_threshold, 10)
        martian.log_info("Global threshold set to {}".format(global_threshold))
    else:
        martian.exit("Count dictionary is empty")

    ctg_mgr = ReferenceManager(args.reference_path)
    contig_chunks = ctg_mgr.make_chunks(30, contigs=ctg_mgr.primary_contigs())

    chunks = []
    for contigs in contig_chunks:
        chunks.append(
            {
                "contigs": contigs,
                "threshold": int(global_threshold),
                "__mem_gb": chunk_mem_gb,
            }
        )

    return {"chunks": chunks, "join": {"__mem_gb": join_mem_gb}}


def main(args, outs):
    """Call peaks on a per-chromosome basis"""

    if args.custom_peaks:
        raise RuntimeError("Peak caller is unexpectedly being run when custom_peaks is not null")

    if args.cut_sites is None:
        outs.peak_metrics = None
        outs.peaks = None
        return
    ref = ReferenceManager(args.reference_path)
    for contig in args.contigs:
        signal, _ = load_signal_track_from_bedgraph(args.cut_sites, ref, contig)
        martian.log_info("Global threshold set to {}".format(args.threshold))

        qval = DEFAULT_GLOBAL_QVAL if args.qval is None else args.qval

        # Generate candidate peaks based on the global threshold
        candidates = threshold_peaks_from_signal(signal, args.threshold, contig)
        peaks = set()

        martian.log_info("Beginning candidate peak processing")
        # Merge nearby candidates into a single candidate region
        for candidate in merge_peaks(candidates, mergedist=10000, smallsize=None, minsize=100):
            martian.log_info("Processing candidate region {}".format(candidate))
            for peak in wavelet_peak_caller(
                signal,
                candidate,
                contig,
                snr_threshold=DEFAULT_SIGNAL_NOISE_RATIO,
                global_threshold=args.threshold,
                qval=qval,
            ):
                padded_peak = extend_peak_length(peak, args.fragments, args.fragments_index)
                if padded_peak is not None:
                    martian.log_info(
                        "Raw peak: {}\nExtended peak: {}\n------".format(peak, padded_peak)
                    )
                    peaks.add(padded_peak)

        # Do a final merge and sort of output peaks
        final_peaks = merge_peaks(sorted(peaks), mergedist=100, smallsize=25, minsize=100)

        # Output peak file for this chromosome
        write_peaks(final_peaks, outs.peaks)


def join(args, outs, chunk_defs, chunk_outs):
    """Join peaks"""

    if args.cut_sites is None:
        outs.peak_metrics = None
        outs.peaks = None
        return

    if args.custom_peaks:
        ctg_mgr = ReferenceManager(args.reference_path)
        check_input_peaks_bed(args.custom_peaks, ctg_mgr)
        # Filter the input peaks file to only peaks on primary contigs
        primary_contigs = set(ctg_mgr.primary_contigs())
        num_lines = 0
        with open(outs.peaks, "w") as outfile, open(args.custom_peaks, "rb") as infile:
            for line in infile:
                line = line.decode()
                if line.startswith("#"):
                    continue
                contig = line.split("\t")[0]
                if contig not in primary_contigs:
                    continue
                outfile.write(line)
                num_lines += 1
        peak_metrics = {
            "peakcaller_method": "custom",
        }
    else:
        # merge peaks in contig order in chunks
        chunk_peaks = [chunk_out.peaks for chunk_out in chunk_outs if chunk_out.peaks is not None]
        combine_csv(chunk_peaks, outs.peaks, header_lines=0)
        pipeline_header = make_pipeline_header(args.sample_id, args.sample_desc, args.assay)
        sort_and_uniq_bed(outs.peaks, args.reference_path, pipeline_header)
        peak_metrics = {
            "peakcaller_method": "revised",
        }

    # count peaks, bases in peaks and fraction genome in peaks over the primary contigs
    peak_metrics.update(calculate_peak_metrics(args.reference_path, outs.peaks))

    martian.log_info(json.dumps(peak_metrics, indent=4))

    if peak_metrics["total_peaks_detected"] <= 15:
        martian.exit(
            "The peak caller identified only {} peak(s). This could be caused by excessive "
            "background transposition, or very low sequencing depth, or is a failure in the "
            "algorithm. Further execution will be halted. Metrics:\n{}".format(
                peak_metrics["total_peaks_detected"], json.dumps(peak_metrics, indent=4)
            )
        )
    if peak_metrics["frac_primary_genome_in_peaks"] > 0.90:
        martian.exit(
            "The peak caller called peak(s) spanning {:.2%} of the genome. This could "
            "be caused by excessive background transposition or is a failure in peak "
            "calling and further execution will be halted. Metrics:\n{}".format(
                peak_metrics["frac_primary_genome_in_peaks"], json.dumps(peak_metrics, indent=4)
            )
        )
    with open(outs.peak_metrics, "w") as f:
        json.dump(peak_metrics, f, indent=4)
