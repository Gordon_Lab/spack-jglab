# Copyright (c) 2020 10x Genomics, Inc. All rights reserved.
"""
Converts a signal track from bedgraph to bigwig format, to make it smaller and faster to access.
"""
from __future__ import absolute_import, division, print_function

import pyBigWig

from atac.tools.ref_manager import ReferenceManager

__MRO__ = """
stage CONVERT_SIGNAL_TRACK(
    in  path       reference_path,
    in  bedgraph   cut_sites,
    out bigwig     cut_sites,
    src py         "stages/processing/convert_signal_track",
) using (
    mem_gb = 4,
)
"""


def write_bigwig_header(filehandle, contig_manager):
    """Writes out the necessary header information (contigs & their lengths) to an empty
    filehandle with an open bigwig file.
    """
    contig_lens = contig_manager.get_contig_lengths()
    filehandle.addHeader(
        [(str(contig), int(contig_lens[contig])) for contig in contig_manager.primary_contigs()]
    )


def main(args, outs):
    ctg_mgr = ReferenceManager(args.reference_path)
    with open(args.cut_sites, "r") as bedgraph_in, pyBigWig.open(outs.cut_sites, "w") as bigwig_out:
        write_bigwig_header(bigwig_out, ctg_mgr)
        for line in bedgraph_in:
            contig, start, end, count = line.split()
            bigwig_out.addEntries([contig], [int(start)], [int(end)], [float(count)])
