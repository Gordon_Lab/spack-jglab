#!/usr/bin/env python
# Copyright (c) 2019 10x Genomics, Inc. All rights reserved.
"""
Chunks up the input fastq data into sets of matched R1, R2, SI, and BC fastq files,

Input args:
  sample_id:  Arbitrary string labeling the sample
  subsample_rate: null or a fraction of reads that are going to be preserved
  sample_def: A list of maps, each with information for each sample to be processed, including:
                bc_in_read - read in which to look for the barcode
                bc_length - length of barcode in read
                read_path - path to FASTQ files
                lanes -
                gem_group -
                library_id
                sample_indices - only for BCL_PROCESSOR, list of valid sample indices, if contains
                    `all`, take all
                sample_names - only for ILMN_BCL2FASTQ, list of sample_names to pass on
                subsample_rate
                fastq_mode - one of BCL_PROCESSOR or ILMN_BCL2FASTQ.  BCL_PROCESSOR uses
                    interleaved reads (RA) and labels fastqs
                    by sample index.  ILMN_BCL2FASTQ uses R1/R2 and labels by sample name.

Note: fastq_mode specifies how FASTQs were generated. There are two modes:

1. "BCL_PROCESSOR"

FASTQs produced by the 10X BCL_PROCESSOR pipeline. This mode assumes the FASTQ files obey the internal
naming conventions and the reads have been interleaved into RA FASTQ files.

2. "ILMN_BCL2FASTQ"

FASTQs produced directly by Illumina BCL2FASTQ v1.8.4. For this mode, BCL2FASTQ must be configured to emit the
index2 read, rather than using it for dual-index demultiplexing:

configureBclToFastq.pl --no-eamss --use-bases-mask=Y100,I8,Y14,Y100 --input-dir=<basecalls_dir> \
    --output-dir=<output_dir> --sample-sheet=<sample_sheet.csv>

The sample sheet must be formatted as per the BCL2FASTQ documentation (10 column csv), and must contain a row for
each sample index used. The sequencer must have been run in dual index mode, with the second index read (used to
read the 10X barcode) emitted as the R2 output file. The --use-bases-mask argument should be set to the read
length used.
"""

from __future__ import absolute_import, division

import gzip
import os.path
import re
from six import ensure_binary, ensure_str

import martian
import tenkit.fasta as tk_fasta
import tenkit.preflight as tk_preflight
import tenkit.safe_json
import atac.preflights as atac_preflight
from atac.barcodes import barcode_whitelist_path
import atac.fastq

__MRO__ = """
struct FastqDef(
    path   read1,
    path   read2,
    bool   reads_interleaved,
    path   barcode,
    path   sample_index,
    int    gem_group,
    float  subsample_rate,
    string read_group,
    map    chemistry,
    string fastq_mode,
)

stage ATAC_SETUP_CHUNKS(
    in  string     sample_id        "id of the sample",
    in  map[]      sample_def       "list of dictionary specifying input data",
    in  float      subsample_rate,
    in  string     barcode_whitelist "List of valid barcodes",
    out FastqDef[] chunks,
    out path       barcode_whitelist_path "Path to valid barcodes",
    src py         "stages/processing/setup_chunks",
)
"""

GZIP_SUFFIX = b"gz"


def main(args, outs):
    """Combine reads from multiple input FASTQ files, and potentially trim.
    Demultiplex outputs a series of FASTQ files with filenames of the form:
    read-[RA|I1|I2]_si-AGTAACGT_lane-001_chunk_001.fastq[.gz].
    """

    validate_input(args)

    global_subsample_rate = args.subsample_rate if args.subsample_rate else 1.0

    chunks = []

    for read_chunk in args.sample_def:
        subsample_rate = global_subsample_rate * (read_chunk.get("subsample_rate") or 1.0)

        bc_in_read = {}
        if read_chunk.get("bc_in_read", None) is not None:
            bc_in_read["bc_in_read"] = read_chunk["bc_in_read"]
            bc_in_read["bc_length"] = read_chunk["bc_length"]

        path = read_chunk["read_path"]
        lanes = read_chunk["lanes"]
        gem_group = read_chunk["gem_group"]
        sample_id = args.sample_id
        library_id = read_chunk.get("library_id", "MissingLibrary")

        # split on BCL_PROCESSOR / ILMN_BCL2FASTQ
        # the main difference is that BCL_PROCESSOR uses interleaved reads and labels FASTQs by sample index;
        # whereas ILMN_BCL2FASTQ uses R1/R2 and labels by sample name

        # cell barcodes and sample indices are embedded in the index reads
        si_read, bc_read = ("I1", "I2")

        if read_chunk["fastq_mode"] == "BCL_PROCESSOR":
            sample_index_strings, msg = tk_preflight.check_sample_indices(read_chunk)
            if sample_index_strings is None:
                martian.exit(msg)

            find_func = tk_fasta.find_input_fastq_files_10x_preprocess

            # Check that data for at least all but one of the sample indices is present.
            present_sample_indices = []
            for sample_index in sample_index_strings:
                barcodes = find_func(path, si_read, sample_index, lanes)
                if len(barcodes) != 0:
                    present_sample_indices.append(sample_index)
            if len(present_sample_indices) < (len(sample_index_strings) - 1):
                martian.exit(
                    "I5 read files are missing for read path {} on more than one sample index: {}, lanes {}".format(
                        path,
                        [idx for idx in sample_index_strings if idx not in present_sample_indices],
                        lanes,
                    )
                )
            if len(present_sample_indices) <= 0:
                martian.exit("No I5 read files were found for any sample index.")

            for sample_index in present_sample_indices:
                read_paths = find_func(path, "RA", sample_index, lanes)

                # allow empty sample index case if all reads in lane are same sample
                sis = find_func(path, si_read, sample_index, lanes)
                if sis is None or len(sis) == 0:
                    sis = [None] * len(read_paths)

                barcodes = find_func(path, bc_read, sample_index, lanes)

                # calculate chunks
                for r, b, si in zip(read_paths, barcodes, sis):
                    (flowcell, lane) = get_run_data(r)
                    if sample_id is not None:
                        rg_string = ":".join(
                            str(item) for item in [sample_id, library_id, gem_group, flowcell, lane]
                        )
                    else:
                        rg_string = "None:None:None:None:None"
                    new_chunk = {
                        "read1": r,
                        "read2": None,
                        "reads_interleaved": True,
                        "barcode": b,
                        "sample_index": si,
                        "gem_group": gem_group,
                        "subsample_rate": subsample_rate,
                        "read_group": rg_string,
                        "chemistry": None,
                        "fastq_mode": "BCL_PROCESSOR",
                    }
                    new_chunk.update(bc_in_read)
                    chunks.append(new_chunk)

        elif read_chunk["fastq_mode"] == "ILMN_BCL2FASTQ":
            sample_names = read_chunk["sample_names"]
            find_func = tk_fasta.find_input_fastq_files_bcl2fastq_demult

            for sample_name in sample_names:
                fq = atac.fastq.find_bcl2fastq_files(path, sample_name, lanes)

                # calculate chunks
                for i, r1 in enumerate(fq.read1):
                    (flowcell, lane) = get_run_data(r1)
                    if sample_id is not None:
                        rg_string = ":".join(
                            str(item) for item in [sample_id, library_id, gem_group, flowcell, lane]
                        )
                    else:
                        rg_string = "None:None:None:None:None"
                    new_chunk = {
                        "read1": r1,
                        "read2": fq.read2[i],
                        "reads_interleaved": False,
                        "barcode": fq.i2[i] if fq.i2 else None,
                        "sample_index": fq.i1[i] if fq.i1 else None,
                        "gem_group": gem_group,
                        "subsample_rate": subsample_rate,
                        "read_group": rg_string,
                        "chemistry": None,
                        "fastq_mode": "ILMN_BCL2FASTQ",
                    }
                    new_chunk.update(bc_in_read)
                    chunks.append(new_chunk)

    if len(chunks) == 0:
        martian.exit("No input FASTQs were found for the requested parameters.")

    martian.log_info("Input reads: %s" % tenkit.safe_json.safe_jsonify(chunks, pretty=True))
    outs.chunks = chunks

    outs.barcode_whitelist_path = barcode_whitelist_path(args.barcode_whitelist)

    check_fastqs(outs.chunks)


def validate_input(args):
    """Does various parsing and checking of input arguments before we enter the main flow path"""
    # Sample ID / pipestance name
    atac_preflight.check_sample_id(args.sample_id)

    # downsample
    atac_preflight.check_subsample_rate(args.subsample_rate)

    # FASTQ input (sample_def)
    atac_preflight.check_fastq_input(args.sample_def)


def check_fastqs(chunks):
    keys = ("read1", "read2", "barcode", "sample_index")

    def check_fastq(fastq):
        assert isinstance(fastq, bytes)
        # Check if fastq is readable
        if not os.access(fastq, os.R_OK):
            martian.exit("Do not have file read permission for FASTQ file: %s" % fastq)

        # Check if fastq is gzipped
        is_gzip_fastq = True
        try:
            with gzip.open(fastq) as f:
                f.read(1)
        except:
            is_gzip_fastq = False

        if is_gzip_fastq and not fastq.endswith(GZIP_SUFFIX):
            martian.exit(
                "Input FASTQ file is gzipped but filename does not have .gz suffix: %s"
                % (ensure_str(fastq))
            )
        if not is_gzip_fastq and fastq.endswith(GZIP_SUFFIX):
            martian.exit(
                "Input FASTQ file is not gzipped but filename has .gz suffix: %s"
                % (ensure_str(fastq))
            )

    for chunk in chunks:
        for key in keys:
            fastq = chunk.get(key)
            if fastq is not None:
                check_fastq(ensure_binary(fastq))


def get_run_data(fn):
    """Parse flowcell + lane from the first FASTQ record.
    NOTE: we don't check whether there are multiple FC / lanes in this file.
    """
    fn = ensure_binary(fn)
    if fn[-2:] == GZIP_SUFFIX:
        reader = gzip.open(fn)
    else:
        reader = open(fn, "rb")

    gen = tk_fasta.read_generator_fastq(reader, paired_end=False)

    try:
        name = next(gen)[0]
        try:
            flowcell, lane = re.split(":", ensure_str(name))[2:4]
        except ValueError:
            flowcell, lane = None, None
        return flowcell, lane
    except StopIteration:
        martian.exit("FASTQ is empty: {}".format(ensure_str(fn)))
    except tk_fasta.FastqParseError as error:
        martian.exit("{}".format(error))
