# Copyright (c) 2019 10x Genomics, Inc. All rights reserved.
"""
Reduce the dimensions of the peak matrix
"""

from __future__ import absolute_import

import os
import shutil

import martian

import cellranger.analysis.constants as analysis_constants
import cellranger.analysis.lsa as cr_lsa
import cellranger.analysis.pca as cr_pca
import cellranger.analysis.plsa as cr_plsa
import cellranger.h5_constants as h5_constants
import cellranger.matrix as cr_matrix

# It is not clear why pylint is unhappy with the atac.constants import.
# See comments on PR: https://github.com/10XDev/cellranger-atac/pull/676
# The import succeeds and this is a false positive.
# pylint: disable=no-name-in-module,import-error
from atac.constants import ALLOWED_FACTORIZATIONS
from cellranger.hacks import get_thread_request_from_mem_gb

__MRO__ = """
stage REDUCE_DIMENSIONS(
    in  string[] factorization,
    in  int      num_dims,
    in  h5       filtered_matrix,
    in  int      num_bcs,
    in  int      num_features,
    in  int      random_seed,
    out path     reduced_data,
    src py       "stages/analysis/reduce_dimensions",
)
"""


def split(args):
    if args.filtered_matrix is None:
        return {"chunks": [{"__mem_gb": h5_constants.MIN_MEM_GB}]}

    if not os.path.exists(args.filtered_matrix):
        raise IOError("processed peak matrix not found at {}".format(args.filtered_matrix))

    if not set(args.factorization).issubset(ALLOWED_FACTORIZATIONS):
        raise ValueError("Invalid factorization provided")
    if args.num_dims is None:
        raise ValueError("Must specify num_dims parameter (default: 15)")

    # memory usage of just loading the full matrix
    matrix_dims = cr_matrix.CountMatrix.load_dims_from_h5(args.filtered_matrix)
    (feature_dim, bcs_dim, nonzero_entries) = matrix_dims
    matrix_mem_gb = cr_matrix.CountMatrix.get_mem_gb_from_matrix_dim(bcs_dim, nonzero_entries)
    chunks = []
    for method in args.factorization:
        # mem alloc
        if method == "pca" or method == "lsa":
            method_mem_gb = cr_pca.get_irlb_mem_gb_from_matrix_dim(
                nonzero_entries, feature_dim, bcs_dim, args.num_dims
            )
        elif method == "plsa":
            method_mem_gb = cr_plsa.get_plsa_mem_gb_from_matrix_dim(nonzero_entries)
        else:
            continue
        mem_gb = max(method_mem_gb + matrix_mem_gb, h5_constants.MIN_MEM_GB)

        # thread alloc
        if method == "pca" or method == "lsa":
            # HACK - give big jobs more threads in order to avoid overloading a node
            threads = get_thread_request_from_mem_gb(mem_gb)
        elif method == "plsa":
            threads = -4
        else:
            threads = 1

        chunks += [
            {
                "method": method,
                "__mem_gb": mem_gb,
                "__vmem_gb": mem_gb + 8,
                "__threads": threads,
            }
        ]
    return {"chunks": chunks}


def main(args, outs):
    if args.filtered_matrix is None:
        return

    if not os.path.exists(outs.reduced_data):
        os.makedirs(outs.reduced_data, exist_ok=True)

    method_dir = os.path.join(outs.reduced_data, args.method)
    os.makedirs(method_dir, exist_ok=True)
    _h5 = os.path.join(method_dir, args.method + ".h5")
    _csv = os.path.join(method_dir, args.method + "_csv")

    matrix = cr_matrix.CountMatrix.load_h5_file(args.filtered_matrix)
    if args.method == "pca":
        pca = cr_pca.run_pca(
            matrix,
            n_pca_components=args.num_dims,
            pca_bcs=args.num_bcs,
            pca_features=args.num_features,
            random_state=args.random_seed,
        )
        pca_key = (
            args.num_dims
            if args.num_dims is not None
            else analysis_constants.PCA_N_COMPONENTS_DEFAULT
        )
        save_pca_data(pca_key, pca, _h5, _csv, matrix)
    elif args.method == "lsa":
        lsa = cr_lsa.run_lsa(
            matrix,
            n_lsa_components=args.num_dims,
            lsa_bcs=args.num_bcs,
            lsa_features=args.num_features,
            random_state=args.random_seed,
        )
        if any(lsa.transformed_lsa_matrix.sum(axis=1) == 0):
            martian.exit(
                "Parameters supplied for LSA dimensionality reduction did "
                "not generate a valid output. The resulting LSA dimensionality "
                "reduction has one or more barcodes located at the origin. "
                "This is likely caused by the use of 'num_dr_bcs' parameter "
                "which randomly selects a subset of barcodes prior to running IRLBA. "
                "Please re-try with new parameters."
            )
        lsa_key = (
            args.num_dims
            if args.num_dims is not None
            else analysis_constants.LSA_N_COMPONENTS_DEFAULT
        )
        save_lsa_data(lsa_key, lsa, _h5, _csv, matrix)
    elif args.method == "plsa":
        # FIXME: temp directory from martian?
        plsa = cr_plsa.run_plsa(
            matrix,
            outs.reduced_data,
            n_plsa_components=args.num_dims,
            threads=martian.get_threads_allocation(),
            plsa_bcs=args.num_bcs,
            plsa_features=args.num_features,
            random_state=args.random_seed,
        )
        plsa_key = (
            args.num_dims
            if args.num_dims is not None
            else analysis_constants.PLSA_N_COMPONENTS_DEFAULT
        )
        save_plsa_data(plsa_key, plsa, _h5, _csv, matrix)


def join(args, outs, chunk_defs, chunk_outs):
    if args.filtered_matrix is None:
        outs.reduced_data = None
        outs.reduction_summary = {}
        return

    if not os.path.exists(outs.reduced_data):
        os.makedirs(outs.reduced_data, exist_ok=True)

    # copy chunk outs
    for chunk_out in chunk_outs:
        shutil.copytree(chunk_out.reduced_data, outs.reduced_data, dirs_exist_ok=True)


def save_pca_data(pca_key, pca, h5_filename, csv_filename, matrix):
    """Save PCA results in h5 and CSV formats"""
    pca_map = {pca_key: pca}
    cr_pca.save_pca_h5(pca_map, h5_filename)
    cr_pca.save_pca_csv(pca_map, matrix, csv_filename)


def save_lsa_data(lsa_key, lsa, h5_filename, csv_filename, matrix):
    """Save LSA results in h5 and CSV formats"""
    lsa_map = {lsa_key: lsa}
    cr_lsa.save_lsa_h5(lsa_map, h5_filename)
    cr_lsa.save_lsa_csv(lsa_map, matrix, csv_filename)


def save_plsa_data(plsa_key, plsa, h5_filename, csv_filename, matrix):
    """Save PLSA results in h5 and CSV formats"""
    plsa_map = {plsa_key: plsa}
    cr_plsa.save_plsa_h5(plsa_map, h5_filename)
    cr_plsa.save_plsa_csv(plsa_map, matrix, csv_filename)
