#!/usr/bin/env python
#
# Copyright (c) 2021 10x Genomics, Inc. All rights reserved.
#
"""
Run LSA for batch correction and output a batch-corrected and L2-normalized transformed matrix
"""
from __future__ import absolute_import

import martian
import numpy as np
from six.moves import cPickle
import cellranger.analysis.pca as cr_pca
import cellranger.analysis.run_fbpca as cr_fbpca
import cellranger.analysis.lsa as cr_lsa
import cellranger.matrix as cr_matrix
import cellranger.h5_constants as h5_constants
from cellranger.library_constants import ATACSEQ_LIBRARY_TYPE
import cellranger.analysis.constants as analysis_constants
from atac.constants import ALLOWED_FACTORIZATIONS

__MRO__ = """
stage REDUCE_DIMENSIONS_FOR_CBC(
    in  h5       matrix_h5,
    in  string[] factorization,
    in  int      num_dims,
    in  int      num_bcs,
    in  int      num_features,
    in  int      random_seed,
    out pickle   dimred_matrix,
    out pickle   matrix_barcode_feature_info,
    src py       "stages/analysis/reduce_dimensions_for_cbc",
) split (
)
"""


def split(args):
    # memory usage to load a h5 matrix
    if not set(args.factorization).issubset(ALLOWED_FACTORIZATIONS):
        raise ValueError("Invalid factorization provided")

    matrix_dims = cr_matrix.CountMatrix.load_dims_from_h5(args.matrix_h5)
    feature_dim, bcs_dim, nonzero_entries = matrix_dims
    matrix_mem_gb = cr_matrix.CountMatrix.get_mem_gb_from_matrix_dim(bcs_dim, nonzero_entries)
    n_dims = analysis_constants.CBC_N_COMPONENTS_DEFAULT if args.num_dims is None else args.num_dims
    lsa_mem_gb = cr_pca.get_irlb_mem_gb_from_matrix_dim(
        nonzero_entries, feature_dim, bcs_dim, n_dims
    )
    mem_gb = max(lsa_mem_gb + matrix_mem_gb, h5_constants.MIN_MEM_GB)
    return {"chunks": [], "join": {"__mem_gb": mem_gb}}


def join(args, outs, chunk_defs, chunk_outs):
    matrix = cr_matrix.CountMatrix.load_h5_file(args.matrix_h5)
    matrix = matrix.select_features_by_type(ATACSEQ_LIBRARY_TYPE)
    n_dims = analysis_constants.CBC_N_COMPONENTS_DEFAULT if args.num_dims is None else args.num_dims

    outs.dimred_matrix = martian.make_path("dimred_matrix.pickle")

    # only consider the first method in the list
    if args.factorization[0] in {
        "lsa",
        "plsa",
        "bclsa",
    }:  # run lsa for batch correction even when "plsa" is specified
        lsa = cr_lsa.run_lsa(
            matrix,
            n_lsa_components=n_dims,
            lsa_bcs=args.num_bcs,
            lsa_features=args.num_features,
            random_state=args.random_seed,
        )
        if any(lsa.transformed_lsa_matrix.sum(axis=1) == 0):
            martian.exit(
                "Parameters supplied for LSA dimensionality reduction did "
                "not generate a valid output. The resulting LSA dimensionality "
                "reduction has one or more barcodes located at the origin. "
                "This is likely caused by the use of 'num_dr_bcs' parameter "
                "which randomly selects a subset of barcodes prior to running IRLBA. "
                "Please re-try with new parameters."
            )
        assert lsa.transformed_lsa_matrix.shape[0] == matrix.get_shape()[1]  # number of barcodes
        lsa = lsa._replace(transformed_lsa_matrix=lsa.transformed_lsa_matrix + 1e-120)
        dr_mat = lsa.transformed_lsa_matrix / np.linalg.norm(
            lsa.transformed_lsa_matrix, axis=1, keepdims=True
        )
        bc_feature_info = {
            "barcodes": matrix.bcs,
            "features": matrix.feature_ref.feature_defs,
        }

    else:  # "pca" or "bcpca"
        dr_mat, bc_feature_info = cr_fbpca.run_fbpca(matrix, n_dims)

    with open(outs.dimred_matrix, "wb") as f:
        cPickle.dump(dr_mat, f, cPickle.HIGHEST_PROTOCOL)
    outs.matrix_barcode_feature_info = martian.make_path("matrix_barcode_feature_info.pickle")
    with open(outs.matrix_barcode_feature_info, "wb") as f:
        cPickle.dump(bc_feature_info, f, cPickle.HIGHEST_PROTOCOL)
