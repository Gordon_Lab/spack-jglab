# Copyright (c) 2020 10x Genomics, Inc. All rights reserved.
"""
compute gc distribution in peaks
"""

from __future__ import absolute_import, division
from atac.tools.ref_manager import ReferenceManager
from atac.gc_bias import get_gcbinned_peaks_and_bg

import pickle
import numpy as np


__MRO__ = """
stage COMPUTE_GC_DISTRIBUTION(
    in  bed    peaks,
    in  string reference_path,
    out pickle GCdict,
    src py     "stages/analysis/compute_gc_dist",
) split (
) using (
    volatile = strict,
)
"""


def split(args):
    ref_mgr = ReferenceManager(args.reference_path)
    return {
        "chunks": [],
        "join": {"__mem_gb": 4, "__vmem_gb": int(np.ceil(ref_mgr.get_vmem_est())) + 3},
    }


def join(args, outs, chunk_defs, chunk_outs):
    """Compute base background in each peak."""
    ref_mgr = ReferenceManager(args.reference_path)

    if len(ref_mgr.list_species()) > 1 or ref_mgr.motifs is None:
        return

    gc_dict = get_gcbinned_peaks_and_bg(args.peaks, args.reference_path)

    if gc_dict:
        # dump
        with open(outs.GCdict, "wb") as f:
            pickle.dump(gc_dict, f)
