#!/usr/bin/env python
#
# Copyright (c) 2021 10X Genomics, Inc. All rights reserved.
#
"""
Check if a "batch" key present in the aggr CSV file
"""
from __future__ import absolute_import

import json
import martian
from cellranger.constants import AGG_ID_FIELD, AGG_BATCH_FIELD
from cellranger.library_constants import ATACSEQ_LIBRARY_TYPE

__MRO__ = """
stage CHECK_BATCH_INFO(
    in  json  library_info,
    out map[] library_info_list,
    out bool  chemistry_batch_correction,
    out bool  no_batch_correction,
    src py    "stages/analysis/check_batch_info",
)
"""


def main(args, outs):
    if args.library_info is None:  # is a count pipeline
        outs.library_info_list = None
        outs.chemistry_batch_correction = False
        outs.no_batch_correction = True
        return

    with open(args.library_info, "r") as f:
        lib_dict = json.load(f)
    lib_list = []
    batch_name_to_id = {}
    chemistry_batch_correction = False
    # ATAC-seq does not allow >1 gem groups in one library
    for gem_group, info in lib_dict.items():
        gem_group = int(gem_group)
        aggr_id = info[AGG_ID_FIELD]
        if AGG_BATCH_FIELD in info:
            chemistry_batch_correction = True
            batch_name = info[AGG_BATCH_FIELD]
        else:
            batch_name = aggr_id

        if batch_name not in batch_name_to_id:
            batch_name_to_id[batch_name] = len(batch_name_to_id)
        lib_list.append(
            {
                "library_id": gem_group - 1,
                "libary_type": ATACSEQ_LIBRARY_TYPE,
                "gem_group": gem_group,
                "aggr_id": aggr_id,
                "batch_name": batch_name,
                "batch_id": batch_name_to_id[batch_name],
            }
        )
    if chemistry_batch_correction is True and len(batch_name_to_id) <= 1:
        chemistry_batch_correction = False
        martian.log_info(
            "Warning: only one batch sepecified in the input csv, chemistry_batch_correction is disabled."
        )

    outs.chemistry_batch_correction = chemistry_batch_correction
    outs.no_batch_correction = not chemistry_batch_correction
    outs.library_info_list = lib_list
