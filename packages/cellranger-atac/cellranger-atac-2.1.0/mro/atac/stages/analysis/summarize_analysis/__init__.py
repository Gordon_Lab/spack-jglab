# Copyright (c) 2019 10x Genomics, Inc. All rights reserved.

from __future__ import absolute_import, division, print_function
import os
import shutil

import h5py as h5
import martian
from scipy.sparse import vstack
import numpy as np
from six import ensure_binary

import cellranger.h5_constants as h5_constants
import cellranger.hdf5 as cr_h5
import cellranger.matrix as cr_matrix
from cellranger.feature_ref import FeatureReference
from atac.constants import DEFAULT_FACTORIZATION
from atac.analysis.annotation import read_peak_annotation

__MRO__ = """
stage ATAC_SUMMARIZE_ANALYSIS(
    in  tsv        peak_annotation,
    in  h5         filtered_peak_bc_matrix,
    in  h5         filtered_tf_bc_matrix,
    in  gz         tf_propZ_matrix,
    in  path       reduced_data,
    in  path       clustering,
    in  path       projection,
    in  path       enrichment_analysis,
    in  string[]   factorization,
    in  float      batch_score_before_correction,
    in  float      batch_score_after_correction,
    out h5         analysis,
    out path       analysis_csv,
    out h5         feature_bc_matrix,
    out path       analysis_h5_path,
    out map<float> batch_scores,
    src py         "atac/stages/analysis/summarize_analysis",
) split (
) using (
    volatile = strict,
)
"""


def split(args):
    # if no cells called
    if args.filtered_peak_bc_matrix is None:
        return {"chunks": [], "join": {}}

    # peak-bc matrix
    peak_matrix_mem_gb = cr_matrix.CountMatrix.get_mem_gb_from_matrix_h5(
        args.filtered_peak_bc_matrix
    )

    # tf-bc matrix, propZ matrix both are dense
    # As former is stored sparse and latter as dense, the multiplying factor is (3 + 1) = 4
    tf_matrices_mem_gb = 0
    if args.filtered_tf_bc_matrix is not None:
        ntfs, nbcs, _ = cr_matrix.CountMatrix.load_dims_from_h5(args.filtered_tf_bc_matrix)
        BYTES_PER_FLOAT = np.dtype(float).itemsize
        BYTES_PER_GB = 1024 ** 3
        tf_matrices_mem_gb = 4 * BYTES_PER_FLOAT * ntfs * nbcs / BYTES_PER_GB

    return {
        "chunks": [],
        "join": {
            "__mem_gb": max(
                int(np.ceil(peak_matrix_mem_gb + tf_matrices_mem_gb)), h5_constants.MIN_MEM_GB
            )
        },
    }


def main(args, outs):
    martian.throw("No chunks defined.")


def join(args, outs, chunk_defs, chunk_outs):
    if args.filtered_peak_bc_matrix is None:
        outs.analysis = None
        outs.analysis_csv = None
        outs.feature_bc_matrix = None
        return

    # Make the FBM
    # build joint Peak + TF count matrix for single genomes
    # combine peak annotations for single genome analysis
    peak_annotation = None
    if args.peak_annotation:
        peak_annotation = read_peak_annotation(args.peak_annotation)
    fbm = cr_matrix.CountMatrix.load_h5_file(args.filtered_peak_bc_matrix)
    fbm.feature_ref = FeatureReference.addtags(
        fbm.feature_ref, ["promoter", "nearby_gene"], peak_annotation
    )
    mapping = None
    if args.filtered_tf_bc_matrix:
        # combine matrices, ensure the barcodes are same and ordered the same way
        tf_matrix = cr_matrix.CountMatrix.load_h5_file(args.filtered_tf_bc_matrix)
        assert (fbm.bcs == tf_matrix.bcs).all()
        if peak_annotation is not None:
            tf_matrix.feature_ref = FeatureReference.addtags(
                tf_matrix.feature_ref, ["promoter", "nearby_gene"]
            )
        common_tags = ["genome", "promoter", "nearby_gene"]
        fbm.feature_ref.all_tag_keys = common_tags
        tf_matrix.feature_ref.all_tag_keys = common_tags
        combined_feature_defs = FeatureReference.join(fbm.feature_ref, tf_matrix.feature_ref)
        combined_matrix = vstack([fbm.m, tf_matrix.m])
        # explicit map linking rows in diffexp to combined matrix
        mapping = np.zeros((tf_matrix.features_dim, 2))
        for x in range(tf_matrix.features_dim):
            mapping[x, 0] = x
            mapping[x, 1] = x + fbm.features_dim
        fbm = cr_matrix.CountMatrix(combined_feature_defs, fbm.bcs, combined_matrix)
    fbm.save_h5_file(outs.feature_bc_matrix, sw_version=martian.get_pipelines_version())

    # Pytables doesn't support variable len strings, so use h5py first
    with h5.File(ensure_binary(outs.feature_bc_matrix), "r") as matrix, h5.File(
        outs.analysis, "w"
    ) as out:
        matrix.copy(matrix[cr_matrix.MATRIX], out, name=cr_matrix.MATRIX)

    use_method = (
        DEFAULT_FACTORIZATION
        if DEFAULT_FACTORIZATION in args.factorization
        else args.factorization[0]
    )

    # if the reduced data is batch-corrected, use the basic method string as dirname
    basic_methods = {"bclsa": "lsa", "bcpca": "pca"}

    h5s_to_add = []
    if args.reduced_data is not None:
        output_key = basic_methods.get(use_method, use_method)
        copy_outputs(
            os.path.join(args.reduced_data, use_method, "{}.h5".format(use_method)),
            os.path.join(args.reduced_data, use_method, "{}_csv".format(use_method)),
            outs.analysis_csv,
            output_key,
            h5s_to_add,
        )

    if args.projection is not None:
        for proj in ["tsne", "umap"]:
            copy_outputs(
                os.path.join(args.projection, "{}_{}.h5".format(use_method, proj)),
                os.path.join(args.projection, "{}_{}_csv".format(use_method, proj)),
                outs.analysis_csv,
                proj,
                h5s_to_add,
            )

    if args.clustering is not None:
        copy_outputs(
            os.path.join(args.clustering, use_method, "{}_clustering.h5".format(use_method)),
            os.path.join(args.clustering, use_method, "{}_csv".format(use_method)),
            outs.analysis_csv,
            "clustering",
            h5s_to_add,
        )

    if args.enrichment_analysis is not None:
        copy_outputs(
            os.path.join(
                args.enrichment_analysis, use_method, "{}_enrichment_h5.h5".format(use_method)
            ),
            os.path.join(
                args.enrichment_analysis, use_method, "{}_enrichment_csv".format(use_method)
            ),
            outs.analysis_csv,
            "enrichment",
            h5s_to_add,
        )

    cr_h5.combine_h5s_into_one(outs.analysis, h5s_to_add)

    # if mapping is present (single genome case), so is the coloring matrix
    if mapping is not None:
        with h5.File(ensure_binary(outs.analysis), "a") as out:
            out.create_dataset("feature_DE_map", data=mapping)
        args.coerce_strings()
        tf_propZ_matrix = np.loadtxt(args.tf_propZ_matrix)
        with h5.File(ensure_binary(outs.analysis), "a") as out:
            out.create_dataset("diffexp_coloring_matrix", data=tf_propZ_matrix)

    os.makedirs(outs.analysis_h5_path)
    shutil.copy(outs.analysis, outs.analysis_h5_path)

    outs.batch_scores = None
    if args.batch_score_before_correction is not None:
        outs.batch_scores = {
            "batch_effect_score_before_correction": args.batch_score_before_correction,
            "batch_effect_score_after_correction": args.batch_score_after_correction,
        }


def copy_outputs(input_h5, input_csv, output_csv, output_key, out_h5s):
    if input_h5 and os.path.exists(input_h5):
        print(output_key, input_h5)
        out_h5s.append(input_h5)
    if input_csv and os.path.exists(input_csv):
        print(output_key, input_csv)
        shutil.copytree(input_csv, os.path.join(output_csv, output_key), dirs_exist_ok=True)
