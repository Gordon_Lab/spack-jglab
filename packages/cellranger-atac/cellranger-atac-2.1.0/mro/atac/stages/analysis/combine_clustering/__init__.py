#
# Copyright (c) 2019 10x Genomics, Inc. All rights reserved.
#

from __future__ import absolute_import

import os
import shutil
import cellranger.analysis.io as analysis_io
import cellranger.analysis.constants as analysis_constants

from atac.constants import ALLOWED_FACTORIZATIONS, CLUSTER_FILE_HEAD

__MRO__ = """
stage ATAC_COMBINE_CLUSTERING(
    in  h5       filtered_matrix,
    in  path     clustered_data,
    in  path     knn_clusters,
    in  string[] factorization,
    out path     clustering,
    out h5       clustering_h5,
    src py       "stages/analysis/combine_clustering",
) using (
    volatile = strict,
)
"""


def main(args, outs):
    outs.clustering_h5 = None
    if args.filtered_matrix is None:
        outs.clustering = None
        return

    if not os.path.exists(outs.clustering):
        os.makedirs(outs.clustering, exist_ok=True)

    for method in args.factorization:
        if method not in ALLOWED_FACTORIZATIONS:
            raise ValueError("invalid method")
        merge_h5 = [
            os.path.join(args.clustered_data, method, "{}.h5".format(CLUSTER_FILE_HEAD[method])),
            os.path.join(args.knn_clusters, method, "clusters.h5"),
        ]
        groups = [
            analysis_constants.ANALYSIS_H5_KMEANS_GROUP,
            analysis_constants.ANALYSIS_H5_CLUSTERING_GROUP,
        ]

        out_method_dir = os.path.join(outs.clustering, method)
        os.makedirs(out_method_dir, exist_ok=True)

        out_clustering_h5 = os.path.join(out_method_dir, "{}_clustering.h5".format(method))
        analysis_io.combine_h5_files(merge_h5, out_clustering_h5, groups)
        if len(args.factorization) == 1:
            outs.clustering_h5 = out_clustering_h5

        _csv1 = os.path.join(args.clustered_data, method, CLUSTER_FILE_HEAD[method] + "_csv")
        _csv2 = os.path.join(args.knn_clusters, method, "clusters_csv")
        out_csv = os.path.join(out_method_dir, method + "_csv")
        shutil.copytree(_csv1, out_csv, dirs_exist_ok=True)
        shutil.copytree(_csv2, out_csv, dirs_exist_ok=True)
