# Copyright (c) 2019 10x Genomics, Inc. All rights reserved.
"""
Annotate peaks with near by genes, TSS/distal position and motif positions.
"""

from __future__ import absolute_import, division

from six import ensure_binary

from pybedtools import BedTool
import martian
from atac.tools.ref_manager import ReferenceManager
import atac.utils as utils
from atac.analysis.annotation import annotate_peaks

__MRO__ = """
stage ANNOTATE_PEAKS(
    in  bed    peaks,
    in  string reference_path,
    out tsv    peak_annotation,
    src py     "stages/analysis/annotate_peaks",
) split (
    in  bool   skip,
    in  int    chunk_start,
    in  int    chunk_end,
    in  bed    filtered_peaks,
) using (
    mem_gb   = 5,
    volatile = strict,
)
"""


def split(args):
    """Compute base background in split and use it in each chunk"""
    ref_mgr = ReferenceManager(args.reference_path)
    # Pre-filter the peaks file to only peaks on primary contigs (e.g., those
    # with annotated genes)
    primary_contigs = set(ref_mgr.primary_contigs())
    filtered_peaks = martian.make_path("filtered_peaks.bed")
    num_lines = 0
    with open(filtered_peaks, "w") as outfile, open(args.peaks, "rb") as infile:
        for line in infile:
            line = line.decode()
            if line.startswith("#"):
                continue
            contig = line.split("\t")[0]
            if contig not in primary_contigs:
                continue
            outfile.write(line)
            num_lines += 1

    if len(ref_mgr.list_species()) > 1 or num_lines == 0 or ref_mgr.tss_track is None:
        chunk_def = [{"skip": True, "chunk_start": None, "chunk_end": None}]
        return {"chunks": chunk_def}

    chunk_def = [
        {
            "__mem_gb": 4,
            "skip": False,
            "chunk_start": chunk[0],
            "chunk_end": chunk[1],
            "filtered_peaks": filtered_peaks,
        }
        for chunk in utils.get_chunks(num_lines, chunk_count=20)
    ]
    return {"chunks": chunk_def}


def build_gene_id_name_map(ref_mgr):
    """Builds a lookup table that maps the gene ID to the gene name, if it's available.
    Otherwise fall back on the gene ID.  Makes annotation files more understandable.
    """
    gtf_iter = BedTool(ref_mgr.genes)
    id_name_map = {}
    for interval in gtf_iter:
        if interval[2] != "gene":
            continue
        try:
            interval.attrs
        except ValueError as err:
            raise ValueError(
                "Parsing error in reading the GTF attributes for interval\n\
                {}\nCheck that no semicolons are present in gene_id, gene_name, \
                or other fields in the the attributes column.\
                ".format(
                    interval
                )
            ) from err

        gene_id = interval.attrs["gene_id"].encode()
        id_name_map[gene_id] = (
            interval.attrs["gene_name"].encode() if "gene_name" in interval.attrs else gene_id
        )

    return id_name_map


def main(args, outs):
    if args.skip or args.chunk_start == args.chunk_end:
        outs.peak_annotation = None
        return

    # compute nearby genes
    peaks = BedTool(args.filtered_peaks)
    peak_chunk = BedTool(peaks[args.chunk_start : args.chunk_end]).saveas()
    annotations = annotate_peaks(peak_chunk, args.reference_path)
    if annotations == []:
        outs.peak_annotation = None
    else:
        ref_mgr = ReferenceManager(args.reference_path)
        # pylint: disable=unexpected-keyword-arg
        annotation_bed = BedTool(annotations).sort(g=ref_mgr.fasta_index).saveas()
        with open(outs.peak_annotation, "wb") as out:
            out.write(b"chrom\tstart\tend\tgene\tdistance\tpeak_type\n")
            for row in annotation_bed:
                for c in row:
                    out.write(ensure_binary(c))
                    out.write(b"\t")
                out.write(b"\n")


def join(args, outs, chunk_defs, chunk_outs):
    if not chunk_defs or chunk_defs[0].skip:
        martian.log_info("Skipping peak annotation")
        outs.peak_annotation = None
        return

    chunk_peak_annotations = [
        chunk.peak_annotation for chunk in chunk_outs if chunk.peak_annotation is not None
    ]
    if not chunk_peak_annotations:
        return

    ref_mgr = ReferenceManager(args.reference_path)
    gene_id_name_map = build_gene_id_name_map(ref_mgr)
    wrote_header = False
    with open(outs.peak_annotation, "wb") as outfile:
        for chunk_file in chunk_peak_annotations:
            with open(chunk_file, "rb") as infile:
                header = infile.readline()
                if not wrote_header:
                    outfile.write(header)
                    wrote_header = True

                for line in infile:
                    fields = line.rstrip().split(b"\t")
                    if fields[3] and fields[3] in gene_id_name_map:
                        # Replace gene IDs with gene names where available
                        gene_name = gene_id_name_map[fields[3]]
                        fields[3] = gene_name
                    outfile.write(b"\t".join(fields))
                    outfile.write(b"\n")
