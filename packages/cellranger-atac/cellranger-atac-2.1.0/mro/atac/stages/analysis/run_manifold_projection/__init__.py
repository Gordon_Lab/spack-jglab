# Copyright (c) 2019 10x Genomics, Inc. All rights reserved.
"""
Obtain a TSNE and/or UMAP projection of the reduced matrix
"""

from __future__ import absolute_import, division

import os
import copy
import shutil
import numpy as np

import cellranger.analysis.pca as cr_pca
import cellranger.analysis.plsa as cr_plsa
import cellranger.analysis.lsa as cr_lsa
import cellranger.analysis.lm_umap as cr_umap
import cellranger.analysis.bhtsne as cr_tsne
import cellranger.analysis.io as analysis_io
import cellranger.analysis.constants as analysis_constants
import cellranger.h5_constants as h5_constants
import cellranger.matrix as cr_matrix

from atac.constants import ALLOWED_FACTORIZATIONS, ALLOWED_PROJECTIONS

__MRO__ = """
struct TsneParams(
    int   perplexity,
    int   input_pcs,
    float theta,
    int   max_iter,
    int   stop_lying_iter,
    int   mom_switch_iter,
    int   max_dims,
)

struct UmapParams(
    int    n_neighbors,
    int    input_pcs,
    int    max_dims,
    float  min_dist,
    string metric,
)

struct ManifoldParams(
    TsneParams tsne,
    UmapParams umap,
)

stage RUN_MANIFOLD_PROJECTION(
    in  h5             filtered_matrix,
    in  path           reduced_data,
    in  ManifoldParams params,
    in  int            random_seed,
    in  string[]       factorization      "lsa|plsa|pca",
    in  string[]       projections        "tsne|umap",
    out path           projection_output,
    src py             "stages/analysis/run_manifold_projection",
) split (
    in  string         method,
    in  string         projection,
    in  int            projection_dims,
) using (
    volatile = strict,
)
"""


def split(args):
    if args.filtered_matrix is None:
        return {"chunks": [{"__mem_gb": h5_constants.MIN_MEM_GB}]}

    if not os.path.exists(args.reduced_data):
        raise IOError("reduced data not found at {}".format(args.reduced_data))

    if not set(args.factorization).issubset(ALLOWED_FACTORIZATIONS):
        raise ValueError("Invalid factorization provided")

    if not set(args.projections).issubset(ALLOWED_PROJECTIONS):
        raise ValueError("Invalid projection provided")
    matrix_mem_gb = cr_matrix.CountMatrix.get_mem_gb_from_matrix_h5(args.filtered_matrix)

    threads_per_projection = {"tsne": 1, "umap": 4}
    chunks = []
    for method in args.factorization:
        for projection in args.projections:
            max_dims = analysis_constants.TSNE_N_COMPONENTS
            if args.params and args.params.get(projection):
                max_dims = max_dims or args.params[projection].get("max_dims")
            for dims in range(2, max_dims + 1):
                if method not in ALLOWED_FACTORIZATIONS:
                    raise ValueError("invalid factorization method {}".format(method))
                mem_gb = max(matrix_mem_gb, h5_constants.MIN_MEM_GB)
                chunks.append(
                    {
                        "method": method,
                        "projection": projection,
                        "projection_dims": dims,
                        "__mem_gb": mem_gb,
                        "__vmem_gb": mem_gb + 8,
                        "__threads": threads_per_projection[projection],
                    }
                )
    return {"chunks": chunks}


def main(args, outs):
    if args.filtered_matrix is None:
        return

    if not os.path.exists(outs.projection_output):
        os.mkdir(outs.projection_output)

    transformed_matrix_h5 = os.path.join(args.reduced_data, args.method, args.method + ".h5")
    transformed_matrix = load_transformed_matrix(transformed_matrix_h5, args.method)

    num_dims = args.projection_dims
    projection_kwargs = {}
    if args.params and args.params[args.projection]:
        projection_kwargs = copy.deepcopy(args.params[args.projection])
        projection_kwargs.pop("max_dims")
    _h5 = os.path.join(outs.projection_output, args.method + "_{}.h5".format(args.projection))
    _csv = os.path.join(outs.projection_output, args.method + "_{}_csv".format(args.projection))
    matrix_bcs = cr_matrix.CountMatrix.load_bcs_from_h5_file(args.filtered_matrix)

    if args.projection == "tsne":
        tsne = cr_tsne.run_tsne(
            transformed_matrix, key=str(num_dims), tsne_dims=num_dims, **projection_kwargs
        )
        cr_tsne.save_tsne_h5(tsne, _h5)
        cr_tsne.save_tsne_csv(tsne, matrix_bcs, _csv)
    elif args.projection == "umap":
        umap = cr_umap.run_umap(
            transformed_matrix,
            name=str(num_dims),
            key=str(num_dims),
            umap_dims=num_dims,
            random_state=args.random_seed,
            **projection_kwargs,
        )
        cr_umap.save_umap_h5(umap, _h5)
        cr_umap.save_umap_csv(umap, matrix_bcs, _csv)
    else:
        raise KeyError("Unknown projection {}".format(args.projection))


def join(args, outs, chunk_defs, chunk_outs):
    if args.filtered_matrix is None:
        outs.projection_output = None
        return

    if not os.path.exists(outs.projection_output):
        os.mkdir(outs.projection_output)

    for method in args.factorization:
        for projection in args.projections:
            chunk_h5s = [
                os.path.join(chunk_out.projection_output, method + "_{}.h5".format(projection))
                for chunk_def, chunk_out in zip(chunk_defs, chunk_outs)
                if chunk_def.method == method and chunk_def.projection == projection
            ]

            chunk_csv_dirs = [
                os.path.join(chunk_out.projection_output, method + "_{}_csv".format(projection))
                for chunk_def, chunk_out in zip(chunk_defs, chunk_outs)
                if chunk_def.method == method and chunk_def.projection == projection
            ]

            analysis_io.combine_h5_files(
                chunk_h5s,
                os.path.join(outs.projection_output, method + "_{}.h5".format(projection)),
                [projection],
            )

            for csv_dir in chunk_csv_dirs:
                shutil.copytree(
                    csv_dir,
                    os.path.join(outs.projection_output, method + "_{}_csv".format(projection)),
                    dirs_exist_ok=True,
                )


def load_transformed_matrix(transformed_matrix_h5, method):
    """
    Load a dimension reduction based on the method = pca|lsa|plsa
    """
    if method in {"pca", "bclsa", "bcpca"}:
        transformed_matrix = cr_pca.load_pca_from_h5(transformed_matrix_h5).transformed_pca_matrix
    if method == "lsa":
        lsa = cr_lsa.load_lsa_from_h5(transformed_matrix_h5)
        lsa = lsa._replace(transformed_lsa_matrix=lsa.transformed_lsa_matrix + 1e-120)
        # transform matrix to unit normed so that euclidean distance in new space is cosine distance
        # in old space
        transformed_matrix = lsa.transformed_lsa_matrix / np.linalg.norm(
            lsa.transformed_lsa_matrix, axis=1, keepdims=True
        )
    if method == "plsa":
        plsa = cr_plsa.load_plsa_from_h5(transformed_matrix_h5)
        plsa = plsa._replace(transformed_plsa_matrix=plsa.transformed_plsa_matrix + 1e-120)
        # transform matrix to unit normed so that euclidean distance in new space is cosine distance
        # in old space
        transformed_matrix = plsa.transformed_plsa_matrix / np.linalg.norm(
            plsa.transformed_plsa_matrix, axis=1, keepdims=True
        )
    return transformed_matrix
