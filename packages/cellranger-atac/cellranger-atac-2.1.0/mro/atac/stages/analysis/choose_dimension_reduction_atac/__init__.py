#!/usr/bin/env python
#
# Copyright (c) 2021 10X Genomics, Inc. All rights reserved.
#
"""
Determine the correct source of dimension-reduced data
"""
from __future__ import absolute_import

import os
import shutil
import martian

__MRO__ = """
stage CHOOSE_DIMENSION_REDUCTION_ATAC(
    in  path     regular_reduced_data,
    in  string[] factorization,
    in  h5       aligned_pca_h5,
    in  path     aligned_pca_csv,
    out path     reduced_data,
    out string[] factorization,
    src py       "stages/analysis/choose_dimension_reduction_atac",
)
"""


def main(args, outs):
    martian.make_path(outs.reduced_data)
    if args.aligned_pca_h5 is not None and args.aligned_pca_csv is not None:
        out_method = "bcpca" if args.factorization[0] == "pca" else "bclsa"
        os.makedirs(outs.reduced_data, exist_ok=True)
        os.makedirs(os.path.join(outs.reduced_data, out_method), exist_ok=True)
        shutil.copytree(
            args.aligned_pca_csv,
            os.path.join(outs.reduced_data, out_method, out_method + "_csv"),
            dirs_exist_ok=True,
        )
        shutil.copy(
            args.aligned_pca_h5,
            os.path.join(outs.reduced_data, out_method, out_method + ".h5"),
        )
        outs.factorization = [out_method]
    else:
        shutil.copytree(args.regular_reduced_data, outs.reduced_data, dirs_exist_ok=True)
        outs.factorization = args.factorization
