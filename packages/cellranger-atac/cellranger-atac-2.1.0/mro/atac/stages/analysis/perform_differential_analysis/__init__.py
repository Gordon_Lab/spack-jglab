# Copyright (c) 2019 10x Genomics, Inc. All rights reserved.

from __future__ import absolute_import, division

import os

import numpy as np
from scipy.sparse import csc_matrix
import martian
import cellranger.analysis.constants as analysis_constants
import cellranger.analysis.diffexp as cr_diffexp
import cellranger.analysis.io as analysis_io
from cellranger.matrix import CountMatrix
from cellranger.analysis.singlegenome import SingleGenomeAnalysis
from atac.constants import ALLOWED_FACTORIZATIONS
import atac.analysis.diffexp_nb as nb2_diffexp
from atac.utils import normalize_matrix
from atac.gc_bias import get_barcode_gc
from atac.tools.ref_manager import ReferenceManager

__MRO__ = """
stage PERFORM_DIFFERENTIAL_ANALYSIS(
    in  bed      peaks,
    in  string   reference_path,
    in  h5       filtered_peak_bc_matrix,
    in  h5       filtered_tf_bc_matrix,
    in  string[] factorization,
    in  path     clustering,
    out path     enrichment_analysis,
    src py       "stages/analysis/perform_differential_analysis",
) split (
    in  string   method,
    in  string   clustering_key,
    in  int      cluster,
    out csv      tmp_diffexp,
)
"""

## Number of threads
THREADS = 4


def disable_stage(args):
    """When do we not run differential analysis"""
    ctg_mgr = ReferenceManager(args.reference_path)
    species = ctg_mgr.list_species()
    if args.filtered_peak_bc_matrix is None or len(species) > 1:
        return True
    cells = CountMatrix.count_cells_from_h5(args.filtered_peak_bc_matrix)
    # For TF differential expression we have 4 covariates in the GLM and you need at least 5 cells
    return cells <= 4


def est_mem_gb(matrix_h5):
    """Memory estimates based on an empirical fit"""
    peaks, _, nnz = CountMatrix.load_dims_from_h5(matrix_h5)
    chunk_mem_gb = 8.22e-7 * peaks + 5.17e-8 * nnz + 0.16
    join_mem_gb = 1.53e-6 * peaks + 6.92e-10 * nnz + 0.13
    # we want to overestimate
    a = 1.25
    b = 0.5
    return a * chunk_mem_gb + b, a * join_mem_gb + b


def split(args):
    if disable_stage(args):
        return {"chunks": []}

    chunks = []

    if not set(args.factorization).issubset(ALLOWED_FACTORIZATIONS):
        raise ValueError("Invalid factorization provided")

    chunk_mem_gb, join_mem_gb = est_mem_gb(args.filtered_peak_bc_matrix)
    # create a chunk for each method x clustering combo
    for method in args.factorization:
        clustering_h5 = os.path.join(args.clustering, method, "{}_clustering.h5".format(method))
        for key in SingleGenomeAnalysis.load_clustering_keys_from_h5(clustering_h5):
            clustering = SingleGenomeAnalysis.load_clustering_from_h5(clustering_h5, key)
            for cluster in set(clustering.clusters):
                chunks.append(
                    {
                        "method": method,
                        "clustering_key": key,
                        "cluster": int(cluster),
                        "__mem_gb": chunk_mem_gb,
                        "__threads": THREADS,
                    }
                )

    return {"chunks": chunks, "join": {"__mem_gb": join_mem_gb}}


def main(args, outs):
    """Run this for each method x clustering key combination from split"""

    # Load the peak-BC matrix and a clustering and perform DE
    peak_matrix = CountMatrix.load_h5_file(args.filtered_peak_bc_matrix)
    clustering_h5 = os.path.join(
        args.clustering, args.method, "{}_clustering.h5".format(args.method)
    )
    clustering = SingleGenomeAnalysis.load_clustering_from_h5(clustering_h5, args.clustering_key)
    mask = clustering.clusters == args.cluster
    clustering.clusters[mask] = 1
    clustering.clusters[np.logical_not(mask)] = 2

    # find depth using peak matrix and normalize
    scale = np.array(peak_matrix.m.sum(axis=0)).squeeze()
    depth = (scale + 1) / np.median(scale)

    cov_peak = [np.log(depth)]
    mmt = csc_matrix(peak_matrix.m.transpose().astype(np.float64))

    threads = martian.get_threads_allocation()
    diffexp_peak = nb2_diffexp.run_differential_expression(
        peak_matrix.m,
        mmt,
        clustering.clusters,
        model="poisson",
        impute_rest=True,
        cov=cov_peak,
        alpha=None,
        threads=threads,
        verbose=True,
    )

    # find empirical estimates of alpha
    tf_matrix = None
    diffexp_tf = None
    # do DE on tf-BC matrix
    if args.filtered_tf_bc_matrix is not None:
        tf_matrix = CountMatrix.load_h5_file(args.filtered_tf_bc_matrix)
        ntfmatrix = normalize_matrix(tf_matrix.m, scale)
        alpha_tf = nb2_diffexp.empirical_dispersion(ntfmatrix)
        barcode_GC = get_barcode_gc(args.reference_path, args.peaks, peak_matrix)
        cov_tf = [barcode_GC, np.log(depth)]
        mmt = csc_matrix(tf_matrix.m.transpose().astype(np.float64))
        diffexp_tf = nb2_diffexp.run_differential_expression(
            tf_matrix.m,
            mmt,
            clustering.clusters,
            model="nb",
            impute_rest=True,
            cov=cov_tf,
            alpha=alpha_tf,
            threads=threads,
            verbose=True,
        )

    # vstack
    diffexp = (
        diffexp_peak
        if tf_matrix is None
        else cr_diffexp.DifferentialExpression(np.vstack([diffexp_peak.data, diffexp_tf.data]))
    )

    # write out temp file
    np.savetxt(outs.tmp_diffexp, diffexp.data, delimiter=",")
    outs.enrichment_analysis = None


def join(args, outs, chunk_defs, chunk_outs):
    if disable_stage(args):
        outs.enrichment_analysis = None
        return

    peak_matrix_features = CountMatrix.load_feature_ref_from_h5_file(args.filtered_peak_bc_matrix)
    tf_matrix_features = (
        CountMatrix.load_feature_ref_from_h5_file(args.filtered_tf_bc_matrix)
        if args.filtered_tf_bc_matrix is not None
        else None
    )
    # for each method, we merge h5 files and copy csv directories to one place
    os.makedirs(outs.enrichment_analysis, exist_ok=True)
    for method in args.factorization:
        method_dir = os.path.join(outs.enrichment_analysis, method)
        os.makedirs(method_dir, exist_ok=True)

        _h5 = os.path.join(method_dir, "{}_enrichment_h5.h5".format(method))
        chunk_h5s = []

        _csv = os.path.join(method_dir, "{}_enrichment_csv".format(method))
        diffexp_prefixes = [(fr.id, fr.name) for fr in peak_matrix_features.feature_defs]
        if args.filtered_tf_bc_matrix is not None:
            diffexp_prefixes += [(fr.id, fr.name) for fr in tf_matrix_features.feature_defs]

        clustering_h5 = os.path.join(args.clustering, method, "{}_clustering.h5".format(method))
        for key in SingleGenomeAnalysis.load_clustering_keys_from_h5(clustering_h5):

            chunk_outs_def_method_clustering = sorted(
                [
                    [chunk_out, chunk_def]
                    for chunk_out, chunk_def in zip(chunk_outs, chunk_defs)
                    if chunk_def.clustering_key == key
                ],
                key=lambda x: x[1].cluster,
            )
            chunk_outs_method_clustering = [c[0] for c in chunk_outs_def_method_clustering]

            # load 1 vs rest tests in sorted order of chunks and combine into one output per clustering
            diffexp = cr_diffexp.DifferentialExpression(
                np.hstack(
                    [
                        np.loadtxt(com.tmp_diffexp, delimiter=",")[:, 0:3]
                        for com in chunk_outs_method_clustering
                    ]
                )
            )

            # write out h5
            chunk_h5 = martian.make_path("{}_enrichment_h5.h5".format(key))
            with analysis_io.open_h5_for_writing(chunk_h5) as f:
                cr_diffexp.save_differential_expression_h5(f, key, diffexp)
            chunk_h5s += [chunk_h5]

            # write out csv
            cr_diffexp.save_differential_expression_csv_from_features(
                key, diffexp, diffexp_prefixes, _csv
            )

        analysis_io.combine_h5_files(
            chunk_h5s,
            _h5,
            [
                analysis_constants.ANALYSIS_H5_DIFFERENTIAL_EXPRESSION_GROUP,
                analysis_constants.ANALYSIS_H5_KMEANS_DIFFERENTIAL_EXPRESSION_GROUP,
            ],
        )
