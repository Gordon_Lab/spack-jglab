# Copyright 2013-2021 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

# ----------------------------------------------------------------------------
# If you submit this package back to Spack as a pull request,
# please first remove this boilerplate and all FIXME comments.
#
# This is a template package file for Spack.  We've put "FIXME"
# next to all the things you'll want to change. Once you've handled
# them, you can save this file and test your package like this:
#
#     spack install py-liftoff
#
# You can edit this file again by typing:
#
#     spack edit py-liftoff
#
# See the Spack documentation for more information on packaging.
# ----------------------------------------------------------------------------

from spack import *


class PyLiftoff(PythonPackage):
    """FIXME: Put a proper description of your package here."""

    # FIXME: Add a proper url for your package's homepage here.
    homepage = "https://www.example.com"
    url      = "https://github.com/agshumate/Liftoff/archive/refs/tags/1.6.3.tar.gz"

    # FIXME: Add a list of GitHub accounts to
    # notify when the package is updated.
    # maintainers = ['github_user1', 'github_user2']

    version('1.6.3', sha256='6cee1ad0961ac38c79d7d3c389624321698506614cf572328c412e7cea37f4b5')

    depends_on('python@3.6:')
    depends_on('py-numpy@1.22.0:')
    depends_on('py-biopython@1.76:')
    depends_on('py-gffutils@0.10.1:')
    depends_on('py-networkx@2.4:')
    depends_on('py-pysam@0.19.1:')
    depends_on('py-pyfaidx@0.5.8:')
    depends_on('py-interlap@0.2.6:')
    depends_on('py-ujson@3.2.0:')
    depends_on('py-parasail-python@1.2.1:')

