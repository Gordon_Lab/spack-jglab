# Copyright 2013-2021 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

# ----------------------------------------------------------------------------
# If you submit this package back to Spack as a pull request,
# please first remove this boilerplate and all FIXME comments.
#
# This is a template package file for Spack.  We've put "FIXME"
# next to all the things you'll want to change. Once you've handled
# them, you can save this file and test your package like this:
#
#     spack install py-magpurify
#
# You can edit this file again by typing:
#
#     spack edit py-magpurify
#
# See the Spack documentation for more information on packaging.
# ----------------------------------------------------------------------------

from spack import *


class PyMagpurify(PythonPackage):
    """FIXME: Put a proper description of your package here."""

    # FIXME: Add a proper url for your package's homepage here.
    homepage = "https://www.example.com"
    url      = "https://github.com/snayfach/MAGpurify/archive/refs/tags/2.1.2.tar.gz"

    # FIXME: Add a list of GitHub accounts to
    # notify when the package is updated.
    # maintainers = ['github_user1', 'github_user2']

    version('2.1.2', sha256='609350ffe9fa7fcdf245e43787fd5acc63657f050c8bffb21d49bacdc852e386')

    # FIXME: Add dependencies if required. Only add the python dependency
    # if you need specific versions. A generic python dependency is
    # added implicity by the PythonPackage class.
    depends_on('python@3.6:', type=('build', 'run'))
    depends_on('py-biopython')
    depends_on('py-numpy')
    depends_on('py-pandas')
    depends_on('py-scikit-learn')
    depends_on('blast-plus@2.12.0')
    depends_on('prodigal@2.6.3')
    depends_on('hmmer@3.3.2')
    depends_on('last@869')
    depends_on('mash@2.3')
    depends_on('coverm@0.6.1')
    
    def build_args(self, spec, prefix):
        # FIXME: Add arguments other than --prefix
        # FIXME: If not needed delete this function
        args = []
        return args
