# Copyright 2013-2021 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

# ----------------------------------------------------------------------------
# If you submit this package back to Spack as a pull request,
# please first remove this boilerplate and all FIXME comments.
#
# This is a template package file for Spack.  We've put "FIXME"
# next to all the things you'll want to change. Once you've handled
# them, you can save this file and test your package like this:
#
#     spack install requests-cache
#
# You can edit this file again by typing:
#
#     spack edit requests-cache
#
# See the Spack documentation for more information on packaging.
# ----------------------------------------------------------------------------

from spack import *


class RequestsCache(AutotoolsPackage):
    """FIXME: Put a proper description of your package here."""

    # FIXME: Add a proper url for your package's homepage here.
    homepage = "https://www.example.com"
    url      = "https://github.com/requests-cache/requests-cache/archive/refs/tags/v1.0.0.tar.gz"

    # FIXME: Add a list of GitHub accounts to
    # notify when the package is updated.
    # maintainers = ['github_user1', 'github_user2']

    version('1.0.0', sha256='07c7939551302414436c943d0571cf04a87170144f3516b904a824e84c4c5b35')

    # FIXME: Add dependencies if required.
    # depends_on('foo')

 #   def configure_args(self):
        # FIXME: Add arguments other than --prefix
        # FIXME: If not needed delete this function
 #       args = []
 #       return args
