@include "_cr_lib_stages.mro"
@include "_cr_vdj_stages.mro"

pipeline SC_VDJ_CONTIG_ASSEMBLER(
    in  int                        gem_well,
    in  ChemistryDef               chemistry_def,
    in  map[]                      chunks,
    in  int                        r1_length,
    in  int                        r2_length,
    in  int                        initial_reads,
    in  float                      subsample_rate,
    in  path                       vdj_reference_folder,
    in  bool                       denovo,
    in  int                        force_cells,
    in  path                       inner_primers,
    in  string                     receptor,
    in  csv                        gex_filtered_barcodes,
    in  bool                       is_antibody_only,
    in  bool                       is_non_targeted_gex,
    in  AntigenSpecificityControls antigen_specificity_controls,
    in  FilterSwitch               filter_switch,
    out json                       summary,
    out ReadShards                 read_shards,
    out int[]                      gem_groups,
    out json                       raw_barcode_counts,
    out json                       corrected_barcode_counts,
    out int                        n50_n50_rpu,
    out int                        processed_read_pairs,
    out bam                        contig_bam,
    out bam.bai                    contig_bam_bai,
    out tsv                        summary_tsv,
    out tsv                        umi_summary_tsv,
    out json                       contig_annotations,
    out csv                        barcode_support,
    out json[]                     barcodes_in_chunks,
    out fastq                      unmapped_sample_fastq,
    out txt                        report,
    out int                        total_read_pairs,
    out arp.bincode                assemblable_reads_per_bc,
    out smf.json                   sequencing_metrics,
    out json.lz4                   asm_filter_diagnostics,
)
{
    call MAKE_SHARD(
        gem_well                     = self.gem_well,
        chemistry_def                = self.chemistry_def,
        read_chunks                  = self.chunks,
        r1_length                    = self.r1_length,
        r2_length                    = self.r2_length,
        subsample_rate               = self.subsample_rate,
        initial_read_pairs           = self.initial_reads,
        libraries_to_translate       = [],
        reference_path               = null,
        target_features              = null,
        target_set                   = null,
        target_set_name              = null,
        feature_reference_path       = null,
        antigen_specificity_controls = self.antigen_specificity_controls,
    )

    call BARCODE_CORRECTION(
        gem_well               = self.gem_well,
        barcode_counts         = MAKE_SHARD.barcode_counts,
        barcode_segment_counts = MAKE_SHARD.barcode_segment_counts,
        chemistry_def          = self.chemistry_def,
        invalid_uncorrected    = MAKE_SHARD.invalid,
        valid_read_metrics     = MAKE_SHARD.bc_correct_summary,
        libraries_to_translate = [],
        min_reads_to_report_bc = 1000,
    )

    call RUST_BRIDGE(
        gem_well                 = self.gem_well,
        valid_uncorrected        = MAKE_SHARD.valid,
        valid_corrected          = BARCODE_CORRECTION.valid_corrected,
        raw_barcode_counts       = MAKE_SHARD.barcode_counts,
        corrected_barcode_counts = BARCODE_CORRECTION.corrected_barcode_counts,
        paired_end               = MAKE_SHARD.paired_end,
    )

    call ASSEMBLE_VDJ(
        bc_sorted_rna_reads      = RUST_BRIDGE.bc_sorted_rna_reads,
        paired_end               = MAKE_SHARD.paired_end,
        vdj_reference_path       = self.vdj_reference_folder,
        n50_n50_rpu              = RUST_BRIDGE.n50_n50_rpu,
        npairs                   = RUST_BRIDGE.processed_read_pairs,
        receptor                 = self.receptor,
        denovo                   = self.denovo,
        force_cells              = self.force_cells,
        inner_enrichment_primers = self.inner_primers,
        total_read_pairs         = MAKE_SHARD.total_read_pairs,
        corrected_bc_counts      = RUST_BRIDGE.corrected_barcode_counts_json,
        filter_switch            = self.filter_switch,
    )

    call HANDLE_GEX_CELLS(
        asm_contig_annotations = ASSEMBLE_VDJ.contig_annotations,
        filtered_barcodes      = self.gex_filtered_barcodes,
        is_antibody_only       = self.is_antibody_only,
        is_non_targeted_gex    = self.is_non_targeted_gex,
    )

    call MERGE_METRICS(
        summaries = [
            MAKE_SHARD.summary,
            BARCODE_CORRECTION.summary,
            ASSEMBLE_VDJ.metrics_summary_json,
        ],
    )

    return (
        summary                  = MERGE_METRICS.summary,
        read_shards              = {
            corrected_reads: BARCODE_CORRECTION.valid_corrected,
            invalid_reads:   BARCODE_CORRECTION.invalid,
            valid_reads:     MAKE_SHARD.valid,
        },
        gem_groups               = RUST_BRIDGE.gem_groups,
        raw_barcode_counts       = RUST_BRIDGE.raw_barcode_counts_json,
        corrected_barcode_counts = RUST_BRIDGE.corrected_barcode_counts_json,
        n50_n50_rpu              = RUST_BRIDGE.n50_n50_rpu,
        processed_read_pairs     = RUST_BRIDGE.processed_read_pairs,
        contig_bam               = ASSEMBLE_VDJ.contig_bam,
        contig_bam_bai           = ASSEMBLE_VDJ.contig_bam_bai,
        summary_tsv              = ASSEMBLE_VDJ.summary_tsv,
        umi_summary_tsv          = ASSEMBLE_VDJ.umi_summary_tsv,
        contig_annotations       = HANDLE_GEX_CELLS.contig_annotations,
        barcode_support          = ASSEMBLE_VDJ.barcode_support,
        barcodes_in_chunks       = ASSEMBLE_VDJ.barcodes_in_chunks,
        unmapped_sample_fastq    = ASSEMBLE_VDJ.unmapped_sample_fastq,
        report                   = ASSEMBLE_VDJ.report,
        total_read_pairs         = MAKE_SHARD.total_read_pairs,
        assemblable_reads_per_bc = ASSEMBLE_VDJ.assemblable_reads_per_bc,
        sequencing_metrics       = MAKE_SHARD.sequencing_metrics,
        asm_filter_diagnostics   = ASSEMBLE_VDJ.filter_diagnostics,
    )
}
